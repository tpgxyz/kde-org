---
aliases:
- ../../fulllog_releases-22.08.3
title: KDE Gear 22.08.3 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Fix opening new windows unnecessary. [Commit.](http://commits.kde.org/dolphin/348794eff8235f46b1f21d5e4b6fe0fe2b06ca57)Fixes bug [#440663](https://bugs.kde.org/440663)
+ Fix Wayland window activation when attaching to an existing instance. [Commit.](http://commits.kde.org/dolphin/ee0ef58fc0cd1d27359cb4f1f1fa559f104acfa6)
{{< /details >}}
{{< details id="ffmpegthumbs" title="ffmpegthumbs" link="https://commits.kde.org/ffmpegthumbs" >}}
+ Don't trigger UB on vector::front. [Commit.](http://commits.kde.org/ffmpegthumbs/1f9b8190efcec842302e77a9ac9727faf7c8c14d)
{{< /details >}}
{{< details id="kalendar" title="kalendar" link="https://commits.kde.org/kalendar" >}}
+ Fix final month dates in schedule view BUG:461200. [Commit.](http://commits.kde.org/kalendar/c4c2db9480bd7eda6ace554846ee3860ccc32a5c)
+ Fix crash when history is still null. [Commit.](http://commits.kde.org/kalendar/69f15b7abf327b9daf97587784b845f4c1811e35)
+ Fix adding an event or task to a default collection when there is no default or previously used collection in Config. [Commit.](http://commits.kde.org/kalendar/078ac836f9d5eee9d5215b590ff057d0ab0815b1)
+ Require the latest stable version of the Akonadi and PIM APIs. [Commit.](http://commits.kde.org/kalendar/c46b6c987a4777fa98b4d29893dd1386715ecb4e)
{{< /details >}}
{{< details id="kalzium" title="kalzium" link="https://commits.kde.org/kalzium" >}}
+ Fix ignoring Overwrite cancelling. [Commit.](http://commits.kde.org/kalzium/60ca58a3c17e5d3cf5c12e6c7e31e29f8dfad791)
{{< /details >}}
{{< details id="kanagram" title="kanagram" link="https://commits.kde.org/kanagram" >}}
+ Fix UI not loading, due to accidentally translated app name. [Commit.](http://commits.kde.org/kanagram/6c512725b189a5327e3b506738240a97df4aab51)
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Fix crash in GitWidget. [Commit.](http://commits.kde.org/kate/321f82922153fdbf07e5e6cf99f2fe45082288cc)
+ Be more defensive when tearing down. [Commit.](http://commits.kde.org/kate/23a282ba958602c65baf293f4f48c499b34c84fe)
+ Avoid some usages of QCursor::pos. [Commit.](http://commits.kde.org/kate/9ec3177d687b8b8381a3cffbaaa585300cde0f13)
+ Set parent for QMenu. [Commit.](http://commits.kde.org/kate/51244c75fc66ae4d014fd3629a357f7818063f55)Fixes bug [#460983](https://bugs.kde.org/460983)
{{< /details >}}
{{< details id="kbruch" title="kbruch" link="https://commits.kde.org/kbruch" >}}
+ Install the 128px version of the icon. [Commit.](http://commits.kde.org/kbruch/cb6214a9fa8478ef6ecba0e43145d525c86b23f2)
{{< /details >}}
{{< details id="kdebugsettings" title="kdebugsettings" link="https://commits.kde.org/kdebugsettings" >}}
+ Use sensible values with e_s_disabled_deprecation_versions. [Commit.](http://commits.kde.org/kdebugsettings/6ae72ebc551f6c2864fa5e5af00637d8fb197183)
{{< /details >}}
{{< details id="kdeconnect-kde" title="kdeconnect-kde" link="https://commits.kde.org/kdeconnect-kde" >}}
+ Virtualmonitor: Make sure we clean up m_process after deleting. [Commit.](http://commits.kde.org/kdeconnect-kde/67e85c14b6ed8feda77cf0764f2f763d80e5d073)
{{< /details >}}
{{< details id="kgeography" title="kgeography" link="https://commits.kde.org/kgeography" >}}
+ Fix Bahrain not showing in Asia: Place Countries in the Map. [Commit.](http://commits.kde.org/kgeography/767cd1e8e4330ce7247017c9103b8ffbe33175f9)Fixes bug [#460742](https://bugs.kde.org/460742)
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Add Italo train ticket extractor script. [Commit.](http://commits.kde.org/kitinerary/58e084a6ad1ac65bf9335650026b284a4ffeccac)
{{< /details >}}
{{< details id="kjumpingcube" title="kjumpingcube" link="https://commits.kde.org/kjumpingcube" >}}
+ Add content rating. [Commit.](http://commits.kde.org/kjumpingcube/6724d4353e230ea4dbe8b0220791f292b07d0119)
{{< /details >}}
{{< details id="kmail" title="kmail" link="https://commits.kde.org/kmail" >}}
+ Avoid reading .mbox files as Latin-1. [Commit.](http://commits.kde.org/kmail/eac31c72c56999da24847241e0d57c313cf42e38)
{{< /details >}}
{{< details id="kmime" title="kmime" link="https://commits.kde.org/kmime" >}}
+ Fix bug 378985: KMail 5.4.3: Save all attachements -> no attachments found. [Commit.](http://commits.kde.org/kmime/4b045ec19f62dcd270d0af4e11af483eb160e414)Fixes bug [#378985](https://bugs.kde.org/378985)
{{< /details >}}
{{< details id="kolf" title="kolf" link="https://commits.kde.org/kolf" >}}
+ Fix game getting stuck sometimes when balls collide. [Commit.](http://commits.kde.org/kolf/17dbb6360529e89d6498e0a6085ac659fed59f30)Fixes bug [#342532](https://bugs.kde.org/342532)
{{< /details >}}
{{< details id="konquest" title="konquest" link="https://commits.kde.org/konquest" >}}
+ Fix generating planet names when there's lots of them. [Commit.](http://commits.kde.org/konquest/8f75bbfb5f09e9db00df7ca24732b11002087d25)Fixes bug [#352926](https://bugs.kde.org/352926)
{{< /details >}}
{{< details id="konsole" title="konsole" link="https://commits.kde.org/konsole" >}}
+ Don't lose environment variables. [Commit.](http://commits.kde.org/konsole/252eab8537d8274ed32cb0a008f5d6ce9109908f)Fixes bug [#458591](https://bugs.kde.org/458591)
+ Avoid using wrong coordinates on extendSelection. [Commit.](http://commits.kde.org/konsole/1d30c7d75c1ee95a73f8240d1747c77f2683f60d)Fixes bug [#458822](https://bugs.kde.org/458822). Fixes bug [#398320](https://bugs.kde.org/398320)
{{< /details >}}
{{< details id="korganizer" title="korganizer" link="https://commits.kde.org/korganizer" >}}
+ Toggle completion of occurrences of recurring todos. [Commit.](http://commits.kde.org/korganizer/06accd7972bb3ba44bbdda2cc57cda5e123a92e8)
+ Remove an if that is always true. [Commit.](http://commits.kde.org/korganizer/c8eccb4c5b727dc04ef1ec6d9b2ac36474584d05)
+ Duplicate class name annoys code quality tool. [Commit.](http://commits.kde.org/korganizer/db09d9eb02f760eb39b0bddecf513b2bf6b49dd5)
+ Handle dissociation in distant time zones. [Commit.](http://commits.kde.org/korganizer/9580426c16f11f4cf0f06a8d2f5eebad7914f859)
{{< /details >}}
{{< details id="ksquares" title="ksquares" link="https://commits.kde.org/ksquares" >}}
+ Fix the yellow hint not being visible. [Commit.](http://commits.kde.org/ksquares/9d727a28f26bc4596e0ea4000d168899660eb44d)Fixes bug [#351809](https://bugs.kde.org/351809)
{{< /details >}}
{{< details id="mailcommon" title="mailcommon" link="https://commits.kde.org/mailcommon" >}}
+ Fix bug 460756:  Converting to sieve filters should use actual folder names instead of translated ones. [Commit.](http://commits.kde.org/mailcommon/bc0ca61528eecaa85e3ee8afe7be62e74eec0801)Fixes bug [#460756](https://bugs.kde.org/460756)
{{< /details >}}
