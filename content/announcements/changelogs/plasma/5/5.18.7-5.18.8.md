---
title: Plasma 5.18.8 complete changelog
version: 5.18.8
hidden: true
plasma: true
type: fulllog
---
{{< details title="Breeze" href="https://commits.kde.org/breeze" >}}
+ Fix informing the underlying widget when leaving SplitterProxy. [Commit.](http://commits.kde.org/breeze/ae9639f71087ea1e5da1f6ffe29816f0ade84db3) Fixes bug [#436473](https://bugs.kde.org/436473)
+ Fix SplitterProxy not clearing when above another QSplitterHandle. [Commit.](http://commits.kde.org/breeze/cc613558bae7c809e36951da89db3c7cc1a5a58e) Fixes bug [#431921](https://bugs.kde.org/431921)
+ Fix logic error in SplitterProxy::setEnabled. [Commit.](http://commits.kde.org/breeze/10fbc0eae42b21f1c8019bebf4de4681e69c066d) 
{{< /details >}}

{{< details title="kactivitymanagerd" href="https://commits.kde.org/kactivitymanagerd" >}}
+ Use correct way to register for a shortcut. [Commit.](http://commits.kde.org/kactivitymanagerd/0429ab68833c46bb513d440d998326698ce41144) Fixes bug [#374575](https://bugs.kde.org/374575)
{{< /details >}}

{{< details title="KDE GTK Config" href="https://commits.kde.org/kde-gtk-config" >}}
+ Make sure to actually commit GSettings changes. [Commit.](http://commits.kde.org/kde-gtk-config/57cc9bd8d92bc48b6a27af63da39fa33a0b13bf5) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Fix renaming shortcut for files selected via selection button and popup. [Commit.](http://commits.kde.org/plasma-desktop/f36b79311f097f08508f78e626fdadf637da4197) Fixes bug [#425436](https://bugs.kde.org/425436)
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ [libcolorcorrect] Sync night colour default values. [Commit.](http://commits.kde.org/plasma-workspace/cfd55830571738958a2f98999ed0f5a692b153c3) Fixes bug [#442253](https://bugs.kde.org/442253)
+ [Notifications] Don't generate previews of zero size. [Commit.](http://commits.kde.org/plasma-workspace/57a4845c4d761a1372f3b2ee48e137e8ca2e9c50) See bug [#430862](https://bugs.kde.org/430862)
+ Revert "[lookandfeel] Fix wake existing screensaver mode with key presses". [Commit.](http://commits.kde.org/plasma-workspace/c4a3a4e5b974fb309a6da61252b2b830d2d90683) Fixes bug [#435233](https://bugs.kde.org/435233)
{{< /details >}}

