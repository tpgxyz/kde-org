------------------------------------------------------------------------
r1016885 | mzanetti | 2009-08-29 08:50:32 +0000 (Sat, 29 Aug 2009) | 2 lines

backport of bugfix commit 1016884

------------------------------------------------------------------------
r1017644 | scripty | 2009-08-31 03:01:17 +0000 (Mon, 31 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1018803 | mzanetti | 2009-09-02 08:50:55 +0000 (Wed, 02 Sep 2009) | 2 lines

removing kscd profile as it is delivered by kscd.

------------------------------------------------------------------------
r1019174 | scripty | 2009-09-03 04:10:28 +0000 (Thu, 03 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1019203 | mlaurent | 2009-09-03 07:20:21 +0000 (Thu, 03 Sep 2009) | 2 lines

Remove from here too

------------------------------------------------------------------------
r1019523 | lueck | 2009-09-03 19:44:47 +0000 (Thu, 03 Sep 2009) | 1 line

utils doc backport for 4.3
------------------------------------------------------------------------
r1020575 | dakon | 2009-09-06 16:35:24 +0000 (Sun, 06 Sep 2009) | 1 line

if systray is deactivated show keysmanager if no command line arguments are given
------------------------------------------------------------------------
r1020785 | mzanetti | 2009-09-07 09:46:56 +0000 (Mon, 07 Sep 2009) | 3 lines

backport of bugfix commit 1020773


------------------------------------------------------------------------
r1020953 | dakon | 2009-09-07 17:31:48 +0000 (Mon, 07 Sep 2009) | 1 line

make "kgpg -d" work again if systray is disabled
------------------------------------------------------------------------
r1021395 | rkcosta | 2009-09-09 03:59:31 +0000 (Wed, 09 Sep 2009) | 10 lines

Backport r1021393.

Remove unnecessary slot.

m_part is always deleted correctly when the dialog is closed, and by
removing this slot we prevent a double delete when the part has already
been deleted (think a KHTMLPart that has executed window.close() here).

CCBUG: 206814

------------------------------------------------------------------------
r1021401 | rkcosta | 2009-09-09 04:30:35 +0000 (Wed, 09 Sep 2009) | 6 lines

Backport 1021399.

Keep broken extracted files after unrar fails with error.

CCBUG: 205131

------------------------------------------------------------------------
r1021666 | dakon | 2009-09-09 19:25:54 +0000 (Wed, 09 Sep 2009) | 1 line

clean up messing around with WA_DeleteOnClose
------------------------------------------------------------------------
r1023559 | kossebau | 2009-09-14 22:34:22 +0000 (Mon, 14 Sep 2009) | 1 line

backport of 1023542: fixed: tooltip coordinates need to have the viewport offset added to test for a bookmark location, also catch tooltip event only in "viewportEvent(QEvent*)"
------------------------------------------------------------------------
r1023560 | kossebau | 2009-09-14 22:36:23 +0000 (Mon, 14 Sep 2009) | 1 line

changed: ++patchlevel, so it's now 0.3.2
------------------------------------------------------------------------
r1023861 | kossebau | 2009-09-15 12:25:02 +0000 (Tue, 15 Sep 2009) | 1 line

backport of 
------------------------------------------------------------------------
r1023875 | kossebau | 2009-09-15 12:51:14 +0000 (Tue, 15 Sep 2009) | 1 line

backport of 1018619: fixed: show more digits for float and double type, as much as decimal precision is given
------------------------------------------------------------------------
r1024331 | mlaurent | 2009-09-16 11:42:58 +0000 (Wed, 16 Sep 2009) | 4 lines

Backport:
Fix trayicon status when wallet is open or not


------------------------------------------------------------------------
r1024442 | scripty | 2009-09-16 16:12:59 +0000 (Wed, 16 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1025887 | rkcosta | 2009-09-19 23:50:36 +0000 (Sat, 19 Sep 2009) | 6 lines

Backport r1025886.

Pass "-v" to unrar in order to correctly list multi-volume archives.

CCBUG: 202787

------------------------------------------------------------------------
r1025891 | rkcosta | 2009-09-20 00:05:41 +0000 (Sun, 20 Sep 2009) | 7 lines

Backport r1025890.

Correct the order the cliplugin choices for handling name clashes are
read.

CCBUG: 206485

------------------------------------------------------------------------
r1026382 | woebbe | 2009-09-21 16:40:30 +0000 (Mon, 21 Sep 2009) | 1 line

compile
------------------------------------------------------------------------
r1027307 | dakon | 2009-09-23 20:02:44 +0000 (Wed, 23 Sep 2009) | 4 lines

make sure that pointer is never stale

CCBUG:208162

------------------------------------------------------------------------
r1029087 | bettio | 2009-09-28 22:18:05 +0000 (Mon, 28 Sep 2009) | 2 lines

Backported "BUGFIX: KDF view musn't be editable." 

------------------------------------------------------------------------
r1029534 | scripty | 2009-09-30 03:07:45 +0000 (Wed, 30 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1029921 | scripty | 2009-10-01 02:46:53 +0000 (Thu, 01 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1030138 | mzanetti | 2009-10-01 16:57:58 +0000 (Thu, 01 Oct 2009) | 2 lines

backport of bugfix commit 1030137

------------------------------------------------------------------------
