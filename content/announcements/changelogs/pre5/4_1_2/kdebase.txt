------------------------------------------------------------------------
r853822 | alexmerry | 2008-08-28 12:37:30 +0200 (Thu, 28 Aug 2008) | 3 lines

This is slightly better.


------------------------------------------------------------------------
r853950 | menard | 2008-08-28 16:05:14 +0200 (Thu, 28 Aug 2008) | 2 lines

backport of commit 853944 from trunk : add an overlay on systray icon

------------------------------------------------------------------------
r853988 | dfaure | 2008-08-28 17:54:14 +0200 (Thu, 28 Aug 2008) | 2 lines

Sync default value with the one in konqueror.kcfg. Ouch. This code should use kconfigxt.

------------------------------------------------------------------------
r854128 | aseigo | 2008-08-28 23:21:43 +0200 (Thu, 28 Aug 2008) | 3 lines

don't trust the windowing system to be sane
CCBUG:170003

------------------------------------------------------------------------
r854207 | aucahuasi | 2008-08-29 08:01:13 +0200 (Fri, 29 Aug 2008) | 4 lines

CCBUG: 167608

Backport of bug fix: 167608

------------------------------------------------------------------------
r854571 | mpyne | 2008-08-29 22:08:50 +0200 (Fri, 29 Aug 2008) | 3 lines

Backport setting a useful default window icon for screensavers to 4.1.  Will
be available with 4.1.2.

------------------------------------------------------------------------
r854812 | edulix | 2008-08-30 12:59:36 +0200 (Sat, 30 Aug 2008) | 4 lines

Backport bugfix: Now the form data and everything possible is restored when detaching a tab or duplicating a tab/window
BUG:114138


------------------------------------------------------------------------
r854912 | lueck | 2008-08-30 17:27:12 +0200 (Sat, 30 Aug 2008) | 12 lines

documentation backport from trunk
CCMAIL:kde-i18n-doc@kde.org
see also doc backports:
kdeutils Revision 854896
kdesdk  Revision 854898
kdepim Revision 854899 
kdenetwork Revision 854901
kdemultimedia Revision 854902
kdegraphics Revision 854903
kdegames Revision 854904
kdeedu Revision 854907
kdeaccessibility Revision 854908
------------------------------------------------------------------------
r855091 | scripty | 2008-08-31 08:33:52 +0200 (Sun, 31 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r855092 | bennett | 2008-08-31 08:41:35 +0200 (Sun, 31 Aug 2008) | 2 lines

don't enable folderview on os x

------------------------------------------------------------------------
r855112 | lunakl | 2008-08-31 10:05:44 +0200 (Sun, 31 Aug 2008) | 5 lines

backport r835374
3d look for default alt-tab window selector/tabbox to make it easily distinguishable
from windows with white backgrounds.


------------------------------------------------------------------------
r855116 | huynhhuu | 2008-08-31 10:14:03 +0200 (Sun, 31 Aug 2008) | 2 lines

Remove something i didn't want to commit

------------------------------------------------------------------------
r855124 | lunakl | 2008-08-31 10:33:35 +0200 (Sun, 31 Aug 2008) | 4 lines

Backport r837761
Fix uninitialized variables.


------------------------------------------------------------------------
r855126 | lunakl | 2008-08-31 10:35:20 +0200 (Sun, 31 Aug 2008) | 3 lines

backport r837891


------------------------------------------------------------------------
r855206 | lunakl | 2008-08-31 12:05:52 +0200 (Sun, 31 Aug 2008) | 4 lines

r853290
Virtuals don't work normally in dtors, call in overriden dtor too.


------------------------------------------------------------------------
r855827 | menard | 2008-09-01 16:53:17 +0200 (Mon, 01 Sep 2008) | 4 lines

Don't disapear when you are on desktop

CCBUG:170137

------------------------------------------------------------------------
r856059 | scripty | 2008-09-02 07:55:38 +0200 (Tue, 02 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r856414 | ppenz | 2008-09-02 22:03:28 +0200 (Tue, 02 Sep 2008) | 3 lines

Backport of 856411: don't clear the filterbar when switching between tabs

CCBUG: 169878
------------------------------------------------------------------------
r856508 | scripty | 2008-09-03 08:34:23 +0200 (Wed, 03 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r856687 | aseigo | 2008-09-03 17:09:59 +0200 (Wed, 03 Sep 2008) | 2 lines

read the entry untranslated; otherwise we lose the setting when switching locales and this is not local dependant

------------------------------------------------------------------------
r856707 | aseigo | 2008-09-03 18:03:18 +0200 (Wed, 03 Sep 2008) | 2 lines

backport; create the menu earlier

------------------------------------------------------------------------
r856901 | scripty | 2008-09-04 08:08:10 +0200 (Thu, 04 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r856991 | mlaurent | 2008-09-04 13:27:19 +0200 (Thu, 04 Sep 2008) | 3 lines

Backport:
fix extract messages

------------------------------------------------------------------------
r857260 | scripty | 2008-09-05 08:08:13 +0200 (Fri, 05 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r857427 | dfaure | 2008-09-05 13:35:05 +0200 (Fri, 05 Sep 2008) | 3 lines

Fix optimization for "don't load the aboutpage if not necessary" so that we don't have the opposite
problem of loading+deleting khtml before showing about:, oops.

------------------------------------------------------------------------
r857430 | dfaure | 2008-09-05 13:41:39 +0200 (Fri, 05 Sep 2008) | 2 lines

SVN_SILENT less debug output; coding style fixes; unit test crash fix

------------------------------------------------------------------------
r857610 | dfaure | 2008-09-05 22:18:41 +0200 (Fri, 05 Sep 2008) | 3 lines

Backports from trunk: r854525:854653
Includes profile loading fixes and xmlgui unification.

------------------------------------------------------------------------
r858387 | menard | 2008-09-08 01:15:54 +0200 (Mon, 08 Sep 2008) | 2 lines

backport of 858381 : Fix crash when a battery is removed

------------------------------------------------------------------------
r859351 | scripty | 2008-09-10 08:46:55 +0200 (Wed, 10 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r859625 | craig | 2008-09-10 22:26:21 +0200 (Wed, 10 Sep 2008) | 2 lines

Fix crash due to QTreeWidgetItem::setExpanded.

------------------------------------------------------------------------
r859743 | osterfeld | 2008-09-11 10:58:55 +0200 (Thu, 11 Sep 2008) | 1 line

use _libs
------------------------------------------------------------------------
r860412 | dfaure | 2008-09-13 01:24:08 +0200 (Sat, 13 Sep 2008) | 2 lines

backport r860411, obvious fix

------------------------------------------------------------------------
r860601 | lueck | 2008-09-13 15:38:38 +0200 (Sat, 13 Sep 2008) | 1 line

remove duplicated para, this does not break doc freeze
------------------------------------------------------------------------
r861040 | tyrerj | 2008-09-15 01:58:30 +0200 (Mon, 15 Sep 2008) | 1 line

Backporting 861039
------------------------------------------------------------------------
r861047 | tyrerj | 2008-09-15 03:18:05 +0200 (Mon, 15 Sep 2008) | 1 line

correcting error
------------------------------------------------------------------------
r861092 | scripty | 2008-09-15 08:08:05 +0200 (Mon, 15 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r861426 | scripty | 2008-09-16 08:09:44 +0200 (Tue, 16 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r861486 | lueck | 2008-09-16 11:43:51 +0200 (Tue, 16 Sep 2008) | 1 line

adjust X-DocPath to documentation backport Revision 854912 
------------------------------------------------------------------------
r861676 | sebas | 2008-09-16 22:02:34 +0200 (Tue, 16 Sep 2008) | 1 line

An applet this big doesn't fit in most panels
------------------------------------------------------------------------
r861764 | scripty | 2008-09-17 08:52:36 +0200 (Wed, 17 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r862119 | scripty | 2008-09-18 08:21:13 +0200 (Thu, 18 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r862160 | ereslibre | 2008-09-18 11:24:26 +0200 (Thu, 18 Sep 2008) | 2 lines

Backport(If you are running in a SSL secured connection, and click on the dropdown arrow of the combo, do not show the SSL information.)

------------------------------------------------------------------------
r862313 | lunakl | 2008-09-18 17:28:13 +0200 (Thu, 18 Sep 2008) | 5 lines

Disable shadow with effects that transform windows, such as wobbly,
since shadow currently can't handle that.
(bug 161330)


------------------------------------------------------------------------
r862455 | lueck | 2008-09-18 23:27:56 +0200 (Thu, 18 Sep 2008) | 3 lines

mouse kcm code is in workspace, moving doc to be coherent said annma for trunk
do it for stable in coordination with aacid too
CCMAIL:annma@kde.org
------------------------------------------------------------------------
r862859 | scripty | 2008-09-20 08:50:17 +0200 (Sat, 20 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r863180 | uwolfer | 2008-09-21 13:25:05 +0200 (Sun, 21 Sep 2008) | 5 lines

Backport:
SVN commit 863178 by uwolfer:

Fix crash if you delete all associations and then add a new one.
#BUG:171395
------------------------------------------------------------------------
r863218 | gberg | 2008-09-21 15:17:49 +0200 (Sun, 21 Sep 2008) | 1 line

Backport of commit 863215
------------------------------------------------------------------------
r863580 | thanngo | 2008-09-22 16:42:46 +0200 (Mon, 22 Sep 2008) | 2 lines

silent

------------------------------------------------------------------------
r863974 | dfaure | 2008-09-23 18:50:17 +0200 (Tue, 23 Sep 2008) | 4 lines

Backport SVN commit 863973:
Fix "invalid url" error message when splitting a view [another regression due to part activation being immediate now]
Cleaned up "immediate" bool, and setActivePart overloading in KonqViewManager.

------------------------------------------------------------------------
r864023 | dfaure | 2008-09-23 21:02:49 +0200 (Tue, 23 Sep 2008) | 5 lines

Backport SVN commit 863998:
Fix split+link+lock for html pages, as reported by wstephenson. With unit test.

Couldn't find a bug report for it on bugs.kde.org, strange.

------------------------------------------------------------------------
r864183 | scripty | 2008-09-24 09:02:38 +0200 (Wed, 24 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r864228 | graesslin | 2008-09-24 10:49:18 +0200 (Wed, 24 Sep 2008) | 1 line

Fix possible crash when using alt+tab again while coverswitch is closing. Backporting patch from rev. 864225. Fixes bug 170860.
------------------------------------------------------------------------
r864234 | graesslin | 2008-09-24 11:06:18 +0200 (Wed, 24 Sep 2008) | 1 line

Ensure that full screen input window is always destroyed when using alt+tab again while flip switch closes. So mouse cursor is shown again. Backport rev 864231.
------------------------------------------------------------------------
r864460 | lueck | 2008-09-24 22:17:36 +0200 (Wed, 24 Sep 2008) | 1 line

fix wrong docpath and CMakeList.txt
------------------------------------------------------------------------
r864588 | scripty | 2008-09-25 08:51:40 +0200 (Thu, 25 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
