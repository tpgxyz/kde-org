------------------------------------------------------------------------
r837607 | mlaurent | 2008-07-25 09:46:27 +0200 (Fri, 25 Jul 2008) | 3 lines

Backport:
Don'y assert here when there is not history

------------------------------------------------------------------------
r837656 | mlaurent | 2008-07-25 13:26:45 +0200 (Fri, 25 Jul 2008) | 3 lines

Backport:
Don't assert

------------------------------------------------------------------------
r837663 | mlaurent | 2008-07-25 13:42:59 +0200 (Fri, 25 Jul 2008) | 3 lines

Backport:
Fix mem leak

------------------------------------------------------------------------
r837668 | mlaurent | 2008-07-25 13:57:38 +0200 (Fri, 25 Jul 2008) | 3 lines

Backport:
Fix potential mem leak

------------------------------------------------------------------------
r837804 | shaforo | 2008-07-25 20:58:31 +0200 (Fri, 25 Jul 2008) | 5 lines

take into account the case when user clicks cancel ;)
thanks for reporting!

BUG: 167401

------------------------------------------------------------------------
r838410 | shaforo | 2008-07-27 21:07:12 +0200 (Sun, 27 Jul 2008) | 5 lines

add a list of recently used projects under the projects menu
also, the last used project is loaded on startup if no --project cmd option was specified

BUG: 167426

------------------------------------------------------------------------
r838455 | woebbe | 2008-07-27 23:39:52 +0200 (Sun, 27 Jul 2008) | 3 lines

GCC 4.3: warning: suggest parentheses around comparison in operand of &

wow, two errors in one line :-(
------------------------------------------------------------------------
r838804 | shaforo | 2008-07-28 21:16:13 +0200 (Mon, 28 Jul 2008) | 3 lines

fix crash when TBX glossary contained several termEntires with the same subjectField


------------------------------------------------------------------------
r838984 | mlaurent | 2008-07-29 09:21:50 +0200 (Tue, 29 Jul 2008) | 3 lines

Backport:
Fix mem leak

------------------------------------------------------------------------
r839902 | scripty | 2008-07-31 06:50:48 +0200 (Thu, 31 Jul 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r840292 | shaforo | 2008-07-31 22:24:10 +0200 (Thu, 31 Jul 2008) | 4 lines

Project view and status leds now use KDE colors from theme for translation status

BUG: 167733

------------------------------------------------------------------------
r840357 | mueller | 2008-08-01 00:31:50 +0200 (Fri, 01 Aug 2008) | 1 line

fix link reduction fallout
------------------------------------------------------------------------
r840358 | mueller | 2008-08-01 00:33:35 +0200 (Fri, 01 Aug 2008) | 1 line

whoops
------------------------------------------------------------------------
r840972 | shaforo | 2008-08-02 13:32:03 +0200 (Sat, 02 Aug 2008) | 4 lines

show second plural form in source view (aka msgid) for single plural form language.

BUG: 167817

------------------------------------------------------------------------
r840981 | shaforo | 2008-08-02 13:53:07 +0200 (Sat, 02 Aug 2008) | 6 lines

use alt+space as default shortcut for msgid2msgstr in CJK locales (as ctrl-space is already taken)

Frank: please use 1 b.k.o. entry for 1 wish next time. Your second request is due to be fulfilled soon though.

BUG: 167807

------------------------------------------------------------------------
r840985 | shaforo | 2008-08-02 13:57:23 +0200 (Sat, 02 Aug 2008) | 2 lines

oops )

------------------------------------------------------------------------
r841002 | shaforo | 2008-08-02 14:33:43 +0200 (Sat, 02 Aug 2008) | 10 lines

don't fail when header has no comments at all.

this leaves few files failing, but thats due to another reason (comment too long),
and that's ain't easily fixable due to strigi limitation of headersize in StreamEndAnalyzer::checkHeader().
users should remove redundant lines from comment in those files themselves
(e.g. http://websvn.kde.org/trunk/l10n-kde4/zh_TW/messages/kdebase/kcmbackground.po)


BUG: 167335

------------------------------------------------------------------------
r841257 | shaforo | 2008-08-02 23:50:23 +0200 (Sat, 02 Aug 2008) | 4 lines

crashes--

BUG: 167020

------------------------------------------------------------------------
r841417 | mlaurent | 2008-08-03 12:01:39 +0200 (Sun, 03 Aug 2008) | 3 lines

Backport:
Fix mem leak

------------------------------------------------------------------------
r842021 | shaforo | 2008-08-04 16:12:04 +0200 (Mon, 04 Aug 2008) | 3 lines

more fixes


------------------------------------------------------------------------
r842206 | shaforo | 2008-08-04 23:26:43 +0200 (Mon, 04 Aug 2008) | 4 lines

- be more specific in PACKAGING file (qt sqlite plugin is required)
- fixes++


------------------------------------------------------------------------
r842221 | shaforo | 2008-08-05 00:13:54 +0200 (Tue, 05 Aug 2008) | 3 lines

correct sync navigation for plural entries


------------------------------------------------------------------------
r842235 | shaforo | 2008-08-05 01:52:42 +0200 (Tue, 05 Aug 2008) | 4 lines

fixes++

BUG: 168251

------------------------------------------------------------------------
r842575 | shaforo | 2008-08-05 17:04:51 +0200 (Tue, 05 Aug 2008) | 3 lines

copy docs from trunk - with example of sync setup and a screenshot


------------------------------------------------------------------------
r842623 | ianjo | 2008-08-05 18:48:40 +0200 (Tue, 05 Aug 2008) | 5 lines

Backport fix for bug 162985 from trunk to branh:
Set the current activespace to the source of the drop event when opening files that were 
dragged and dropped.


------------------------------------------------------------------------
r842709 | lueck | 2008-08-05 22:07:14 +0200 (Tue, 05 Aug 2008) | 2 lines

revert #842575 - string freeze in branch!
CCMAIL:shafff@ukr.net
------------------------------------------------------------------------
r843082 | shaforo | 2008-08-06 14:01:31 +0200 (Wed, 06 Aug 2008) | 3 lines

fixes++


------------------------------------------------------------------------
r843839 | scripty | 2008-08-08 06:53:09 +0200 (Fri, 08 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r844011 | woebbe | 2008-08-08 13:36:40 +0200 (Fri, 08 Aug 2008) | 1 line

oops, forgot to bump it
------------------------------------------------------------------------
r846163 | dhaumann | 2008-08-13 09:26:31 +0200 (Wed, 13 Aug 2008) | 2 lines

backport: fix crash

------------------------------------------------------------------------
r848553 | scripty | 2008-08-18 07:08:01 +0200 (Mon, 18 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r852002 | scripty | 2008-08-25 07:25:43 +0200 (Mon, 25 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r852228 | woebbe | 2008-08-25 17:40:37 +0200 (Mon, 25 Aug 2008) | 5 lines

Backport of rev. 847528:

Allow cvsnt users to login to repositories.

CCBUG:162523
------------------------------------------------------------------------
r852230 | woebbe | 2008-08-25 17:41:10 +0200 (Mon, 25 Aug 2008) | 1 line

bump version number
------------------------------------------------------------------------
r852973 | sengels | 2008-08-27 00:37:24 +0200 (Wed, 27 Aug 2008) | 1 line

fix linking under windows
------------------------------------------------------------------------
r853064 | scripty | 2008-08-27 08:04:49 +0200 (Wed, 27 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r853348 | ereslibre | 2008-08-27 17:40:21 +0200 (Wed, 27 Aug 2008) | 4 lines

Backport of fix. Restore settings even if no session is chosen.

CCMAIL: kwrite-devel@kde.org

------------------------------------------------------------------------
r853352 | ereslibre | 2008-08-27 17:43:49 +0200 (Wed, 27 Aug 2008) | 2 lines

Backport revision 853351. Correctly use kdelibs for restoring window toolbar positions & dock windows

------------------------------------------------------------------------
r853515 | woebbe | 2008-08-27 22:03:38 +0200 (Wed, 27 Aug 2008) | 2 lines

fix linking under non windows

------------------------------------------------------------------------
