------------------------------------------------------------------------
r1169754 | scripty | 2010-08-30 14:25:00 +1200 (Mon, 30 Aug 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1170098 | aacid | 2010-08-31 06:50:33 +1200 (Tue, 31 Aug 2010) | 2 lines

removing people's code is not nice

------------------------------------------------------------------------
r1170663 | lueck | 2010-09-02 07:28:40 +1200 (Thu, 02 Sep 2010) | 1 line

doc backport for 4.5.2
------------------------------------------------------------------------
r1170782 | scripty | 2010-09-02 15:20:01 +1200 (Thu, 02 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1171142 | ltoscano | 2010-09-03 09:23:17 +1200 (Fri, 03 Sep 2010) | 8 lines

Backport r1171140: Make xmlcheck plugin working with external Docbook XML DTDs.

- Import only the ksgmltools2/customization/catalog.xml (the internal 
  catalog), which in turn imports the system-provided catalog.
- Use XML_CATALOG_FILES instead of SGML_CATALOG_FILES (and thus exclude
  --catalog from xmllint command line)


------------------------------------------------------------------------
r1172875 | scripty | 2010-09-08 15:23:38 +1200 (Wed, 08 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1173152 | ilic | 2010-09-09 07:24:33 +1200 (Thu, 09 Sep 2010) | 1 line

Extract code from some elements when they have code='true' attribute.
------------------------------------------------------------------------
r1173674 | scripty | 2010-09-10 14:45:53 +1200 (Fri, 10 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1174345 | scripty | 2010-09-12 14:57:41 +1200 (Sun, 12 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1174639 | scripty | 2010-09-13 14:38:31 +1200 (Mon, 13 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1175018 | pino | 2010-09-14 09:57:49 +1200 (Tue, 14 Sep 2010) | 2 lines

call the mimetype icon after the mimetype name

------------------------------------------------------------------------
r1175365 | lueck | 2010-09-15 08:30:16 +1200 (Wed, 15 Sep 2010) | 1 line

was renamed to kdesrc-build.desktop
------------------------------------------------------------------------
r1176218 | scripty | 2010-09-17 14:47:31 +1200 (Fri, 17 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1176556 | scripty | 2010-09-18 14:39:17 +1200 (Sat, 18 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1176722 | lueck | 2010-09-19 00:40:57 +1200 (Sun, 19 Sep 2010) | 1 line

backport from trunk Revision 1176721: add missing i18n()
------------------------------------------------------------------------
r1176920 | scripty | 2010-09-19 14:48:34 +1200 (Sun, 19 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1177469 | kudryashov | 2010-09-20 21:41:20 +1200 (Mon, 20 Sep 2010) | 3 lines

Backport 1177468 from trunk

Don't build documentation for components that are not built
------------------------------------------------------------------------
r1177655 | lueck | 2010-09-21 07:48:14 +1200 (Tue, 21 Sep 2010) | 3 lines

backport: change gui strings from ktts to Jovie
@Jeremy: all gui strings are backported now, documentation follows
CCMAIL:jpwhiting@kde.org
------------------------------------------------------------------------
r1177740 | scripty | 2010-09-21 15:40:12 +1200 (Tue, 21 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1178030 | scripty | 2010-09-22 14:53:34 +1200 (Wed, 22 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1178429 | scripty | 2010-09-23 14:51:13 +1200 (Thu, 23 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1178844 | scripty | 2010-09-24 14:45:19 +1200 (Fri, 24 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1179299 | scripty | 2010-09-25 14:50:19 +1200 (Sat, 25 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1179698 | scripty | 2010-09-26 16:14:41 +1300 (Sun, 26 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1180122 | scripty | 2010-09-27 15:55:59 +1300 (Mon, 27 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1180453 | scripty | 2010-09-28 16:25:39 +1300 (Tue, 28 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
