------------------------------------------------------------------------
r948813 | gateau | 2009-04-03 20:41:04 +0000 (Fri, 03 Apr 2009) | 1 line

Bump version numbers
------------------------------------------------------------------------
r951463 | cgilles | 2009-04-09 12:44:46 +0000 (Thu, 09 Apr 2009) | 2 lines

backpôrt from trunk

------------------------------------------------------------------------
r954742 | gateau | 2009-04-16 08:02:15 +0000 (Thu, 16 Apr 2009) | 3 lines

Correctly detect gif containing only one frame and a graphic control extension.

BUG:185523
------------------------------------------------------------------------
r954743 | gateau | 2009-04-16 08:02:25 +0000 (Thu, 16 Apr 2009) | 1 line

Updated
------------------------------------------------------------------------
r955054 | sars | 2009-04-16 20:27:47 +0000 (Thu, 16 Apr 2009) | 1 line

Backport bug fix for (saving) mono color QImage.
------------------------------------------------------------------------
r955423 | wstephens | 2009-04-17 15:10:41 +0000 (Fri, 17 Apr 2009) | 1 line

Backport r955421: Restore default shortcuts lost in KDE4 port
------------------------------------------------------------------------
r955953 | pino | 2009-04-18 23:18:55 +0000 (Sat, 18 Apr 2009) | 2 lines

backport: use the kde toolbar font for the text in the sidebar

------------------------------------------------------------------------
r955982 | sars | 2009-04-19 06:57:05 +0000 (Sun, 19 Apr 2009) | 1 line

Bump the ABI age ("also mono QImage needs a color table" Fix)
------------------------------------------------------------------------
r956529 | scripty | 2009-04-20 07:41:52 +0000 (Mon, 20 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r957333 | pino | 2009-04-21 20:39:30 +0000 (Tue, 21 Apr 2009) | 2 lines

backport: force the sidebar widget to not be transparent

------------------------------------------------------------------------
r957396 | aacid | 2009-04-21 22:19:45 +0000 (Tue, 21 Apr 2009) | 3 lines

fix the catalog name to match the real catalogname
found by Burkhard Lück

------------------------------------------------------------------------
r957796 | pino | 2009-04-22 22:31:59 +0000 (Wed, 22 Apr 2009) | 2 lines

SVN_SILENT: remove obsolete debug-like message

------------------------------------------------------------------------
r957821 | aacid | 2009-04-22 23:03:24 +0000 (Wed, 22 Apr 2009) | 6 lines

Backport r957819 | aacid | 2009-04-23 01:02:00 +0200 (Thu, 23 Apr 2009) | 5 lines

Add some userMutex to remove some helgrind warnings
May or may not fix bug 190336


------------------------------------------------------------------------
r958376 | aacid | 2009-04-23 22:33:28 +0000 (Thu, 23 Apr 2009) | 2 lines

Add a dummy cpp file with hplip messages so they are translatable given hplip people don't care at all about translatability of their software

------------------------------------------------------------------------
r958510 | scripty | 2009-04-24 07:33:59 +0000 (Fri, 24 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r960735 | pino | 2009-04-28 23:01:28 +0000 (Tue, 28 Apr 2009) | 2 lines

backport: raise a bit the tolerance

------------------------------------------------------------------------
r960736 | pino | 2009-04-28 23:02:56 +0000 (Tue, 28 Apr 2009) | 2 lines

bump version to 0.8.3

------------------------------------------------------------------------
r961321 | darioandres | 2009-04-29 22:00:26 +0000 (Wed, 29 Apr 2009) | 11 lines

Backport to 4.2branch of:
SVN commit 961311 by darioandres:

Prevent a crash if the config file says that we will use XF86Config, but Gamma
value is not present
in the Xorg.conf file. (Also, unselect the "Save settings system wide" in this
situation)

CCBUG: 183124


------------------------------------------------------------------------
