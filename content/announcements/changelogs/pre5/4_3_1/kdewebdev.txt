------------------------------------------------------------------------
r1014248 | isdale | 2009-08-22 05:58:40 +0000 (Sat, 22 Aug 2009) | 6 lines

Remove dependancy on pthread instead use QThread subclass
Remove some uncessary thread status console messages

BUG:203812


------------------------------------------------------------------------
r1014256 | isdale | 2009-08-22 07:31:57 +0000 (Sat, 22 Aug 2009) | 5 lines

Remove connection as it is not required at this time and just results
in a runtime warning
Update ChangeLog


------------------------------------------------------------------------
r1016202 | scripty | 2009-08-27 03:38:08 +0000 (Thu, 27 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
