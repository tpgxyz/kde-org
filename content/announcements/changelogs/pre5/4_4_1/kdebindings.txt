------------------------------------------------------------------------
r1085304 | arnorehn | 2010-02-04 20:26:44 +0000 (Thu, 04 Feb 2010) | 7 lines

backport from trunk:
loop till i <= numClasses - otherwise the last wrapped
class gets lost.

CCMAIL kde-bindings@kde.org


------------------------------------------------------------------------
r1085357 | rdale | 2010-02-05 02:40:48 +0000 (Fri, 05 Feb 2010) | 4 lines

* Raise the QtRuby version number to 2.1.0 - better late than never

CCMAIL: kde-bindings@kde.org

------------------------------------------------------------------------
r1085468 | sedwards | 2010-02-05 10:33:08 +0000 (Fri, 05 Feb 2010) | 10 lines

Incorporate the changes introduced in revision 1084111 to kdelibs/plasma/animations/animation.h. This fixes PyKDE4 for 4.4.0.

(Thanks to AiX 27/249 for the patch.)

CCMAIL: release-team@kde.org
CCMAIL: kde-packages@kde.org
CCMAIL: kde-bindings@kde.org



------------------------------------------------------------------------
r1086678 | rdale | 2010-02-07 20:59:02 +0000 (Sun, 07 Feb 2010) | 2 lines

* Add ${QT_INCLUDES}

------------------------------------------------------------------------
r1088476 | arnorehn | 2010-02-10 22:41:56 +0000 (Wed, 10 Feb 2010) | 4 lines

Fix handling of non-Qt types in QVariant.

CCMAIL: kde-bindings@kde.org

------------------------------------------------------------------------
r1090622 | arnorehn | 2010-02-15 17:11:36 +0000 (Mon, 15 Feb 2010) | 11 lines

backport from trunk:

Create global references for visible top-level widgets,
so they aren't garbage collected while being shown. This
should create a more natural behaviour. Fixes a bug reported
by Linuxoid Oz. Thanks!

CCMAIL: kde-bindings@kde.org
CCMAIL: linuxoidoz@yahoo.com.au


------------------------------------------------------------------------
r1093831 | sedwards | 2010-02-21 14:43:11 +0000 (Sun, 21 Feb 2010) | 3 lines

Added support for Solid::Networking::Notifier.


------------------------------------------------------------------------
r1095383 | sedwards | 2010-02-24 07:05:33 +0000 (Wed, 24 Feb 2010) | 5 lines

Regenerated API docs. This fixes a couple of problems such as:
* enums were not being printed correctly
* API text missing from constructors


------------------------------------------------------------------------
