------------------------------------------------------------------------
r1089771 | mpyne | 2010-02-13 23:42:53 +0000 (Sat, 13 Feb 2010) | 4 lines

Backport fix for bug 226587 (JuK crash on exit in certain configurations) to KDE 4.4.1.

BUG:226587

------------------------------------------------------------------------
r1092480 | darioandres | 2010-02-19 01:39:08 +0000 (Fri, 19 Feb 2010) | 2 lines

- Fix i18n issue (args not being used properly causing I18N error messages)

------------------------------------------------------------------------
r1092484 | darioandres | 2010-02-19 01:54:02 +0000 (Fri, 19 Feb 2010) | 4 lines

- Scale the cover image to the proper DesktopIcon size; otherwise, the KStatusNotifierItem tooltip will shown an unknown 
icon


------------------------------------------------------------------------
r1093689 | schwarzer | 2010-02-21 11:36:55 +0000 (Sun, 21 Feb 2010) | 9 lines

fix xml markup

<nl> -> <nl/>
mistyped markup is shown in the UI message

This fuzzies 1 message for translators.

CCMAIL: kde-i18n-doc@kde.org

------------------------------------------------------------------------
