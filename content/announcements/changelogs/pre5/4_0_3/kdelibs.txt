2008-02-29 16:53 +0000 [r780634]  mwoehlke

	* branches/KDE/4.0/kdelibs/kate/utils/kateschema.cpp,
	  branches/KDE/4.0/kdelibs/kate/utils/kateconfig.cpp: Backport
	  780632 to 4.0: new way of calculating default colors that should
	  work adequately for both dark and light color schemes (unlike
	  either of the previous attempts that had problems with one or the
	  other) CCBUG: 157628

2008-02-29 19:32 +0000 [r780692]  dfaure

	* branches/KDE/4.0/kdelibs/kdecore/tests/kconfigtest.cpp,
	  branches/KDE/4.0/kdelibs/kdecore/config/kconfig.cpp,
	  branches/KDE/4.0/kdelibs/kdecore/tests/kconfigtest.h: Fix
	  KConfig::copyTo, used by KOpenWithDialog when associating an
	  application with a mimetype. BUG: 154595

2008-02-29 21:33 +0000 [r780728]  dfaure

	* branches/KDE/4.0/kdelibs/kio/kfile/kopenwithdialog.cpp: Backport
	  reformattings and r780723: Make KOpenWithDialog write to
	  mimeapps.list when asked to remember an association between a
	  mimetype and an existing application. It still has to write a new
	  .desktop file when the user types the name of a program without a
	  .desktop file, or when checking "Run in terminal" (but in that
	  case it's not a copy of another desktop file, so the previous fix
	  to KConfig::copyTo is now unrelated to this code). CCBUG: 154595

2008-03-02 21:07 +0000 [r781432]  aacid

	* branches/KDE/4.0/kdelibs/kdeui/xmlgui/ktoolbarhandler.cpp:
	  backport r781431 **************** Use
	  KStandardAction::ShowToolbar text instead of writing it here, has
	  two good pluses, maintains it synced and adds a &

2008-03-03 16:42 +0000 [r781727]  dfaure

	* branches/KDE/4.0/kdelibs/khtml/khtml_part.cpp: Fix nonsense after
	  the privatization of m_url in ReadOnlyPart:
	  this->url().setPath("/"); .... no-op!

2008-03-03 20:50 +0000 [r781900]  dfaure

	* branches/KDE/4.0/kdelibs/kdecore/tests/kconfigtest.cpp,
	  branches/KDE/4.0/kdelibs/kdecore/config/kconfigini.cpp,
	  branches/KDE/4.0/kdelibs/kdecore/tests/kconfigtest.h: Backport
	  r781898: do create the parent directory if necessary. Seems
	  canonicalPath returns empty for a non-existing dir.

2008-03-03 22:53 +0000 [r781940]  orlovich

	* branches/KDE/4.0/kdelibs/khtml/ecma/kjs_html.cpp: Properly
	  normalize whitespace in URLs when getting from JS. Fixes article
	  links @ http://milliyet.com.tr/ (Thanks to cartman for the
	  report)

2008-03-04 01:24 +0000 [r781960]  dfaure

	* branches/KDE/4.0/kdelibs/kdecore/services/kservice.cpp: turn
	  assert into warning so that spstarr can log into kde despite a
	  broken Swiftfox.desktop

2008-03-04 16:31 +0000 [r782229]  dfaure

	* branches/KDE/4.0/kdelibs/kio/kio/kbuildsycocaprogressdialog.cpp:
	  Run kbuildsycoca4 ourselves when we can't ask kded to do it for
	  us BUG: 152332

2008-03-04 17:15 +0000 [r782245]  dfaure

	* branches/KDE/4.0/kdelibs/kio/kfile/kpropertiesdialog.cpp,
	  branches/KDE/4.0/kdelibs/kio/kfile/kopenwithdialog.cpp: Exec is
	  no path - backport of r782227, bug 119263.

2008-03-04 21:14 +0000 [r782335]  dfaure

	* branches/KDE/4.0/kdelibs/khtml/khtmlimage.cpp,
	  branches/KDE/4.0/kdelibs/khtml/khtmlimage.h: Fix mimetype used in
	  popupmenu over a full-view image... it offered "open in firefox"
	  and "preview in KHTML" instead of image viewers. (this bug is in
	  kde3 too)

2008-03-04 21:20 +0000 [r782342-782340]  neundorf

	* branches/KDE/4.0/kdelibs/cmake/modules/CMakeLists.txt,
	  branches/KDE/4.0/kdelibs/cmake/modules/cmake-modules-styleguide.txt
	  (added): -add the cmake styleguide also in the stable branch Alex

	* branches/KDE/4.0/kdelibs/cmake/modules/cmake-modules-styleguide.txt:
	  -add note that all-lower-case and indent-using-spaces is prefered
	  Alex

2008-03-05 18:29 +0000 [r782677]  reitelbach

	* branches/KDE/4.0/kdelibs/kdoctools/docbook/xsl/common/de.xml: Use
	  correct quotes for german docs CCMAIL:kde-i18n-de@kde.org

2008-03-05 22:35 +0000 [r782774]  mlaurent

	* branches/KDE/4.0/kdelibs/security/crypto/certexport.cpp:
	  Backport: Fix layout

2008-03-06 10:18 +0000 [r782856]  staniek

	* branches/KDE/4.0/kdelibs/kdecore/localization/klocale.cpp:
	  backport r782853: Windows: fall back to system locale for l10n.
	  Now it is enough to remove Country and Language from [Locale]
	  group in kdeglobals, to be in sync with Windows registry
	  settings.

2008-03-06 13:31 +0000 [r782912]  tokoe

	* branches/KDE/4.0/kdelibs/kdeui/paged/kpageview_p.cpp,
	  branches/KDE/4.0/kdelibs/kdeui/paged/kpageview_p.h: Backport:
	  Don't use setMinimumSize( minimumSizeHint() ) here, as at this
	  point the tab pages may not be filled with widgets, so a wrong
	  minimum size is calculated, which can't be fixed later. So
	  instead reimplement minimumSizeHint()

2008-03-06 15:11 +0000 [r782936]  staniek

	* branches/KDE/4.0/kdelibs/kdecore/localization/klocale.cpp:
	  Backported r782935: Windows: process rawList also when useEnv ==
	  false remove extra QStringList rawList;

2008-03-06 20:37 +0000 [r783037]  dfaure

	* branches/KDE/4.0/kdelibs/kdecore/tests/kmimetypetest.cpp,
	  branches/KDE/4.0/kdelibs/kdecore/services/kmimetype.cpp: Albert
	  calls findByFileContent with a URL instead of a path and gets
	  11021 as accuracy :) It was not set in all code paths inside
	  findByFileContent -> fixed.

2008-03-06 21:29 +0000 [r783049]  mrybczyn

	* branches/KDE/4.0/kdelibs/kdoctools/customization/pl/user.entities:
	  Added FAQ, Backspace and kdm-narzednik (for faq).

2008-03-06 22:25 +0000 [r783067]  reitelbach

	* branches/KDE/4.0/kdelibs/kdoctools/docbook/xsl/common/de.xml: hm,
	  fix it. decimal values are expected here ...

2008-03-07 15:50 +0000 [r783258]  rjarosz

	* branches/KDE/4.0/kdelibs/khtml/khtmlview.cpp,
	  branches/KDE/4.0/kdelibs/khtml/khtmlview.h: Backport commit
	  783256. Remove flickering during resize event. Flickering is
	  caused by move call in
	  QScrollAreaPrivate::updateWidgetPosition(). The move call
	  immediately moves the view without executing paint event and
	  after that KHTMLView moves it back to correct position, so this
	  patch suppresses the move calls. It also forces scrollbars range
	  update on resize if view is hidden otherwise we can't move the
	  view to new area until we show it.

2008-03-10 14:21 +0000 [r784042]  mueller

	* branches/KDE/4.0/kdelibs/includes/KNS/Provider (removed),
	  branches/KDE/4.0/kdelibs/includes/KNS/ProviderDialog (removed),
	  branches/KDE/4.0/kdelibs/includes/KNS/Button (removed),
	  branches/KDE/4.0/kdelibs/includes/KNS/Action (removed),
	  branches/KDE/4.0/kdelibs/includes/Phonon/Experimental/OverlayApi
	  (removed), branches/KDE/4.0/kdelibs/includes/KNS/ProviderLoader
	  (removed),
	  branches/KDE/4.0/kdelibs/includes/KTextEditor/HighlightingInterface
	  (removed), branches/KDE/4.0/kdelibs/includes/CMakeLists.txt,
	  branches/KDE/4.0/kdelibs/includes/KNS/Feed (removed),
	  branches/KDE/4.0/kdelibs/includes/KNS/UploadDialog (removed),
	  branches/KDE/4.0/kdelibs/includes/KSettings/ComponentsDialog
	  (removed), branches/KDE/4.0/kdelibs/includes/KNS/EntryLoader
	  (removed), branches/KDE/4.0/kdelibs/includes/KNS/ProviderHandler
	  (removed), branches/KDE/4.0/kdelibs/includes/SafeSite (removed):
	  merge forwarding header fixes from trunk

2008-03-10 17:09 +0000 [r784105]  orlovich

	* branches/KDE/4.0/kdelibs/khtml/ecma/kjs_html.cpp,
	  branches/KDE/4.0/kdelibs/khtml/ecma/kjs_html.h: Use the new KJS
	  hook to better hide hidden collections BUG: 130376

2008-03-10 23:04 +0000 [r784334]  lunakl

	* branches/KDE/4.0/kdelibs/phonon/videowidget.cpp,
	  branches/KDE/4.0/kdelibs/kdeui/actions/ktogglefullscreenaction.cpp,
	  branches/KDE/4.0/kdebase/apps/konsole/src/MainWindow.cpp,
	  branches/KDE/4.0/kdelibs/kdeui/actions/ktogglefullscreenaction.h,
	  branches/KDE/4.0/kdebase/apps/konqueror/src/konqmisc.cpp,
	  branches/KDE/4.0/kdebase/apps/konqueror/src/konqmainwindow.cpp:
	  Backport r784333 (remove usage of QWidget::showFullScreen()
	  etc.). BUG: 157941

2008-03-11 12:48 +0000 [r784472]  dfaure

	* branches/KDE/4.0/kdelibs/kdeui/widgets/ktoolbar.cpp: This struct
	  is not used anymore (we lack the QMainWindow API for placing
	  toolbars ourselves...), so comment it out for now.

2008-03-11 18:36 +0000 [r784565]  ilic

	* branches/KDE/4.0/kdelibs/kio/kio/accept-languages.codes: Add pt
	  as fallback for pt-BR in User-Agent. (bport: 784564)

2008-03-12 00:27 +0000 [r784664]  pino

	* branches/KDE/4.0/kdelibs/includes/KNS/EntryHandler (removed),
	  branches/KDE/4.0/kdelibs/includes/CMakeLists.txt: remove forward
	  include to non-existing include

2008-03-12 08:35 +0000 [r784734-784718]  ggarand

	* branches/KDE/4.0/kdelibs/khtml/khtmlview.cpp,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_docimpl.h,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_docimpl.cpp: automatically
	  merged revision 783804: Tighten our paint prevention timings to
	  provide more responsiveness while keeping smooth page
	  transitions. Proper balance is not easy to achieve, as preventing
	  the "white flash" is just part of the equation. Layout stability,
	  allowing early readability, is another important term - as is
	  bandwidth. Engines with aggressive timings would get an
	  unpleasnt, jerky feeling we want to avoid. We'll use subtly
	  progressive timings to get a good compromise.

	* branches/KDE/4.0/kdelibs/kdeui/widgets/ktabwidget.cpp:
	  automatically merged revision 783805: People should be very
	  careful with Qt 4's setUpdatesEnabled() as one can end up
	  unwillingly repainting enormous surfaces for no good reason.
	  "Re-enabling updates implicitly calls update() on the widget."
	  Here, the entire widget was repainted when setting the tab title,
	  leading to flicker and breakage of web page transitions in
	  konqueror.

	* branches/KDE/4.0/kdelibs/khtml/khtmlview.cpp: automatically
	  merged revision 783806: fix flicker at scroll position
	  restoration from cache which I noticed thanks to discussion with
	  Roman. I considered generalizing Roman's bypassing of QScrollArea
	  and doing the bliting using QWidget::scroll instead but it proved
	  slower than native QWidget::move for whatever reason.

	* branches/KDE/4.0/kdelibs/khtml/khtmlview.cpp,
	  branches/KDE/4.0/kdelibs/khtml/khtmlview.h: automatically merged
	  revision 783985: make sure external widgets are removed from view
	  when scrolling pages with static backgound. (e.g. flash window
	  sometimes not wanting to scroll out of view)

	* branches/KDE/4.0/kdelibs/khtml/khtmlview.cpp: automatically
	  merged revision 783986: disable alien widgets on khtmlview while
	  they break scroll blitting CCBUG: 157883

	* branches/KDE/4.0/kdelibs/khtml/rendering/render_canvas.cpp:
	  automatically merged revision 784123: Need to relayout the host
	  RenderWidget when switching an iframe from redirected mode to
	  non-redirected mode (e.g. because it ended up containing an
	  external widget), otherwise the widget mask would not get
	  computed. Fix dhtml-menu-behind-frame-with-flash at
	  http://www.reallifecomics.de/

	* branches/KDE/4.0/kdelibs/khtml/rendering/render_layer.cpp:
	  automatically merged revision 784122: ameliorate the algorithm
	  computing z-order mask for external widgets so that it also walk
	  parent stacking contexts. This should take care of all remaining
	  cases of dhtml menus being displayed behind Flash/Java widgets.
	  BUG: 158759

	* branches/KDE/4.0/kdelibs/khtml/rendering/render_table.cpp:
	  automatically merged revision 784125: merge typo fix for css3
	  backgrounds in tables WC/#13095/r29241/

	* branches/KDE/4.0/kdelibs/khtml/rendering/render_replaced.cpp,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_form.cpp,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_replaced.h,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_line.cpp,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_table.cpp,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_box.cpp,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_line.h,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_body.cpp,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_box.h:
	  automatically merged revision 784702: rename paintBackground ->
	  paintOneBackground paintBackgrounds -> paintAllBackgrounds

	* branches/KDE/4.0/kdelibs/khtml/khtmlview.cpp,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_block.h,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_canvas.h,
	  branches/KDE/4.0/kdelibs/khtml/khtmlview.h,
	  branches/KDE/4.0/kdelibs/khtml/css/cssstyleselector.cpp,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_object.cpp,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_canvas.cpp,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_layer.cpp:
	  automatically merged revision 784703: Large optimization of
	  scrolling for pages containing "position: fixed" objects.
	  .replace the dumb approach of repainting the whole page always,
	  by collecting fixed regions, repainting them individually, then
	  blit-scrolling the remaining surface. .also maintain a tighter
	  count of fixed objects, so that changes of styles might trigger a
	  switch from static to blitting (i.e. fast) background.
	  .consequently, go all the way to using QWidget::scroll() instead
	  of native QWidget::move() for scrolling, so as to avoid the
	  unescapable flicker when switching from a method to the other.
	  QWidget::scroll is - I believe, a tad slower, but not
	  significantly so, and the gain in overall browsing smoothness is
	  very satisfactory. -change of scrolling method conveniently
	  allows to avoid the serious scrolling bugs in 4.4 beta, making it
	  possible to remove the disabling of alien widgets, which in turn
	  should fix the problems Roman encountered in Kopete. CCMAIL:
	  kedgedev@centrum.cz

2008-03-12 23:43 +0000 [r784991]  dfaure

	* branches/KDE/4.0/kdelibs/kdeui/xmlgui/kxmlguibuilder.cpp,
	  branches/KDE/4.0/kdelibs/kdeui/tests/kxmlgui_unittest.cpp,
	  branches/KDE/4.0/kdelibs/kdeui/tests/kxmlgui_unittest.h: Fix
	  off-by-one porting error in KXMLGUIBuilder, which moved all the
	  separators up by one in the Go menu when merging okularpart with
	  konqueror. With unit test.

2008-03-13 12:00 +0000 [r785148]  dfaure

	* branches/KDE/4.0/kdelibs/cmake/modules/FindBlitz.cmake,
	  branches/KDE/4.0/kdelibs/cmake/modules/FindQImageBlitz.cmake:
	  Also use pkgconfig to find QImageBlitz, since it installs a .pc
	  file.

2008-03-13 19:41 +0000 [r785314]  porten

	* branches/KDE/4.0/kdelibs/kjs/CMakeLists.txt: Fixed what looks
	  like a wrong and undefined variable name being referenced.

2008-03-13 20:14 +0000 [r785323]  mueller

	* branches/KDE/4.0/kdelibs/kdecore/util/kde_file.h: add KDE_signal
	  to make kdepimlibs from 4.1 compile against KDE 4.0 codebase

2008-03-13 20:57 +0000 [r785347]  dfaure

	* branches/KDE/4.0/kdelibs/kio/kio/kdirmodel.cpp: 10x speedup in
	  KDirModel::nodeForUrl() (called by indexForItem), by comparing
	  paths instead of urls (KUrl::url() is too expensive for tight
	  loops, and the whole logic of this method is to assume direct
	  children of the directory url anyway)

2008-03-13 21:57 +0000 [r785378]  aacid

	* branches/KDE/4.0/kdelibs/kdeui/widgets/knuminput.cpp: Backport
	  r785377 **************** chop the suffix before doing the int
	  conversion

2008-03-14 18:51 +0000 [r785693-785691]  orlovich

	* branches/KDE/4.0/kdelibs/khtml/ecma/kjs_html.cpp,
	  branches/KDE/4.0/kdelibs/khtml/ecma/kjs_html.h: Handle
	  non-standard, but widely supported click() on button. (Noticed
	  while making a testcase)

	* branches/KDE/4.0/kdelibs/khtml/ecma/kjs_events.cpp,
	  branches/KDE/4.0/kdelibs/khtml/ecma/kjs_window.cpp,
	  branches/KDE/4.0/kdelibs/khtml/ecma/kjs_events.h,
	  branches/KDE/4.0/kdelibs/khtml/ecma/kjs_window.h: Apply a patch
	  from Vyacheslav Tokarev (username tsjoker, hostname gmail, tld
	  com) that fixes multiple issues with event listeners: 1. Use the
	  proper 'this' object for object-style event listeners. 2. Use the
	  object, not function, for comparison of object-style event
	  listeners 3. Properly keep html-style and normal listener objects
	  separate --- they behave independent, even if they wrap the same
	  function, and of course have different event-cancellation
	  semantics BUG:147249 BUG:111568 BUG:128416

2008-03-14 21:02 +0000 [r785749]  porten

	* branches/KDE/4.0/kdelibs/khtml/ecma/kjs_dom.cpp,
	  branches/KDE/4.0/kdelibs/khtml/ecma/kjs_dom.h,
	  branches/KDE/4.0/kdelibs/khtml/ecma/kjs_window.cpp,
	  branches/KDE/4.0/kdelibs/khtml/ecma/kjs_window.h: Added JS
	  pseudo-constructors for NodeList and HTMLCollection

2008-03-14 21:51 +0000 [r785761]  porten

	* branches/KDE/4.0/kdelibs/khtml/ecma/kjs_html.cpp,
	  branches/KDE/4.0/kdelibs/khtml/ecma/kjs_html.h: Forgot to commit
	  this earlier - overlapped with a yet uncommited change.

2008-03-14 22:03 +0000 [r785770]  porten

	* branches/KDE/4.0/kdelibs/kjs/interpreter.cpp: Merged r785759 from
	  trunk.

2008-03-14 22:14 +0000 [r785771]  porten

	* branches/KDE/4.0/kdelibs/khtml/ecma/kjs_window.cpp: These
	  built-in objects don't need to be enumerable.

2008-03-14 23:01 +0000 [r785783]  porten

	* branches/KDE/4.0/kdelibs/khtml/ecma/kjs_dom.cpp,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_nodeimpl.h,
	  branches/KDE/4.0/kdelibs/khtml/CMakeLists.txt,
	  branches/KDE/4.0/kdelibs/khtml/xml/ClassNodeList.h (added),
	  branches/KDE/4.0/kdelibs/khtml/xml/ClassNames.h (added),
	  branches/KDE/4.0/kdelibs/khtml/ecma/kjs_dom.h,
	  branches/KDE/4.0/kdelibs/khtml/xml/Document.h (added),
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_nodeimpl.cpp,
	  branches/KDE/4.0/kdelibs/khtml/xml/ClassNodeList.cpp (added),
	  branches/KDE/4.0/kdelibs/khtml/xml/ClassNames.cpp (added):
	  Feature of HTML 5: getElementsByClassName(). Tried to introduce a
	  bit of the WebCore style to make code better comparable but in
	  some parts the differences were to great so I wrote them the
	  khtml way.

2008-03-16 09:46 +0000 [r786185]  mkretz

	* branches/KDE/4.0/kdelibs/phonon/kcm/outputdevicechoice.cpp:
	  backport r786076 by Michael Jansen Bug 159370: Crash when
	  clicking in the white space of the outputcategorywidget

2008-03-16 10:01 +0000 [r786188]  mlaurent

	* branches/KDE/4.0/kdelibs/phonon/kcm/outputdevicechoice.cpp:
	  Backport: Fix missing signal/slot between showCheckBox and signal
	  changed()

2008-03-16 10:25 +0000 [r786194]  mkretz

	* branches/KDE/4.0/kdelibs/phonon/kcm/outputdevicechoice.cpp:
	  revert incorrect backport: showCheckBox is new in 4.1 CCMAIL:
	  montel@kde.org

2008-03-16 13:11 +0000 [r786233]  porten

	* branches/KDE/4.0/kdelibs/khtml/xml/dom_nodeimpl.h,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_docimpl.cpp: Fields with a
	  negative tabindex property are supposed to be skipped during
	  tabbing. Lost the ability to cover the full range up to 32767
	  now. What happens on overflows needs to be investigated either
	  way.

2008-03-16 13:41 +0000 [r786242]  porten

	* branches/KDE/4.0/kdelibs/khtml/xml/dom_docimpl.cpp: For SHRT_MAX.
	  Already surprised it compiled for me.

2008-03-16 15:23 +0000 [r786269]  porten

	* branches/KDE/4.0/kdelibs/khtml/ecma/kjs_dom.cpp: Fixed type
	  checks in contains() function (IE extension).

2008-03-16 16:44 +0000 [r786300]  lunakl

	* branches/KDE/4.0/kdelibs/kinit/kinit.cpp: When shutting down
	  during KDE shutdown, kill also kded4, otherwise it'll stay
	  running.

2008-03-16 17:23 +0000 [r786309]  porten

	* branches/KDE/4.0/kdelibs/khtml/rendering/render_container.cpp,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_object.cpp,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_object.h: Merged
	  revision 786289: Protect anonymous blocks from being deleted
	  while they are actively insterting a new child BUG:150006

2008-03-16 19:11 +0000 [r786329]  porten

	* branches/KDE/4.0/kdelibs/khtml/xml/dom_elementimpl.cpp,
	  branches/KDE/4.0/kdelibs/khtml/html/html_formimpl.cpp,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_elementimpl.h,
	  branches/KDE/4.0/kdelibs/khtml/html/html_formimpl.h: copy input
	  element values on cloneNode()

2008-03-16 20:28 +0000 [r786348-786346]  porten

	* branches/KDE/4.0/kdelibs/khtml/dom/dom_element.h,
	  branches/KDE/4.0/kdelibs/khtml/dom/dom_doc.cpp,
	  branches/KDE/4.0/kdelibs/khtml/dom/dom_element.cpp,
	  branches/KDE/4.0/kdelibs/khtml/dom/dom_doc.h: Publish
	  getElementsByClassName() functions as requested by Mathieu
	  Ducharme.

	* branches/KDE/4.0/kdelibs/khtml/dom/dom_element.h,
	  branches/KDE/4.0/kdelibs/khtml/dom/dom_doc.cpp,
	  branches/KDE/4.0/kdelibs/khtml/dom/dom_element.cpp,
	  branches/KDE/4.0/kdelibs/khtml/dom/dom_doc.h: Oops. Didn't mean
	  to commit this to the 4.0 branch.

2008-03-16 23:07 +0000 [r786408]  ilic

	* branches/KDE/4.0/kdelibs/kdoctools/customization/sr/entities/help-menu.docbook,
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr@latin/entities/help-menu.docbook:
	  Add missing tag.

2008-03-17 11:31 +0000 [r786563]  dfaure

	* branches/KDE/4.0/kdelibs/kio/kio/kdirwatch.cpp,
	  branches/KDE/4.0/kdelibs/kio/kio/kdirwatch_p.h: Backport
	  KDirWatch crash fix (r786560)

2008-03-17 13:07 +0000 [r786613]  mueller

	* branches/KDE/4.0/kdelibs/solid/solid/backends/hal/halaudiointerface.cpp,
	  branches/KDE/4.0/kdelibs/solid/solid/backends/hal/halcamera.cpp,
	  branches/KDE/4.0/kdelibs/solid/solid/device.cpp,
	  branches/KDE/4.0/kdelibs/solid/solid/backends/fakehw/fakebattery.cpp,
	  branches/KDE/4.0/kdelibs/solid/solid/backends/hal/halprocessor.cpp,
	  branches/KDE/4.0/kdelibs/solid/solid/device_p.h: backport
	  bugfixes from /trunk, mostly to make it work with Qt 4.4

2008-03-17 14:12 +0000 [r786634]  lunakl

	* branches/KDE/4.0/kdelibs/kdeui/windowmanagement/netwm.cpp: Fix
	  error that leads to skipping the last icon in the set
	  (bnc:371677).

2008-03-17 20:57 +0000 [r786743]  dfaure

	* branches/KDE/4.0/kdelibs/khtml/ecma/kjs_events.cpp: The DOM (and
	  khtml) use 0 for LMB, 1 for MMB and 2 for RMB but MSIE uses
	  1=LMB, 2=RMB, 4=MMB, as a bitfield. => let's be a good browser
	  and let's use the DOM values in event.button, unless emulating
	  IE.

2008-03-17 21:02 +0000 [r786746]  dfaure

	* branches/KDE/4.0/kdelibs/khtml/ecma/kjs_events.cpp: revert --
	  this was meant for trunk. Better make such changes in 4.1 in case
	  websites with konq-specific code want to adjust based on the konq
	  version.

2008-03-20 12:04 +0000 [r788000]  dfaure

	* branches/KDE/4.0/kdelibs/kfile/kurlnavigator.cpp: backport fix
	  for #157886, dolphin fails to start up

2008-03-20 14:12 +0000 [r788052-788048]  ggarand

	* branches/KDE/4.0/kdelibs/khtml/misc/paintbuffer.h: automatically
	  merged revision 785944: fix alpha channel of CSS Opacity buffers.

	* branches/KDE/4.0/kdelibs/khtml/khtmlview.cpp,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_canvas.h,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_container.cpp,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_object.cpp,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_box.cpp,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_canvas.cpp,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_box.h:
	  automatically merged revision 785946: extend the scrolling
	  optimization of "position:fixed" content, so that it applies to
	  all remaining fixed objects. IOW, scrolling pages with
	  "background-attachment:fixed" images can now be optimized too.

	* branches/KDE/4.0/kdelibs/khtml/khtmlview.cpp,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_style.h,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_box.cpp:
	  automatically merged revision 785947: last, let iframes use the
	  scrolling optimizations too, when they have non-transparent
	  background.

2008-03-21 22:14 +0000 [r788596]  porten

	* branches/KDE/4.0/kdelibs/khtml/xml/dom2_rangeimpl.cpp: Take a
	  shortcut if we reach the end of the range. Fixes an off-by-one
	  bug found by Acid3 but the code looks like it can be simplified a
	  lot.

2008-03-22 14:55 +0000 [r788806]  porten

	* branches/KDE/4.0/kdelibs/khtml/xml/dom2_rangeimpl.cpp: Same fix
	  as the one I did for toString() yesterday: a 0 end offset means
	  we don't have to dive into the end container at all. This time I
	  used WebCore r7649 to fix it as it also brings some sanity checks
	  for offset overflows.

2008-03-22 18:42 +0000 [r788863]  porten

	* branches/KDE/4.0/kdelibs/khtml/ecma/kjs_dom.cpp,
	  branches/KDE/4.0/kdelibs/khtml/ecma/kjs_dom.h: Disabling an
	  IE-specific feature: access to custom attributes via JS
	  properties.

2008-03-22 20:51 +0000 [r788928-788924]  ggarand

	* branches/KDE/4.0/kdelibs/khtml/rendering/render_replaced.cpp:
	  automatically merged revision 788917: apply patch by Slava
	  Tokarev fixing unwanted changes of form widgets font size CCMAIL:
	  tsjoker@gmail.com BUG: 142722

	* branches/KDE/4.0/kdelibs/khtml/rendering/render_canvas.h,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_container.cpp,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_object.cpp,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_canvas.cpp:
	  automatically merged revision 788919: fix some possible
	  regressions after scroll optimisation work .do a stricter
	  tracking of fixed content objects (code could get confused by
	  dynamic insertion/removal of objects) .get a better rectangle for
	  inlineFlows .count the outline in

	* branches/KDE/4.0/kdelibs/khtml/khtmlview.cpp: automatically
	  merged revision 788920: Fix mouse events propagated to CSS
	  container's scrollbars, so that they account for transforms.

2008-03-24 16:54 +0000 [r789585]  mlaurent

	* branches/KDE/4.0/kdelibs/cmake/modules/FindPostgreSQL.cmake:
	  Backport: Fix include path

2008-03-25 12:06 +0000 [r789836]  whiting

	* branches/KDE/4.0/kdelibs/knewstuff/knewstuff2/core/ktranslatable.cpp,
	  branches/KDE/4.0/kdelibs/knewstuff/knewstuff2/core/coreengine.cpp:
	  backport fix ktranslatable copy ctor and operator= to not get
	  occasional crashes

2008-03-25 12:18 +0000 [r789839]  mueller

	* branches/KDE/4.0/kdelibs/CMakeLists.txt: 4.0.3 preparations

2008-03-25 16:31 +0000 [r789902-789901]  orlovich

	* branches/KDE/4.0/kdelibs/khtml/khtml_part.h,
	  branches/KDE/4.0/kdelibs/khtml/khtmlpart_p.h,
	  branches/KDE/4.0/kdelibs/khtml/khtmlview.cpp,
	  branches/KDE/4.0/kdelibs/khtml/khtml_part.cpp: Remove dead code,
	  making this file about 0.01% smaller..

	* branches/KDE/4.0/kdelibs/khtml/xml/dom_docimpl.h,
	  branches/KDE/4.0/kdelibs/khtml/html/html_formimpl.cpp,
	  branches/KDE/4.0/kdelibs/khtml/html/htmlparser.cpp,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_docimpl.cpp: Be more
	  careful not to make a mistake when restoring form entries. Still
	  imperfect, but I am not sure what perfect means here..
	  CCBUG:56188

2008-03-25 23:44 +0000 [r790157-790155]  ggarand

	* branches/KDE/4.0/kdelibs/khtml/css/cssstyleselector.cpp:
	  automatically merged revision 790074: empty value should not
	  match empty class list CCBUG: 159201

	* branches/KDE/4.0/kdelibs/khtml/html/htmlparser.cpp: automatically
	  merged revision 790075: merge WC/r29775/#16982/ by David Hyatt
	  <hyatt at apple dot com> "Make sure to make <head> the current
	  block if it is created before a <body>" to show the buckets.
	  CCBUG:156947

	* branches/KDE/4.0/kdelibs/khtml/html/html_headimpl.cpp,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_docimpl.h,
	  branches/KDE/4.0/kdelibs/khtml/css/css_ruleimpl.h,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_elementimpl.h,
	  branches/KDE/4.0/kdelibs/khtml/html/html_headimpl.h,
	  branches/KDE/4.0/kdelibs/khtml/misc/loader.cpp,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_xmlimpl.cpp,
	  branches/KDE/4.0/kdelibs/khtml/misc/loader_client.h,
	  branches/KDE/4.0/kdelibs/khtml/misc/loader.h,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_docimpl.cpp,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_xmlimpl.h,
	  branches/KDE/4.0/kdelibs/khtml/css/css_ruleimpl.cpp:
	  automatically merged revision 790077: stylesheets with
	  unappropriate mimetype should be ignored in strict mode.
	  red->black CCBUG:156947

2008-03-26 04:54 +0000 [r790202]  ggarand

	* branches/KDE/4.0/kdelibs/khtml/rendering/render_object.cpp:
	  automatically merged revision 790201: fix some visibleFlowRegion
	  regressions, that would make some Flash widget mask calculations
	  incorrect.

2008-03-26 11:25 +0000 [r790287]  dfaure

	* branches/KDE/4.0/kdelibs/kdecore/kernel/kstandarddirs.cpp,
	  branches/KDE/4.0/kdelibs/kded/vfolder_menu.cpp: Backport 790285:
	  Fix broken handling of symlinks because of the d_type
	  optimization, bug 150307.

2008-03-26 11:57 +0000 [r790293]  dfaure

	* branches/KDE/4.0/kdelibs/kdecore/tests/kmimetypetest.cpp,
	  branches/KDE/4.0/kdelibs/kdecore/tests/kmimetypetest.h: backport
	  788495: cleanup the faketextpart.desktop service, otherwise
	  konq's unit tests will fail due to trying to use a non-working
	  part for text/plain

2008-03-26 22:13 +0000 [r790540]  staniek

	* branches/KDE/4.0/kdelibs/kio/kio/krun.cpp: backport of r790535
	  Avoid "Do not delete object, 'KRun::timer' during its event
	  handler!" warning. We have the warning because of calling 'delete
	  this'. Use deleteLater().

2008-03-26 22:42 +0000 [r790558]  sebsauer

	* branches/KDE/4.0/kdelibs/kjsembed/kjsembed/variant_binding.cpp:
	  backport r790557: fix crash if functions without a returnvalue
	  got called

2008-03-26 22:59 +0000 [r790562]  sebsauer

	* branches/KDE/4.0/kdelibs/kross/kjs/kjsscript.cpp: backport
	  r790559; * publish ourself and the manager too like at the other
	  backends. * fix another assert by handling the err.

2008-03-26 23:43 +0000 [r790586]  sebsauer

	* branches/KDE/4.0/kdelibs/kross/kjs/kjsscript.cpp: backport
	  r790585 and don't clear the err here (and specialy not before the
	  debug-output ;)

2008-03-27 01:09 +0000 [r790622]  staniek

	* branches/KDE/4.0/kdelibs/kdecore/services/ktraderparse.cpp:
	  backported r790621: use K_GLOBAL_STATIC to avoid memory leak

2008-03-27 01:58 +0000 [r790628]  sebsauer

	* branches/KDE/4.0/kdelibs/kjsembed/kjsembed/qobject_binding.cpp,
	  branches/KDE/4.0/kdelibs/kjsembed/kjsembed/variant_binding.cpp:
	  backport r790627 fix handling of QVariant's

2008-03-27 09:39 +0000 [r790716]  sebsauer

	* branches/KDE/4.0/kdelibs/kjsembed/kjsembed/qobject_binding.cpp,
	  branches/KDE/4.0/kdelibs/kjsembed/kjsembed/variant_binding.cpp:
	  and backport also r790709: handle array=>variantlist also in the
	  extractVariant() method.

2008-03-27 19:52 +0000 [r790895]  bram

	* branches/KDE/4.0/kdelibs/kate/syntax/data/haskell.xml: Highlight
	  'import'.

