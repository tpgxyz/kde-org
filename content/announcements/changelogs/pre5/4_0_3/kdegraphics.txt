2008-03-04 23:33 +0000 [r782381]  pino

	* branches/KDE/4.0/kdegraphics/okular/part.cpp: Backport: enable
	  again the slider in the "Go to Page" dialog.

2008-03-06 13:35 +0000 [r782915]  tokoe

	* branches/KDE/4.0/kdegraphics/okular/ui/propertiesdialog.cpp:
	  Backport of bugfix #158570

2008-03-10 20:21 +0000 [r784176]  pino

	* branches/KDE/4.0/kdegraphics/okular/ui/tocmodel.cpp: Backport:
	  correctly respect the open state of the TOC branches. CCBUG:
	  159089

2008-03-10 23:28 +0000 [r784343]  lunakl

	* branches/KDE/4.0/kdeedu/kwordquiz/src/kwordquiz.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/kstarsactions.cpp,
	  branches/KDE/4.0/kdeedu/kig/kig/kig_view.cpp,
	  branches/KDE/4.0/kdesdk/kate/app/katemainwindow.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/QtMainWindow.cpp,
	  branches/KDE/4.0/kdegames/kgoldrunner/src/kgoldrunner.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/kopete/chatwindow/chatview.cpp,
	  branches/KDE/4.0/kdegraphics/kolourpaint/mainWindow/kpMainWindow_Settings.cpp,
	  branches/KDE/4.0/kdegraphics/okular/shell/shell.cpp,
	  branches/KDE/4.0/kdegames/ktuberling/toplevel.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/marble_part.cpp,
	  branches/KDE/4.0/kdeedu/kmplot/kmplot/kmplot.cpp: Backport
	  r784333 (remove usage of QWidget::showFullScreen() etc.), use new
	  KToggleFullScreenAction::setFullScreen() helper.

2008-03-12 08:38 +0000 [r784724]  pino

	* branches/KDE/4.0/kdegraphics/okular/generators/poppler/CMakeLists.txt,
	  branches/KDE/4.0/kdegraphics/okular/generators/poppler/generator_pdf.cpp,
	  branches/KDE/4.0/kdegraphics/okular/generators/poppler/config-okular-poppler.h.cmake:
	  change the detection for the "unstable" version as
	  HAVE_POPPLER_0_7 (mostly backport was it's in trunk already)

2008-03-12 08:50 +0000 [r784736]  pino

	* branches/KDE/4.0/kdegraphics/okular/generators/poppler/generator_pdf.cpp:
	  Backport: apply Albert's patch to use the new functions in
	  Poppler-Qt4 for getting the bounding box of the characaters in
	  the correct way (almost). (Unfortunately, this requires the
	  master version from Poppler's GIT repository, to be released
	  hopefully today as 0.8RC1.) CCBUG: 158517

2008-03-12 14:31 +0000 [r784824]  pino

	* branches/KDE/4.0/kdegraphics/okular/generators/djvu/kdjvu.cpp:
	  backport: free the minilisp expression of the outline after the
	  conversion to the dom document

2008-03-13 13:07 +0000 [r785177]  pino

	* branches/KDE/4.0/kdegraphics/okular/ui/tocmodel.cpp: Backport:
	  return the tooltip as well. CCBUG: 159237

2008-03-17 00:26 +0000 [r786446]  gateau

	* branches/KDE/4.0/kdegraphics/gwenview/app/mainwindow.cpp,
	  branches/KDE/4.0/kdegraphics/gwenview/app/mainwindow.h: Double
	  click on a thumbnail now switches to view mode. Backported
	  https://svn.kde.org/home/kde/trunk/KDE/kdegraphics@786443

2008-03-19 17:04 +0000 [r787704]  pino

	* branches/KDE/4.0/kdegraphics/okular/core/generator.cpp: do not
	  crash when a non-active generator asks for the document

2008-03-19 20:12 +0000 [r787749]  pino

	* branches/KDE/4.0/kdegraphics/okular/ui/sidebar.cpp: remove space
	  between the side icon list and its panel

2008-03-23 23:15 +0000 [r789327]  pino

	* branches/KDE/4.0/kdegraphics/okular/ui/annotwindow.cpp: backport:
	  - Albert's r760478 to make the text widget a KTextEdit - my
	  r784138 to get and set the annotation text in a better way

2008-03-23 23:18 +0000 [r789332]  pino

	* branches/KDE/4.0/kdegraphics/okular/ui/annotwindow.cpp: Backport
	  a couple of fixes to the annotation window: - explicitely deny
	  rich text - set the plain text w/o an implicit html conversion
	  CCBUG: 159641

2008-03-24 22:56 +0000 [r789705-789703]  gateau

	* branches/KDE/4.0/kdegraphics/gwenview/app/mainwindow.cpp: Show
	  correct filename in the "file exists, overwrite it" dialog.

	* branches/KDE/4.0/kdegraphics/gwenview/app/savebar.cpp: Fix
	  progress bar range in the "saving all changes" dialog.

	* branches/KDE/4.0/kdegraphics/gwenview/app/main.cpp: Bump version
	  number.

2008-03-27 13:16 +0000 [r790783]  pino

	* branches/KDE/4.0/kdegraphics/okular/core/version.h,
	  branches/KDE/4.0/kdegraphics/okular/VERSION: bump version to
	  0.6.3

