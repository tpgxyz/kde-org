------------------------------------------------------------------------
r1182033 | scripty | 2010-10-03 15:55:55 +1300 (Sun, 03 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1184509 | mzanetti | 2010-10-11 05:05:31 +1300 (Mon, 11 Oct 2010) | 3 lines

Disable Try button if action is not fully configured
Backport of bugfix commit 1184501

------------------------------------------------------------------------
r1185344 | rkcosta | 2010-10-13 13:42:07 +1300 (Wed, 13 Oct 2010) | 5 lines

[libarchive] Remove the temporary file when compression fails.

Backport of r1185340.

CCBUG: 252812
------------------------------------------------------------------------
r1186369 | scripty | 2010-10-16 15:44:27 +1300 (Sat, 16 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1186377 | rkcosta | 2010-10-16 19:06:58 +1300 (Sat, 16 Oct 2010) | 8 lines

Manually call lstat() and pass the stat struct to libarchive.

This is not a direct backport from r1186375 because the code is a bit
different on the 4.5 (we need to call readlink() and
archive_set_symlink() ourselves).

CCBUG: 253059
FIXED-IN: 4.5.3
------------------------------------------------------------------------
r1186842 | rkcosta | 2010-10-18 07:28:02 +1300 (Mon, 18 Oct 2010) | 5 lines

Explicitly include <KGuiItem>

Backport of r1186839.

SVN_SILENT
------------------------------------------------------------------------
r1186843 | rkcosta | 2010-10-18 07:28:07 +1300 (Mon, 18 Oct 2010) | 3 lines

Use KStandardGuiItem's wherever possible.

Backport of r1186840.
------------------------------------------------------------------------
r1188012 | rkcosta | 2010-10-21 15:49:32 +1300 (Thu, 21 Oct 2010) | 13 lines

Backport r118011.

Fix compilation errors on DragonFly BSD.

From the bug report description:

* add DragonFly preprocessor macro ifs where necessary to make superkaramba
work on DragonFly.

Patch by Alex Hornung <alexh AT dragonflybsd DOT org>

CCBUG: 247790

------------------------------------------------------------------------
r1188016 | scripty | 2010-10-21 15:55:32 +1300 (Thu, 21 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
