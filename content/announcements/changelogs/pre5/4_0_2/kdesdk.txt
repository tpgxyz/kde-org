2008-02-05 11:58 +0000 [r771164]  dfaure

	* branches/KDE/4.0/kdesdk/kate/app/katemainwindow.cpp,
	  branches/KDE/4.0/kdesdk/kate/app/katemainwindow.h: Remove a large
	  bunch of useless KAction*-to-KAction* casts. Fix wrong method
	  signature in reimplementation of readProperties() method; might
	  fix some session-management problems.

2008-02-09 15:17 +0000 [r772759]  aacid

	* branches/KDE/4.0/kdesdk/kate/app/katemain.cpp: Backport r772753
	  **************** make kate -u work correctly (that is, still
	  accept other kate -u afterwards) when there's no kate running
	  Ok'ed by alund

2008-02-12 14:13 +0000 [r774109]  dfaure

	* branches/KDE/4.0/kdesdk/scripts/kdesvn-build: backport fix for
	  "Unable to create log directory
	  /d/kde/src/4/log/2008-02-12-01/kdelibs" (on dual-core machines at
	  least)

2008-02-13 15:10 +0000 [r774581]  dfaure

	* branches/KDE/4.0/kdesdk/kioslave/svn/svnhelper/subversion.desktop:
	  fix kbuildsycoca4 warning

2008-02-15 20:38 +0000 [r775438]  dfaure

	* branches/KDE/4.0/kdesdk/scripts/makeobj: don't go up one level if
	  we have a Makefile in the current dir (happens with the "mkdir
	  build; cd build; cmake .." kind of setup)

2008-02-15 21:24 +0000 [r775455]  edulix

	* branches/KDE/4.0/kdesdk/kate/plugins/filebrowser/katefileselector.cpp:
	  bugfix backport from trunk for a small bug in kate: the
	  filesystem broswer will always start in detailed view mode, no
	  matter if you saved session with simple view.

2008-02-18 14:23 +0000 [r776605]  dfaure

	* branches/KDE/4.0/kdesdk/scripts/kde-devel-gdb: gdb's "set print
	  object" is too buggy (needs a null-pointer check...)

2008-02-18 22:02 +0000 [r776862]  dhaumann

	* branches/KDE/4.0/kdesdk/kate/plugins/findinfiles/kategrepdialog.cpp:
	  backport: set correct focus if activating a found item. CCBUG:
	  157522

2008-02-18 22:06 +0000 [r776866]  dhaumann

	* branches/KDE/4.0/kdesdk/kate/plugins/findinfiles/katefindinfiles.cpp:
	  backport SVN commit 773319 by cullmann: findinfiles is now pure
	  qt without any shell access, guess missed to remove this

2008-02-18 22:10 +0000 [r776874]  dhaumann

	* branches/KDE/4.0/kdesdk/kate/plugins/findinfiles/kategrepthread.cpp:
	  only do recursive search if wanted CCBUG: 157521

2008-02-19 13:10 +0000 [r777030]  mlaurent

	* branches/KDE/4.0/kdesdk/kioslave/svn/svnhelper/subversion_toplevel.desktop,
	  branches/KDE/4.0/kdesdk/kioslave/svn/svnhelper/subversion.desktop:
	  Backport: fix entry

2008-02-19 18:37 +0000 [r777093]  mlaurent

	* branches/KDE/4.0/kdesdk/kioslave/svn/svnhelper/subversion.desktop:
	  Backport: export feature is not implemented => disable for the
	  moment

2008-02-19 18:42 +0000 [r777095]  mlaurent

	* branches/KDE/4.0/kdesdk/kioslave/svn/svnhelper/subversion.desktop:
	  Backport: Import is not implemented

2008-02-26 23:04 +0000 [r779770]  woebbe

	* branches/KDE/4.0/kdesdk/cervisia/logplainview.cpp: backport rev.
	  779768: workaround Qt bug 166111 (<hr> underlines wrong lines)

2008-02-27 14:53 +0000 [r779948-779947]  woebbe

	* branches/KDE/4.0/kdesdk/cervisia/logplainview.cpp: obviously Qt
	  4.3 works differently

	* branches/KDE/4.0/kdesdk/cervisia/version.h: bumps version number

