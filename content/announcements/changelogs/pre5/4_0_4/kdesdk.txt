2008-03-28 16:12 +0000 [r791171]  woebbe

	* branches/KDE/4.0/kdesdk/cervisia/annotatecontroller.cpp: fix
	  regression: KDateTime can't parse dates only In HEAD I avoided
	  K3RFCDate, as I didn't want to add another dependency on
	  kde3support

2008-04-12 13:54 +0000 [r796082]  woebbe

	* branches/KDE/4.0/kdesdk/cervisia/version.h: bump version number

