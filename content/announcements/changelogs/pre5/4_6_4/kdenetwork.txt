------------------------------------------------------------------------
r1230205 | mfuchs | 2011-05-03 01:38:51 +1200 (Tue, 03 May 2011) | 2 lines

Backport r1230204
ChecksumSearchController also starts a list job for sftp.
------------------------------------------------------------------------
r1230361 | mfuchs | 2011-05-04 22:48:21 +1200 (Wed, 04 May 2011) | 2 lines

Backport r1228944
Cleans up the NepomukHandler and also does not use originURL since that would be redundant (with copiedFrom):
------------------------------------------------------------------------
r1230362 | mfuchs | 2011-05-04 22:48:24 +1200 (Wed, 04 May 2011) | 2 lines

Backport r1230357
Improves Nepomuk handling by adding more properties and by using the correct ontologies.
------------------------------------------------------------------------
r1230379 | lappelhans | 2011-05-05 00:39:32 +1200 (Thu, 05 May 2011) | 3 lines

Backport of rev1228879:
This should fix our recent version problems again

------------------------------------------------------------------------
r1231311 | aacid | 2011-05-11 09:01:41 +1200 (Wed, 11 May 2011) | 4 lines

Convert these extracomment to comment because:
 * We had a bug in our scripts that makes extracomment not work (will try to fix this asap)
 * Makes sense to be a comment since we want it to be a disambiguation text

------------------------------------------------------------------------
r1231749 | mfuchs | 2011-05-14 09:19:19 +1200 (Sat, 14 May 2011) | 4 lines

Workaround: KGet runner does not use the KGet interface anymore. That way a crash in upstream is worked around.
TODO: Reenable once the bug has been fixed upstream.

CCBUG:259873
------------------------------------------------------------------------
r1233517 | poboiko | 2011-05-26 03:21:08 +1200 (Thu, 26 May 2011) | 6 lines

Backporting commit 1233516.
Fix crash when user is trying to set contacts language from menubar and no contacts are selected.

CCBUG: 273070
CCBUG: 270797

------------------------------------------------------------------------
r1234347 | scripty | 2011-06-01 04:18:46 +1200 (Wed, 01 Jun 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
