---
aliases:
- ../../plasma-5.20.0
date: 2020-10-13
description: Plasma 5.20, by all accounts a massive release, is here. Plasma 5.20
  comes with tons of new features and improvements which will make your Plasma desktop
  experience easier, smoother and more fun.
layout: plasma-5-20
scssFiles:
- /scss/plasma-5-20.scss
title: 'Plasma 5.20: One absolutely massive release'
---

{{< container >}}

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/5.20/Video.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/5.20/Video.mp4" poster="https://cdn.kde.org/promo/Announcements/Plasma/5.20/Thumbnail.png" >}}

Everyday utilities and tools, such as the Panels, Task Manager, Notifications and System Settings, have all been overhauled to make them more usable, efficient, and friendlier.

Meanwhile, developers are hard at work adapting Plasma and all its bits and pieces to Wayland. Once done, Plasma will not only be readier for the future, but will also work better with touchscreens and multiple screens with different refresh rates and DPIs. Plasma will also offer better support for hardware-accelerated graphics, be more secure, and enjoy many more advantages. Although still work in progress, 5.20 already offers users many of the benefits of Plasma on Wayland.

Read on to find out more about the new features and improvements included in Plasma 5.20...

{{< /container >}}

{{< plasma-5-20-wallpaper alt="Plasma new wallpaper" title="Look and feel" description="Some of the changes will be obvious the moment Plasma desktop and its beautiful new wallpaper (<I>Shell</I>, by the talented Lucas Andrade) loads onto your screen." >}}

{{< container >}}

{{< feature src="task-manager.png" alt="The icon only Task Manager" >}}

### Task Manager

The Task Manager, for example, has not only changed its looks (it is now icon-only by default) but also how it behaves: when you open several windows with the same application (like when you open several LibreOffice documents), the Task Manager will group them together. Clicking on grouped windows will cycle through them, bringing each to the forefront until you reach the document you want. You can also decide not to minimize the active task when you click on it in the Task Manager. As with most things in Plasma, these behaviors are configurable and you can keep them or ditch them; it's up to you.

{{< /feature >}}

{{< feature src="tray.png" alt="System tray grid" >}}

### System Tray

Other changes are not so immediately evident, like the System Tray popup now shows items in a grid rather than a list, and the icon view on your panel can now be configured to scale the icons along with the panel thickness.

Talking of scaling, the Web Browser widget also lets you zoom in and out from its contents by holding down the [Ctrl] key and rolling the mouse wheel. The Digital Clock widget has changed too and is now more compact and shows the current date by default. Throughout KDE apps in general, each toolbar button which displays a menu when clicked now shows a downward-pointing arrow to indicate this.

{{< /feature >}}

{{< feature src="osd.png" alt="On-screen displays" >}}

### On-screen displays

On-screen displays that appear when changing the volume or display brightness (for example) have been redesigned to be less obtrusive. When using the 'raise maximum volume' setting, the volume on-screen display will subtly warn you when you go over 100% volume. Plasma helps you protect your eardrums. And finally, when you change the display brightness, it now transitions smoothly.

{{< /feature >}}

{{< /container >}}

### KWin

Then there are the changes that are not visual at all, but that affect usability and how you interact with your Plasma environment. For example, in Plasma versions prior to 5.20, you could hover the cursor over the window, hold down the [Alt] key, and then drag with the mouse to move the window. Using the [Alt] key for this conflicted with many popular productivity applications that used the same gesture for different functions, so the default shortcut for moving and resizing is now to hold down the [Meta] ("Windows") key and drag instead.

In a similar vein, you can push windows into a section of the screen so they take up half or a quarter of the available space (this is called "tiling") using a keyboard shortcut. To tile a window to the top half of the screen, for example, hold down the [Meta] key and tap the up arrow key. If you want to tile a window into the upper left corner, hold down the [Meta] key and then tap the up and left arrow keys in quick succession; and so on and so forth.

### Notifications

Then there are a lot of changes to the notification system. For one, you now get notified when your system is about to run out of space on your disk even if your home directory is on a different partition -- very important if you want to avoid sticky situations where you can't even save changes to a document because you have no more room on your hard drive.

Speaking of disks and notifications, the <I>Device Notifier</I> applet has been renamed to <I>Disks & Devices</I> and it is now easier to use, as you can now show all disks, not just the removable ones. Keeping things tidy, unused audio devices are filtered out of the audio applet and System Settings page by default. As for keeping things safe, you can configure a charge limit below 100% to preserve the battery health on supported laptops. If what you need is peace and quiet, Plasma 5.20 lets you enter the <I>Do Not Disturb</I> mode by simply middle-clicking on the Notifications applet or system tray icon.

{{< img src="disk-tray.png" alt="Show all disks" caption="Show all disks" >}}

Moving on to KRunner, KDE's application launcher, search-and-conversion utility, and general all-round useful thing, it now remembers the prior search text and you can choose to have it centered on the screen, rather than attached to the top. KRunner can now also search and open pages in <a href='https://www.falkon.org/'>Falkon</a>, KDE's advanced web browser.

And that's not all. Look out for the dozens of more subtle improvements sprinkled throughout Plasma 5.20 that make your experience with KDE's desktop sleeker and more enjoyable.

## System Settings & Info Center

System Settings is the place where you go to configure most aspects of your system, and it too gets its fair share of improvements in Plasma 5.20.

One of the most useful new features is that you can always know what you changed in your configurations with the <I>Highlight Changed Settings</I> feature: click the button in the bottom left corner of the window and Settings will show the things that have been modified from their default values.

{{< img src="systemsetting-change.png" alt="Show all disks" caption="Keep tabs on what you changed" >}}


Another things that will make your life easier is that the <I>Standard Shortcuts</I> and <I>Global Shortcuts</I> pages have been combined into one, simply called <I>Shortcuts</I>. Additionally, the <I>Autostart</I>, <I>Bluetooth</I> and <I>User Manager</I> pages have been rewritten from scratch and redesigned according to modern user interface standards.

S.M.A.R.T is a technology included in many hard disks, SSDs and eMMC drives. It can help predict the failure of a storage device before it happens. Install <a href="https://invent.kde.org/plasma/plasma-disks/">Plasma Disks</a> from <a href="https://kde.org/applications/en/discover">Discover</a> and System Settings in Plasma 5.20 will be able to monitor S.M.A.R.T notifications and inform you of the state of your disks, allowing you to save your data before something catastrophic happens.

Other new stuff includes an audio balance option that lets you adjust the volume of each audio channel, and tools to adjust the touchpad cursor speed more to your liking.

{{< boxes >}}

{{< box src="bluetooth.png" alt="New Bluetooth KCM" >}}

{{< box src="autostart.png" alt="New Autostart KCM" >}}

{{< box src="kcm_user.png" alt="New User KCM" >}}

{{< box src="plasma-disks.png" alt="Plasma S.M.A.R.T monitor" >}}

{{< /boxes >}}

{{< diagonal-box color="purple" logo="/images/wayland.png" alt="Wayland logo" >}}

## Wayland

Back in 2019, the KDE Community decided to set an official goal to prioritize adapting Plasma and its applications to Wayland. The effort is starting to pay off big time as more and more features and utilities make their way to the new display server protocol. For example, the Klipper clipboard utility and middle-click paste are now fully operational on Wayland, and the multi-purpose launcher-calculator-searcher KRunner now shows up in the correct place when you use a top panel.

Mouse and touchpad support are nearly on par with their X counterparts and screencasting is also supported in Plasma 5.20. The Task Manager shows window thumbnails and in general the whole desktop is more stable and no longer crashes even if XWayland does.

These are just the highlights of Plasma 5.20. There is so much more! If you want a full list of everything that has gone into Plasma 5.20, check out <a href="/announcements/changelogs/plasma/5/5.19.5-5.20.0">the full changelog</a> and enjoy your new (and improved) desktop!

{{< /diagonal-box >}}
