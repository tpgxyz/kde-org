---
aliases:
- ../../plasma-5.14.2
changelog: 5.14.1-5.14.2
date: 2018-10-23
layout: plasma
figure:
  src: /announcements/plasma/5/5.14.0/plasma-5.14.png
asBugfix: true
---

- Fix plasmashell freeze when trying to get free space info from mounted remote filesystem after losing connection to it. <a href="https://commits.kde.org/plasma-workspace/be3b80e78017cc6668f9227529ad429150c27faa">Commit.</a> Fixes bug <a href="https://bugs.kde.org/397537">#397537</a>. Phabricator Code review <a href="https://phabricator.kde.org/D14895">D14895</a>. See bug <a href="https://bugs.kde.org/399945">#399945</a>
- Add accessibility information to desktop icons. <a href="https://commits.kde.org/plasma-desktop/498c42fed65df76ca457955bab18a252d63ca409">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D16309">D16309</a>
- Fix bookmarks runner with Firefox 58 and newer version. <a href="https://commits.kde.org/plasma-workspace/99fa6ccc57c5038ffb16d2e999893d55dc91f5b1">Commit.</a> Fixes bug <a href="https://bugs.kde.org/398305">#398305</a>. Phabricator Code review <a href="https://phabricator.kde.org/D15305">D15305</a>
