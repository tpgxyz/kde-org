---
aliases:
- ../../plasma-5.12.5
changelog: 5.12.4-5.12.5
date: 2018-05-01
layout: plasma
youtube: xha6DJ_v1E4
figure:
  src: /announcements/plasma/5/5.12.0/plasma-5.12.png
  class: text-center mt-4
asBugfix: true
---

- Discover: improve display of Progress element. <a href="https://commits.kde.org/discover/ff6f7b86a94c25b0a322dd5ad1d86709d54a9cd5">Commit.</a> Fixes bug <a href="https://bugs.kde.org/393323">#393323</a>
- Weather dataengine: fix BBC provider to adapt to change RSS feed. <a href="https://commits.kde.org/plasma-workspace/558a29efc4c9f055799d23ee6c75464e24489e5a">Commit.</a> Fixes bug <a href="https://bugs.kde.org/392510">#392510</a>. Phabricator Code review <a href="https://phabricator.kde.org/D11808">D11808</a>
- Fixed bug that caused power management system to not work on a fresh install. <a href="https://commits.kde.org/powerdevil/be91abe7fc8cc731b57bec4cf2c004c07b0fd79b">Commit.</a> Fixes bug <a href="https://bugs.kde.org/391782">#391782</a>
