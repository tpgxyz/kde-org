---
date: 2022-03-29
changelog: 5.24.3-5.24.4
layout: plasma
video: false
asBugfix: true
draft: false
---

+ Sddm/lockscreen: Fix weird behaviour. [Commit.](http://commits.kde.org/plasma-workspace/f4bf8a278df58f14643613539d4f64bf7418f694)
+ Plymouth KControl: Substantially bump the helper timeout. [Commit.](http://commits.kde.org/plymouth-kcm/df2cc926359f986bf30ec334ee2e2aba4406fd47) Fixes bug [#400641](https://bugs.kde.org/400641)
+ Systemsettings runner: Ensure that we match keywords case insensitively. [Commit.](http://commits.kde.org/systemsettings/99908ccb3a8c1d0ffb6ccf6eca008feed6a42749) Fixes bug [#451634](https://bugs.kde.org/451634)
