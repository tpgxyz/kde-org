---
aliases:
- ./20.90
date: '2021-01-21T15:00:00Z'
desc: Plasma 5.21 is all about upgrading the looks and usability of Plasma.
description: Plasma 5.21 is a looker! The new wallpaper and new app launcher make
  Plasma not only easier on the eyes, but also easier to use and much more accessible.
features:
- content: Support unattended updates
  logo: https://apps.kde.org/app-icons/org.kde.discover.svg
  title: Discover
- content: Pin KRunner (doesn't close automatically)
  logo: /breeze-icons/krunner.svg
  title: KRunner
- content: Better support for timezones
  logo: /breeze-icons/kalarm.svg
  title: Digital Clock
- content: The sound applet now displays the live microphone volume
  logo: /breeze-icons/sound.svg
  title: Sound Applet
images:
- /announcements/plasma/5/5.20.90/1.jpg
layout: plasma-5-21
scssFiles:
- /scss/plasma-5-21.scss
subtitle: We did a thing. A pretty one
title: Plasma 5.21 Beta
---

{{< container >}}

## Plasma 5.21 Beta

Development of the upcoming version of Plasma 5.21 has, among other
things, introduced many improvements into Plasma's design, utilities and themes,
with the aim of providing end users with a more pleasant and accessible environment.

Although most things in Plasma 5.21 beta will work fine, please note it is still
beta software and released mainly for testing purposes. Using this software in
production is NOT recommended.

The final version of Plasma 5.21 will be available on the 16th of February.

{{< feature src="kickoff.png" class="round-top-right" >}}

## New Application Launcher

Plasma 5.21 introduces a new application launcher that features a double-pane UI,
improvements to keyboard and mouse navigation, better accessibility and
support for languages with RTL writing.

The new launcher includes an alphabetical “All Applications” view, a grid-style
favorites view, and power actions visible by default with their labels.

{{< todo >}}

Find, reach and run your apps faster and easier than ever with Plasma's
new app launcher.

The new launcher features two panes to make it simple to locate your programs,
comes with improved keyboard, mouse, and touch input, and boosts the
accessibility across the board. Support for languages with right-to-left
writing has also been improved. We have also included an alphabetical "All
Applications" view, a grid view for your favorite tools, and all the power actions ("Sleep",
"Restart", "Shut Down", etc.) are available and visible.

{{< /todo >}}

Last but not least, we have [fixed most of the bugs reported by users](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/258), guaranteeing a smoother access to all your stuff.

The old Kickoff app launcher is still available at [store.kde.org](https://store.kde.org).

{{< /feature >}}

{{< feature src="header1.png" class="round" >}}

## Application theme improvements

Applications using Plasma's default theme now have a refreshed color scheme
and sport a brand new unified headerbar style with a clean, cool new look.

{{< /feature >}}

{{< /container >}}

{{< twilight class="text-center " >}}

## Breeze Twilight

{{< laptop src="breeze-twilight.jpg" class="mb-4" caption="Screenshot showing Breeze Twilight with a dark panel and light applications" >}}

Meet Breeze Twilight: a combination of a dark theme for Plasma and a light theme
for applications, so you can enjoy the best of both worlds. It's available in the
Global Theme settings

{{< /twilight >}}

{{< container class="monitor" >}}

![System monitor logo](https://apps.kde.org/app-icons/org.kde.plasma-systemmonitor.svg)

## Plasma System Monitor

Plasma System Monitor is a new UI for monitoring system resources. It is built
on top of Kirigami and a system statistics service called "KSystemStats". It
shares code with the new system monitor applets introduced in Plasma 5.19 and is
designed to succeed KSysGuard.

{{< todo >}}

Plasma System Monitor is a brand new app for monitoring system resources
and is now an integral part of Plasma.

{{< /todo >}}

Plasma System Monitor provides many different views, offering an
overview page that provides information on important core resources, like memory,
disk space, network and CPU usage. It also provides a quick view of the applications
consuming the most resources.

If you need more details, the Applications page shows you all the running
applications along with detailed statistics and graphs. A process page is
also available for per process information.

History shows the evolution of the use of your machine's resources over time.

Finally, you can also create new customized pages using the page editor. This
lets you tailor the information you get from your system to your needs.

{{< /container >}}

{{< boxes >}}

{{< box src="systemmonitor.png" alt="Overview page" >}}

{{< box src="systemmonitor2.png" alt="History page" >}}

{{< box src="systemmonitor3.png" alt="Application resource usage overview" >}}

{{< box src="systemmonitor4.png" alt="New page editor" >}}

{{< /boxes >}}

{{< diagonal-box color="purple" logo="/images/wayland.png" alt="Wayland logo" >}}

## KWin and Wayland 

KDE is pushing to have first class support for Wayland, and Plasma 5.21 makes
great headway to reach that goal.

The compositing code in KWin has been extensively refactored and should
reduce latency throughout all compositing operations. We have also added
a control in the compositing settings so you can choose whether you
prefer lower latency or smoother animations.

In addition, we have also added support for mixed-refresh-rate display setups
on Wayland, e.g. you can have one screen refreshing at 144Hz and another at
60Hz. Preliminary support for multiple GPUs was also added on Wayland.

The virtual keyboard in Wayland has been improved and now supports GTK
applications using the `text-input-v3` protocol. The support for graphical tablets has also been improved and
now includes all the controls that were missing in the previous version, such
as pad ring and pad buttons.

Apart from the numerous improvements in stability, there are quite a few Plasma
components that are getting much better support in Wayland. For example, KRunner
is now able to list all open windows in Wayland, and we now support features required
for GTK4, so GTK4 application will now work.

{{< /diagonal-box >}}

{{< container class="text-center monitor" >}}

![System Settings](https://apps.kde.org/app-icons/org.kde.plasma-systemmonitor.svg)

## System Settings

Plasma 5.21 brings a new page to the System Settings: the Plasma Firewall settings.
This configuration module lets you set up and edit a Firewall for your system and
is a graphical frontend for both UFW and firewalld.

Multiple pre-existing configuration pages have been completely rewritten and are now cleaner and easier
to use. This has been the case for the Accessibility, Desktop Session and SDDM configuration
modules.

{{< /container >}}

{{< boxes >}}

{{< box src="kcm_access.png" alt="New Accessibility KCM" >}}

{{< box src="kcmserver.png" alt="Desktop Session KCM" >}}

{{< box src="sddm.png" alt="New SDDM KCM" >}}

{{< box src="firewall.png" alt="New Firewall KCM" >}}

{{< /boxes >}}

{{< diagonal-box color="blue" class="text-center applets" >}}

## Applets

The Media Player applet's layout has been improved and now includes the
list of applications currently playing music in the header as a tab bar.
Another upgrade is that the album cover now takes up the whole
width of the applet.

![Media Player Applets](media.png)

{{< /diagonal-box >}}

{{< container class="text-center monitor" >}}

![Plasma Mobile](https://www.plasma-mobile.org/img/logo.svg)

## Plasma Mobile

Plasma has always been designed to be form factor flexible.
It can work on a desktop but it's also easily adaptable to work on a mobile.
[The PinePhone KDE Community Edition](https://kde.org/announcements/plasma-mobile/pinephone-plasma-mobile-edition/)
is now shipping. In Plasma 5.21 we are adding two new components for mobile
in the official release.

* The Plasma Phone Components contains the mobile shell but also specific
plasma widgets adapted for Plasma Mobile.

* QQC2 Breeze Style is a pure Qt Quick Controls 2 style. It visually fits
in with the widget based desktop Breeze theme and was optimized for lower
RAM and GPU usage.

{{< /container >}}

{{< feature-grid title="Other Updates" >}}

{{< container >}}

A full changelog is available [here](/announcements/changelogs/plasma/5/5.20.5-5.20.90).

{{< /container >}}
