---
aliases:
- ../../plasma-5.13.2
changelog: 5.13.1-5.13.2
date: 2018-06-26
layout: plasma
youtube: C2kR1_n_d-g
figure:
  src: /announcements/plasma/5/5.13.0/plasma-5.13.png
  class: text-center mt-4
asBugfix: true
---

- Fix tooltip woes. <a href="https://commits.kde.org/plasma-desktop/1e218b405bee4d75a5d26a4c28da2724d967953a">Commit.</a> Fixes bug <a href="https://bugs.kde.org/382571">#382571</a>. Fixes bug <a href="https://bugs.kde.org/385947">#385947</a>. Fixes bug <a href="https://bugs.kde.org/389469">#389469</a>. Fixes bug <a href="https://bugs.kde.org/388749">#388749</a>. Phabricator Code review <a href="https://phabricator.kde.org/D13602">D13602</a>
- Revert Touchpad KDED module: Convert to JSON metadata. <a href="https://commits.kde.org/plasma-desktop/3432c3342b1f8014cf59f39565aca53d280c7f86">Commit.</a> Fixes bug <a href="https://bugs.kde.org/395622">#395622</a>
- Discover: include the donation URL from KNS. <a href="https://commits.kde.org/discover/edeacba8db7a56bc4d0f053f10febffebcb3f683">Commit.</a>
