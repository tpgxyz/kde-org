---
aliases:
- ../../plasma-5.20.3
changelog: 5.20.2-5.20.3
date: 2020-11-10
layout: plasma
video: true
asBugfix: true
---

+ Plasma Disks: Actually erase devices. [Commit.](http://commits.kde.org/plasma-disks/916f0081b2334759b3d92ba2ca5c042d2df85535) Fixes bug [#428746](https://bugs.kde.org/428746)
+ Plasma Network Management: Do not show absurdedly high speeds on first update. [Commit.](http://commits.kde.org/plasma-nm/08efc47603e59ae702e4cf181eb26374d0af97af) 
+ Fix missing "Switch User" button on lockscreen with systemd 246. [Commit.](http://commits.kde.org/plasma-workspace/aaa7a59a6710a89f21ebd441616df13be5ba8fef) Fixes bug [#427777](https://bugs.kde.org/427777)
