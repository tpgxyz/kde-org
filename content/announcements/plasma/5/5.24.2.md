---
date: 2022-02-22
changelog: 5.24.1-5.24.2
layout: plasma
video: false
asBugfix: true
draft: false
---

+ Plasma Desktop Hot New Stuff: Only trust the expiration date if it's less than 24 hours. [Commit.](http://commits.kde.org/plasma-desktop/1842af5da5d2f73415c94bdfb3de0cc5a89fa6c2)
+ Emoji Selector: Use a more appropriate icon for the Symbols page. [Commit.](http://commits.kde.org/plasma-desktop/e903d9e291246428bdab93c83b58ab4b0624a999) Fixes bug [#450380](https://bugs.kde.org/450380)
+ Don't install two copies of kcm_fontinst. [Commit.](http://commits.kde.org/plasma-workspace/5cea29ed6ac0cb789520421325bb547bd2e8b614)
