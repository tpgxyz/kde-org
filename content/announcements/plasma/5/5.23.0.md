---
aliases:
  - ../../plasma-5.23.0
title: "Plasma - 25th Anniversary Edition"
subtitle: "25 Years of Awesomness"
description: "Plasma - 25th Anniversary Edition brings new looks, features and enhancements to the classic free desktop."
date: "2021-10-14"
images:
 - /announcements/plasma/5/5.23.0/plasma.png
layout: plasma-5.23
scssFiles:
  - /scss/plasma-5-23.scss
authors:
  - SPDX-FileCopyrightText: 2021 Nate Graham <nate@kde.org>
  - SPDX-FileCopyrightText: 2021 Paul Brown <paul.brown@kde.org>
  - SPDX-FileCopyrightText: 2021 Aniqa Khokhar <aniqa.khokhar@kde.org>
  - SPDX-FileCopyrightText: 2021 Björn Feber <bfeber@protonmail.com>
  - SPDX-FileCopyrightText: 2021 Guilherme Marçal Silva <guimarcalsilva@gmail.com>
SPDX-License-Identifier: CC-BY-4.0
---

{{< container >}}

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/5.23/Plasma_25AE_Final.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/5.23/Plasma_25AE_Final.mp4" poster="https://kde.org/announcements/plasma/5/5.23.0/Plasma25AE_b.png" >}}

[25 years](https://25years.kde.org) ago today, Matthias Ettrich [sent an email](https://groups.google.com/g/de.comp.os.linux.misc/c/SDbiV3Iat_s/m/zv_D_2ctS8sJ) to the de.comp.os.linux.misc newsgroup explaining a project he was working on. The latest and direct result of that email (plus a quarter of a century of relentless experimentation, development and innovation) has just landed in KDE's repositories.

This time around, Plasma renews its looks and, not only do you get a new wallpaper, but also a gust of fresh air from an updated theme: _Breeze - Blue Ocean_. The new Breeze theme makes KDE apps and tools not only more attractive, but also easier to use both on the desktop and your phone and tablet. 

Of course, looks are not the only you can expect from Plasma 25AE: extra speed, increased reliability and new features have also found their way into the app launcher, the software manager, the Wayland implementation, and most other Plasma tools and utilities.

Read on to find out all the details that make the new Plasma 25AE so deserving of a celebration:

{{< /container >}}

{{< feature src="breeze-application-style.png" >}}

## Looks

Apart from the new wallpaper, the first thing you will notice is the new theme. _Breeze — Blue Ocean_ improves the look of Plasma and also makes clearer what they are for. Active elements in a dialog window, for example, "light up" when the window gets focus, checkboxes show actual ticks and radio buttons switch on like bulbs. Scrollbars and spinboxes are bigger, making them more accessible and easier to use with touchscreens, but have been re-designed in such a way as to still look elegant on desktop and laptop machines.

Among other things, a few more details serve to add glossiness and style to the desktop. For example, when starting an app or waiting for a process to complete, you will see a spinning gear until the task finishes; and desktop widgets now have a blurred background and get a  highlight effect when you move them close to the panel.

Subtle details like these, not only enhance the look of the desktop, but also give you nearly subliminal feedback, hinting at what is active and what is not and how the different elements on the Plasma desktop will interact with you and each other.

{{< /feature >}}

{{< diagonal-box color="purple" logo="/breeze-icons/systemsettings.svg" class="system-settings" alt="System Settings" >}}

## System Settings

... And, as long as we are talking about Plasma's new look, we must talk about the new stuff in _System Settings_. Under _Global Theme_ > _Colors_ you'll find a new option that lets you pick the desktops _accent colors_. _Accent colors_ are the colors of highlighted items on drop-down menus and lists, backgrounds of the icons of selected apps in the panel, the actual bars in progress bars, the backgrounds of checkboxes and radio buttons, and so on.

{{< figure src="system-settings-accent-colour.png" alt="override a color scheme's accent color" >}}

Now say that, instead of the cool blue of Plasma's default color scheme, you are feeling fabulous and want a hot pink for all those things: that is actually the first choice in the list. Or maybe you want like a certain chameleon-based distro and an intense green is your thing? You can do that too. To make sure your colors match those of the overall theme, just click on the _From current color scheme_ and things will go back to what they were.

Talking of walking stuff back, changing the monitor resolution can lead to an unusable desktop. That's why now you get a countdown: under _System Settings_ > _Display and Monitor_ > _Display Configuration_ , once you pick a resolution and press the _Apply_ button, a dialog window with the countdown will pop up. If after 30 seconds you haven't clicked the _Keep_ button (say, because your desktop has become inaccessible) the system will revert to the original, safe resolution. And this is just one of the ways of protecting you against accidental configuration mishaps.

Because keeping your system operational and safe is one of our main concerns. That is why we always advise you when you are going to share data you may not be aware of. Take, for example, _Night Color_: one of the things you can do is use geolocation to synchronize the color temperature of your screen with the periods of day and night of where you live. The _Night Color_ page in your _System Settings_ will tell you when sharing your location with a third party's service that provides the times of sunrise and sundown in your area, and will also tell you who said the third party is. Likewise, the _Feedback_ page now will show you a history of data that you have chosen to send to KDE. Note that _Feedback_ will always be set to _off_ by default. You have to explicitly and consciously switch it on before any data leaves your machine.

Another thing you will want to keep an eye on is your Bluetooth adapter. That is why you can set its status on login. Your options are _Enable Bluetooth_, _Disable Bluetooth_ and _Remember previous status_.

If finding all the options you need seems daunting, don't fear! We have made things easier by adding many keywords to improve searches. Just type in what you are looking for in the _Search_ box and _System Settings_ will provide you with all the related options.

{{< /diagonal-box >}}

## Kickoff

{{< feature src="kickoff.png" >}}

Of course, once you are done configuring, you will actually want to reach your apps. You can do that through _Kickoff_, the application launcher that, in a default Plasma layout, lives in the bottom left-hand corner of your screen. Kickoff was already given a massive overhaul a few versions ago, but again we have rewritten large parts of the code to make it faster and easier to use.

And we added new options, of course. For example, now you can make Kickoff stay open on the screen with the new pin button located in the upper right-hand corner. By clicking on the _Configure Application Launcher_ button to the left of the pin, you can choose whether to use a list or a grid for the apps views and configure the power/session action buttons.

We have also made Kickoff more touch friendly and, if you press-and-hold with a finger on the touchscreen, Kickoff's context menu will pop open.

{{< /feature >}}

{{< container >}}

## System Tray

{{< figure src="plasma-nm.png" alt="details about the currently-connected network" style="float: right" >}}

You will find the _System Tray_, on a default Plasma desktop layout, on the right-hand side of the bottom panel bar. The _System Tray_ is important because it contains things you would typically want to keep an eye on, like notifications, the sound volume, the network status, etc. As the _System Tray_ is a vital and (literally) iconic part of the Plasma desktop for many users, care has been taken to improve its usability and that of the widgets it contains.

One widget you can access is the _Clipboard_, the place stuff you copy goes and where applications get the things when you paste them somewhere else. The _Clipboard_ has been improved in several ways, for example, it now remembers 20 items by default and ignores selections that you did not explicitly copy. This gives you more to pick from when copying, while at the same time decluttering the list by removing snippets you may have accidentally or absentmindedly highlighted while working with Plasma. To further help you keep things tidy, you can also remove selected items from the clipboard's popup by pressing the [Delete] key on the keyboard. Talking of copying and pasting, you can now easily copy text from notifications with the <kbd>Ctrl</kbd> + <kbd>C</kbd> keyboard shortcut.

{{< figure src="media-player-applet.png" alt="Media Player Applet" style="float: right" >}}

A new thing in the _Audio Volume_ applet is that it now distinguishes between applications that are currently playing or recording audio and the _Media Player_ widget always displays the album art and its blurred background at the same time of any song that is playing.  We also added an easy way to switch power profiles from the _Battery and Brightness_ widget. You can choose between "power-saver", "balanced" and "performance". 

As for the network widget, Plasma now shows more details about the currently-connected network, and you can fine-tune the manual speed setting for wired Ethernet connections. The applet also supports additional authentication settings/protocols/requirements for OpenVPN connections and allows you to disable IPv6.

{{< /container >}}

{{< diagonal-box color="purple" logo="/images/wayland.png" alt="Wayland logo" >}}

## Wayland

Making Plasma fully functional under Wayland is a priority for the KDE Community. Wayland is a protocol that will allow Plasma developers to do much more for the desktop, improving performance, increasing stability and implementing features, such as those required by devices with touchscreens.

In this release, the cursor now shows animated icon feedback when launching apps and we added a new screen rotation animation for mobile devices, such as phones, tablets and hybrid laptop/tablet computers. On a related note, entering tablet mode in Wayland increases the size of _System Tray_ icons, making them easier to tap on a touchscreen. The System Tray will also notify you when something is recording the screen and will let you cancel it.

While we reach full Wayland support for the desktop and applications, we also support the XWayland intermediate protocol and, in Plasma 25th AE, you can now use a middle-click to paste, and drag-and-drop stuff between native Wayland and XWayland apps.

{{< /diagonal-box >}}

{{< container >}}

{{< feature-grid title="Other Updates" >}}

- Discover, Plasma's app center, now loads faster and is more transparent, as it tells you where you are installing your programs from directly in the install button, without you needing to read the full description.
- If you have more than one monitor, you are in luck, as multi-screen layouts are now retained across X11 and Wayland sessions.
- If something does go wrong, DrKonqi, the application that lets you easily send us feedback, now warns you when an app is unmaintained.
- In an effort to further declutter windows from unnecessary buttons,  the question mark button in the titlebar is hidden by default on dialogs and _System Settings_.
- While still on the topic of removing unhelpful components, passwordless accounts without autologin now show a simple login button and no text field for a password, because why would they?
- And, as always, we have improved the overall stability by hunting down even the most elusive bugs that could crash or hang your system.

... And there's much more going on. If you would like to see the full list of changes, [check out the changelog for Plasma - 25th Anniversary Edition](/announcements/changelogs/plasma/5/5.22.90-5.23.0).

{{< /container >}}
