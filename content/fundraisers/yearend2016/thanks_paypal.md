---
title: Donation received - Thank you!
---

Thank you very much for your donation to the Year End 2016 fundraiser!

In case your donation qualifies for a greeting card gift we will contact you at the beginning of December at your PayPal the design you want and address you want to send them to.

Remember you can become a "KDE Supporting Member" by doing recurring donations. Learn more at https://relate.kde.org.

You can see your donation on <a href="..">the Year End 2016 fundraiser page</a>.
