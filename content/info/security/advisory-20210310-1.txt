KDE Project Security Advisory
=============================

Title:           Discover: Missing URI scheme validation
Risk Rating:     Low
CVE:             CVE-2021-28117
Versions:        Discover >= 5.15.0 <= 5.21.3
Author:          Aleix Pol Gonzalez <aleixpol@kde.org>
Date:            10 March 2021

Overview
========

Discover fetches the description and related texts of some applications/plugins from store.kde.org. That text is displayed to the user, after turning into a clickable link any
part of the text that looks like a link. This is done for any kind of link, be it smb:// nfs:// etc. when in fact it only makes sense for http/https links.

Impact
======

Opening links that the user has clicked on is not very problematic but can be used to chain to other attack vectors. Given the intended functionality of the feature is just for http/https links it makes sense to do that verification.

Workaround
==========

Only click on http/https links in Discover.

Solution
========

Install Plasma 5.21.3, 5.18.7 or apply these patches
Plasma 5.21: https://commits.kde.org/plasma/discover/94478827aab63d2e2321f0ca9ec5553718798e60
Plasma 5.18: https://commits.kde.org/plasma/discover/fcd3b30552bf03a384b1a16f9bb8db029c111356


Credits
=======

Thanks to Fabian Bräunlein for reporting the issue.
