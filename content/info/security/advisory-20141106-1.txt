KDE Project Security Advisory
=============================

Title:          kde-workspace, plasma-desktop: privilege escalation
Risk Rating:    Medium
CVE:            CVE-2014-8651
Platforms:      All
Versions:       kde-workspace < 4.11.14, plasma-desktop < 5.1.1
Author:         David Edmundson davidedmundson@kde.org
Date:           06 November 2014

Overview
========

KDE workspace configuration module for setting the date and time has a helper program
which runs as root for performing actions. This is secured with polkit.

This helper takes the name of the ntp utility to run as an argument. This allows a hacker
to run any arbitrary command as root under the guise of updating the time.

Impact
======

An application can gain root priveledges from an admin user with either misleading information
or no interaction.

On some systems the user will be shown a prompt to change the time. However, if the system has
policykit-desktop-privileges installed, the datetime helper will be invoked by an admin user
without any prompts.

Workaround
==========

Add a polkit rule to disable the org.kde.kcontrol.kcmclock.save action.

Solution
========

For kde-workspace 4 upgrade kde-workspace to 4.11.14 once released or apply the following patch:
 https://projects.kde.org/projects/kde/kde-workspace/repository/diff?rev=54d0bfb5effff9c8cf60da890b7728cbe36a454e&rev_to=fd2aa9deed44fad6107625ad7360157fea7296f6

For plasma-desktop 5 upgrade to plasma-desktop 5.1.1 once release or apply the following patch:
 https://projects.kde.org/projects/kde/workspace/plasma-desktop/repository/diff?rev_to=683b66889b8abbeec82eedcbb1c9ff08c06e9582&rev=58bb376fb9ffb2ecb9ce0a89a0a312bfa091bd3f

Credits
=======

Thanks to David Edmundson for finding and fixing the issue
