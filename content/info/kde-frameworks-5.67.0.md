---
version: "5.67.0"
title: "KDE Frameworks 5.67.0 Source Info and Download"
type: info/frameworks
date: 2020-02-02
patches:
- 5.67.1
---
