---
version: "5.29.0"
title: "KDE Frameworks 5.29.0 Source Info and Download"
type: info/frameworks
date: 2016-12-12
patches:
- 5.29.1
---
