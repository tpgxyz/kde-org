---
version: "5.18.7"
title: "KDE Plasma 5.18.7, Bugfix Release"
type: info/plasma5
signer: Jonathan Riddell
signing_fingerprint: EC94D18F7F05997E
draft: false
---

This is a Bugfix release of KDE Plasma, featuring Plasma Desktop and
other essential software for your computer. Details in the
[Plasma 5.18.7 announcement](/announcements/plasma/5/5.18.7).
