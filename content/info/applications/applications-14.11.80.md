---
version: "14.11.80"
title: "KDE Applications 14.11.80 Info Page"
announcement: "/announcements/announce-applications-14.12-beta1"
type: "info/application-v1"
build_instructions: "https://techbase.kde.org/Getting_Started/Build/Historic/KDE4"
---

