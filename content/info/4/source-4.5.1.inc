<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">SHA1&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.5.1/src/kdeaccessibility-4.5.1.tar.bz2">kdeaccessibility-4.5.1</a></td><td align="right">5,2MB</td><td><tt>e0b3411cbb388e5d66cd2f9811ae72079a940329</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.5.1/src/kdeadmin-4.5.1.tar.bz2">kdeadmin-4.5.1</a></td><td align="right">1,2MB</td><td><tt>bf4e1239f03d01ddc7c8f7a2dec4ab7fcf00abbf</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.5.1/src/kdeartwork-4.5.1.tar.bz2">kdeartwork-4.5.1</a></td><td align="right">106MB</td><td><tt>107b8aef5ecac1e29cafe6594c2ebf3711d3a629</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.5.1/src/kdebase-4.5.1.tar.bz2">kdebase-4.5.1</a></td><td align="right">2,5MB</td><td><tt>fbeaa5cf6770e3ea82865c3523708f85689d5107</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.5.1/src/kdebase-runtime-4.5.1.tar.bz2">kdebase-runtime-4.5.1</a></td><td align="right">5,5MB</td><td><tt>454667eb411e722de10a9b921b2d30d0a052ab97</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.5.1/src/kdebase-workspace-4.5.1.tar.bz2">kdebase-workspace-4.5.1</a></td><td align="right">63MB</td><td><tt>b902ae08b2b64613b18ba8be478b244a569f8e0f</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.5.1/src/kdebindings-4.5.1.tar.bz2">kdebindings-4.5.1</a></td><td align="right">6,1MB</td><td><tt>0f75b9b787fd58ff5e36ce6ac87329ea31ad66b6</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.5.1/src/kdeedu-4.5.1.tar.bz2">kdeedu-4.5.1</a></td><td align="right">63MB</td><td><tt>5a6c34a70472dbe43167346b5175db88e9c44444</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.5.1/src/kdegames-4.5.1.tar.bz2">kdegames-4.5.1</a></td><td align="right">56MB</td><td><tt>a489bc4d8a34c93dc056ed4ed592bd3a713dfab2</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.5.1/src/kdegraphics-4.5.1.tar.bz2">kdegraphics-4.5.1</a></td><td align="right">4,5MB</td><td><tt>dc987ea24a11a1b6bdc9c0f41f01364cfcda23fd</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.5.1/src/kdelibs-4.5.1.tar.bz2">kdelibs-4.5.1</a></td><td align="right">14MB</td><td><tt>9d3eff7c35040bba6b6345a41e98a8e29ed2a1a3</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.5.1/src/kdemultimedia-4.5.1.tar.bz2">kdemultimedia-4.5.1</a></td><td align="right">1,5MB</td><td><tt>f92da1414f0ddb0cc193ff49f807992a3634cc9d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.5.1/src/kdenetwork-4.5.1.tar.bz2">kdenetwork-4.5.1</a></td><td align="right">7,8MB</td><td><tt>21c30bd592b22056f3e474d761fc2943ea280c33</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.5.1/src/kdepimlibs-4.5.1.tar.bz2">kdepimlibs-4.5.1</a></td><td align="right">2,6MB</td><td><tt>a5e05e7a921e8fd7d719e9a3cd49b08385c6da7e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.5.1/src/kdeplasma-addons-4.5.1.tar.bz2">kdeplasma-addons-4.5.1</a></td><td align="right">1,7MB</td><td><tt>c3418fc1e524c1a7a4effbdd7316f922d81f02ad</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.5.1/src/kdesdk-4.5.1.tar.bz2">kdesdk-4.5.1</a></td><td align="right">5,6MB</td><td><tt>6632d85f862a3e2ec7d75772f94770aa6aa5a182</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.5.1/src/kdetoys-4.5.1.tar.bz2">kdetoys-4.5.1</a></td><td align="right">396KB</td><td><tt>016978891ff7920706640d6e3d9a8c89c604bee3</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.5.1/src/kdeutils-4.5.1.tar.bz2">kdeutils-4.5.1</a></td><td align="right">3,7MB</td><td><tt>1a5a09aac97f7bd394817cebe5759aef7d02d4a5</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.5.1/src/kdewebdev-4.5.1.tar.bz2">kdewebdev-4.5.1</a></td><td align="right">2,1MB</td><td><tt>8b650c6e31a743e1563c1ad2705d50ff6423c226</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.5.1/src/oxygen-icons-4.5.1.tar.bz2">oxygen-icons-4.5.1</a></td><td align="right">135MB</td><td><tt>f28de6182336dcdcbc40be35e1e6d8f021a21505</tt></td></tr>
</table>
