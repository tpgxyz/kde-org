<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">SHA1&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.1/src/kdeaccessibility-4.6.1.tar.bz2">kdeaccessibility-4.6.1</a></td><td align="right">5,0MB</td><td><tt>820c3d85b01e0714826a0e00c779d4ca84ddf093</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.1/src/kdeadmin-4.6.1.tar.bz2">kdeadmin-4.6.1</a></td><td align="right">776KB</td><td><tt>b8eec397c97c19ea12f969134f8cf56886c03a26</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.1/src/kdeartwork-4.6.1.tar.bz2">kdeartwork-4.6.1</a></td><td align="right">112MB</td><td><tt>47c55cccd39f634a9754e9ff0b2dd9791aaf3ee4</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.1/src/kdebase-4.6.1.tar.bz2">kdebase-4.6.1</a></td><td align="right">2,6MB</td><td><tt>3993cc44ce9c43028bec7f314e354345da9931aa</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.1/src/kdebase-runtime-4.6.1.tar.bz2">kdebase-runtime-4.6.1</a></td><td align="right">5,6MB</td><td><tt>7c6704d72577ed88acccc767d0a6a8adf7e8e9ac</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.1/src/kdebase-workspace-4.6.1.tar.bz2">kdebase-workspace-4.6.1</a></td><td align="right">67MB</td><td><tt>e37691d88ddbf92cc95ec39849a1331404c84fd5</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.1/src/kdebindings-4.6.1.tar.bz2">kdebindings-4.6.1</a></td><td align="right">6,8MB</td><td><tt>1a71e656df19a4b132546242aa344778554693ff</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.1/src/kdeedu-4.6.1.tar.bz2">kdeedu-4.6.1</a></td><td align="right">69MB</td><td><tt>90843f6a472aa38ece3fc2a237fffd020572fad7</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.1/src/kdegames-4.6.1.tar.bz2">kdegames-4.6.1</a></td><td align="right">57MB</td><td><tt>4c7ef990bcf9eedb1a666491514fbe5e04ab9421</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.1/src/kdegraphics-4.6.1.tar.bz2">kdegraphics-4.6.1</a></td><td align="right">4,9MB</td><td><tt>8a96d927be9a41ff9a7d222474d0e4759346415b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.1/src/kdelibs-4.6.1.tar.bz2">kdelibs-4.6.1</a></td><td align="right">13MB</td><td><tt>5868d43084b2dc9de4c0404dbec137375664e893</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.1/src/kdemultimedia-4.6.1.tar.bz2">kdemultimedia-4.6.1</a></td><td align="right">1,6MB</td><td><tt>ad8f6e45d60d2b9cc0fd806e000076fcc0c07a5f</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.1/src/kdenetwork-4.6.1.tar.bz2">kdenetwork-4.6.1</a></td><td align="right">8,3MB</td><td><tt>dc09ffe178070147d67d5c27b9c340a86a2e14c7</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.1/src/kdepimlibs-4.6.1.tar.bz2">kdepimlibs-4.6.1</a></td><td align="right">3,1MB</td><td><tt>bba2ed4593c8d658efba4d4a4346d514529a3127</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.1/src/kdeplasma-addons-4.6.1.tar.bz2">kdeplasma-addons-4.6.1</a></td><td align="right">1,9MB</td><td><tt>8b959c1e2aa5f00b2f0897c9415ba018b018a3d7</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.1/src/kdesdk-4.6.1.tar.bz2">kdesdk-4.6.1</a></td><td align="right">5,8MB</td><td><tt>b46270c338ebee84ee2fd069d1551d19e1f37d30</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.1/src/kdetoys-4.6.1.tar.bz2">kdetoys-4.6.1</a></td><td align="right">396KB</td><td><tt>f4870f9f2c38d7a81b08790957a143b4a9e359b2</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.1/src/kdeutils-4.6.1.tar.bz2">kdeutils-4.6.1</a></td><td align="right">3,6MB</td><td><tt>f3fa79f15bc23cd38c6d72170ce957ec3d96e0db</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.1/src/kdewebdev-4.6.1.tar.bz2">kdewebdev-4.6.1</a></td><td align="right">2,2MB</td><td><tt>e915e8ea6d886f46ddc770f60362d3758b593713</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.1/src/konq-plugins-4.6.1.tar.bz2">konq-plugins-4.6.1</a></td><td align="right">220KB</td><td><tt>48d342633dbe9a62052e9fc3b49a70061b13acf7</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.1/src/oxygen-icons-4.6.1.tar.bz2">oxygen-icons-4.6.1</a></td><td align="right">288MB</td><td><tt>437b2f1f99861a3ee0f083aa715cfec5d0407e9a</tt></td></tr>
</table>
