<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">MD5&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.96/src/kdeaccessibility-4.1.96.tar.bz2">kdeaccessibility-4.1.96</a></td><td align="right">6,4MB</td><td><tt>13d430b62f0b345b7e53b54e9236550d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.96/src/kdeadmin-4.1.96.tar.bz2">kdeadmin-4.1.96</a></td><td align="right">1,9MB</td><td><tt>6c7af1887587a5f864344550fa6c9af0</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.96/src/kdeartwork-4.1.96.tar.bz2">kdeartwork-4.1.96</a></td><td align="right">20MB</td><td><tt>2c4f0cd7bf767551df395645248d5cb8</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.96/src/kdebase-4.1.96.tar.bz2">kdebase-4.1.96</a></td><td align="right">4,1MB</td><td><tt>d9010a13848e17d6f1cef3003a035b13</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.96/src/kdebase-runtime-4.1.96.tar.bz2">kdebase-runtime-4.1.96</a></td><td align="right">67MB</td><td><tt>2c34b66f9d1e7dfc1e0fa78087ddd773</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.96/src/kdebase-workspace-4.1.96.tar.bz2">kdebase-workspace-4.1.96</a></td><td align="right">42MB</td><td><tt>e4fd6e11f45e5099de521867797bf86b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.96/src/kdebindings-4.1.96.tar.bz2">kdebindings-4.1.96</a></td><td align="right">4,6MB</td><td><tt>7669193f0ce227bd7e113aaedc131225</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.96/src/kdeedu-4.1.96.tar.bz2">kdeedu-4.1.96</a></td><td align="right">57MB</td><td><tt>b8c1cc06d06dff0852ca16daabeec51b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.96/src/kdegames-4.1.96.tar.bz2">kdegames-4.1.96</a></td><td align="right">37MB</td><td><tt>7d9be8e33e9626774cb6893259564620</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.96/src/kdegraphics-4.1.96.tar.bz2">kdegraphics-4.1.96</a></td><td align="right">3,5MB</td><td><tt>7c8d131404c8e464f553fa1515cebc40</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.96/src/kdelibs-4.1.96.tar.bz2">kdelibs-4.1.96</a></td><td align="right">9,6MB</td><td><tt>b9254b5afda5bf1855b8d381b6d4509b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.96/src/kdemultimedia-4.1.96.tar.bz2">kdemultimedia-4.1.96</a></td><td align="right">1,5MB</td><td><tt>98d66627f6ae4376b6415ed1ad23ded4</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.96/src/kdenetwork-4.1.96.tar.bz2">kdenetwork-4.1.96</a></td><td align="right">7,2MB</td><td><tt>01c69a5982758b4939627a6c4aa8fd55</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.96/src/kdepim-4.1.96.tar.bz2">kdepim-4.1.96</a></td><td align="right">13MB</td><td><tt>4dceae1e430dfef40551f94495cb1f53</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.96/src/kdepimlibs-4.1.96.tar.bz2">kdepimlibs-4.1.96</a></td><td align="right">1,6MB</td><td><tt>1ce1fd2ad92e765678c3f858c2a89fd0</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.96/src/kdeplasma-addons-4.1.96.tar.bz2">kdeplasma-addons-4.1.96</a></td><td align="right">4,1MB</td><td><tt>323cc2d3da230e1916ad2fda0784da38</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.96/src/kdesdk-4.1.96.tar.bz2">kdesdk-4.1.96</a></td><td align="right">5,2MB</td><td><tt>19ed9d019b0dd81dcb442bffb0561457</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.96/src/kdetoys-4.1.96.tar.bz2">kdetoys-4.1.96</a></td><td align="right">1,3MB</td><td><tt>9a822c9986bc29eab0d92a0598b12dd5</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.96/src/kdeutils-4.1.96.tar.bz2">kdeutils-4.1.96</a></td><td align="right">2,2MB</td><td><tt>3e154bdb5d84501b372e2305150c4b47</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.96/src/kdewebdev-4.1.96.tar.bz2">kdewebdev-4.1.96</a></td><td align="right">2,5MB</td><td><tt>3e10d388f932ce19351541bd2ae16fd6</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.96/src/phonon-4.2.96.tar.bz2">phonon-4.2.96</a></td><td align="right">556KB</td><td><tt>35b64db6e850df7d492f812a85b16982</tt></td></tr>
</table>
