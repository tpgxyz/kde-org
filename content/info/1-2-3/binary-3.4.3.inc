<ul>

<!-- ARCH LINUX -->
<li><a href="http://www.archlinux.org/">Arch Linux</a>
  :
  <ul type="disc">
    <li>Packages: <a href="ftp://ftp.archlinux.org/extra/os/i686">ftp://ftp.archlinux.org/extra/os/i686</a></li>
    <li>
      To install: pacman -S kde
    </li>
  </ul>
  <p />
</li>

<!-- KUBUNTU -->
<li><a href="http://www.kubuntu.org/">Kubuntu</a>
    <ul type="disc">
      <li>
         Hoary (Intel i386): <tt><a href="http://www.kubuntu.org/announcements/hoary-kde-343.php">Kubuntu KDE 3.4.3 packages page</a></tt>
      </li>
    </ul>
  <p />
</li>

<!-- SLACKWARE LINUX -->
<li>
  <a href="http://www.slackware.org/">Slackware</a> (Unofficial contribution)
 (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.3/contrib/Slackware/10.1/README">README</a>)
   :
   <ul type="disc">
     <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.3/contrib/Slackware/noarch/">Language packages</a>
     </li>
     <li>
        10.2: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.3/contrib/Slackware/10.2/">Intel i486</a>
     </li>
     <li>
        10.1: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.3/contrib/Slackware/10.1/">Intel i486</a>
     </li>
   </ul>
  <p />
</li>

<!--   SUSE LINUX -->
<li>
  <a href="http://www.novell.com/linux/suse/">SuSE Linux</a>
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.3/SuSE/README">README</a>)
      :
  <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.3/SuSE/noarch/">Language
        packages</a> (all versions and architectures)
    </li>
    <li>
      10.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.3/SuSE/ix86/10.0/">Intel i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.3/SuSE/x86_64/10.0/">AMD x86-64</a>
    </li>
    <li>
      9.3:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.3/SuSE/ix86/9.3/">Intel i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.3/SuSE/x86_64/9.3/">AMD x86-64</a>
    </li>
    <li>
      9.2:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.3/SuSE/ix86/9.2/">Intel i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.3/SuSE/x86_64/9.2/">AMD x86-64</a>
    </li>
    <li>
      9.1:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.3/SuSE/ix86/9.1/">Intel i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4.3/SuSE/x86_64/9.1/">AMD x86-64</a>
    </li>
  </ul>
  <p />
</li>

</ul>
