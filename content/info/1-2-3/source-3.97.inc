<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">MD5&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.97/src/kdeaccessibility-3.97.0.tar.bz2">kdeaccessibility-3.97.0</a></td><td align="right">7.4MB</td><td><tt>57098713946fcb1138eac7c9c292c12f</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.97/src/kdeadmin-3.97.0.tar.bz2">kdeadmin-3.97.0</a></td><td align="right">1.4MB</td><td><tt>eec1ebff24d0323997b076a15f26b556</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.97/src/kdeartwork-3.97.0.tar.bz2">kdeartwork-3.97.0</a></td><td align="right">44MB</td><td><tt>def225e9f8c6c0f2d488935909d8a9f8</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.97/src/kdebase-3.97.0.tar.bz2">kdebase-3.97.0</a></td><td align="right">4.0MB</td><td><tt>49db92b1c1c9f757e2f2906ac59e188c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.97/src/kdebase-runtime-3.97.0.tar.bz2">kdebase-runtime-3.97.0</a></td><td align="right">55MB</td><td><tt>1efc8beac7ff5d9c5163f551094332ee</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.97/src/kdebase-workspace-3.97.0.tar.bz2">kdebase-workspace-3.97.0</a></td><td align="right">9.8MB</td><td><tt>8ba08f95bf3eab7967531379c3a5502e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.97/src/kdebindings-3.97.0.tar.bz2">kdebindings-3.97.0</a></td><td align="right">4.1MB</td><td><tt>e5f252f0eee663ff8a2d6b19f54599b7</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.97/src/kdeedu-3.97.0.tar.bz2">kdeedu-3.97.0</a></td><td align="right">41MB</td><td><tt>a3e3106d4c96ae9255cb06f18fed15c2</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.97/src/kdegames-3.97.0.tar.bz2">kdegames-3.97.0</a></td><td align="right">27MB</td><td><tt>f332d8968c74f1fd14bf235a8d1cd44d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.97/src/kdegraphics-3.97.0.tar.bz2">kdegraphics-3.97.0</a></td><td align="right">2.3MB</td><td><tt>a2980aea280da3fd52c44bbe6537a09f</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.97/src/kdelibs-3.97.0.tar.bz2">kdelibs-3.97.0</a></td><td align="right">8.7MB</td><td><tt>0c1a52a6f67aa92bf40f3f4d94365691</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.97/src/kdemultimedia-3.97.0.tar.bz2">kdemultimedia-3.97.0</a></td><td align="right">1.1MB</td><td><tt>e0e596ef7f904e77fe33e76e8bd82c1d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.97/src/kdenetwork-3.97.0.tar.bz2">kdenetwork-3.97.0</a></td><td align="right">6.3MB</td><td><tt>efffb1c28457c3485632b40c8c995762</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.97/src/kdepim-3.97.0.tar.bz2">kdepim-3.97.0</a></td><td align="right">13MB</td><td><tt>6f775d1a194a87988b2d80a0037bf675</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.97/src/kdepimlibs-3.97.0.tar.bz2">kdepimlibs-3.97.0</a></td><td align="right">1.7MB</td><td><tt>222faa48a948af7c8c4540a282c2995e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.97/src/kdesdk-3.97.0.tar.bz2">kdesdk-3.97.0</a></td><td align="right">4.2MB</td><td><tt>29a0810dd12200eda2ab5dd6f22bcd29</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.97/src/kdetoys-3.97.0.tar.bz2">kdetoys-3.97.0</a></td><td align="right">2.2MB</td><td><tt>c6daab3638be1f2494246d984c89021b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.97/src/kdeutils-3.97.0.tar.bz2">kdeutils-3.97.0</a></td><td align="right">2.3MB</td><td><tt>472921bca9e159eb3f5e0e2623a2aa14</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.97/src/kdewebdev-3.97.0.tar.bz2">kdewebdev-3.97.0</a></td><td align="right">9.7MB</td><td><tt>72317745d5af447f6d7057966403cd28</tt></td></tr>
</table>
