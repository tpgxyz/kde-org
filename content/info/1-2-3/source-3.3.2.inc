<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">MD5&nbsp;Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.2/src/arts-1.3.2.tar.bz2">arts-1.3.2</a></td>
   <td align="right">952kB</td>
   <td><tt>a3d22f7cc5c641204a28d3f77e441a84</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.2/src/kdeaccessibility-3.3.2.tar.bz2">kdeaccessibility-3.3.2</a></td>
   <td align="right">1.6MB</td>
   <td><tt>2d1fc370ce1e6a58c82d4dc283ee206d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.2/src/kdeaddons-3.3.2.tar.bz2">kdeaddons-3.3.2</a></td>
   <td align="right">1.9MB</td>
   <td><tt>d1ad11def2ac30965642144ef29d738a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.2/src/kdeadmin-3.3.2.tar.bz2">kdeadmin-3.3.2</a></td>
   <td align="right">1.9MB</td>
   <td><tt>d12b12925dda1f4e6ba162e856730a5e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.2/src/kdeartwork-3.3.2.tar.bz2">kdeartwork-3.3.2</a></td>
   <td align="right">17MB</td>
   <td><tt>9a712da253bacb87e0d4fd28cec183a1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.2/src/kdebase-3.3.2.tar.bz2">kdebase-3.3.2</a></td>
   <td align="right">19MB</td>
   <td><tt>edbd721a2a4970977dfe5f45d9e38923</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.2/src/kdebindings-3.3.2.tar.bz2">kdebindings-3.3.2</a></td>
   <td align="right">7.3MB</td>
   <td><tt>a8ae8e2ef4dd3680d0756adf76086d85</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.2/src/kdeedu-3.3.2.tar.bz2">kdeedu-3.3.2</a></td>
   <td align="right">21MB</td>
   <td><tt>2ea54bb7aee669582eb0877d3c6f0b3d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.2/src/kdegames-3.3.2.tar.bz2">kdegames-3.3.2</a></td>
   <td align="right">9.4MB</td>
   <td><tt>41791396e595b9fc8a84e08ae63b552d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.2/src/kdegraphics-3.3.2.tar.bz2">kdegraphics-3.3.2</a></td>
   <td align="right">6.4MB</td>
   <td><tt>03092b8be2f7054d71895b8fd58ad26e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.2/src/kde-i18n-3.3.2.tar.bz2">kde-i18n-3.3.2</a></td>
   <td align="right">187MB</td>
   <td><tt>20135e722cd5f94cbe4997765941b455</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.2/src/kdelibs-3.3.2.tar.bz2">kdelibs-3.3.2</a></td>
   <td align="right">15MB</td>
   <td><tt>0473fb4c6c2cd2bc0f267cfa201f3fd8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.2/src/kdemultimedia-3.3.2.tar.bz2">kdemultimedia-3.3.2</a></td>
   <td align="right">5.6MB</td>
   <td><tt>2f393da809542dab5bf75bf7a91d1ec0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.2/src/kdenetwork-3.3.2.tar.bz2">kdenetwork-3.3.2</a></td>
   <td align="right">7.1MB</td>
   <td><tt>652a5703b8dc937c4009e002dc3035f3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.2/src/kdepim-3.3.2.tar.bz2">kdepim-3.3.2</a></td>
   <td align="right">10MB</td>
   <td><tt>73852792762c4f229e870314c51c081a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.2/src/kdesdk-3.3.2.tar.bz2">kdesdk-3.3.2</a></td>
   <td align="right">4.6MB</td>
   <td><tt>906bbcde1b3db2eaac8a257c8574e033</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.2/src/kdetoys-3.3.2.tar.bz2">kdetoys-3.3.2</a></td>
   <td align="right">3.1MB</td>
   <td><tt>5d911f7f0034e71beb087fac3e8e68af</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.2/src/kdeutils-3.3.2.tar.bz2">kdeutils-3.3.2</a></td>
   <td align="right">2.6MB</td>
   <td><tt>bf50db108408da11e2f2fcacd6b46b51</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.2/src/kdevelop-3.1.2.tar.bz2">kdevelop-3.1.2</a></td>
   <td align="right">8.0MB</td>
   <td><tt>706dfcf25f013c544220a0ca69b74846</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3.2/src/kdewebdev-3.3.2.tar.bz2">kdewebdev-3.3.2</a></td>
   <td align="right">5.0MB</td>
   <td><tt>582d0f3073d5829b4ab21b03411ba697</tt></td>
</tr>

</table>
