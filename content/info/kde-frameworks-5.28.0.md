---
version: "5.28.0"
title: "KDE Frameworks 5.28.0 Source Info and Download"
type: info/frameworks
date: 2016-11-15
patches:
- 5.28.1
---
