---
aliases:
- ../announce-applications-19.08.2
changelog: true
date: 2019-10-10
description: KDE rilascia Applications 19.08.2.
layout: application
major_version: '19.08'
release: applications-19.08.2
title: KDE rilascia Applications 19.08.2.
version: 19.08.2
---
{{% i18n_date %}}

Oggi KDE ha rilasciato il secondo aggiornamento di stabilizzazione per <a href='../19.08.0'>KDE Applications 19.08</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, e costituisce un aggiornamento sicuro e gradevole per tutti.

Più di venti errori corretti includono, tra gli altri, miglioramenti a Kontact, Dolphin, Gwenview, Kate, Kdenlive, Konsole, Lokalize e Spectacle.

I miglioramenti includono:

- Supporto High-DPI migliorato in Konsole e altre applicazioni
- Il passaggio tra ricerche differenti in Dolphin ora aggiorna correttamente i parametri di ricerca
- KMail è di nuovo in grado di salvare direttamente i messaggi nelle cartelle remote
