---
aliases:
- ../announce-applications-17.08.3
changelog: true
date: 2017-11-09
description: KDE rilascia KDE Applications 17.08.3
layout: application
title: KDE rilascia KDE Applications 17.08.3
version: 17.08.3
---
9 novembre 2017. Oggi KDE ha rilasciato il terzo aggiornamento di stabilizzazione per <a href='../17.08.0'>KDE Applications 17.08</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, e costituisce un aggiornamento sicuro e gradevole per tutti.

Circa una dozzina di errori corretti includono, tra gli altri, miglioramenti a Kontact, Ark, Gwenview, KGpg, KWave, Okular e Spectacle.

Questo rilascio include inoltre l'ultima versione con supporto a lungo termine di KDE Development Platform 4.14.38.

I miglioramenti includono:

- Risolve una regressione Samba 4.7 con condivisioni SMB protette da password
- Okular non si arresta più in modo anomalo dopo alcune operazioni di rotazione
- Ark conserva le date di modifica dei file quando si estraggono archivi ZIP
