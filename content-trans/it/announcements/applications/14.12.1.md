---
aliases:
- ../announce-applications-14.12.1
changelog: true
date: '2015-01-08'
description: KDE rilascia KDE Applications 14.12.1.
layout: application
title: KDE rilascia KDE Applications 14.12.1
version: 14.12.1
---
13 gennaio 2015. Oggi KDE ha rilasciato il primo aggiornamento di stabilizzazione per <a href='../14.12.0'>KDE Applications 14.12</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, costituisce un aggiornamento sicuro e gradevole per tutti.

Gli oltre 50 bug corretti includono miglioramenti allo strumento per l'archiviazione Ark, allo strumento per UML Umbrello, al visualizzatore di documenti Okular, all'applicazione per l'apprendimento della pronuncia Artikulate ed al client per desktop remoto KRDC.

Questo rilascio include inoltre le versioni con supporto a lungo termine (Long Term Support) di Plasma Workspaces 4.11.15, KDE Development Platform 4.14.4 e della suite Kontact 4.14.4.
