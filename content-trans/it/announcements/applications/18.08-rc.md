---
aliases:
- ../announce-applications-18.08-rc
date: 2018-08-03
description: KDE rilascia Applications 18.08 Release Candidate.
layout: application
release: applications-18.07.90
title: KDE rilascia la Release Candidate di KDE Applications 18.08
---
3 agosto 2018. Oggi KDE ha rilasciato la release candidate della nuova versione di KDE Applications. Con il &quot;congelamento&quot; di dipendenze e funzionalità, l'attenzione degli sviluppatori KDE è adesso concentrata sulla correzione degli errori e sull'ulteriore rifinitura dei programmi.

Controlla le <a href='https://community.kde.org/Applications/18.08_Release_Notes'>note di rilascio della comunità</a> per informazioni sugli archivi e sui problemi noti. Un annuncio più completo verrà reso disponibile in concomitanza con la versione finale

A causa della grande quantità di cambiamenti, i rilasci della serie 18.08 hanno bisogno di una verifica accurata per mantenere e migliorare la qualità e l'esperienza utente. Gli utenti &quot;reali&quot; sono fondamentali per mantenere la qualità di KDE, perché gli sviluppatori non possono testare completamente ogni possibile configurazione. Contiamo su di voi per aiutarci a trovare i bug al più presto possibile perché possano essere eliminati prima della versione finale. Valutate la possibilità di partecipare alla squadra installando la release candidate <a href='https://bugs.kde.org/'>e segnalando ogni problema</a>.
