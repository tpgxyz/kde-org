---
aliases:
- ../4.11
custom_about: true
custom_contact: true
date: 2013-08-14
description: KDE rilascia la versione 4.11 degli spazi di lavoro Plasma (Workspaces),
  delle applicazioni (Applications) e della piattaforma (Platform)
title: KDE Software Compilation 4.11
---
{{<figure src="/announcements/4/4.11.0/screenshots/plasma-4.11.png" class="text-center" caption=`Plasma Workspaces 4.11` >}} <br />

14 agosto 2013. La comunità KDE è fiera di annunciare i più recenti aggiornamenti degli spazi di lavoro Plasma (Workspaces), delle applicazioni (Applications) e della piattaforma di sviluppo (Development Platform) che portano nuove funzionalità e correzioni mentre, allo stesso tempo, predispongono la piattaforma per i futuri sviluppi. La nuova versione 4.11 degli spazi di lavoro Plasma (Plasma Workspaces) sarà supportata a lungo termine mentre i suoi sviluppatori si concentrano sulla parte tecnica della transizione a Frameworks 5, la prossima versione modulare di Platform. Questo è dunque l'ultimo rilascio in cui i tre componenti (spazi di lavoro Plasma, applicazioni e piattaforma di sviluppo) utilizzano lo stesso numero di versione.<br />

Questo rilascio è dedicato alla memoria di <a href='http://en.wikipedia.org/wiki/Atul_Chitnis'>Atul 'toolz' Chitnis</a>, un grande campione indiano del software libero e open source. Atul ha guidato le conferenze Linux Bangalore e FOSS.IN dal 2001, ed entrambi gli eventi sono state dei punti di riferimento per la scena FOSS indiana. KDE India è nata durante la prima FOSS.in nel dicembre 2005. Molti contributori indiani di KDE hanno iniziato durante questi eventi. È solo grazie all'incoraggiamento di Atul che il KDE Project Day durante FOSS.IN è sempre stato un grande successo. Atul ci ha lasciati il 3 giugno dopo aver combattuto la battaglia con il cancro. Possa la sua anima riposare in pace. Siamo grati per i suoi contributi per un mondo migliore.

Questi rilasci sono tutti tradotti in 54 lingue; è molto probabile che altre si aggiungeranno in concomitanza dei successivi rilasci mensili di correzione dei bug. La squadra di documentazione ha aggiornato 91 manuali in occasione di questa versione.

## <a href="./plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Plasma Workspaces 4.11 continua ad affinare l'esperienza utente</a>

In preparazione per il supporto a lungo termine, Plasma Workspaces porta ulteriori miglioramenti alle funzionalità di base con una barra delle applicazioni più fluida, un indicatore della batteria più intelligente ed un mixer audio migliorato. L'introduzione di KScreen porta in Workspaces una gestione intelligente di monitor multipli, mentre miglioramenti su larga scala delle prestazioni combinati con piccole correzioni all'usabilità producono un'esperienza d'uso complessiva più gradevole. 

## <a href="./applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> KDE Application 4.11 introduce grandi passi avanti nella gestione delle informazioni personali (PIM) e miglioramenti globali</a>

Questo rilascio vede imponenti miglioramenti in KDE PIM che forniscono migliori prestazioni e molte nuove funzioni. Kate migliora la produttività degli sviluppatori Python e Javascript con nuove estensioni, Dolphin è diventato più veloce e le applicazioni didattiche hanno varie nuove funzionalità.

## <a href="./platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> KDE Platform 4.11 porta prestazioni migliori</a>

Questo rilascio di KDE Platform 4.11 continua a concentrarsi sulla stabilità. Nuove funzionalità sono in corso di implementazione in KDE Frameworks 5.0, il nostro futuro rilascio, ma in questa versione stabile siamo riusciti a far entrare delle ottimizzazioni per la nostra infrastruttura Nepomuk.

<br />
Quando aggiorni presta attenzione alle <a href='http://community.kde.org/KDE_SC/4.11_Release_Notes'>note di rilascio</a>.<br />

## Diffondi la notizia e segui quello che succede: marca come &quot;KDE&quot;

KDE incoraggia le persone a diffondere la notizia sui siti del web sociale. Invia delle storie ai siti di notizie, usa canali come delicious, digg, reddit, twitter, identi.ca. Carica le schermate in servizi come Facebook, Flickr, ipernity e Picasa, e inviale nei gruppi appropriati. Crea degli screencast e inviali su YouTube, Blip.tv e Vimeo. Usa il tag &quot;KDE&quot; per le notizie, i post e tutti i materiali inviati. Questo rende semplice trovarli e fornisce al gruppo di KDE Promo un modo per analizzare la copertura del rilascio 4.11 del software KDE.

## Feste di rilascio

Come al solito, i membri della comunità KDE organizzano delle feste di rilascio in tutto il mondo. Varie sono già state programmate e altre arriveranno in seguito. Trova <a href="http://community.kde.org/Promo/Events/Release_Parties/4.11">qui l'elenco delle feste</a>. Tutti sono invitati a partecipare! Si potrà trovare una combinazione di compagnia interessante e conversazioni stimolanti oltre a cibo e bevande. È una grande occasione per imparare di più su quello che succede in KDE, partecipare o semplicemente incontrare altri utenti e collaboratori.

Incoraggiamo le persone a organizzare le proprie feste. Sono divertenti da gestire e aperte a tutti! Controlla <a href='http://community.kde.org/Promo/Events/Release_Parties'>i consigli su come organizzare una festa</a>.

## Informazioni su questi annunci di rilascio

Questi annunci di rilascio sono stati preparati da Jos Poortvliet, Sebastian Kügler, Markus Slopianka, Burkhard Lück, Valorie Zimmerman, Maarten De Meyer, Frank Reininghaus, Michael Pyne, Martin Gräßlin ed altri membri del gruppo di promozione di KDE e della più ampia comunità KDE. Essi coprono i punti salienti dei vari cambiamenti apportati al software KDE negli ultimi sei mesi.

#### Supporta KDE

<a href="http://jointhegame.kde.org/"> <img src="/announcements/4/4.9.0/images/join-the-game.png" class="img-fluid float-left mr-3" alt="Join the Game"/> </a>

Il nuovo <a href='http://jointhegame.kde.org/'>programma di KDE e.V. per i membri sostenitori</a> è aperto.  Con 25 &euro; al trimestre puoi permettere alla comunità internazionale di KDE di continuare a crescere creando software libero di livello mondiale.
