---
aliases:
- ../announce-applications-17.12.3
changelog: true
date: 2018-03-08
description: KDEk, KDE Aplikazioak 17.12.3 kaleratzen du
layout: application
title: KDEk, KDE Aplikazioak 17.12.3 kaleratzen du
version: 17.12.3
---
March 8, 2018. Today KDE released the third stability update for <a href='../17.12.0'>KDE Applications 17.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

About 25 recorded bugfixes include improvements to Kontact, Dolphin, Gwenview, JuK, KGet, Okular, Umbrello, among others.

Improvements include:

- Akregator no longer erases the feeds.opml feed list after an error
- Gwenview's fullscreen mode now operates on the correct filename after renaming
- Several rare crashes in Okular have been identified and fixed
