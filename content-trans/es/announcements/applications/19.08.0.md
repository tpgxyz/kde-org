---
aliases:
- ../announce-applications-19.08.0
changelog: true
date: 2019-08-15
description: KDE lanza las Aplicaciones 19.08.
layout: application
release: applications-19.08.0
title: KDE lanza las Aplicaciones de KDE 19.08.0
version: '19.08'
version_number: 19.08.0
---
{{< peertube "/6443ce38-0a96-4b49-8fc5-a50832ed93ce" >}}

{{% i18n_date %}}

La comunidad KDE se complace al anunciar el lanzamiento de las Aplicaciones de KDE 19.08.

Este lanzamiento es parte del compromiso de KDE de proporcionar continuamente versiones mejoradas de los programas que entregamos a nuestros usuarios. Las nuevas versiones de las Aplicaciones integran más funcionalidades y un software mejor diseñado que aumenta la usabilidad y la estabilidad de aplicaciones como Dolphin, Konsole, Kate, Okular y del resto de sus utilidades favoritas de KDE. Nuestro propósito es asegurar que pueda seguir siendo productivo y hacer que el software de KDE le resulte más fácil y más placentero de usar.

Esperamos que disfrute de todas las nuevas mejoras que encontrará en 19.08.

## Novedades de las Aplicaciones de KDE 19.08

Se han resuelto más de 170 fallos. Estas correcciones vuelven a implementar funciones desactivadas, normalizan accesos rápidos de teclado y resuelven bloqueos, haciendo que las aplicaciones sean más amigables y que le permitan trabajar y jugar con más habilidad.

### Dolphin

Dolphin es el explorador de archivos y de carpetas de KDE que ahora se puede lanzar desde cualquier lugar usando el nuevo acceso rápido de teclado <keycap>Meta + E</keycap>. También contiene una nueva funcionalidad que minimiza el desorden del escritorio: cuando Dolphin ya está en ejecución, al abrir carpetas con otras aplicaciones, estas se abrirán en una nueva pestaña de la ventana existente en lugar de en una nueva ventana de Dolphin. Tenga en cuenta que este comportamiento es ahora el predeterminado, aunque se puede desactivar.

El panel de información (situado de forma predeterminada a la derecha de la ventana principal de Dolphin) se ha mejorado de varios modos. Por ejemplo, es posible reproducir automáticamente archivos multimedia al resaltarlos en el panel principal. Ahora también se puede seleccionar y copiar el texto que se muestra en dicho panel. Si desea modificar la información que se muestra en el panel, puede hacerlo directamente en él, ya que Dolphin no abre una ventana separada cuando escoge modificar el panel.

También hemos limado muchas asperezas y corregido pequeños fallos para asegurarnos de que su experiencia usando Dolphin sea más suave en general.

{{<figure src="/announcements/applications/19.08.0/dolphin_bookmark.png" alt=`Nueva funcionalidad de marcadores de Dolphin` caption=`Nueva funcionalidad de marcadores de Dolphin` width="600px" >}}

### Gwenview

Gwenview es el visor de imágenes de KDE. En este lanzamiento, los desarrolladores han mejorado la funcionalidad de mostrar miniaturas de forma global. Gwenview puede usar ahora un «modo de bajo uso de recursos» que carga miniaturas de baja resolución (si están disponibles). Este nuevo modo es mucho más rápido y más eficiente con los recursos cuando se cargan miniaturas para imágenes JPEG y archivos RAW. Si Gwenview no puede generar la miniatura de alguna imagen, mostrará en su lugar una imagen genérica en lugar de volver a usar la miniatura de la imagen anterior. También se han resuelto los problemas que tenía Gwenview para mostrar miniaturas de las imágenes de cámaras Sony y Canon.

Además de los cambios en el apartado de las miniaturas, Gwenview también implementa un nuevo menú «Compartir» que permite enviar imágenes a diversos lugares, enviando y mostrando correctamente los archivos de las ubicaciones remotas a las que accede usando KIO. La nueva versión de Gwenview también muestra una mayor cantidad de metadatos EXIF en las imágenes RAW.

{{<figure src="/announcements/applications/19.08.0/gwenview_share.png" alt=`El nuevo menú «Compartir» de Gwenview` caption=`El nuevo menú «Compartir» de Gwenview` width="600px" >}}

### Okular

Los desarrolladores han introducido muchas mejoras en las anotaciones de Okular, el visor de documentos de KDE. Además de mejorar la interfaz del usuario en los diálogos de configuración de las anotaciones, las anotaciones de línea pueden mostrar ahora diversas florituras visuales en sus extremos, permitiendo que se conviertan en flechas, por ejemplo. Otra cosa que se puede hacer ahora con las anotaciones es desplegarlas y contraerlas todas a la vez.

El uso de documentos EPub de Okular también ha recibido un empuje en esta versión. Okular ya no se bloquea al intentar cargar archivos ePub mal formados; también se ha mejorado significativamente su rendimiento con grandes archivos ePub. La lista de cambios de este lanzamiento incluye bordes de página mejorados y una herramienta de marcador para el modo de presentación en pantallas de alta densidad.

{{<figure src="/announcements/applications/19.08.0/okular_line_end.png" alt=`Las preferencias de la herramienta de anotaciones de Okular con la nueva opción de finales de línea` caption=`Las preferencias de la herramienta de anotaciones de Okular con la nueva opción de finales de línea` width="600px" >}}

### Kate

Gracias a nuestros desarrolladores, se han corregido tres molestos errores en esta versión del editor de texto avanzado de KDE. Kate vuelve a traer al primer plano la ventana existente cuando se le solicita que abra un nuevo documento desde otra aplicación. La funcionalidad de «apertura rápida» ordena los elementos por antigüedad de uso (primero los más recientes) y preselecciona el primero de ellos. El tercer cambio está en la funcionalidad de «documentos recientes», que ahora funciona cuando la configuración actual está ajustada para no guardar las preferencias de las ventanas individuales.

### Konsole

El cambio más notable en Konsole, la aplicación de emulación de terminal de KDE, es la mejora de la funcionalidad de distribución en mosaico. Ahora se puede dividir el panel principal del modo que se desee, tanto vertical como horizontalmente. Los paneles hijos se pueden volver a dividir del mismo modo. Esta versión también añade la posibilidad de arrastrar y soltar paneles, por lo que puede organizarlos fácilmente para que se ajusten a su metodología de trabajo.

Aparte de todo esto, la ventana de preferencias se ha revisado para hacerla más clara y más fácil de usar.

{{< video src-webm="/announcements/applications/19.08.0/konsole-tabs.webm" >}}

### Spectacle

Spectacle es la aplicación de capturas de pantalla de KDE y cada vez está ganando más funcionalidades interesantes con cada nueva versión. Esta versión no es ninguna excepción, ya que Spectacle contiene varios ajustes nuevos para regular la demora de las capturas. Al hacer capturas tras un tiempo de demora, Spectacle muestra ahora el tiempo restante en el título de su ventana. Esta información también es visible en el gestor de tareas.

También en la funcionalidad de la demora de las capturas, el botón de Spectacle en el gestor de tareas muestra una barra de avance, por lo que puede hacerse una idea de lo que va a tardar en hacerse la captura. Finalmente, si había minimizado previamente la ventana de Spectacle y la vuelve a restaurar mientras está en espera para hacer una captura, podrá comprobar que el botón «Realizar nueva captura de pantalla» se ha convertido en un botón «Cancelar». Este botón también contiene una barra de avance, dándole la oportunidad de detener la cuenta atrás.

También existe una nueva funcionalidad a la hora de guardar las capturas de pantalla. Tras guardar una captura de pantalla, Spectacle muestra un mensaje en la aplicación que le permite abrir la captura de pantalla o la carpeta que la contiene.

{{< video src-webm="/announcements/applications/19.08.0/spectacle_progress.webm" >}}

### Kontact

Kontact, la suite general de trabajo en grupo de KDE con programas de correo electrónico, calendario y contactos, proporciona emoticonos Unicode en color y uso de Markdown al compositor de correo electrónico. La nueva versión de KMail no solo hace que sus mensajes tengan mejor aspecto, sino que, gracias a la integración con los comprobadores gramaticales (como LanguageTool y Grammalecte), le ayudará a comprobar y corregir el texto.

{{<figure src="/announcements/applications/19.08.0/kontact_emoji.png" alt=`Selector de emoticonos` caption=`Selector de emoticonos` width="600px" >}}

{{<figure src="/announcements/applications/19.08.0/kmail_grammar.png" alt=`Integración de la comprobación gramatical en KMail` caption=`Integración de la comprobación gramatical en KMail` width="600px" >}}

Al panificar eventos, los mensajes de invitación de KMail ya no se borran tras responder a ellos. Ahora es posible mover un evento de un calendario a otro en el editor de eventos de KOrganizer.

Finalmente, aunque no menos importante, KAddressBook puede enviar ahora mensajes SMS a los contactos mediante KDE Connect, lo que se traduce en una integración más conveniente del escritorio con los dispositivos móviles.

### Kdenlive

La nueva versión de Kdenlive, el software de edición de vídeo de KDE, contiene un nuevo conjunto de combinaciones de teclado y ratón que le ayudarán a ser más productivo. Por ejemplo, puede cambiar la velocidad de un clip en la línea de tiempos pulsando CTRL y arrastrando el clip, o activar la vista previa de las miniaturas de los clips de vídeo manteniendo pulsada la tecla «Mayúsculas» y moviendo el cursor sobre la miniatura de un clip en la bandeja del proyecto. Los desarrolladores también han centrado sus esfuerzos en la usabilidad, haciendo que las operaciones de edición de tres puntos sean consistentes con las de otros editores de vídeo, lo que seguramente apreciará si ha cambiado a Kdenlive después de usar otro editor.

{{<figure src="https://cdn.kde.org/screenshots/kdenlive/19-08.png" alt=`Kdenlive 19.08.0` caption=`Kdenlive 19.08.0` width="600px" >}}
