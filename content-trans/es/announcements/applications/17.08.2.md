---
aliases:
- ../announce-applications-17.08.2
changelog: true
date: 2017-10-12
description: KDE lanza las Aplicaciones de KDE 17.08.2
layout: application
title: KDE lanza las Aplicaciones de KDE 17.08.2
version: 17.08.2
---
Hoy, 12 de octubre de 2017, KDE ha lanzado la segunda actualización de estabilización para las <a href='../17.08.0'>Aplicaciones 17.08</a>. Esta versión solo contiene soluciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 25 correcciones de errores registradas, se incluyen mejoras en Kontact, Dolphin, Gwenview, Kdenlive, Marble y Okular, entre otras aplicaciones.

También se incluye la versión de la Plataforma de desarrollo de KDE 4.14.37 que contará con asistencia a largo plazo.

Las mejoras incluyen:

- Se ha corregido una fuga de memoria y un fallo en la configuración del complemento de eventos de Plasma.
- Los mensajes leídos no se eliminan de forma inmediata del filtro de «No leídos» en Akregator.
- El importador de Gwenview usa ahora la fecha y la hora EXIF.
