---
aliases:
- ../announce-applications-15.08-beta
date: 2015-07-28
description: KDE lanza la Beta de las Aplicaciones 15.08.
layout: application
release: applications-15.07.80
title: KDE lanza la primera beta para las Aplicaciones 15.08
---
Hoy, 28 de julio de 2015, KDE ha lanzado la versión beta de las Aplicaciones KDE. Una vez congeladas las dependencias y las funcionalidades, el equipo de KDE se ha centrado en corregir errores y en pulir aún más la versión.

Debido a las numerosas aplicaciones que se basan en KDE Frameworks 5, es necesario probar la versión 15.08 de manera exhaustiva con el fin de mantener y mejorar la calidad y la experiencia del usuario. Los usuarios reales son de vital importancia para mantener la alta calidad de KDE porque los desarrolladores no pueden probar todas las configuraciones posibles. Contamos con ustedes para ayudar a encontrar errores de manera temprana de forma que se puedan corregir antes de la versión final. Considere la idea de unirse al equipo instalando la beta <a href='https://bugs.kde.org/'>e informando de cualquier error que encuentre</a>.
