---
aliases:
- ../../kde-frameworks-5.0
- ./5.0
customIntro: true
date: '2014-07-07'
description: KDE lanza la primera versión de Frameworks 5.
layout: framework
qtversion: 5.2
title: KDE lanza la primera versión de Frameworks 5
---
7 de julio de 2014. La Comunidad de KDE se complace en anunciar KDE Frameworks 5.0. Frameworks 5 es la siguiente generación de bibliotecas KDE, modularizada y optimizada para lograr una fácil integración en las aplicaciones Qt. Frameworks ofrece una gran variedad de funciones de uso habitual en bibliotecas muy depuradas, revisadas por expertos y probadas exhaustivamente con licencias fáciles de usar. Esta versión la forman más de 50 infraestructuras que dan solución, entre otras funciones, a la integración de hardware, a la compatibilidad de formatos de archivos, a elementos gráficos adicionales, a funciones gráficas, a la revisión de ortografía. Muchas de las infraestructuras se pueden ejecutar en distintas plataformas y no tienen dependencias adicionales o tienen muy pocas, lo que facilita su compilación y hace más sencillo añadirlas a cualquier aplicación Qt.

KDE Frameworks representa un esfuerzo por reescribir las potentes bibliotecas de la plataforma 4 de KDE en un conjunto de módulos independientes y ejecutables en todas las plataformas que estará disponible para todos los desarrolladores de Qt y que simplificará y acelerará el desarrollo en QT y reducirá su coste. Cada una de las infraestructuras se puede ejecutar en todas las plataformas, está bien documentada y probada y su uso resultará familiar a los desarrolladores de Qt, ya que sigue el estilo y los criterios establecidos por el proyecto Qt. Frameworks está desarrollado bajo el probado modelo de gobierno abierto de KDE con una planificación de versiones predecible, un proceso de colaboraciones claro e independiente de proveedores y un sistema abierto de licencias (LGPL).

Las Frameworks tienen una clara estructura de dependencias, divididas en categorías y niveles. Las categorías se refieren a dependencias en tiempo de ejecución:

- Los elementos <strong>funcionales</strong> no tienen dependencias en tiempo de ejecución.
- La <strong>integración</strong> designa código que puede necesitar dependencias en tiempo de ejecución dependiendo de lo que ofrezca el SO o la plataforma.
- Las <strong>soluciones</strong> tienen dependencias obligadas en tiempo de ejecución.

Los <strong>niveles</strong> se refieren a dependencias de otras Frameworks en tiempo de compilación. Las Frameworks del nivel 1 no tienen dependencias dentro de las Frameworks y solo necesitan Qt y otras bibliotecas relevantes. Las Frameworks del nivel 2 solo pueden depender de las del nivel 1. Las Frameworks del nivel 3 pueden depender de otras Frameworks de los niveles 1, 2 y 3.

La transición desde la Plataforma hasta Frameworks se ha realizado a lo largo de más de 3 años, guiada por los mejores colaboradores técnicos de KDE. Para obtener más información sobre Frameworks 5, puede leer <a href='http://dot.kde.org/2013/09/25/frameworks-5'>este artículo del año pasado</a>.

## Puntos destacados

Actualmente, hay más de 50 Frameworks disponibles. Puede ver una lista del conjunto completo <a href='http://api.kde.org/frameworks-api/frameworks5-apidocs/'>en la documentación en línea de la API</a>. A continuación, un resumen de algunas de las funcionalidades que Frameworks ofrece a los desarrolladores de aplicaciones Qt.

<strong>KArchive</strong> ofrece compatibilidad con los códecs de compresión más populares en una biblioteca de compresión y extracción en un archivo autocontenido, con todas las funcionalidades y fácil de utilizar. Solo hace falta enviarle archivos, no hace falta reinventar una función de compresión en las aplicaciones basadas en Qt.

<strong>ThreadWeaver</strong> proporciona una potente API de alto nivel para gestionar la ejecución de hilos mediante interfaces basadas en scripts en segundo plano y en colas. Permite planificar de manera sencilla la ejecución de hilos mediante la especificación de dependencias entre los hilos y ejecutándolos mientras satisfagan dichas dependencias, simplificando en gran medida el uso de varios hilos.

<strong>KConfig</strong> es una infraestructura para gestionar el almacenamiento y la recuperación de preferencias de configuración. Incorpora una API orientada a grupos. Funciona con archivos INI y con directorios compatibles con XDG. Genera código basado en archivos XML.

<strong>Solid</strong>realiza la detección de hardware y puede informar a una aplicación sobre los dispositivos y volúmenes de almacenamiento, CPU, estado de la batería, gestión de energía, estado de la red y de las interfaces y Bluetooth. Para las particiones cifradas, la energía y la red es necesario ejecutar demonios.

<strong>KI18n</strong> añade a las aplicaciones compatibilidad con Gettext, haciendo más sencilla la integración con el flujo de trabajo de las aplicaciones Qt en la infraestructura general de traducción de muchos proyectos.
