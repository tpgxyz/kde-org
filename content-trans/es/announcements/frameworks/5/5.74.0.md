---
aliases:
- ../../kde-frameworks-5.74.0
date: 2020-09-06
layout: framework
libCount: 70
---
### Attica

+ Usar «Q_DECLARE_OPERATORS_FOR_FLAGS» en el mismo espacio de nombres que la definición de indicadores.

### Baloo

+ Se ha cambiado la licencia de muchos archivos a LGPL 2.0 o posterior.
+ Usar código común de creación de UDS también para etiquetas (error 419429).
+ Excluir código común de creación de UDS de los auxiliares de KIO.
+ [balooctl] Mostrar formatos permitidos en el texto de ayuda.
+ [balooctl] Mostrar el archivo actual en la salida de estado para el estado de indexación.
+ [balooctl] Definir el modo «QDBusServiceWatcher» en el constructor.
+ [balooctl] Limpieza de formateo.
+ Usar «Q_DECLARE_OPERATORS_FOR_FLAGS» en el mismo espacio de nombres que la definición de indicadores.
+ [OrPostingIterator] No avanzar cuando el ID deseado es menor que el actual.
+ [Extractor] Eliminar la dependencia de «QWidgets» de la aplicación auxiliar del extractor.
+ [Extractor] Eliminar «KAboutData» del ejecutable de la aplicación auxiliar del extractor.
+ Actualizar varias referencias en el archivo README.
+ [FileContentIndexer] Eliminar argumento no usado de configuración en el constructor y en miembros.
+ [balooctl] Se ha corregido la advertencia de uso obsoleto de «QProcess::start» proporcionando una lista de argumentos vacía.
+ [Motor] Propagar los errores de transacciones (error 425017).
+ [Motor] Se ha eliminado el método no usado «hasChanges» de «{Write}Transaction».
+ No indexar archivos «.ytdl» (error 424925).

### Iconos Brisa

+ Hacer que el icono «keepassxc» sea más fiel al oficial.
+ Se han añadido más enlaces simbólicos para los nuevos nombres de los iconos de «keepassxc» para la bandeja del sistema (error 425928).
+ Se ha añadido otro alias para el icono «keepass» (error 425928).
+ Se ha añadido el icono para el tipo MIME del proyecto Godot.
+ Se ha añadido un icono para el instalador Anaconda.
+ Se han añadido iconos de 96 píxeles para los lugares.
+ Se han eliminado lugares sin uso para comunicar.
+ Ejecutar «application-x-bzip-compressed-tar» a través de <code>scour</code> (error 425089).
+ Hacer que «application-gzip» sea un enlace simbólico a «application-x-gzip» (error 425059).
+ Se han añadido alias para los iconos de MP3 (error 425059).

### Módulos CMake adicionales

+ Se han eliminado los ceros iniciales de la versión numérica de los números en el código C++.
+ Se ha añadido un tiempo límite para las llamadas a «qmlplugindump».
+ Se ha añadido un módulo para encontrar «WaylandProtocols».
+ Invocar «update-mime-database» con el parámetro «-n».

### Integración con Frameworks

+ Se ha clarificado la declaración de la licencia según el historial de KDElibs.

### KActivitiesStats

+ Usar «Boost::boost» para versiones antiguas de CMake.

### KActivities

+ Se ha eliminado el «X-KDE-PluginInfo-Depends» vacío.

### Herramientas KDE Doxygen

+ Dependencias de documentos en «requirements.txt» y su instalación en «setup.py».
+ Opt-In Display of Library License

### KAuth

+ Usar «Q_DECLARE_OPERATORS_FOR_FLAGS» en el mismo espacio de nombres que la definición de indicadores.

### KBookmarks

+ KBookmarkManager: borrar el árbol de memoria cuando se borra el archivo.

### KCalendarCore

+ Guardar siempre las propiedades «X-KDE-VOLATILE-XXX» como volátiles.
+ Documentar las transiciones de zona horaria esperadas en Praga.

### KCMUtils

+ KCModuleData: Fix headers, improve api doc and rename method
+ Extender «KCModuleData» con las funcionalidades «revertToDefaults» y «matchQuery».
+ Intentar evitar la barra de desplazamiento horizontal en «KCMultiDialog».
+ Añadir «KCModuleData» como clase base para complementos.
+ Permitir que se pueda añadir un botón adicional a «KPluginSelector» (fallo 315829).

### KConfig

+ Hacer «KWindowConfig::allConnectedScreens()» «static» e «internal» (error 425953).
+ Se ha añadido un acceso rápido estándar para «Crear carpeta».
+ Introduce method to query KConfigSkeletonItem default value
+ Remember window sizes on a per-screen-arrangement basis
+ Extract code to get list of connected screens into a re-usable function
+ Add functions to save and restore window positions on non-Wayland platforms (bug 415150)

### KConfigWidgets

+ Avoid swapping defaults to read KConfigSkeletonItem default value
+ Fix KLanguageName::nameForCodeInLocale for codes that QLocale doesn't know about
+ KLanguageName::allLanguageCodes: Take into account there can be more than one locale directory
+ KConfigDialog: tratar de evitar las barras de desplazamiento horizontales.
+ Función que devuelve la lista de códigos de idiomas.

### KContacts

+ Addressee::parseEmailAddress(): Check length of full name before trimming

### KCoreAddons

+ Add *.kcrash glob pattern to KCrash Report MIME type
+ Usar «Q_DECLARE_OPERATORS_FOR_FLAGS» en el mismo espacio de nombres que la definición de indicadores.
+ No esperar eventos «fam» de forma indefinida (error 423818).
+ [KFormat] Allow formatting values to arbitrary binary units
+ Permitir que se pueda usar «KPluginMetadata» desde QML.
+ [KFormat] Se ha corregido el ejemplo binario.

### KDAV

+ metainfo.yaml: Add tier key and tweak description

### KDeclarative

+ [KKeySequenceItem] Hacer que funcionen los accesos rápidos Meta+Mayúsculas+núm.
+ Exponer la propiedad «checkForConflictsAgainst».
+ Se ha añadido la nueva clase «AbstractKCM».
+ Port KRunProxy away from KRun

### KDED

+ org.kde.kded5.desktop: Add missing required key "Name" (bug 408802)

### KDocTools

+ Se ha actualizado «contributor.entities».
+ Use placeholder markers

### KEmoticons

+ Recuperar la información de la licencia de «EmojiOne».

### KFileMetaData

+ Convert old Mac line endings in lyrics tags (bug 425563)
+ Se ha portado al nuevo módulo «FindTaglib» de ECM.

### KGlobalAccel

+ Load service files for shortcuts for applications data dir as fallback (bug 421329)

### KI18n

+ Fix possible preprocessor race condition with i18n defines

### KIO

+ StatJob: make mostLocalUrl work only with protoclClass == :local
+ KPropertiesDialog: Cargar también complementos con metadatos JSON.
+ Se ha revertido «[KUrlCompletion] No añadir / a las carpetas completadas» (fallo 425387).
+ Se ha simplificado el constructor de «KProcessRunner».
+ Allow CCBUG and FEATURE keywords for bugs.kde.org (bug )
+ Update help text for editing the app command in a .desktop entry to comply with current spec (bug 425145)
+ KFileFilterCombo: No añadir la opción «allTypes» si solo existe un elemento.
+ Usar «Q_DECLARE_OPERATORS_FOR_FLAGS» en el mismo espacio de nombres que la definición de indicadores.
+ Se ha corregido un fallo potencial al borrar un menú en el manejador de eventos (fallo 402793).
+ [filewidgets] Fix KUrlNavigatorButton padding on breadcrumb (bug 425570)
+ ApplicationLauncherJob: tweak docs
+ Corregir los elementos sin traducir de «kfileplacesmodel».
+ ApplicationLauncherJob: fix crash if there's no open-with handler set
+ Rename "Web Shortcuts" KCM to "Web Search Keywords"
+ KFileWidget: reparse config to grab dirs added by other instances of the app (bug 403524)
+ Mover el módulo «kcm» de los accesos rápidos web a la categoría de búsqueda.
+ Automatically remove trailing whitespace instead of showing a warning
+ Avoid systemd launched applications from closing forked children (bug 425201)
+ smb: fix share name availability check
+ smb: keep stderr of net commands on add/remove (bug 334618)
+ smb: rejigger guest allowed check and publish as areGuestsAllowed
+ KFileWidget: Borrar las URL antes de reconstruir la lista.
+ KFileWidget: remove default URLs top path combo
+ KFilePlacesModel: add default places when upgrading from older version
+ Fix 2-years regression in KUrlComboBox, setUrl() didn't append anymore

### Kirigami

+ kirigami.pri: se ha añadido «managedtexturenode.cpp».
+ make buddyfor custom positioning work again
+ Account for More button size if we already know it will be visible
+ Add a property to ToolBarLayout to control how to deal with item height (bug 425675)
+ Hacer que las etiquetas de las casillas de verificación se puedan volver a leer.
+ Permitir «displayComponent» para las acciones.
+ Hacer que los submenús sean visibles.
+ Corregir la declaración anticipada de «QSGMaterialType».
+ Make OverlaySheet header and footer use appropriate background colors
+ Check low power environment variable for value, not just if it is set
+ Add setShader function to ShadowedRectangleShader to simplify setting shaders
+ Añadir una versión de baja potencia del archivo de funciones «sdf».
+ Make overloads of sdf_render use the full-argument sdf_render function
+ Remove need for core profile defines in shadowedrect shader main
+ Base inner rectangle on outer rectangle for bordered shadowedrectangle
+ Use the right shaders for ShadowedTexture when using core profile
+ Add "lowpower" versions of the shadowedrectangle shaders
+ Inicializar el miembro «m_component» en «PageRoute».
+ Corregir el «SwipeListItem» de Material.
+ new logic to remove acceleration marks (bug 420409)
+ Se ha eliminado «forceSoftwarerendering» por ahora.
+ just show the source item on software rendering
+ a software fallback for the shadowed texture
+ Usar la nueva propiedad «showMenuArrow» en el fondo de la flecha de menú.
+ don't hide the header when overshooting up
+ Se ha movido «ManagedTextureNode» a su propio archivo.
+ ToolBarLayout: Add spacing to visibleWidth if we're displaying the more button
+ Do not override Heading pixel size in BreadCrumbControl (bug 404396)
+ Actualizar plantilla de aplicación.
+ [notificación pasiva] Definir el relleno explícitamente (error 419391).
+ Activar recorte en la «StackView» de «GlobalDrawer».
+ Eliminar la opacidad del «PrivateActionToolButton» desactivado.
+ Hacer un control de «ActionToolBar».
+ swipenavigator: add control over which pages are displayed
+ Declare the underlying type of the DisplayHint enum to be uint
+ Se ha añadido «ToolBarLayout/ToolBarLayoutDelegate» al archivo «pri».
+ kirigami.pro: Usar la lista de archivos de código fuente de «kirigami.pri».
+ Do not delete incubators in completion callback
+ Ensure menuActions remains an array instead of a list property
+ Add return type to DisplayHint singleton lambda
+ Account for item height when vertically centering delegates
+ Always queue a new layout, even if we are currently layouting
+ Se ha revisado el diseño de «InlineMessage» usando anclajes.
+ Usar la anchura completa para comprobar si caben todas las acciones.
+ Se ha añadido un comentario sobre el espacio adicional para centrar.
+ Se ha corregido la comprobación del texto de ayuda emergente en «PrivateActionToolButton».
+ Workaround qqc2-desktop-style ToolButton not respecting non-flat IconOnly
+ Preferir el plegado de las acciones «KeepVisible» sobre ocultar las acciones «KeepVisible» posteriormente.
+ Ocultar todas las acciones que siguen cuando la primera acción se oculta.
+ Se ha añadido una señal de notificación para «ToolBarLayout::actions» que se emite en el momento oportuno.
+ Exponer la acción al separador de «ActionsMenu».
+ Se ha cambiado el nombre de la propiedad «kirigamiAction» del «loaderDelegate» de «ActionsMenu» a «action».
+ Se ha añadido el «loaderDelegate» que faltaba para comprobar la visibilidad.
+ Ocultar los delegados de acciones hasta que estén posicionados.
+ ToolBarLayout: Se ha sustituido la carga relajada personalizada de delegados con «QQmlIncubator».
+ Mover «displayHintSet» a C++ para que se pueda llamar desde aquí.
+ Permitir el uso del modo de derecha a izquierda en «ToolBarLayout».
+ Se ha añadido la propiedad «minimumWidth» a «ToolBarLayout».
+ Se ha revisado «PrivateActionToolButton» para mejorar su rendimiento.
+ Desaconsejar el uso de «ActionToolBar::hiddenActions».
+ Definir las sugerencias de diseño para «ActionToolBar».
+ Usar el método no desaconsejado «DisplayHint» en «ToolBarPageHeader».
+ Mostrar errores de los componentes si falla la inicialización de los elementos del delegado.
+ Ocultar los delegados de las acciones que se han eliminado.
+ Borrar los elementos del delegado completo/iconos durante la destrucción del delegado.
+ Forzar la visibilidad de elementos del delegado completo/iconos.
+ Usar «ToolBarLayout» para el diseño de «ActionToolBar».
+ Se han añadido algunos comentarios a «ToolBarLayout::maybeHideDelegate».
+ Se ha añadido la propiedad «visibleWidth» a «ToolBarLayout».
+ Se ha introducido el objeto nativo «ToolBarLayout».
+ Se ha movido «DisplayHint» de la acción al archivo de enumeradores en C++.
+ Se han añadido los iconos usados en la página «acerca de» a la macro de empaquetado de iconos de CMake.

### KNewStuff

+ Se han corregido los problemas de sincronización con el diálogo de QtQuick (error 417985).
+ Se ha eliminado el «X-KDE-PluginInfo-Depends» vacío.
+ Eliminar las entradas si ya no satisfacen el filtro (error 425135).
+ Se ha corregido el caso del borde donde se atasca «KNS» (error 423055).
+ Hacer que la tarea del trabajo interno de «kpackage» sea menos frágil (error 425811).
+ Eliminar el archivo descargado cuando se usa la instalación de «kpackage».
+ Permitir un estilo diferente de «knsrc» de «kpackage» al que recurrir.
+ Contemplar la notación «/*» para «RemoveDeadEntries» (error 425704).
+ Hacer que sea más fácil obtener motores ya inicializados de la caché.
+ Se ha añadido una búsqueda inversa para las entradas basadas en sus archivos instalados.
+ Usar la notación «/*» para la descompresión de subdirectorios y permitir la descompresión de subdirectorios si el archivo es un archivo comprimido.
+ Se ha corregido la inundación de advertencias «Pixmap es un mapa de bits nulo».
+ Eliminar las barras del nombre de entrada cuando se interpreta como una ruta (error 417216).
+ Se ha corregido un fallo intermitente con la tarea «KPackageJob» (error 425245).
+ Usar la misma ruleta para la carga y la inicialización (error 418031).
+ Se ha eliminado el botón de detalles (error 424895).
+ Ejecutar el guion de desinstalación de forma asíncrona (error 418042).
+ Desactivar la búsqueda cuando no esté disponible.
+ Ocultar la ruleta de más descargas al actualizar/instalar (error 422047).
+ No añadir el directorio de destino de la descarga a los archivos de las entradas.
+ Eliminar el carácter «*» cuando se pasa un directorio a un guion.
+ No ceder el foco al primer elementos en el modo de vista de iconos (fallo 424894).
+ Evitar el inicio innecesario del trabajo de desinstalación.
+ Se ha añadido una opción «RemoveDeadEntries» a los archivos «knsrc» (error 417985).
+ [Diálogo de QtQuick] Se ha corregido la última instancia de icono de actualización incorrecto.
+ [Diálogos QtQuick] Usar iconos de desinstalación más apropiados.

### Framework KPackage

+ No borrar la raíz del paquete si se ha borrado el paquete (error 410682).

### KQuickCharts

+ Se ha añadido una propiedad «fillColorSource» a los gráficos de líneas.
+ Calcular el suavizado según el tamaño del elemento y la proporción de píxeles del dispositivo.
+ Usar el promedio de tamaño en lugar del máximo para determinar la suavidad de la línea.
+ Cantidad base de suavizado en gráficos de líneas según el tamaño del gráfico.

### KRunner

+ Se ha añadido una plantilla para el lanzador de Python.
+ Se ha actualizado la plantilla para la compatibilidad con la KDE Store y se ha mejorado el archivo «README».
+ Permitir el uso de la sintaxis de lanzadores en los lanzadores de «dbus».
+ Guardar «RunnerContext» tras cada sesión coincidente (error 424505).

### KService

+ Implementar «invokeTerminal» en Windows el directorio de trabajo, la orden y el entorno.
+ Corregir el orden de preferencia de la aplicación para tipos MIME con herencia múltiple (error 425154).
+ Empaquetar el tipo de servicio de la aplicación en un archivo «qrc».
+ Expandir el carácter de tilde al leer el directorio de trabajo (error 424974).

### KTextEditor

+ Modo VI: Hacer que se puede acceder directamente al pequeño registro de borrado (-).
+ Modo VI: Copiar el comportamiento de los registros numerados de «vim».
+ Modo VI: Simplificar la implementación de «append-copy».
+ Hacer que «buscar la selección» funcione si no hay selección.
+ El modo multilínea solo tiene sentido para expresiones regulares multilínea.
+ Añadir un comentario sobre la comprobación «pattern.isEmpty()».
+ Portar la interfaz de búsqueda para que use QRegularExpression en lugar de QRegExp.
+ Modo VI: Implementar «append-copy».
+ Acelerar *mucho* la carga de archivos grandes.
+ Mostrar el nivel de ampliación solo cuando no es del 100%.
+ Se ha añadido un indicador de ampliación a la barra de estado.
+ Se ha añadido una opción de configuración separada para la vista previa de la coincidencia paréntesis.
+ Mostrar una vista previa de la línea del correspondiente paréntesis abierto.
+ Permitir un mayor control sobre la invocación de modelos de completación cuando no se usa la invocación automática.

### Framework KWallet

+ Evitar una colisión de nombres con una macro en «ctype.h» de OpenBSD.

### KWidgetsAddons

+ Se ha añadido «KRecentFilesMenu» para sustituir «KRecentFileAction».

### KWindowSystem

+ Instalar los complementos de la plataforma en un directorio que no tenga puntos en su nombre (fallo 425652).
+ [xcb] Geometría de icono escalada correctamente en todas partes.

### KXMLGUI

+ Permitir optar por no recordar posiciones de las ventanas en X11 (fallo 415150).
+ Guardar y restaurar la posición de la ventana principal (error 415150).

### Framework de Plasma

+ [PC3/BusyIndicator] Evitar que se ejecute una animación no visible.
+ No usar «highlightedTextColor» en «TabButtons».
+ Se ha eliminado «Layout.minimumWidth» de «Button» y de «ToolButton».
+ Usar la propiedad «spacing» para el espaciado entre los iconos y las etiquetas de «Button/ToolButton».
+ Añadir «private/ButtonContent.qml» para «Buttons» y «ToolButtons» de PC3.
+ Cambiar «implicitWidth» e «implicitHeight» de «Button» y «ToolButton» de PC3 para tener en cuenta los valores insertados.
+ Se han añadido «implicitWidth» e «implicitHeight» a «ButtonBackground».
+ Corregir el valor predeterminado incorrecto para «PlasmaExtras.ListItem» (fallo 425769).
+ No permitir que el fondo sea más pequeño que el SVG (fallo 424448).
+ Usar «Q_DECLARE_OPERATORS_FOR_FLAGS» en el mismo espacio de nombres que la definición de indicadores.
+ Hacer que el aspecto visual de «BusyIndicator» de PC3 mantenga la proporción 1:1 (fallo 425504).
+ Usar «ButtonFocus» y «ButtonHover» en «ComboBox» de PC3.
+ Usar «ButtonFocus» y «ButtonHover» en «RoundButton» de PC3.
+ Usar «ButtonFocus» y «ButtonHover» en «CheckIndicator» de PC3.
+ Unificar el comportamiento plano/normal de los «Buttons/ToolButtons» de PC3 (fallo 425174).
+ Hacer que «Heading» use «Label» de PC3.
+ [PlasmaComponents3] Hacer que el texto de las casillas de verificación rellene su distribución.
+ Proporcionar «implicitWidth» e «implicitHeight» al deslizador de PC2.
+ Copiar archivos en lugar de enlaces simbólicos rotos.
+ Se han corregido los márgenes de «toolbutton-hover» en «button.svg» (fallo #425255).
+ [PlasmaComponents3] Refactorización muy pequeña del código de sombreado de «ToolButton».
+ [PlasmaComponents3] Corregir la condición inversa para la sombra plana de «ToolButton».
+ [PlasmaComponents3] Descartar los símbolos &amp; de los mnemónicos del texto de las ayudas emergentes.
+ Dibujar el indicador del foco solo cuando obtengamos el foco a través del teclado (fallo 424446).
+ Descartar los tamaños mínimos implícitos de los botones PC2 y PC3.
+ Añadir el equivalente PC3 para ListItem de PC2.
+ Corregir SVG de la barra de herramientas.
+ [pc3] Hacer que «ToolBar» esté más alineado con el de «qqc2-desktop-style».
+ Exponer los metadatos de la miniaplicación en «AppletInterface».
+ No truncar DPR a un entero en caché ID.
+ Se ha añadido la propiedad «timeout» a ToolTipArea.
+ Definir el tipo del diálogo con indicadores si el tipo es «Dialog::Normal».

### Purpose

+ Aplicar los datos de configuración iniciales cuando se carga un archivo UI de configuración.
+ Restaurar el comportamiento de «AlternativesView».
+ [jobcontroller] Desactivar proceso separado.
+ Se ha reelaborado el manejo de la vista de trabajos (fallo 419170).

### QQC2StyleBridge

+ Se han corregido las secuencias de accesos rápidos «StandardKey» en «MenuItem» que se mostraban como números.
+ No usar la altura/anchura del padre para el tamaño implícito de «ToolSeparator» (fallo 425949).
+ Usar el estilo «foco» en los botones de herramientas no planos solo cuando se pulsan.
+ Añadir relleno superior e inferior a «ToolSeparator».
+ Hacer que «ToolSeparator» respete los valores «topPadding» y «bottomPadding».
+ Hacer que «MenuSeparator» use la altura calculada del fondo, no la altura implícita.
+ Corregir los botones de herramienta con menus usando el nuevo Brisa.
+ Dibujar el control «CheckBox» completamente usando «QStyle».

### Solid

+ Añadir el método estático «storageAccessFromPath», necesario para https://phabricator.kde.org/D28745

### Sindicación

+ Se ha corregido la excepción de la licencia.

### Resaltado de sintaxis

+ Convertir todos los temas a nuevas claves para los colores del editor.
+ Cambiar el formato JSON del tema, usar nombres de enumeración de metaobjetos para los colores del editor.
+ Comprobar «kateversion &gt;= 5.62» para «fallthroughContext» sin «fallthrough="true"» y usar «attrToBool» para atributos booleanos.
+ Se ha añadido la definición de sintaxis para «todo.txt».
+ Corregir «matchEscapedChar()»: el último carácter de una línea se ignora.
+ Corregir isDigit(), isOctalChar() y isHexChar(): Solo deben comparar caracteres ASCII.
+ Generar resúmenes de temas.
+ Añadir metadatos del tema al encabezado de las páginas HTML que generamos.
+ Comenzar a generar la página de la colección de temas.
+ Se ha cambiado «Predeterminado» por «Brisa clara».
+ Varnish, Vala y TADS3: Usar el estilo de color predeterminado.
+ Se ha mejorado el tema de colores «Vim oscuro».
+ Añadir el tema de color «Vim oscuro».
+ Se han añadido archivos de temas de resaltado de sintaxis para JSON.
+ Ruby/Rails/RHTML: Añadir «spellChecking» en «itemDatas».
+ Ruby/Rails/RHTML: Usar el estilo de color predeterminado y otras mejoras.
+ LDIF, VHDL, D, Clojure y ANS-Forth94: Usar el estilo de color predeterminado.
+ ASP: Usar el estilo de color predeterminado y otras mejoras.
+ Dejar que gane objective-c para los archivos «.m».
+ «.mm» es preferiblemente Objective-C++ frente a «meta math».
+ Usar «notAsciiDelimiters» solo con un carácter no ASCII.
+ Optimizar «isWordDelimiter(c)» con un carácter ASCII.
+ Optimizar «Context::load».
+ Usar «std::make_shared», que eliminar la asignación en el bloque de control.
+ Añadir la licencia correcta para actualizar Scripty.
+ Mover el guion de actualización para «kate-editor.org/syntax» al repositorio «syntax».
+ SELinux CIL y Scheme: Actualizar los colores de los paréntesis para los temas oscuros.
+ POV-Ray: Usar el estilo de color predeterminado.
+ Usar el script NSIS de instalación de Krita como entrada de ejemplo, tomado de krita.git.
+ Añadir sugerencia para actualizar el guion del sitio web.
+ Añadir un ejemplo mínimo de plantilla para Django.
+ Actualizar referencias tras los últimos cambios de «hl».
+ CMake: Corregir colores ilegibles en temas oscuros y otras mejoras.
+ Añadir ejemplo de tubería y «ggplot2».
+ Se ha corregido un error de nombres para el resaltado de Varnish.
+ Usar resaltado de sintaxis correcto para 68k ASM: Motorola 68k (VASM/Devpac)
+ Corregir el XML para que sea válido respetando a XSD.
+ BrightScript: Se ha añadido sintaxis para excepciones.
+ Se han mejorado los comentarios en algunas definiciones de sintaxis (parte 3).
+ Guiones de R: usar el estilo de color predeterminado y otras mejoras.
+ PicAsm: Corregir el resaltado de palabras clave del preprocesador desconocidas.
+ Se han mejorado los comentarios en algunas definiciones de sintaxis (parte 2).
+ Modelines: Eliminar las reglas «LineContinue».
+ Corrección rápida de archivo «hl» dañado.
+ Optimización: Comprobar si estamos en un comentario antes de usar el «##Doxygen» que contiene varias expresiones regulares.
+ Lenguajes ensamblador: Varias correcciones y más contexto resaltado.
+ ColdFusion: Usar el estilo de color predeterminado y sustituir algunas reglas de expresiones regulares.
+ Se han mejorado los comentarios en varias definiciones de sintaxis, parte 1.
+ Usar llaves angulares para la información de contexto.
+ Se ha revertido la eliminación de la marca de orden de bytes.
+ xslt: se ha cambiado el color de las etiquetas XSLT.
+ Ruby, Perl, QML, VRML y xslt: usar el estilo de color predeterminado y mejoras en los comentarios.
+ txt2tags: mejoras y correcciones, usar el estilo de color predeterminado.
+ Se han corregido errores y se ha añadido «fallthroughContext=AttrNormal» para cada contexto de «diff.xml» debido a que todas las reglas contienen «column=0».
+ Se han corregido problemas encontrados por el comprobador estático.
+ Importar el resaltado de sintaxis de «Pure» de https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/pure.xml
+ Se ha corregido el atributo «kateversion».
+ Hacer que el resaltado busque traducciones independientes del orden.
+ Se han corregido el formato de la versión y problemas de espaciado.
+ Importar el resaltado de sintaxis de «Modula-3» de https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/modula-3.xml
+ Se ha corregido el formato de la versión.
+ Se han corregido fallos relacionados con el espaciado encontrados por el comprobador estático.
+ Importar resaltado de sintaxis de «LLVM» de https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/llvm.xml
+ Se han corregido fallos relacionados con el espaciado encontrados por el comprobador estático.
+ Importar «Idris» de https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/idris.xml
+ Se han corregido fallos encontrados por el comprobador estático.
+ Importar «ATS» de https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/ats.xml
+ Desprender solo para datos no creados recientemente.
+ Crear «StateData» bajo demanda.

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.
