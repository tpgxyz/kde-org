---
aliases:
- ../../kde-frameworks-5.72.0
date: 2020-07-04
layout: framework
libCount: 70
---
### KDAV: Nuevo módulo.

Implementación del protocolo DAV con KJobs.

Se pueden usar calendarios y tareas pendientes, usando GroupDAV o CalDAV; los contactos se pueden usar mediante GroupDAV o CardDAV.

### Baloo

+ [Indexadores] Ignorar tipos MIME basados en nombres para las decisiones de indexación inicial (fallo 422085).

### BluezQt

+ Exponer los datos de publicidad del servicio de un dispositivo.

### Iconos Brisa

+ Se han corregido tres iconos de 16 píxeles de «pplication-x-...» para que sean más «perfectos en píxeles» (error 423727).
+ Permitir el uso de escalado superior al 200% en iconos que deben permanecer monocromáticos.
+ Recortar el centro de «window-close-symbolic».
+ Actualización de los iconos de la aplicación «cervisia» y de «action».
+ Se ha añadido una sección «README» sobre «webfont».
+ Usar «grunt-webfonts» en lugar de «grunt-webfont» y desactivar la generación de iconos de enlace simbólico.
+ Se ha añadido el icono «kup» desde el repositorio de Kup.

### Módulos CMake adicionales

+ Se ha eliminado el uso de «png2ico».
+ Tratar con el código CMake de Qt que modifica «CMAKE_SHARED_LIBRARY_SUFFIX».
+ Se ha añadido un módulo para encontrar «FindTaglib».

### Herramientas KDE Doxygen

+ Permitir que «repo_id» de «metainfo.yaml» tenga preferencia sobre el nombre estimado del repositorio.

### KCalendarCore

+ Comprobar si hay errores de escritura en «save()» cuando el disco está lleno (error 370708).
+ Corregir los nombres de los iconos para las tareas pendientes recurrentes.
+ Corregir la serialización de la fecha de inicio de las tareas pendientes recurrentes (error 345565).

### KCMUtils

+ Corregir la señal cambiada para el selector de complementos.

### KCodecs

+ Se han añadido algunos saltos de línea para que el texto no sea demasiado largo en el cuadro de mensaje.

### KConfig

+ Pasar también «locationType» a «KConfigSkeleton» cuando se construye desde el grupo.
+ Hacer que el texto «Cambiar el idioma de la aplicación...» sea más consistente.

### KConfigWidgets

+ Hacer que el texto «Cambiar el idioma de la aplicación...» sea más consistente.

### KCoreAddons

+ «KRandom::random» -&gt; «QRandomGenerator::global()».
+ Marcar como obsoleto «KRandom::random».

### KCrash

+ Add missing declaration of environ, otherwise available only on GNU

### KDocTools

+ Usar un estilo consistente para el letrero de FDL (error 423211).

### KFileMetaData

+ Adapt kfilemetadata to "audio/x-speex+ogg" as recently changed in shared-mime-info

### KI18n

+ Also add quotes around rich text &lt;filename&gt; tagged text

### KIconThemes

+ Descartar asignación en «resetPalette».
+ Devolver «QPalette()» si no disponemos de una paleta personalizada.
+ Respetar «QIcon::fallbackSearchpaths()» (error 405522).

### KIO

+ [kcookiejar] Fix reading "Accept For Session" cookie setting (bug 386325)
+ KDirModel: fix hasChildren() regression for trees with hidden files, symlinks, pipes and character devices (bug 419434)
+ OpenUrlJob: fix support for shell scripts with a space in the filename (bug 423645)
+ Añadir accesos rápidos web para DeepL y ArchWiki.
+ [KFilePlacesModel] Mostrar los dispositivos AFC (Apple File Conduit).
+ Also encode space characters for webshortcut URLs (bug 423255)
+ [KDirOperator] Activar realmente «dirHighlighting» de forma predeterminada.
+ [KDirOperator] Highlight the previous dir when going back/up (bug 422748)
+ [Trash] Handle ENOENT error when renaming files (bug 419069)
+ File ioslave: set nano sec timestamp when copying (bug 411537)
+ Ajustar las URL para los proveedores de búsqueda Qwant (error 423156).
+ File ioslave: Add support for reflink copying (bug 326880)
+ Fix setting of default shortcut in webshortcuts KCM (bug 423154)
+ Ensure readability of webshortcuts KCM by setting minimum width (bug 423153)
+ FileSystemFreeSpaceJob: Emitir un error si el «kioslave» no proporciona los metadatos.
+ [Trash] Remove trashinfo files referencing files/dirs that don't exist (bug 422189)
+ kio_http: Guess the mime-type ourselves if server returns application/octet-stream
+ Marcar como obsoletas las señales «totalFiles» y «totalDirs» debido a que no se emiten.
+ Se ha corregido la recarga de nuevos accesos rápidos web (error 391243).
+ kio_http: Fix status of rename with overwriting enabled
+ Do not interpret name as hidden file or file path (bug 407944)
+ Do not display deleted/hidden webshortcuts (bug 412774)
+ Se ha corregido un fallo de la aplicación al borrar entradas (error 412774).
+ No permitir que se puedan asignar accesos rápidos existentes.
+ [kdiroperator] Usar mejores ayudas emergentes para las acciones «atrás» y «adelante» (error 422753).
+ [BatchRenameJob] Usar «KJob::Items» al notificar la información de avance (error 422098).

### Kirigami

+ [aboutpage] Usar «OverlaySheet» para el texto de licencia.
+ Corregir los iconos del modo plegado.
+ Usar separadores claros para «DefaultListItemBackground».
+ Se ha añadido la propiedad «weight» al separador.
+ [overlaysheet] Evitar la altura fraccionaria para «contentLayout» (error 422965).
+ Corregir la documentación de «pageStack.layers».
+ Volver a introducir el uso de capas en «Page.isCurrentPage».
+ Permitir el uso de color en iconos.
+ Usar el componente interno «MnemonicLabel».
+ Reducir el relleno a la izquierda de la barra de herramientas global cuando no se usa el título.
+ Definir «implicit{Width,Height}» solo para «Separator».
+ Actualizar «KF5Kirigami2Macros.cmake» para que use «https» con el repositorio git para evitar errores cuando intente acceder a él.
+ Corrección: Carga de avatar.
+ Make PageRouterAttached#router WRITE-able
+ Corregir el cierre de «OverlaySheet» cuando se pulsa dentro de su disposición (error 421848).
+ Se ha añadido un identificador que faltaba a «GlobalDrawerActionItem» en «GlobalDrawer».
+ Corregir que «OverlaySheet» sea demasiado alto.
+ Corregir el ejemplo de código «PlaceholderMessage».
+ Generar el borde en la parte inferior de los grupos.
+ Usar un componente LSH.
+ Añadir fondo a las cabeceras.
+ Mejor manejo del plegado.
+ Mejor presentación para los elementos de la cabecera de las listas.
+ Añadir la propiedad «bold» a «BasicListItem».
+ Corregir «Kirigami.Units.devicePixelRatio=1.3» cuando debería ser «1.0» para 96dpi.
+ Ocultar el asa de «OverlayDrawer» cuando no es interactivo.
+ Ajustar los cálculos de «ActionToolbarLayoutDetails» para hacer un mejor uso del estado real de la pantalla.
+ ContextDrawerActionItem: Preferir la propiedad «text» sobre «tooltip».

### KConfigWidgets

+ Integrar los «KJob::Unit::Items» (fallo 422098).

### KJS

+ Corregir un fallo al usar «KJSContext::interpreter».

### KNewStuff

+ Mover el texto explicativo de la cabecera de la hoja a la cabecera de la lista.
+ Corregir rutas del «script» de instalación y de descompresión.
+ Ocultar el «ShadowRectangle» para las vistas previas no cargadas (error 422048).
+ No permitir que el contenido exceda los delegados de la cuadrícula (error 422476).

### KNotification

+ No usar «notifybysnore.h» en MSYS2.

### KParts

+ Desaconsejar el uso de «PartSelectEvent» y relacionados.

### KQuickCharts

+ Elide value Label of LegendDelegate when there isn't enough width
+ Corrección para el error «C1059: expresión no constante...».
+ Tener en cuenta la anchura de la línea al comprobar los límites.
+ No usar «fwidth» al generar las líneas de los gráficos de líneas.
+ Reescribir «removeValueSource» para que no use «QObjects» destruidos.
+ Usar «insertValueSource» en «Chart::appendSource».
+ Inicializar correctamente «ModelHistorySource::{m_row,m_maximumHistory}».

### KRunner

+ Corregir «RunnerContextTest» para que no dé por supuesta la existencia de «.bashrc».
+ Use embedded JSON metadata for binary plugins &amp; custom for D-Bus plugins
+ Emitir «queryFinished» cuando hayan terminado todos los trabajos de la consulta actual (error 422579).

### KTextEditor

+ Hacer que «ir a línea» funcione hacia atrás (error 411165).
+ fix crash on view deletion if ranges are still alive (bug 422546)

### Framework KWallet

+ Introduce three new methods that return all "entries" in a folder

### KWidgetsAddons

+ Fix KTimeComboBox for locales with unusual characters in formats (bug 420468)
+ KPageView: Eliminar «pixmap» invisible en la parte derecha de la cabecera.
+ KTitleWidget: Mover de la propiedad «QPixmap» a la propiedad «QIcon».
+ Desaconsejar el uso de parte de la API de «KMultiTabBarButton/KMultiTabBarTab» que usa «QPixmap».
+ KMultiTabBarTab: Hacer que la lógica del estado de «styleoption» siga a «QToolButton» todavía más.
+ KMultiTabBar: No mostrar botones marcados en «QStyle::State_Sunken».
+ [KCharSelect] Initially give focus to the search lineedit

### KWindowSystem

+ [xcb] Send correctly scaled icon geometry (bug 407458)

### KXMLGUI

+ Usar «kcm_users» en lugar de «user_manager».
+ Usar la nueva API «KTitleWidget::icon/iconSize».
+ Mover «Cambiar idioma de la aplicación» al menú Preferencias (fallo 177856).

### Framework de Plasma

+ Mostrar una advertencia más clara si no se encuentra el KCM solicitado.
+ [spinbox] No usar elementos de QQC2 cuando se debe usar PlasmaComponents (fallo 423445).
+ Componentes de Plasma: Restaurar el control de color perdido de la etiqueta de TabButton.
+ Introducción de PlaceholderMessage.
+ [calendario] Reducir el tamaño de la etiqueta del mes.
+ [ExpandableListItem] Usar la misma lógica para la acción y la visibilidad del botón de flecha.
+ [Sombras de diálogos] Portar a la API de sombreado de «KWindowSystem» (fallo 416640).
+ Crear enlace simbólico de «widgets/plasmoidheading.svgz» en Brisa claro/oscuro.
+ Corregir «Kirigami.Units.devicePixelRatio=1.3» cuando debería ser «1.0» para 96dpi.
+ Añadir propiedad para acceder al elemento del cargador de «ExpandableListItem».

### Prison

+ Desaconsejar el uso de «AbstractBarcode::minimumSize()» también para el compilador.

### Purpose

+ Usar explícitamente unidades de Kirigami en lugar de usar unidades de Plasma implícitamente.
+ Portar los diálogos de trabajos a «Kirigami.PlaceholderMessage».

### QQC2StyleBridge

+ Definir «selectByMouse» como «true» en «SpinBox».
+ Corregir que los iconos de menú sean borrosos con algunos tamaños de letra (fallo 423236).
+ Evitar que los delegados y las barras de desplazamiento se solapen en los desplegables de las cajas combinadas.
+ Fix Slider vertical implicitWidth and a binding loop
+ Make ToolTips more consistent with Breeze widget style tooltips
+ Hacer que «SpinBox» sea editable por defecto.
+ Se han corregido advertencias de conexiones en Menu.qml.

### Solid

+ Se ha activado el motor UDisks2 en FreeBSD de forma incondicional.

### Sonnet

+ Fix "Using QCharRef with an index pointing outside the valid range of a QString"
+ Se ha restaurado el comportamiento de detección automática predeterminado.
+ Se ha corregido el lenguaje predeterminado (error 398245).

### Sindicación

+ Se han corregido un par de cabeceras de la licencia BSD-2-Clause.

### Resaltado de sintaxis

+ Cmake: Actualizaciones para CMake 3.18.
+ Se ha añadido el resaltado de sintaxis para el lenguaje Snort/Suricata.
+ Añadir el lenguaje YARA.
+ Se ha eliminado el el binario discontinuado «json» en favor de «cbor».
+ JavaScript/TypeScript: resaltar sintaxis de etiquetas en plantillas.

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.
