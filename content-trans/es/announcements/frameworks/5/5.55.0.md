---
aliases:
- ../../kde-frameworks-5.55.0
date: 2019-02-09
layout: framework
libCount: 70
---
### Baloo

- [tags_kio] Desactivar el acceso con URL que contienen una barra doble, como «tags://» (error 400594).
- Instanciar QAplication antes que KCrash/KCatalog.
- Ignorar todas las señales «deviceAdded» de Solid que no son de almacenamiento.
- Usar K_PLUGIN_CLASS_WITH_JSON, que es más amigable.
- Eliminar las comprobaciones de Qt 5.10 porque ahora es la versión mínima necesaria.

### Iconos Brisa

- Añadir el icono de cursores para KCM.
- Añadir y cambiar el nombre de algunos iconos y enlaces simbólicos de YaST.
- Mejorar el icono de la campana de notificación usando el diseño de KAlarm (error 400570).
- Añadir «yast-upgrade».
- Mejorar los iconos «weather-storm-*» (error 403830).
- Añadir enlaces simbólicos para «font-otf», semejantes a los de «font-ttf».
- Añadir iconos «edit-delete-shred» correctos (error 109241).
- Añadir iconos de recortar márgenes y de recortar a la selección (error 401489).
- Borrar los enlaces simbólicos para «edit-delete-shred» en preparación para sustituirlos por iconos reales.
- Cambiar el nombre del icono KCM de actividades.
- Añadir el icono KCM de actividades.
- Añadir icono KCM para escritorios virtuales.
- Añadir iconos KCM para pantalla táctil y para bordes de la pantalla.
- Corregir los nombres de los iconos relacionados con las preferencias de compartir archivos.
- Añadir icono para «preferences-desktop-effects».
- Añadir un icono para las preferencias del tema Plasma.
- Mejorar el contraste de «preferences-system-time» (error 390800).
- Añadir un nuevo icono «preferences-desktop-theme-global».
- Añadir icono de herramientas.
- El icono «document-new» sigue el esquema de color del texto.
- Añadir un icono «preferences-system-splash» para el KCM de la pantalla de bienvenida.
- Incluir «applets/22».
- Añadir iconos para el tipo MIME Kotlin (.kt).
- Mejorar el icono «preferences-desktop-cryptography».
- Rellenar la cerradura de «preferences-desktop-user-password».
- Rellenar la cerradura de modo consistente en el icono para cifrado.
- Usar un icono para Kile similar al original.

### Módulos CMake adicionales

- FindGperf: definir SKIP_AUTOMOC en ecm_gperf_generate para el archivo generado.
- Mover «-Wsuggest-override» y «-Wlogical-op» a las preferencias normales del compilador.
- Corregir la generación de bibliotecas de enlace para las clases que contienen constructores de copia borrados.
- Corregir la generación del módulo qmake para Qt 5.12.1.
- Usar más «https» en los enlaces.
- API de dox: añadir las entradas que faltaban para algunos módulos y para encontrar módulos.
- FindGperf: mejorar la API de dox: ejemplo de uso de marcas.
- ECMGenerateQmlTypes: corregir la API de dox: el título necesita más marcas «---».
- ECMQMLModules: corregir la API de dox: el título coincide con el nombre del módulo y añadir un «Since» que faltaba.
- FindInotify: corregir la API de dox: etiqueta «.rst» y añadir un «Since» que faltaba.

### KActivities

- Corregir para macOS.

### KArchive

- Guardar dos llamadas KFilterDev::compressionTypeForMimeType.

### KAuth

- Eliminar la implementación para pasar QVariants de la interfaz gráfica a auxiliares de KAuth.

### KBookmarks

- Compilar sin D-Bus en Android.
- Usar más constantes.

### KCMUtils

- [kcmutils] Añadir elipsis para buscar etiquetas en KPluginSelector.

### KCodecs

- nsSJISProber::HandleData: Evitar un cuelgue si «aLen» vale 0.

### KConfig

- kconfig_compiler: borrar el operador de asignación y el constructor copia.

### KConfigWidgets

- Compilar sin KAuth ni D-Bus en Android.
- Añadir KLanguageName.

### KCrash

- Comentar por qué se necesita cambiar el «ptracer».
- [KCrash] Establecer un conector para permitir el cambio de «ptracer».

### KDeclarative

- [Vista en rejilla de controles de KCM] Añadir la eliminación de animación.

### Soporte de KDELibs 4

- Corregir algunas banderas de países para que usen todo el pixmap.

### KDESU

- Manejar contraseñas incorrectas cuando se usa «sudo» para que solicite otra contraseña (error 389049).

### KFileMetaData

- exiv2extractor: añadir el uso de bmp, gif, webp y tga.
- Corregir la comprobación de fallos de los datos GPS de exiv.
- Comprobar datos GPS vacíos y con valor cero.
- Añadir la implementación para más tipos MIME a «taglibwriter».

### KHolidays

- holidays/plan2/holiday_ua_uk: actualizar para 2019.

### KHTML

- Añadir metadatos JSON al binario del complemento «khtmlpart».

### KIconThemes

- Compilar sin D-Bus en Android.

### KImageFormats

- xcf: Corregir la opacidad que cae fuera de límites.
- Descomentar las inclusiones de «qdebug».
- tga: Corregir el uso de valores no inicializados en archivos dañados.
- La opacidad máxima es 255.
- xcf: Corregir la aserción en archivos con dos PROP_COLORMAP.
- ras: Corregir la aserción porque «ColorMapLength» es demasiado grande.
- pcx: Corregir un cuelgue en un archivo difuso.
- xcf: Implementar robustez cuando PROP_APPLY_MASK no está presente en el archivo.
- xcf: loadHierarchy: Obedecer al «layer.type» y no a «bpp».
- tga: No permitir más de 8 bits de alfa.
- ras: Devolver «false» si la asignación de memora para la imagen falla.
- rgb: Corregir un desbordamiento de enteros en archivos difusos.
- rgb: Corregir un desbordamiento de búfer del montículo en archivos difusos.
- psd: Corregir cuelgue en archivos difusos.
- xcf: Inicializar «x/y_offset».
- rgb: Corregir cuelgue en imágenes difusas.
- pcx: Corregir cuelgue en imágenes difusas.
- rgb: Corregir cuelgue en archivos difusos.
- xcf: Inicializar el modo de capas.
- xcf: Inicializar la opacidad de capas.
- xcf: Definir el búfer a 0 si se leen menos datos de los que se esperan.
- bzero -&gt; memset
- Corregir varias lecturas y escrituras de OOB en «kimg_tga» y en «kimg_xcf».
- pic: volver a cambiar el tamaño del identificador de la cabecera si no se han leído 4 bytes, como cabía esperar.
- xcf: Rellenar con ceros el búfer si se han leído menos datos de los que se esperaban.
- xcf: Llamar a «setDotsPerMeterX/Y» solo si se encuentra PROP_RESOLUTION.
- xcf: Inicializar «num_colors».
- xcf: Inicializar la propiedad de capa visible.
- xcf: No interpretar enteros como enumeradores que no pueden contener dicho valor entero.
- xcf: No desbordar enteros en la llamada a «setDotsPerMeterX/Y».

### KInit

- KLauncher: manejar los procesos que terminan sin errores (error 389678).

### KIO

- Mejorar los controles del teclado del widget de suma de verificación.
- Añadir una función auxiliar para desactivar redirecciones (útil para «kde-open»).
- Revertir «Refactorizar SlaveInterface::calcSpeed» (error 402665).
- No definir la política CMP0028 de CMake como «old». No tenemos destinos con «::», a menos que se importen.
- [kio] Añadir elipsis a la etiqueta de búsqueda en la sección de cookies.
- [KNewFileMenu] No emitir «fileCreated» cuando se crea un directorio (error 403100).
- Usar (y sugerir el uso) del más amigable K_PLUGIN_CLASS_WITH_JSON.
- Evitar bloquear «kio_http_cache_cleaner» y asegurar la salida con la sesión (error 367575).
- Corregir la comprobación de fallo de «knewfilemenu» y el motivo subyacente para su fallo.

### Kirigami

- Permitir que se puedan elevar solo los botones que poseen un color (error 403807).
- Corregir la altura.
- Usar la misma política de tamaño de márgenes que el el resto de elementos de la lista.
- Operadores «===».
- Permitir el uso del concepto de elementos que se pueden expandir.
- No borrar todo cuando se sustituyen todas las páginas.
- El relleno horizontal es doble que el vertical.
- Tener en cuenta el relleno para calcular las sugerencias de tamaño.
- [kirigami] No usar estilos de tipos de letra claros para cabeceras (2/3) (error 402730).
- Corregir la disposición de «AboutPage» en dispositivos pequeños.
- Hacer que parpadee «stopatBounds» en el visor de migas de pan.

### KItemViews

- [kitemviews] Cambiar la búsqueda del comportamiento del escritorio y actividades para que sea más acorde con otras etiquetas de búsqueda.

### KJS

- Definir SKIP_AUTOMOC para algunos archivos generados para manejar CMP0071.

### KNewStuff

- Corregir la semántica de «ghns_exclude» (error 402888).

### KNotification

- Corregir una fuga de memoria al pasar datos de iconos a Java.
- Eliminar la dependencia de la biblioteca AndroidX.
- Añadir la implementación del canal de notificaciones de Android.
- Hacer que funcionen las notificaciones en Android con el nivel de API &lt; 23.
- Mover las comprobaciones del nivel de API de Android al tiempo de ejecución.
- Eliminar una declaración anticipada que no se usaba.
- Reconstruir AAR cuando cambian las fuentes de Java.
- Construir el lado de Java con Gradle, como AAR en lugar de JAR.
- No depender de la integración del espacio de trabajo de Plasma en Android.
- Buscar la configuración de eventos de notificación en recursos «qrc».

### Framework KPackage

- Hacer que funcionen las traducciones.

### KPty

- Corregir la discordancia «struct/class».

### KRunner

- Eliminar el uso explícito de ECM_KDE_MODULE_DIR, que es parte de ECM_MODULE_PATH.

### KService

- Compilar sin D-Bus en Android.
- Sugerir el uso de K_PLUGIN_CLASS_WITH_JSON.

### KTextEditor

- Qt 5.12.0 tiene problemas en la implementación de expresiones regulares en QJSEngine; los sangradores se pueden comportar de forma incorrecta. Qt 5.12.1 contendrá una solución para ello.
- KateSpellCheckDialog: Eliminar la acción «Selección de corrector».
- Actualizar la biblioteca de JavaScript «underscore.js» a la versión 1.9.1.
- Corregir el error 403422: Volver a permitir el cambio del tamaño del marcador (error 403422).
- SearchBar: Añadir el botón «Cancelar» para detener tareas de larga duración (error 24424).
- Eliminar el uso explícito de ECM_KDE_MODULE_DIR, que es parte de ECM_MODULE_PATH.
- Revisar KateGotoBar.
- ViewInternal: Corregir «Ir al paréntesis correspondiente» en el modo de sobrescritura (error 402594).
- Usar HTTPS, si está disponible, en los enlaces visibles al usuario.
- Revisar KateStatusBar.
- ViewConfig: Añadir opción para pegar en la posición del cursor usando el ratón (error 363492).
- Usar K_PLUGIN_CLASS_WITH_JSON, que es más amigable.

### KWayland

- [servidor] Generar identificadores de toques correctos.
- Hacer que se cumplan las especificaciones de XdgTest.
- Añadir una opción para usar «wl_display_add_socket_auto».
- [servidor] Enviar «org_kde_plasma_virtual_desktop_management.rows» inicialmente.
- Añadir información de filas al protocolo de escritorio virtual de Plasma.
- [cliente] Envolver «wl_shell_surface_set_{clase,título}».
- Vigilar el borrado de recursos en OutputConfiguration::sendApplied.

### KWidgetsAddons

- [KWidgetsAddons] No usar estilos de fuentes ligeras para cabeceras (3/3) (error 402730).

### KXMLGUI

- Compilar sin D-Bus en Android.
- Hacer que KCheckAccelerators sea menos invasiva para las aplicaciones que no enlazan directamente a KXmlGui.

### ModemManagerQt

- Corregir la implementación el operador &gt;&gt; de QVariantMapList.

### Framework de Plasma

- [Plantillas de fondos de pantalla] Añadir la entrada «Comment=» que faltaba en el archivo de escritorio.
- Compartir las instancias de «Plasma::Theme» entre múltiples «ColorScope».
- Hacer que las sombras SVG del reloj sean lógicamente más correctas y visualmente más apropiadas (error 396612).
- [frameworks] No usar estilos de tipos de letra finos para cabeceras (1/3) (error 402730).
- [Diálogos] No alterar la visibilidad de «mainItem».
- Reiniciar «parentItem» cuando «mainItem» cambia.

### Purpose

- Usar K_PLUGIN_CLASS_WITH_JSON, que es más amigable.

### QQC2StyleBridge

- Corregir el tamaño inicial de las listas desplegables (error 403736).
- Definir como modales las listas desplegables emergentes (error 403403).
- Revertir parcialmente 4f00b0cabc1230fdf.
- Ajustar dinámicamente la anchura de las ayudas emergentes largas (error 396385).
- ComboBox: Corregir el delegado por omisión.
- Falsear «mousehover» mientras la lista desplegable está abierta.
- Definir «QStyleOptionState == On» en las listas desplegables en lugar de mostrarlas hundidas para que coincidan con los qwidgets (error 403153).
- Corregir las listas desplegables.
- Mostrar siempre la ayuda emergente encima del resto de cosas.
- Implementar ayudas emergentes sobre «MouseArea».
- No forzar que se muestre el texto de «ToolButton».

### Solid

- Compilar sin D-Bus en Android.

### Sonnet

- No llamar este código si solo tenemos espacio.

### Resaltado de sintaxis

- Corregir el final de la región de plegado en las reglas con «lookAhead=true».
- AsciiDoc: Corregir el resaltado de las directivas de inclusión.
- Añadir el uso de AsciiDoc.
- Se ha corregido un error que causaba un bucle infinito al resaltar archivos de KConfig.
- Comprobar los switches de contexto interminable.
- Ruby: Corregir la expresión regular tras «: » y corregir/mejorar la detección de «HEREDOC» (error 358273).
- Haskell: Hacer que «=» sea un símbolo especial.

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.
