---
aliases:
- ../../kde-frameworks-5.27.0
date: 2016-10-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Nuevos iconos de tipos MIME.

{{<figure src="/announcements/frameworks/5/5.27.0/kf-5.27-mimetypes-icons.png" >}}

### Baloo

- Use la entrada de configuración correcta en la condición de inicio automático.
- Corregir la inserción ordenada (también conocida como «flat_map» como inserción) (error 367991).
- Se ha añadido el cierre de «env» que faltaba del que ha informado Loïc Yhuel (error 353783).
- Transacción no creada =&gt; no intentar interrumpirlos.
- Corregir la asignación «m_env = nullptr» que faltaba.
- Hacer que sea seguro el hilo «Baloo::Query», por ejemplo.
- En sistemas de 64 bits, baloo permite ahora almacenamiento de índices superior a 5 GB (error 364475).
- Permitir «ctime/mtime == 0» (error 355238).
- Manejar la corrupción de la base de datos de índices para «baloo_file»; intentar volver a crear la base de datos o interrumpir si eso falla.

### BluezQt

- Corregido el bloqueo al intentar añadir un dispositivo a un adaptador desconocido (error 364416).

### Iconos Brisa

- Nuevos iconos de tipos MIME.
- Actualizar algunos iconos de KStars (fallo 364981)
- Estilo erróneo «actions/24/format-border-set» (error 368980).
- Añadir icono de aplicación para wayland
- Añadir icono de aplicación para xorg (error 368813)
- Revertir distribuir-aleatorizar, ver-calendario + volver a aplicar la corrección de transformación (error 367082).
- Cambiar los documentos de carpeta de un archivo a varios archivos porque en una carpeta se incluye más de un archivo (error 368224).

### Módulos CMake adicionales

- Asegurarnos de que no añadimos la comprobación de «appstream» dos veces.

### KActivities

- Ordenar las actividades en la caché de forma alfabética por su nombre (error 362774).

### Herramientas KDE Doxygen

- Numerosos cambios en el diseño general de la documentación de API generada.
- Ruta correcta de las etiquetas, dependiendo de si la biblioteca es parte de un grupo o no.
- Búsqueda: Corregir la «href» de las bibliotecas que no son parte de un grupo.

### KArchive

- Se ha corregido una fuga de memoria en «KCompressionDevice» de «KTar».
- KArchive: Se ha corregido una fuga de memoria cuando ya existe una entrada con el mismo nombre.
- Se ha corregido una fuga de memoria en «KZip» cuando se manejan directorios vacíos.
- K7Zip: Corregir algunas fugas de memoria.
- Se ha corregido una fuga de memoria detectada por ASAN cuando falla «open()» en el dispositivo subyacente.
- Se ha eliminado un moldeado incorrecto de «KFilterDev» detectado por ASAN.

### KCodecs

- Se han añadido macros de exportación ausentes en las clases «Decoder» y «Encoder».

### KConfig

- Se ha corregido una fuga de memoria en «SignalsTestNoSingletonDpointer» encontrada por ASAN.

### KCoreAddons

- Registrar «QPair&lt;QString,QString&gt;» como metatipo en «KJobTrackerInterface».
- No convertir como URL una URL que tenga caracteres de comillas dobles.
- Corregir compilación en Windows.
- Se ha corregido un error muy antiguo al eliminar espacios en una URL, como «foo &lt;&lt;url&gt; &lt;url&gt;&gt;».

### KCrash

- Opción de CMake «KCRASH_CORE_PATTERN_RAISE» para reenviar al kernel.
- Cambiar el nivel de registro predeterminado de «Warning» a «Info».

### Soporte de KDELibs 4

- Limpieza. No instalar archivos de inclusión que apuntan a inclusiones no existentes y también eliminar dichos archivos.
- Uso más correcto y con C++11 de «std::remove_pointer».

### KDocTools

- Corregir «checkXML5 imprime el HTML generado en stdout para docbooks válidos» (error 369415).
- Se ha corregido el error que no permitía ejecutar herramientas nativas en el paquete usando «kdoctools» de compilación cruzada.
- Configurar objetivos para compilación cruzada ejecutando «kdoctools» de otros paquetes.
- Permitir compilación cruzada para «docbookl10nhelper».
- Permitir compilación cruzada para «meinproc5».
- Convertir «checkxml5» en un ejecutable de Qt para permitir compilación multiplataforma.

### KFileMetaData

- Mejorar el extractor de «epub», menos «segfaults» (error 361727).
- Hacer que el indexador ODF sea más prueba de errores, comprobar si los archivos están ahí (y si realmente son archivos) (meta.xml + content.xml).

### KIO

- Corregir los esclavos KIO que solo usan TLS 1.0.
- Corregir la rotura de ABI en «kio».
- KFileItemActions: añadir «addPluginActionsTo(QMenu *)».
- Mostrar los botones de copia solo cuando se haya calculado la suma de verificación.
- Añadir información al usuario ausente al calcular sumas de verificación (error 368520).
- Corregir que «KFileItem::overlays» devuelva valores de cadena vacía.
- Se ha corregido el lanzamiento de archivos «.desktop» de terminal con «Konsole».
- Clasificar los montajes «nfs4» como «probablySlow» como «nfs/cifs/..».
- KNewFileMenu: Mostrar el acceso rápido de la acción «Nueva carpeta» (error 366075).

### KItemViews

- En el modo de vista de lista, usar la implementación predeterminada de «moveCursor».

### KNewStuff

- Añadir comprobaciones de «KAuthorized» para permitir la desactivación de «ghns» en «kdeglobals» (error 368240).

### Framework para paquetes

- No generar archivos «appstream» para componentes que no están en «rdn».
- Hacer que «kpackage_install_package» funcione con «KDE_INSTALL_DIRS_NO_DEPRECATED».
- Eliminar la variable no usada «KPACKAGE_DATA_INSTALL_DIR».

### KParts

- Corregir que las URL con una barra final se traten siempre como directorios.

### KPeople

- Corregir la compilación de ASAN («duplicates.cpp» usa «KPeople::AbstractContact», que está en «KF5PeopleBackend»).

### KPty

- Usar la ruta de ECM para encontrar el binario «utempter», más fiable que usar solo el prefijo de cmake.
- Llamar de forma manual al executable auxiliar «utempter» (fallo 364779).

### KTextEditor

- Archivos XML: Eliminar los valores de colores codificados a mano.
- XML: Eliminar los valores de colores codificados a mano.
- Definición del esquema XML: Convertir «version» en un «xs:integer».
- Archivos de definición de resaltado sintáctico: Redondear la versión al siguiente entero.
- Permitir capturas de múltiples caracteres solo en {xxx} para evitar regresiones.
- Permitir sustituciones de expresiones regulares con capturas &gt; 9, como 111 (error 365124).
- Corregir la representación de caracteres que se extienden hasta la siguiente línea; por ejemplo, los subrayados ya no se cortan con algunos tipos de letra o con algunos tamaños de texto (error 335079).
- Se ha corregido un fallo de aplicación: asegurar que el cursor de la pantalla es válido tras plegar texto (error 367466).
- «KateNormalInputMode» necesita volver a ejecutar los métodos de entrada de «SearchBar».
- Tratar de «arreglar» la representación de subrayados y cosas semejantes (error 335079).
- Mostrar el botón «Ver diferencias» solo si está instalado 'diff'.
- Usar un «widget» de mensaje no modal para notificaciones de archivos modificados externamente (error 353712).
- Corrección de regresión: «testNormal» solo funcionaba porque se ejecutaba la prueba a la vez.
- Dividir la prueba de sangrado en ejecuciones separadas.
- Volver a permitir el uso de la acción «Desplegar nodos de nivel superior» (error 335590).
- Se ha corregido un fallo al mostrar mensajes superiores o inferiores múltiples veces.
- Corregir asignación de «eol» en las líneas de modo (error 365705).
- Resaltar los archivos «.nix» como bash; se supone que no tiene efectos dañinos (error 365006).

### Framework KWallet

- Comprobar si «kwallet» está activada en «Wallet::isOpen(nombre)» (error 358260).
- Añadir la cabecera «boost» que faltaba
- Eliminar la búsqueda duplicada de «KF5DocTools».

### KWayland

- [servidor] No enviar la liberación de tecla para las teclas no pulsadas y sin doble pulsación de tecla (error 366625).
- [servidor] Al sustituir la selección del portapapeles, se debe cancelar el anterior «DataSource» (error 368391).
- Añadir la implementación de los eventos «entrar» y «salir» de Surface
- [cliente] Realizar un seguimiento de todas las salidas creadas y añadir método «get» estático.

### KXmlRpcClient

- Convertir las categorías en «org.kde.pim.*».

### NetworkManagerQt

- Necesitamos definir el estado durante la inicialización.
- Sustituir todas las llamadas bloqueantes para la inicialización con una sola llamada bloqueante.
- Usar la interfaz estándar «o.f.DBus.Properties» para la señal «PropertiesChanged» para NM 1.4.0+ (error 367938).

### Iconos de Oxígeno

- Eliminar el directorio incorrecto de «index.theme».
- Introducir prueba de duplicados de los iconos Brisa.
- Convertir todos los iconos duplicados en enlaces simbólicos.

### Framework de Plasma

- Mejorar la salida del rastreador de tiempo.
- [ToolButtonStyle] Corregir la flecha del menú.
- i18n: Manejo de cadenas en archivos «kdevtemplate».
- i18n: Revisión de cadenas en archivos «kdevtemplate».
- Añadir «removeMenuItem» a «PlasmaComponents.ContextMenu».
- Actualizar el icono de KTorrent (fallo 369302)
- [WindowThumbnail] Descartar mapa de bits en eventos de mapeado.
- No incluir «kdeglobals» al tratar con una configuración de caché.
- Corregir «Plasma::knownLanguages».
- Cambiar el tamaño de la vista justo después de definir el contenedor.
- Evitar la creación de un «KPluginInfo» a partir de una instancia de «KPluginMetaData».
- Las tareas en ejecución deben tener algún indicador.
- Líneas de la barra de tareas de acuerdo con RR 128802.
- [AppletQuickItem] Salir del bucle cuando se encuentra un diseño.

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
