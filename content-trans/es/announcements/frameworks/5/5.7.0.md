---
aliases:
- ../../kde-frameworks-5.7.0
date: '2015-02-14'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### General

- Numerosas soluciones de errores para la compilación del próximo Qt 5.5

### KActivities

- Solucionada la función de iniciar y detener actividades
- Se ha solucionado el error de la vista previa de actividades que en ocasiones mostraba un fondo de pantalla incorrecto

### KArchive

- Los archivos temporales se crean en el directorio temp en lugar de en el directorio actual

### KAuth

- Se ha solucionado la generación de archivos del servicio de ayuda KAuth DBus

### KCMUtils

- Se ha solucionado la afirmación cuando las rutas de dbus contenían un «.».

### KCodecs

- Ahora KCharsets admite CP949

### KConfig

- Desde la versión KDE SC 4, kconf_update ya no procesa el archivo *.upd. Se añade «Version=5» al principio del archivo upd para actualizaciones que se deberán aplicar a las aplicaciones Qt5/KF5.
- Se ha corregido el comportamiento de KCoreConfigSkeleton al conmutar un valor cuando se guarda entre medias

### KConfigWidgets

- KRecentFilesAction: corregido el orden de las opciones de menú (de forma que coincida con el orden de kdelibs4)

### KCoreAddons

- KAboutData: se llama a addHelpOption y addVersionOption automáticamente por comodidad y para aumentar la consistencia.
- KAboutData: devuelve "Utilice http://bugs.kde.org para informar de fallos." cuando no hay ninguna otra URLo dirección de correo.
- KAutoSaveFile: ahora allStaleFiles() funciona como se esperaba para los archivos locales; también se ha corregido staleFiles()
- KRandomSequence ahora utiliza int internamente y muestra int-api para evitar la ambigüedad en la versión de 64-bit.
- Definiciones del tipo Mime: los archivos de proyecto *.qmltypes y *.qmlproject también tienen el tipo Mime text/x-qml
- KShell: construye las URLS quoteArgs con QChar::isSpace(); los caracteres de espacio no habituales no se trataban adecuadamente
- KSharedDataCache: se ha corregido la creación del directorio que contiene la caché (error de migración)

### KDBusAddons

- Se ha añadido el método de ayuda KDEDModule::moduleForMessage para escribir demonios más del estilo de kded, como kiod

### KDeclarative

- Se ha añadido el componente de impresión de gráficos
- Se ha añadido un método de sobrecarga para que Formats::formatDuration admita int
- Se han añadido las nuevas propiedades a QPixmapItem y QImageItem
- Se ha corregido el dibujo de QImageItem y QPixmapItem

### Kded

- Ahora permite cargar módulos con metadatos JSON

### KGlobalAccel

- Ahora incluye el componente runtime, lo que lo convierte en una infraestructura del Nivel 3.
- El motor para Windows vuelve a funcionar
- Se ha vuelto a habilitar el motor para Mac
- Se ha corregido KGlobalAccel X11 para que no deje de funcionar al apagarlo en tiempo de ejecución

### KI18n

- Se marcan los resultados como obligatorios para advertir de cuándo se utiliza la API de manera inadecuada
- Se ha añadido la opción de compilación del sistema BUILD_WITH_QTSCRIPT para permitir una menor configuración de funcionalidades en sistemas integrados.

### KInit

- OSX: ahora carga las bibliotecas compartidas correctas en tiempo de ejecución
- Mingw: correcciones de errores de compilación

### KIO

- Se ha corregido para evitar que deje de funcionar al enlazarlo a KIOWidgets utilizando solo una QCoreApplication
- Se ha corregido la edición de los accesos rápidos
- Se ha añadido la opción KIOCORE_ONLY para compilar solo KIOCore y sus programas de ayuda, pero no KIOWidgets ni KIOFileWidgets, con lo que se han reducido enormemente las dependencias necesarias.
- Se ha añadido la clase KFileCopyToMenu, la cual añade Cpiar a / Mover A a los menús emergentes
- SSL-enabled protocols: added support for TLSv1.1 and TLSv1.2 protocols, remove SSLv3
- Fixed negotiatedSslVersion and negotiatedSslVersionName to return the actual negotiated protocol
- Apply the entered URL to the view when clicking the button that switches the URL navigator back to breadcrumb mode
- Fixed two progress bars/dialogs appearing for copy/move jobs
- KIO now uses its own daemon, kiod, for out-of-process services previously running in kded, in order to reduce dependencies; currently only replaces kssld
- Fixed "Could not write to &lt;path&gt;" error when kioexec is triggered
- Fixed "QFileInfo::absolutePath: Constructed with empty filename" warnings when using KFilePlacesModel

### KItemModels

- Fixed KRecursiveFilterProxyModel for Qt 5.5.0+, due to QSortFilterProxyModel now using the roles parameter to the dataChanged signal

### KNewStuff

- Cargar siempre los datos XML de URL remotas

### KNotifications

- Documentation: mentioned the file name requirements of .notifyrc files
- Se ha corregido un puntero descontrolado a KNotification.
- Corregir pérdida de memoria en knotifyconfig
- Instalar cabecera ausente de knotifyconfig

### KPackage

- Se ha cambiado el nombre de la página man de kpackagetool a kpackagetool5
- Se ha corregido la instalación en sistemas de archivos que no distinguen entre mayúsculas y minúsculas

### Kross

- Se ha corregido Kross::MetaFunction para que funcione con el sistema de metaobjetos de Qt5

### KService

- Incluir propiedades desconocidas al convertir KPluginInfo desde KService
- KPluginInfo: fixed properties not being copied from KService::Ptr
- OS X: performance fix for kbuildsycoca4 (skip app bundles)

### KTextEditor

- Fixed high-precision touchpad scrolling
- No emitir «documentUrlChanged» durante la recarga.
- Do not break cursor position on document reload in lines with tabs
- Do not re(un)fold the first line if it was manually (un)folded
- vimode: command history through arrow keys
- Do not try to create a digest when we get a KDirWatch::deleted() signal
- Rendimiento: eliminar inicializaciones globales

### KUnitConversion

- Fixed infinite recursion in Unit::setUnitMultiplier

### KWallet

- Automatically detect and convert old ECB wallets to CBC
- Se ha corregido el algoritmo de cifrado CBC.
- Ensured wallet list gets updated when a wallet file gets removed from disk
- Eliminar un &lt;/p&gt; perdido en texto visible al usuario

### KWidgetsAddons

- Use kstyleextensions to specify custom control element for rendering kcapacity bar when supported, this allow the widget to be styled properly
- Proporcionar un nombre accesible para KLed

### KWindowSystem

- Se ha corregido que «NETRootInfo::setShowingDesktop(bool)» no funcionara en Openbox.
- Se ha añadido el método KWindowSystem::setShowingDesktop(bool) por comodidad
- Correcciones en el manejo del formato de iconos
- Added method NETWinInfo::icccmIconPixmap provides icon pixmap from WM_HINTS property
- Added overload to KWindowSystem::icon which reduces roundtrips to X-Server
- Permitir el uso de _NET_WM_OPAQUE_REGION

### NetworkmanagerQt

- Do not print a message about unhandled "AccessPoints" property
- Added support for NetworkManager 1.0.0 (not required)
- Se ha corregido el manejo de secretos de «VpnSetting».
- Added class GenericSetting for connections not managed by NetworkManager
- Added property AutoconnectPriority to ConnectionSettings

#### Plasma framework

- Fixed errorneously opening a broken context menu when middle clicking Plasma popup
- Trigger button switch on mouse wheel
- Never resize a dialog bigger than the screen
- Undelete panels when applet gets undeleted
- Se han corregido accesos rápidos de teclado.
- Restore hint-apply-color-scheme support
- Reload the configuration when plasmarc changes
- ...

### Solid

- Se han añadido «energyFull» y «energyFullDesign» a «Battery».

### Cambios en el sistema de construcción (módulos extra de cmake).

- Nuevo módulo «ECMUninstallTarget» para crear objetivos de desinstalación.
- Hacer que «KDECMakeSettings» importe «ECMUninstallTarget» por omisión.
- KDEInstallDirs: warn about mixing relative and absolute installation paths on the command line
- Added ECMAddAppIcon module to add icons to executable targets on Windows and Mac OS X
- Fixed CMP0053 warning with CMake 3.1
- No borrar asignaciones de variables en caché en «KDEInstallDirs».

### Integración con Frameworks

- Fix updating of single click setting at runtime
- Multiple fixes to the systemtray integration
- Only install color scheme on toplevel widgets (to fix QQuickWidgets)
- Actualizar las preferencias de «XCursor» en la plataforma X11.

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
