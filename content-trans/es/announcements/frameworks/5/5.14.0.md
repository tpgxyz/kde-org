---
aliases:
- ../../kde-frameworks-5.14.0
date: 2015-09-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### En muchas infraestructuras

- Renombrar las clases privadas para evitar exportarlas por error

### Baloo

- Añadir la interfaz org.kde.baloo al objeto raíz para garantizar la compatibilidad con versiones previas
- Instalar un falso org.kde.baloo.file.indexer.xml para corregir la compilación de plasma-desktop 5.4
- Reorganizar las interfaces de D-Bus
- Usar metadatos JSON en el complemento de kded y corregir el nombre del complemento
- Crear una instancia de la base de datos por proceso (error 350247)
- Impedir que baloo_file_extractor sea matado mientras entrega datos
- Generar archivo de interfaz XML usando qt5_generate_dbus_interface
- Correcciones en el monitor Baloo
- Mover la exportación del URL del archivo al hilo principal
- Asegurarse de que se tienen en cuenta configuraciones en cascada
- No instalar namelink para la biblioteca privada
- Instalar traducciones, advertido por Hrvoje Senjan.

### BluezQt

- No enviar la señal deviceChanged después de que se haya eliminado el dispositivo (error 351051)
- Respetar -DBUILD_TESTING=OFF

### Módulos CMake adicionales

- Añadir macro para generar declaraciones de categorías de registro para Qt5.
- ecm_generate_headers: Añadir la opción COMMON_HEADER y funcionalidad de múltiples cabeceras
- Añadir «-pedantic» para el código de KF5 (cuando se usa gcc o clang)
- KDEFrameworkCompilerSettings: activar iteraciones estrictas solo en el modo de depuración
- Definir la visibilidad por omisión como oculta también para el código en C.

### Integración con Frameworks

- Propagar también los títulos de las ventanas para los diálogos de archivos de sólo carpetas.

### KActivities

- Lanzar solo un cargador de acciones (hilo) cuando las acciones de FileItemLinkingPlugin no están inicializadas (error 351585)
- Corregir los problemas de compilación introducidos al cambiar el nombre clases privadas (11030ffc0)
- Añadir la ruta del archivo Boost que faltaba para compilar en OS X
- Configurar los accesos rápidos que se han pasado a las preferencias de actividad
- Ya funciona la configuración del modo de actividad privada
- Refactorizar las interfaces gráficas de preferencias
- Los métodos básicos de actividades son funcionales
- Interfaz gráfica para la configuración de actividades y borrar ventanas emergentes
- Interfaz gráfica básica para la sección de creación/borrado/configuración de actividades en KCM
- Se ha aumentado el tamaño del fragmento para la carga de resultados
- Se ha añadido una cabecera que faltaba para std::set

### Herramientas KDE Doxygen

- Corrección para Windows: eliminar los archivos existentes antes de sustituirlos con os.rename.
- Usar rutas nativas cuando se llama a Python para corregir compilaciones en Windows

### KCompletion

- Corregir un mal comportamiento al ejecutar OOM en Windows (error 345860)

### KConfig

- Optimizar readEntryGui
- Evitar QString::fromLatin1() en el código generado
- Minimizar las llamadas al costoso QStandardPaths::locateAll()
- Terminar la adaptación a QCommandLineParser (ahora contiene addPositionalArgument)

### Soporte de KDELibs 4

- Adaptar el complemento solid-networkstatus de kded a metadatos json
- KPixmapCache: crear directorio si no existe

### KDocTools

- Sincronizar el archivo user.entities del catalán con la versión inglesa (en).
- Añadir entidades para «sebas» y «plasma-pa»

### KEmoticons

- Rendimiento: poner en caché aquí una instancia de KEmoticons, no un KEmoticonsTheme.

### KFileMetaData

- PlainTextExtractor: activar la rama O_NOATIME en las plataformas libc de GNU
- PlainTextExtractor: hacer que la rama de Linux también funcione sin O_NOATIME
- PlainTextExtractor: corregir la comprobación de errores cuando falla open(O_NOATIME)

### KGlobalAccel

- Iniciar kglobalaccel5 solo cuando es necesario.

### KI18n

- Manejar de forma elegante el hecho de que no haya nueva línea al final del archivo pmap

### KIconThemes

- KIconLoader: corregir reconfigure() olvidando los temas heredados y los directorios de aplicaciones
- Adherirse mejor a las especificaciones de carga de iconos

### KImageFormats

- eps: corregir los includes relacionados con el registro categorizado de Qt

### KIO

- Usar Q_OS_WIN en lugar de Q_OS_WINDOWS
- Hacer que KDE_FORK_SLAVES funcione en Windows
- Desactivar la instalación del archivo de escritorio para el módulo kded de ProxyScout
- Proporcionar ordenación determinista para KDirSortFilterProxyModelPrivate::compare
- Volver a mostrar los iconos de carpetas personalizados (error 350612)
- Mover kpasswdserver de kded a kiod
- Corregir errores de adaptación en kpasswdserver
- Eliminar código heredado para dialogar con versiones realmente antiguas de kpasswdserver.
- KDirListerTest: usar QTRY_COMPARE en ambas sentencias para corregir una carrera mostrada por CI
- KFilePlacesModel: implementar el antiguo TODO sobre el uso de trashrc en lugar de un KDirLister en estado avanzado.

### KItemModels

- Nuevo modelo de proxy: KConcatenateRowsProxyModel
- KConcatenateRowsProxyModelPrivate: corregir el manejo de layoutChanged.
- Más comprobaciones en la selección tras ordenar.
- KExtraColumnsProxyModel: se ha corregido un error en sibling() que rompía, por ejemplo, las selecciones.

### Framework para paquetes

- kpackagetool puede desinstalar paquetes procedentes de un archivo de paquete.
- kpackagetool es ahora más inteligente al encontrar el tipo de servicio correcto.

### KService

- KSycoca: comprobar marcas de hora y ejecutar kbuildsycoca si es necesario. Ya no existe dependencia de kded.
- No cerrar ksycoca justo después de su apertura.
- KPluginInfo maneja ahora correctamente los metadatos de FormFactor

### KTextEditor

- Fusionar la asignación de TextLineData y el bloque de contadores de referencias.
- Cambiar el acceso rápido de teclado predeterminado para «ir a la línea de edición anterior»
- Correcciones en el resaltado de comentarios en la sintaxis de Haskell
- Acelerar la aparición de la ventana emergente de terminación de código
- minimap: Intentar mejorar el aspecto y comportamiento (error 309553)
- comentarios anidados en el resaltado de sintaxis para Haskell
- Corregir problema con disminución de sangrado errónea en python (error 351190)

### KWidgetsAddons

- KPasswordDialog: permitir que el usuario cambie la visibilidad de la contraseña (error 224686)

### KXMLGUI

- Corregir que KSwitchLanguageDialog no muestre la mayoría de los idiomas

### KXmlRpcClient

- Evitar QLatin1String porque asigna memoria del «heap»

### ModemManagerQt

- Corregir conflicto de metatipos con la última modificación de nm-qt

### NetworkManagerQt

- Añadir nuevas propiedades de las últimas versiones de NM

### Framework de Plasma

- Cambiar el padre a «flickable», si es posible.
- corregir el listado de paquetes
- plasma: corregir acciones de miniaplicaciones pueden ser nullptr (error 351777)
- La señal onClicked de PlasmaComponents.ModelContextMenu funciona ahora correctamente
- Ahora, PlasmaComponents ModelContextMenu permite crear secciones de menú
- Adaptar el complemento platformstatus de kde para que use metadatos json.
- Manejar metadatos no válidos en PluginLoader
- Dejar que «RowLayout» determine el tamaño de la etiqueta.
- mostrar el menú de edición siempre que el cursor sea visible
- Corregir bucle en ButtonStyle
- No modificar el aspecto plano de un botón al pulsarlo
- En las pantallas táctiles y móviles, las barras de desplazamiento son efímeras.
- Ajustar la velocidad y la desaceleración del movimiento a los PPP.
- Delegado de cursor personalizado solo si es móvil.
- Cursor de texto con compatibilidad táctil.
- Solucionada la política de relacionar con el padre y de información emergente
- declarar __editMenu
- Añadir delegados de asas de cursor que faltaban.
- reescribir la implementación de EditMenu
- usar el menú móvil solo condicionalmente
- Cambiar el padre del menú a la raíz.

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
