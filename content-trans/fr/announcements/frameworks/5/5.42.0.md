---
aliases:
- ../../kde-frameworks-5.42.0
date: 2018-01-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Changements généraux

- Correction des alarmes « AUTOMOC » pour « cmake 3.10+ »
- Utilisation plus répandue de la journalisation catégorisée, pour désactiver la sortie de débogage par défaut (utiliser « kdebugsetting » pour la réactiver).

### Baloo

- État de « balooctl » : traitement de tous les arguments
- Correction de multiples demandes d'étiquettes de mots
- Simplifier les conditions de renommage
- Correction des noms incorrects d'affichage « UDSEntry »

### Icônes « Breeze »

- Fix icon name "weather-none" -&gt; "weather-none-available" (bug 379875)
- remove Vivaldi icon cause the origin app icon fits perfect with breeze (bug 383517)
- Ajout de quelques types « MIME » manquants
- Breeze-icons add document-send icon (bug 388048)
- Mise à jour de l'icône pour l'artiste de l'album
- Ajout de la prise en charge de « labplot-editlayout »
- Suppression des duplicatas et mise à jour du thème « Dark »
- Ajout de la prise en charge de l'icône de style « Breeze » pour GNUmeric

### Modules additionnels « CMake »

- Utiliser « readelf » pour identifier les dépendances de projet
- Introduce INSTALL_PREFIX_SCRIPT to easily set up prefixes

### KActivities

- avoid crash in kactivities if no dbus connection is available

### KConfig

- API docs: explain how to use KWindowConfig from a dialog constructor
- Obsolescence de « KDesktopFile::sortOrder() »
- Fix the result of KDesktopFile::sortOrder()

### KCoreAddons

- Extend CMAKE_AUTOMOC_MACRO_NAMES also for own build
- Correspondance des clés de licences par « spdx »

### KDBusAddons

- Fix absolute path detection for cmake 3.5 on Linux
- Ajout d'une fonction « cmake » pour « kdbusaddons_generate_dbus_service_file »

### KDeclarative

- Contrôle « Qml » pour la création « kcm »

### KDED

- Use cmake function 'kdbusaddons_generate_dbus_service_file' from kdbusaddons to generate dbus service file
- Add used property to service file definition

### Prise en charge de « KDELibs 4 »

- Inform the user if the module can not be registered with org.kde.kded5 and exit with error
- Correction de compilation pour « Mingw32 »

### KDocTools

- Ajout d'une entité pour Michael Pyne
- add entities for Martin Koller to contributor.entities
- Correction de l'entrée « Debian »
- Ajout d'une entité « Debian » à « general.entities »
- Ajout d'une entité « kbackup », déjà importée
- add entity latex, we have already 7 entities in different index.docbooks in kf5

### KEmoticons

- Add scheme (file://). It's necessary when we use it in qml and we added all

### KFileMetaData

- Supprimer l'extracteur reposant sur « QtMultimedia »
- Check for Linux instead of TagLib and avoid building the usermetadatawritertest on Windows
- Restore # 6c9111a9 until a successful build and link without TagLib is possible
- Remove the taglib dependency, caused by a left-over include

### KHTML

- Finally allow to disable debug output by using categorized logging

### KI18n

- do not treat ts-pmap-compile as executable
- Correction d'une fuite mémoire dans « KuitStaticData »
- KI18n : correction d'un nombre dans les recherches dupliquées

### KInit

- Supprimer l'impossibilité d'atteindre le code

### KIO

- Properly parse dates in cookies when running in non-English locale (bug 387254)
- [kcoredirlister] correction de la création des sous-emplacements
- Report de l'état de la corbeille dans « iconNameForUrl »
- Forward QComboBox signals instead of QComboBox lineedit signals
- Fixed webshortcuts showing their file path instead of their human-readable name
- TransferJob : correction dans le cas où le signal « readChannelFinished » a déjà été émis avant que l'appel à « start() » (bogue 386246)
- Fix crash, presumably since Qt 5.10? (bug 386364)
- KUriFilter : ne pas renvoyer une erreur pour des fichiers non existants 
- Correction de la création d'emplacements
- Implement a kfile dialog where we can add custom widget
- Verify that qWaitForWindowActive doesn't fail
- KUriFilter: port away from KServiceTypeTrader
- API dox: use class names in titles of example screenshots
- API dox: also deal with KIOWIDGETS export macros in QCH creation
- fix handling of KCookieAdvice::AcceptForSession (bug 386325)
- Created 'GroupHiddenRole' for KPlacesModel
- forward socket error string to KTcpSocket
- Refactor and remove duplicate code in kfileplacesview
- Émission du signal « groupHiddenChanged »
- Refactoring the hidding/showing animation use within KFilePlacesView
- User can now hide an entire places group from KFilePlacesView (bug 300247)
- Hidding place groups implementation in KFilePlacesModel (bug 300247)
- [KOpenWithDialog] Remove redundant creation of KLineEdit
- Ajout de la prise en charge de l'annulation de « BatchRenameJob »
- Ajout de « BatchRenameJob » à « KIO »
- Fix doxygen code block not ending with endcode

### Kirigami

- Maintien d'une interaction réactive
- Préfixe également correct pour les icônes
- Correction du dimensionnement de formes
- read wheelScrollLines from kdeglobals if existing
- add a prefix for kirigami files to avoid conflicts
- Quelques correction de liaisons statiques
- Déplacement des styles de Plasma vers « plasma-framework »
- Use single quotes for matching characters + QLatin1Char
- FormLayout

### KJobWidgets

- Offer QWindow API for KJobWidgets:: decorators

### KNewStuff

- Limiter la taille du cache de requêtes
- Require the same internal version as you're building
- Prevent global variables from been using after freeing

### KNotification

- [KStatusNotifierItem] Don't "restore" widget position on its first show
- Use positions of legacy systray icons for Minimize/Restore actions
- Handle positions of LMB clicks on legacy systray icons
- Ne pas traiter le menu contextuel comme une fenêtre
- Ajout d'un commentaire explicatif
- Lazy-instanciate and lazy-load KNotification plugins

### Environnement de développement « KPackage »

- Invalidation du cache d'exécution à l'installation
- experimental support for rcc files loading in kpackage
- Compilation avec Qt 5.7
- Fix up package indexing and add runtime caching
- Nouvelle méthode « KPackage::fileUrl() »

### KRunner

- [RunnerManager] Don't mess with ThreadWeaver thread count

### KTextEditor

- Correction la correspondance avec les caractères « joker » pour les modificateurs
- Fix a regression caused by changing backspace key behavior
- port to non-deprecated API like already used at other place (bug 386823)
- Ajout d'un « include » manquant pour « std::array »
- MessageInterface: Add CenterInView as additional position
- Nettoyage du lanceur « QStringList »

### Environnement de développement « KWallet »

- Use correct service executable path for installing kwalletd dbus service on Win32

### KWayland

- Correction des incohérences de nommage
- Create interface for passing server decoration palettes
- Inclure explicitement les fonctions « std::bind »
- [server] Add a method IdleInterface::simulateUserActivity
- Fix regression caused by backward compatibility support in data source
- Add support for version 3 of data device manager interface (bug 386993)
- Scope exported/imported objects to the test
- Fix error in WaylandSurface::testDisconnect
- Ajout d'un protocole explicite « AppMenu »
- Correction de l'exclusion de fichiers produits dans la fonctionnalité « automoc »

### KWidgetsAddons

- Correction de plantage de « setMainWindow » dans « Wayland »

### KXMLGUI

- API dox: make doxygen cover session related macros &amp; functions again
- Disconnect shortcutedit slot on widget destruction (bug 387307)

### NetworkManagerQt

- 802-11-x : prise en charge de la méthode « EAP »

### Environnement de développement de Plasma

- [Air theme] Add task bar progress graphic (bug 368215)
- Templates: remove stray * from license headers
- make packageurlinterceptor as noop as possible
- Revert "Don't tear down renderer and other busy work when Svg::setImagePath is invoked with the same arg"
- Déplacement ici des styles « Kirigami » de Plasma 
- Suppression des barres de défilement sur mobile.
- reuse KPackage instance between PluginLoader and Applet
- [AppletQuickItem] Only set QtQuick Controls 1 style once per engine
- Don't set a window icon in Plasma::Dialog
- [RTL] - align properly the selected text for RTL (bug 387415)
- Initialize scale factor to the last scale factor set on any instance
- Revert "Initialize scale factor to the last scale factor set on any instance"
- Don't update when the underlying FrameSvg is repaint-blocked
- Initialize scale factor to the last scale factor set on any instance
- Déplacer si vérifier à l'intérieur de #ifdef
- [FrameSvgItem] Ne créer pas de nœuds inutiles
- Don't tear down renderer and other busy work when Svg::setImagePath is invoked with the same arg

### Prison

- Veuillez aussi regarder « qrencode » avec le suffixe « debug »

### QQC2StyleBridge

- Simplifier et n'essayer pas de bloquer les évènements de souris
- si aucun «  », utiliser les lignes globales de défilement avec la molette
- Les barres d'onglets possèdent différentes largeurs pour chaque onglet
- Vérification que la taille de l'astuce est non nulle. 

### Sonnet

- Ne pas exporter des exécutable internes d'assistant
- Sonnet: fix wrong language for suggestions in mixed-language texts
- Suppression du contournement ancient et cassé
- Ne pas créer de liens circulaires sous Windows

### Coloration syntaxique

- Highlighting indexer: Warn about context switch fallthroughContext="#stay"
- Highlighting indexer: Warn about empty attributes
- Indexeur de coloration syntaxique : activation des erreurs
- Highlighting indexer: report unused itemDatas and missing itemDatas
- Prolog, RelaxNG, RMarkDown: Fix highlighting issues
- Haml : correction des « itemDatas » non valables ou non utilisés
- ObjectiveC++: Remove duplicate comment contexts
- Diff, ObjectiveC++: Cleanups and fix highlighting files
- XML (Debug): Fix incorrect DetectChar rule
- Highlighting Indexer: Support cross-hl context checking
- Revert: Add GNUMacros to gcc.xml again, used by isocpp.xml
- email.xml: add *.email to the extensions
- Highlighting Indexer: Check for infinite loops
- Highlighting Indexer: Check for empty context names and regexps
- Fix referencing of non-existing keyword lists
- Correction les cas simples de contextes dupliqués
- Correction les duplicatas « itemDatas »
- Correction des règles « DetectChar » et « Detect2Chars »
- Highlighting Indexer: Check keyword lists
- Highlighting Indexer: Warn about duplicate contexts
- Highlighting Indexer: Check for duplicate itemDatas
- Highlighting indexer: Check DetectChar and Detect2Chars
- Validate that for all attributes an itemData exists

### Informations sur la sécurité

Le code publié a été signé en « GPG » avec la clé suivante  pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empreinte de la clé primaire : 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Vous pouvez discuter et partager vos idées sur cette version dans la section des commentaires de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article</a>.
