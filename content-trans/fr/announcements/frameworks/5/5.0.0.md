---
aliases:
- ../../kde-frameworks-5.0
- ./5.0
customIntro: true
date: '2014-07-07'
description: KDE publie la première version des environnements de développement en
  version 5.
layout: framework
qtversion: 5.2
title: Première publication de KDE Frameworks 5
---
Le 07 juillet 2014 . La communauté KDE est fière d'annoncer les environnements de développement 5.0 de KDE. Ces derniers représentent la prochaine génération de bibliothèques KDE, modulaires et optimisées pour une intégration facile dans les applications Qt. Ils offrent une grande variété de fonctionnalités couramment utilisées dans des bibliothèques matures, revues par des pairs et bien testées, avec des conditions de licence conviviales. Il y a plus de 50 environnements de développements faisant partie de cette version et fournissant des solutions telles que l'intégration matérielle, la prise en charge des formats de fichiers, des composants graphiques supplémentaires, des fonctions de traçage, la vérification orthographique et bien plus encore. La plupart des environnements de développement sont multi plate-formes et n'ont que peu ou pas de dépendances supplémentaires, ce qui les rend faciles à construire et à ajouter à toute application Qt.

Les environnements de développement de KDE représentent un effort pour retravailler les puissantes bibliothèques de KDE Platform 4 en un ensemble de modules indépendants et multi plate-formes, facilement disponibles pour tous les développeurs Qt afin de simplifier, accélérer et réduire le coût du développement Qt. Tous ces environnements de développement sont multi plate-formes, bien documentés et testés et leur utilisation sera familière aux développeurs Qt, suivant le style et les standards établis par le projet Qt. Ils sont développés selon le modèle de gouvernance éprouvé de KDE, avec un calendrier de publication prévisible, un processus de contribution clair et neutre, une gouvernance ouverte et une licence flexible (LGPL).

The Frameworks have a clear dependency structure, divided into Categories and Tiers. The Categories refer to runtime dependencies:

- <strong>Functional</strong> elements have no runtime dependencies.
- <strong>Integration</strong> designates code that may require runtime dependencies for integration depending on what the OS or platform offers.
- <strong>Solutions</strong> have mandatory runtime dependencies.

Les <strong>Tiers</strong> font référence aux dépendances durant la compilation vers d'autres environnements de développement. Les environnements de développement de niveau 1 n'ont pas de dépendances au sein des environnements de développement et n'ont besoin que de Qt et d'autres bibliothèques pertinentes. Les environnements de développement de niveau 2 ne peuvent dépendre que du niveau 1. Les environnements de développement de niveau 3 peuvent dépendre d'autres environnements de développement de niveau 3 ainsi que de certains de niveaux 2 et 1.

La transition de la plate-forme de développement vers les environnements de développement est en cours depuis plus de 3 ans, guidée par les meilleurs contributeurs techniques de KDE. Apprenez-en plus sur les environnements de développement 5.0 de KDE <a href='http://dot.kde.org/2013/09/25/frameworks-5'>dans cet article de l'année dernière</a>.

## Changements

Il y a plus de 50 environnements de développement actuellement disponibles. Parcourez l'ensemble complet <a href='http://api.kde.org/frameworks-api/frameworks5-apidocs/'>grâce à la documentation API en ligne</a>. Ci-dessous une impression de certaines des fonctionnalités que les environnements de développement offrent aux développeurs d'applications sous Qt.

<strong>KArchive</strong> offre la prise en charge de nombreux codecs de compression populaires dans une bibliothèque d'archivage et d'extraction de fichiers, autonome, riche en fonctionnalités et facile à utiliser. Il suffit de lui fournir des fichiers. Il n'y a aucun besoin de réinventer une fonction d'archivage dans votre application reposant sur Qt !

<strong>ThreadWeaver</strong> offre une « API » de haut niveau pour gérer les processus en utilisant des interfaces reposant sur des tâches et des files d'attente. Il permet de planifier facilement l'exécution des processus en spécifiant les dépendances entre les processus et en les exécutant en satisfaisant ces dépendances, ce qui simplifie grandement l'utilisation de plusieurs processus.

<strong>KConfig</strong> est un environnement de développement pour traiter le stockage et la récupération des paramètres de configuration. Il dispose d'une API orientée groupe. Il fonctionne avec des fichiers « INI » et des répertoires en arborescence conformes à « XDG ». Il génère du code reposant sur des fichiers « XML ».

<strong>Solid</strong> offre une fonction de détection matérielle et peut informer une application sur les périphériques et les volumes de stockage, le processeur, l'état de la batterie, la gestion de l'alimentation, l'état et les interfaces du réseau et la connexion Bluetooth. Pour les partitions chiffrées, l'alimentation et le réseau, des démons en cours d'exécution sont nécessaires.

<strong>KI18n</strong> ajoute la prise en charge de « Gettext » aux applications, facilitant l'intégration du flux de traduction des applications Qt dans l'infrastructure de traduction générale de nombreux projets.
