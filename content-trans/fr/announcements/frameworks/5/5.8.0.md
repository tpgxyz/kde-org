---
aliases:
- ../../kde-frameworks-5.8.0
date: '2015-03-13'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Nouveaux environnements de développement :

- KPeople fournit un accès à tous les contacts et les personnes qui les possèdent.
- KXmlRpcClient, interaction avec les services « XMLRPC »

### Général

- Corrections de plusieurs erreurs de compilation pour les compilations avec la version ultérieure Qt 5.5.

### KActivities

- Le service de notation des ressources est maintenant finalisé.

### KArchive

- Éliminer l'erreur sur les fichiers « ZIP » avec des descripteurs de données redondantes

### KCMUtils

- Restauration de « KCModule::setAuthAction »

### KCoreAddons

- KPluginMetadata : ajout de la prise en charge de la touche masquée

### KDeclarative

- Préférer l'exposition des listes par « QML » avec « QJsonArray »
- Gérer les « devicePixelRatios » personnalisés dans les images
- Exposer « hasUrls » dans « DeclarativeMimeData »
- Permettre aux utilisateurs de configurer combien de lignes horizontales sont à dessiner

### KDocTools

- Correction de la compilation sous MacOSX lors de l'utilisation de Homebrew
- Meilleur effet de styles sur les objets multimédia (images, etc.) dans la documentation.
- Encodage des caractères non valables utilisés dans « XML DTD », en évitant des erreurs.

### KGlobalAccel

- Définition de l'horodatage d'activation comme une propriété dynamique sur une « QAction » déclenchée.

### KIconThemes

- Correction de « QIcon::fromTheme(xxx, someFallback) » pour renvoyer la procédure de secours

### KImageFormats

- Rendre le lecteur d'images « PSD » indépendant du format interne des données.

### KIO

- Rendre obsolète la méthode « UDSEntry::listFields » et ajouter la méthode « UDSEntry::fields » retournant un « QVector » sans surcoût de conversion.
- Synchronisation du gestionnaire de marques, uniquement si une modification était faite par ce processus (bogue 343735)
- Correction du démarrage du service « D-Bus » « kssld5 »
- Implémentation de « quota-used-bytes » et « quota-available-bytes » selon la « RFC 4331 » pour autoriser les informations d'espace libre dans le module « http ioslave ».

### KNotifications

- Retarder l'initialisation audio jusqu'à réellement nécessaire
- Correction de la configuration de notifications ne s'appliquant pas immédiatement
- Correction des notifications audio, qui s'arrêtaient après la lecture du premier fichier

### KNotifyConfig

- Ajout de dépendances optionnelles pour « QtSpeech » pour ré-activer les notifications vocales.

### KService

- KPluginInfo : prise en charge des « stringlists » comme propriétés

### KTextEditor

- Ajout de statistiques de comptage de mots dans la barre d'état
- vimode : correction d'un plantage lors de la suppression de la dernière linge en mode « Visual Ligne »

### KWidgetsAddons

- Mise en place de la compatibilité de « KRatingWidget » avec « devicePixelRatio »

### KWindowSystem

- « KSelectionWatcher »et « KSelectionOwner » peuvent maintenant être utilisés sans dépendance avec « QX11Info »
- « KXMessages » peuvent maintenant être utilisés sans dépendance avec « QX11Info »

### NetworkManagerQt

- Ajout de nouvelles propriétés et méthodes pour le gestionnaire de réseaux (NetworkManager) 1.0.0

#### Environnement de développement de Plasma

- Correction de « plasmapkg2 » pour les systèmes traduits
- Amélioration du format des astuces
- Permettre aux composants graphiques de charger des scripts en dehors du paquet « plasma ».

### Buildsystem changes (extra-cmake-modules)

- Extension de la macro « ecm_generate_headers » pour prendre aussi en charge les en-têtes de « CamelCase.h »

Vous pouvez discuter et partager vos idées sur cette version dans la section des commentaires de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article</a>.
