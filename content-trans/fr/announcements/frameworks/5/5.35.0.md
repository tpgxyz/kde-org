---
aliases:
- ../../kde-frameworks-5.35.0
date: 2017-06-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Améliorer la notification des erreurs

### BluezQt

- Pass an explicit list of arguments. This prevents QProcess from trying to handle our space containing path through a shell
- Fix property changes being missed immediately after an object is added (bug 377405)

### Icônes « Breeze »

- update awk mime as it's a script language (bug 376211)

### Modules additionnels « CMake »

- restore hidden-visibility testing with Xcode 6.2
- ecm_qt_declare_logging_category(): more unique include guard for header
- Add or improve "Generated. Don't edit" messages and make consistent
- Ajout d'un nouveau module « FindGperf »
- Change default pkgconfig install path for FreeBSD

### KActivitiesStats

- Correction de « kactivities-stats » dans « tier3 »

### Outils KDE avec « DOxygen »

- Do not consider keyword Q_REQUIRED_RESULT

### KAuth

- Verify that whoever is calling us is actually who he says he is

### KCodecs

- Génération d'une sortie « gperf » durant la phase de compilation

### KCoreAddons

- Ensure proper per thread seeding in KRandom
- Ne pas regarder les emplacements « QRC » (bug 374075)

### KDBusAddons

- Don't include the pid in the dbus path when on flatpak

### KDeclarative

- Consistently emit MouseEventListener::pressed signal
- Ne pas laisser fuite l'objet de données « MIME » (bogue 380270)

### Prise en charge de « KDELibs 4 »

- Handle having spaces in the path to CMAKE_SOURCE_DIR

### KEmoticons

- Correction : l'utilisation de « Qt5::DBus » ne peut être que privée

### KFileMetaData

- Utilisation de « /usr/bin/env » pour trouver « python2 »

### KHTML

- Generate kentities gperf output at build time
- Generate doctypes gperf output at build time

### KI18n

- Extend Programmer's Guide with notes about influence of setlocale()

### KIO

- Addressed an issue where certain elements in applications (e.g. Dolphin's file view) would become inaccessible in high-dpi multi-screen setup (bug 363548)
- [RenameDialog] Enforce plain text format
- Identify PIE binaries (application/x-sharedlib) as executable files (bug 350018)
- core: expose GETMNTINFO_USES_STATVFS in config header
- PreviewJob: skip remote directories. Too expensive to preview (bug 208625)
- PreviewJob: clean up empty temp file when get() fails (bug 208625)
- Speed up detail treeview display by avoiding too many column resizes

### KNewStuff

- Use a single QNAM (and a disk cache) for HTTP jobs
- Internal cache for provider data on initialisation

### KNotification

- Fix KSNIs being unable to register service under flatpak
- Use application name instead of pid when creating SNI dbus service

### KPeople

- Ne pas exporter les symboles des bibliothèques privées
- Fix symbol exporting for KF5PeopleWidgets and KF5PeopleBackend
- Limitation des avertissements à « gcc »

### Environnement de développement « KWallet »

- Remplacement de « kwalletd4 » après la fin de la migration
- Fin de traitement du signal de l'agent de migration
- Only start timer for migration agent if necessary
- Vérification pour une instance unique d'application le plus tôt possible

### KWayland

- Ajout de « requestToggleKeepAbove » / ci-dessous
- Maintien de « QIcon::fromTheme » dans le processus principal
- Remove pid changedSignal in Client::PlasmaWindow
- Ajout du « PID » dans le protocole de gestion de fenêtres de Plasma

### KWidgetsAddons

- KViewStateSerializer: Fix crash when view is destroyed before state serializer (bug 353380)

### KWindowSystem

- Better fix for NetRootInfoTestWM in path with spaces

### KXMLGUI

- Set main window as parent of stand-alone popup menus
- When building menu hierarchies, parent menus to their containers

### Environnement de développement de Plasma

- Ajouter l'icône de VLC dans la boîte à miniatures
- Plasmoid templates: use the image which is part of the package (again)
- Ajout d'un modèle pour le composant graphique « QML » Plasma avec extension « QML »
- Recreate plasmashellsurf on exposed, destroy on hidden

### Coloration syntaxique

- Haskell: highlight "julius" quasiquoter using Normal##Javascript rules
- Haskell: enable hamlet highlighting for "shamlet" quasiquoter too

### Informations sur la sécurité

Le code publié a été signé en « GPG » avec la clé suivante  pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empreinte de la clé primaire : 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Vous pouvez discuter et partager vos idées sur cette version dans la section des commentaires de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article</a>.
