---
aliases:
- ../announce-applications-19.04-rc
date: 2019-04-05
description: KDE publie la version candidate 19.04 des applications.
layout: application
release: applications-19.03.90
title: KDE publie la version candidate des applications de KDE en version 19.04
version_number: 19.03.90
version_text: 19.04 Release Candidate
---
05 Avril 2019. Aujourd'hui, KDE a publié la version candidate des nouvelles versions des applications KDE. Les dépendances et les fonctionnalités sont figées. L'attention de l'équipe KDE se porte à présent sur la correction des bogues et les dernières finitions.

Veuillez vérifier les <a href='https://community.kde.org/Applications/19.04_Release_Notes'>notes de mises à jour de la communauté</a> pour plus d'informations sur les nouveaux fichiers compressés et les problèmes connus. Une annonce plus complète sera disponible avec la mise à jour finale.

Les versions des applications de KDE 19.04 ont besoin de tests intensifs pour maintenir et améliorer la qualité et l'interface utilisateur. Les utilisateurs actuels sont très importants pour maintenir la grande qualité de KDE. En effet, les développeurs ne peuvent tester toutes les configurations possibles. Votre aide est nécessaire pour aider à trouver les bogues suffisamment tôt pour qu'ils puissent être corrigés avant la version finale. Veuillez contribuer à l'équipe en installant la version candidate et <a href='https://bugs.kde.org/'>en signalant tout bogue</a>.
