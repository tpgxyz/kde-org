---
aliases:
- ../announce-applications-18.12.1
changelog: true
date: 2019-01-10
description: Es distribueixen les aplicacions 18.12.1 del KDE.
layout: application
major_version: '18.12'
title: KDE distribueix les aplicacions 18.12.1 del KDE
version: 18.12.1
---
{{% i18n_date %}}

Avui, KDE distribueix la primera actualització d'estabilització per a les <a href='../18.12.0'>Aplicacions 18.12 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, i proporciona una actualització segura i millor per a tothom.

Hi ha unes 20 esmenes registrades d'errors que inclouen millores al Kontact, Cantor, Dolphin, JuK, Kdenlive, Konsole i Okular entre d'altres.

Les millores inclouen:

- L'Akregator ara funciona amb el WebEngine de les Qt 5.11 o més noves
- S'ha solucionat l'ordenació de les columnes al reproductor musical JuK
- El Konsole ara torna a representar correctament els caràcters per dibuixar quadres
