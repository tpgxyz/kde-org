---
aliases:
- ../announce-applications-17.08.1
changelog: true
date: 2017-09-07
description: KDE distribueix les aplicacions 17.08.1 del KDE
layout: application
title: KDE distribueix les aplicacions 17.08.1 del KDE
version: 17.08.1
---
7 de setembre de 2017. Avui KDE distribueix la primera actualització d'estabilització per a les <a href='../17.08.0'>aplicacions 17.08 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, proporcionant una actualització segura i millor per a tothom.

Hi ha més de 20 esmenes registrades d'errors que inclouen millores al Kontact, Gwenview, Kdenlive, Konsole, KWalletManager, Okular, Umbrello i els jocs del KDE, entre d'altres.

Aquest llançament també inclou les versions de suport a llarg termini de la plataforma de desenvolupament KDE 4.14.36.
