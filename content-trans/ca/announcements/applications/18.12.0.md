---
aliases:
- ../announce-applications-18.12.0
changelog: true
date: 2018-12-13
description: Es distribueixen les aplicacions 18.12 del KDE.
layout: application
release: applications-18.12.0
title: KDE distribueix les aplicacions 18.12.0 del KDE
version: 18.12.0
---
{{% i18n_date %}}

S'han publicat les Aplicacions 18.12 del KDE.

{{%youtube id="ALNRQiQnjpo"%}}

Treballem contínuament per a millorar el programari inclòs a les sèries de les aplicacions del KDE, i esperem que trobeu útils totes les millores i esmenes d'errors!

## Novetats a les Aplicacions 18.12 del KDE

S'han resolt més de 140 errors a les aplicacions incloent-hi el paquet Kontact, Ark, Cantor, Dolphin, Gwenview, Kate, KmPlot, Konsole, Lokalize, Okular, Spectacle, Umbrello i més!

### Gestió de fitxers

{{<figure src="/announcements/applications/18.12.0/app1812_dolphin01.png" width="600px" >}}

El <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, el potent gestor de fitxers del KDE:

- Una implementació nova del MTP que el fa plenament usable per producció
- Una gran millora de rendiment en llegir fitxer amb el protocol SFTP
- Per a vistes prèvies de miniatures, ara només es dibuixen els marcs i les ombres per a fitxers d'imatge sense transparència, millorant la visualització de les icones
- Vistes prèvies noves per als documents del LibreOffice i les aplicacions AppImage
- Els fitxers de vídeo d'una mida superior a 5 MB ara es mostren a les miniatures del directori quan s'ha activat la vista de miniatures de directori
- En llegir CD d'àudio, ara el Dolphin pot canviar la taxa de bits per al codificador de MP3 i soluciona els segells de temps per als FLAC
- El menú «Control» del Dolphin ara mostra elements «Crea nou…» i té un element de menú «Mostra els llocs ocults» nou
- El Dolphin ara es tanca quan només hi ha una pestanya oberta i es prem la drecera estàndard de teclat «tanca pestanya» (Ctrl+w)
- Després de desmuntar un volum des del plafó de Llocs, ara es pot tornar a muntar una altra vegada
- La vista de Documents recents (disponible navegant amb «recentdocuments:/» al Dolphin) ara només mostra els documents reals, i filtra automàticament els URL web
- El Dolphin ara mostra un avís abans de permetre reanomenar un fitxer o directori de manera que esdevingui ocult immediatament
- Ja no es pot intentar desmuntar els discs del sistema operatiu actiu o del directori personal des del plafó de Llocs

El <a href='https://www.kde.org/applications/utilities/kfind'>KFind</a>, el cercador de fitxers tradicional del KDE, ara té un mètode de cerca de metadades basat en el KFileMetaData.

### Oficina

El <a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, el potent client de correu electrònic del KDE:

- El KMail ara pot mostrar una bústia d'entrada unificada
- Connector nou: Genera correu HTML a partir del llenguatge Markdown
- Ús del Purpose per compartir text (com a correu)
- Els correus HTML ara es poden llegir sense importar l'esquema de color que s'usi

{{<figure src="/announcements/applications/18.12.0/app1812_okular01.png" width="600px" >}}

L'<a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, el visualitzador versàtil de documents del KDE:

- Nova eina d'anotacions «Màquina d'escriure» que es pot usar per escriure text a qualsevol lloc
- La vista jeràrquica de la taula de continguts ara té la possibilitat d'expandir o reduir tot, o només una secció específica
- Comportament millorat de l'ajust de paraula a les anotacions en línia
- En passar el ratolí per damunt d'un enllaç, ara l'URL sempre es mostra que es pot clicar, en lloc de fer-ho només en mode navegació
- Els fitxers ePub que contenen recursos amb espais als seus URL, ara es mostren correctament

### Desenvolupament

{{<figure src="/announcements/applications/18.12.0/app1812_kate01.png" width="600px" >}}

El <a href='https://www.kde.org/applications/utilities/kate/'>Kate</a>, l'editor avançat de text del KDE:

- En usar el terminal incrustat, ara sincronitza automàticament el directori actual amb la ubicació al disc del document actiu
- El terminal incrustat del Kate ara es pot enfocar i desenfocar usant la drecera de teclat F4
- El commutador de pestanyes integrat del Kate ara mostra els camins complets per als fitxers amb noms similars
- Els números de les línies ara estan actius de manera predeterminada
- L'increïblement útil i potent connector Filtre de text ara està actiu de manera predeterminada i és més fàcil de descobrir
- En obrir un document ja obert usant la funcionalitat Obertura ràpida, ara canvia a aquest document
- La funcionalitat Obertura ràpida ja no mostrarà més entrades duplicades
- En usar diverses Activitats, els fitxers ara s'obren a l'Activitat correcta
- El Kate ara mostra les icones correctes quan s'executa en el GNOME usant el tema d'icones del GNOME

{{<figure src="/announcements/applications/18.12.0/app1812_konsole.png" width="600px" >}}

El <a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, l'emulador de terminal del KDE:

- El Konsole ara permet els caràcters d'«emoji» complerts
- Les icones de les pestanyes inactives ara es ressalten quan reben un senyal de timbre
- Els dos punts al final ja no es consideren part d'una paraula per a la finalitat de selecció amb el clic doble, fent més senzill seleccionar camins i la sortida del «grep»
- Quan es connecta un ratolí amb botons d'enrere i endavant, ara el Konsole pot usar aquests botons per commutar entre pestanyes
- El Konsole ara té un element de menú per reiniciar la mida del tipus de lletra al predeterminat del perfil, si s'ha ampliat o reduït la mida
- Ara és més difícil desacoblar accidentalment les pestanyes, i més ràpid reordenar-les amb precisió
- Comportament millorat de la selecció amb Majúscules-clic
- S'ha solucionat el clic doble en una línia de text que excedeix l'amplada de la finestra
- La barra de cerca es torna a tancar en prémer la tecla d'escapada

El <a href='https://www.kde.org/applications/development/lokalize/'>Lokalize</a>, l'eina de traduccions del KDE:

- Oculta els fitxers traduïts a la pestanya del projecte
- S'ha afegit un suport bàsic del «pology», el sistema de comprovació de sintaxi i glossari
- S'ha simplificat la navegació amb l'ordenació de pestanyes i l'obertura de diverses pestanyes
- S'han esmenat diversos «segfaults» per accessos concurrents a objectes de la base de dades
- S'ha solucionat un arrossegament i deixar anar incoherent
- S'ha solucionat un error a les dreceres que eren diferents entre els editors
- Millor comportament de la cerca (trobarà i mostrarà formes plurals)
- Restaura la compatibilitat amb el Windows gràcies al sistema de compilació Craft

### Utilitats

El <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, el visualitzador d'imatges del KDE:

- L'eina «Reducció d'ulls vermells» ha rebut diverses millores agraïdes d'usabilitat
- El Gwenview ara mostra un diàleg d'avís quan s'oculta la barra de menús que diu com retornar-la

{{<figure src="/announcements/applications/18.12.0/app1812_spectacle01.png" width="600px" >}}

L'<a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, la utilitat de captura de pantalla del KDE:

- L'Spectacle ara té la capacitat de numerar seqüencialment els fitxers de captura de pantalla, i aquest esquema de nom serà el valor predeterminat si es neteja el camp de text de nom de fitxer
- S'ha solucionat el desament d'imatges en un format diferent del «.png», en usar Desa com a…
- En usar l'Spectacle per obrir una captura de pantalla en una aplicació externa, ara es pot modificar i desar la imatge després de fer-ho
- L'Spectacle ara obre la carpeta correcta en fer clic a Eines > Obre la carpeta de captures de pantalla
- Els segells de temps de les captures de pantalla ara indiquen quan es va crear la imatge, no quan es va desar
- Totes les opcions per desar ara estan ubicades a la pàgina «Desa»

L'<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, el gestor d'arxius del KDE:

- S'ha afegit la implementació del format Zstandard (arxius tar.zst)
- S'ha solucionat la previsualització a l'Ark de determinats fitxers (p. ex. Open Document) com a arxius en lloc d'obrir-los a l'aplicació adequada

### Matemàtiques

El <a href='https://www.kde.org/applications/utilities/kcalc/'>KCalc</a>, la calculadora senzilla del KDE, ara té una opció per a repetir el darrer càlcul diverses vegades.

El <a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, el frontal matemàtic del KDE:

- Afegeix un tipus d'entrada del Markdown
- Ressaltat animat de l'entrada d'ordre actualment calculada
- Visualització de les entrades d'ordre pendents (a la cua, però encara no calculades)
- Permet donar format a entrades d'ordre (color de fons, color de primer pla, propietats del tipus de lletra)
- Permet inserir noves entrades d'ordres en llocs arbitraris del full de càlcul situant el cursor a la posició desitjada i iniciant el tecleig
- Per a expressions que tenen diverses ordres, mostra els resultats com a objectes independents de resultat al full de càlcul
- Afegeix la implementació per a obrir des de la consola diversos fulls de càlcul pel camí relatiu
- S'ha afegit la implementació per obrir diversos fitxers en un intèrpret del Cantor
- Canvia el color i el tipus de lletra quan es demana informació addicional per tal de discriminar millor l'entrada normal a la línia d'ordres
- S'ha afegit dreceres per a la navegació entre els fulls de càlcul (Ctrl+Av Pàg, Ctrl+Re Pàg)
- Afegeix una acció al submenú «Visualitza» per reiniciar el zoom
- Activa la baixada de projectes del Cantor des de store.kde.org (de moment les pujades només funcionen des del lloc web)
- Obre el full de càlcul en mode només lectura si el dorsal no està disponible en el sistema

El <a href='https://www.kde.org/applications/education/kmplot/'>KmPlot</a>, el traçador de funcions del KDE, ha solucionat molts problemes:

- S'han solucionat els noms incorrectes dels gràfics de les derivades i les integrals a la notació convencional
- L'exportació a SVG del KmPlot ara funciona adequadament
- La funcionalitat de la primera derivada no té notació prima
- En desmarcar una funció no enfocada, ara s'oculta el seu gràfic:
- S'ha solucionat una fallada del KmPlot en obrir recursivament «Edita constants» des de l'editor de funcions
- S'ha solucionat una fallada del KmPlot després de suprimir una funció i l'apuntador del ratolí segueix al gràfic
- Ara es poden exportar dades dibuixades com a qualsevol format d'imatge
