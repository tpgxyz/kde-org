---
aliases:
- ../announce-applications-15.04.3
changelog: true
date: 2015-07-01
description: KDE distribueix les aplicacions 15.04.3 del KDE
layout: application
title: KDE distribueix les aplicacions 15.04.3 del KDE
version: 15.04.3
---
1 de juliol de 2015. Avui KDE distribueix la tercera actualització d'estabilització per a les <a href='../15.04.0'>aplicacions 15.04 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, proporcionant una actualització segura i millor per a tothom.

Hi ha més de 20 esmenes registrades d'errors que inclouen millores al Kdenlive, Kdepim, Kopete, ktp-contact-list, Marble, Okteta i Umbrello.

Aquest llançament també inclou les versions de suport a llarg termini dels espais de treball Plasma 4.11.21, la plataforma de desenvolupament KDE 4.14.10 i el paquet Kontact 4.14.10.
