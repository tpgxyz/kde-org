---
aliases:
- ../../kde-frameworks-5.77.0
date: 2020-12-12
layout: framework
libCount: 83
qtversion: 5.13
---
### Attica

* Corregeix una fallada en carregar els proveïdors verificant l'apuntador de resposta abans del «deref» (error 427974)

### Baloo

* [DocumentUrlDB] Suprimeix de la BBDD l'entrada de la llista de fills si és buida
* Afegeix el tipus de document Presentació per als passis de diapositives i plantilles OpenXML de l'Office
* [MetaDataMover] Corregeix la cerca de l'ID del document pare
* [DocumentUrlDB] Afegeix un mètode per a canvis de nom i moviments trivials
* [MetaDataMover] Fa que reanomenar sigui només una operació de BBDD
* [Document] Afegeix les ID del document pare i les emplena
* Substitueix la crida «syslog» directa per un missatge d'enregistrament categoritzat

### Icones Brisa

* Afegeix variants de camp de text: -frameless (-> text-field), -framed
* Afegeix un enllaç simbòlic amb nom simbòlic a les icones «input-*»
* Afegeix una icona per als tipus de lletra True Type XML
* Afegeix «add-subtitle»
* Canvia la icona de MathML per a usar una fórmula i usar un tipus MIME específic
* Afegeix una icona per a les imatges de disc del QEMU i les imatges de SquashFS
* Afegeix la icona d'acció «edit-move»
* Afegeix una icona per als bolcats del nucli
* Afegeix una pila de tipus MIME de subtítols
* Elimina el difuminat sense utilitat de la icona «kontrast»

### Mòduls extres del CMake

* Corregeix l'extracció de les categories dels fitxers «desktop»
* Defineix les variables del directori d'instal·lació de les plantilles de fitxers
* Afegeix la generació de metadades «fastlane» per a les construccions Android
* (Qt)WaylandScanner: Marca adequadament els fitxers com a SKIP_AUTOMOC

### KActivitiesStats

* ResultModel: exposa el recurs MimeType
* Afegeix un esdeveniment de filtratge per a fitxers i directoris (error 428085)

### KCalendarCore

* Corregeix el mantenidor, que se suposa que és l'Allen, no jo :)
* Afegeix la implementació per a la propietat CONFERENCE
* Afegeix un mètode d'utilitat «alarmsTo» al Calendari
* Verifica que les recurrències per dia no precedeixen a la «dtStart»

### KCMUtils

* Elimina el pedaç que trenca els KCM multinivell en el mode icona

### KConfig

* Corregeix KConfigGroup::copyTo amb KConfigBase::Notify (error 428771)

### KCoreAddons

* Evita fallar quan la factoria és buida (es retorna un error)
* KFormat: afegeix més casos de data i hora relatius
* Habilita KPluginFactory per a passar opcionalment KPluginMetaData als connectors

### KDAV

* L'elimina perquè crea massa errors

### KDeclarative

* Sincronitza els marges d'AbstractKCM a SimpleKCM
* Elimina el text obsolet de llicència
* Canvia la llicència del fitxer a la LGPL-2.0 o posterior
* Canvia la llicència del fitxer a la LGPL-2.0 o posterior
* Canvia la llicència del fitxer a la LGPL-2.0 o posterior
* Canvia la llicència del fitxer a la LGPL-2.0 o posterior
* Reescriu KeySequenceItem (i el «helper») per a usar KeySequenceRecorder (error 427730)

### KDESU

* Analitza adequadament les cometes dobles escapades
* Afegeix la implementació del «doas(1)» de l'OpenBSD

### KFileMetaData

* Corregeix diverses fuites als extractors d'OpenDocument i d'OpenXML d'Office
* Afegeix diversos subtipus per als documents OpenDocument i OpenXML

### KGlobalAccel

* Carrega els connectors d'interfície «kglobalacceld» enllaçats estàticament

### Complements de la IGU del KDE

* Fa que la inhibició de dreceres funcioni immediatament (error 407395)
* Corregeix una fallada potencial en tancar/retrocedir l'inhibidor del Wayland (error 429267)
* CMake: cerca Qt5::GuiPrivate quan s'ha habilitat la implementació del Wayland
* Afegeix KeySequenceRecorder com a base per a KKeySequenceWidget i KeySequenceItem (error 407395)

### KHolidays

* Corregeix l'arrodoniment dels esdeveniments de la posició del sol a menys de 30 s abans de l'hora següent
* Evita l'anàlisi dues vegades de cada fitxer de festiu a «defaultRegionCode()»
* Calcula les estacions astronòmiques només una vegada per cada ocurrència
* Corregeix la cerca de les regions de festius per als codis ISO 3166-2
* Fa que HolidayRegion es pugui copiar/moure
* Afegeix la implementació per a calcular les hores del crepuscle civil

### KIdleTime

* Carrega els connectors «poller» del sistema enllaçats estàticament

### KImageFormats

* Ja no redueix la profunditat de color a 8 bits per als fitxers PSD no comprimits de 16 bits

### KIO

* Diàleg NewFile: permet acceptar la creació del directori abans que s'executi l'«stat» (error 429838)
* Evita fuites al fil de DeleteJob
* KUrlNavBtn: fa que obrir subdirectoris des del menú desplegable funcioni amb el teclat (error 428226)
* DropMenu: usa dreceres traduïdes
* [ExecutableFileOpenDialog] Focus al botó Cancel·la
* Vista de Llocs: ressalta el lloc només quan es mostra (error 156678)
* Afegeix una propietat per a mostrar accions de menú al submenú «Accions»
* Elimina un mètode presentat nou
* KIO::iconNameForUrl: resol les icones per a fitxers remots en funció del nom (error 429530)
* [kfilewidget] Usa una drecera estàndard nova per a «Crea una carpeta»
* Refactoritza la càrrega del menú contextual i el fa més escalable
* RenameDialog: permet sobreescriure quan els fitxers són més antics (error 236884)
* DropJob: usa la icona nova «edit-move» per a «Mou aquí»
* Corregeix la generació de «moc_predefs.h» quan s'ha habilitat «ccache» a través de -DCMAKE_CXX_COMPILER=ccache CMAKE_CXX_COMPILER_ARG1=g++
* Ara es requereix Qt 5.13, s'elimina l'«ifdef»
* Adapta KComboBox a QComboBox
* Correcció de la documentació sol·licitada per Méven Car @meven
* Refactoritza diversos bucles usant C++ modern
* Neteja de codi mort
* Elimina una comprovació redundant si existeix la clau
* Simplifica el codi de RequiredNumberOfUrls
* Elimina diverses línies buides
* Simplifica el codi i el fa més coherent
* Ioslaves: corregeix els permisos arrel de «remote:/»
* KFileItem: «isWritable» usa KProtocolManager per als fitxers remots
* Afegeix una sobrecàrrega per a anteposar accions al menú «Accions»
* Usa un estil modern de codi
* No afegeix separadors innecessaris (error 427830)
* Usa un «initializer_list» entre claus en lloc de l'operador <<
* MkPathJob: reescriu el codi compilat condicionalment per a millorar-ne la llegibilitat

### Kirigami

* Fa que la capçalera del GlobalDrawer defineixi la posició de les ToolBars/TabBars/DialogButtonBoxes
* Propietat adjunta «inViewport»
* Corregeix el lliscament de la capçalera en una pantalla tàctil
* Refactoritza AbstractapplicationHeader amb un concepte «ScrollIntention» nou
* A l'escriptori sempre omple les àncores al pare
* [controls/BasicListItem]: No ancora a un element no existent
* No mostra el text de l'avatar en la mida petita
* Elimina # i @ del procés d'extracció de la inicial de l'avatar
* Inicialitza la propietat a «sizeGroup»
* Usa la interacció del ratolí a «isMobile» per a facilitar les proves
* Revisió de «leading/trailing» usant els valors finals per al marge separador inicial
* [controls/BasicListItem]: Afegeix les propietats «leading/trailing»
* Corregeix el posicionament del full en redimensionar el contingut
* Aplica el comportament antic en el mode ample i no afegeix «topMargin» a FormLayout
* Corregeix FormLayout en pantalla petita
* No es pot usar un AbstractListItem en un SwipeListItem
* Corregeix «No es pot assignar [indefinit] a un enter» a OverlaySheet
* [overlaysheet]: No fa una transició lenta quan canvia l'alçada del contingut
* [overlaysheet]: Anima els canvis d'alçada
* Corregeix el posicionament de l'OverlaySheet
* Sempre defineix l'índex en fer clic a una pàgina
* Corregeix l'arrossegament del FAB en el mode RTL
* Refina l'aparença del separador de llista (error 428739)
* Corregeix les manetes del calaix en el mode RTL
* Corregeix la renderització de les vores amb la mida adequada amb el programari de reserva (error 427556)
* No situa l'element del programari de reserva fora dels límits de l'element rectangle ombrejat
* Usa «fwidth()» per a suavitzar en mode de baix consum (error 427553)
* També renderitza un color de fons en mode de baix consum
* Habilita la renderització transparent de Shadowed(Border)Texture en baix consum
* No cancel·la els components alfa per al rectangle ombrejat en mode de baix consum
* No usa un valor de suavitzat en renderitzar la textura de ShadowedBorderTexture
* Elimina els passos de retallat del rectangle ombrejat i dels «shaders» relacionats
* Usa «icon.name» en lloc d'«iconName» a la documentació
* [Avatar] Fa que la icona usi una mida d'icona propera a la mida del text
* [Avatar] Fa que les inicials usin més espai i millora alineació vertical
* [Avatar] Exposa les propietats «sourceSize» i «smooth» a qualsevol que vulgui animar la mida
* [Avatar] Estableix la mida original per a evitar que les imatges siguin borroses
* [Avatar] Estableix el color de fons una vegada
* [Avatar] Canvia el degradat del fons
* [Avatar] Canvia l'amplada de la vora a 1px per a coincidir amb altres amplades molestes
* [Avatar] Fa que sempre funcionin «padding», «verticalPadding» i «horizontalPadding»
* [Avatar] Afegeix degradat als colors

### KItemModels

* KRearrangeColumnsProxyModel: només té fills la columna 0

### KMediaPlayer

* Instal·la els fitxers de definició de tipus de servei «player» i «engine» per al tipus de coincidència de nom de fitxer

### KNewStuff

* Corregeix la desinstal·lació quan l'entrada no és a la memòria cau
* Quan es crida per verificar si hi ha actualitzacions, s'esperen actualitzacions (error 418082)
* Reutilitza el diàleg QWidgets (error 429302)
* Embolcalla el bloc de compatibilitat a KNEWSTUFFCORE_BUILD_DEPRECATED_SINCE
* No escriu a la memòria cau els estats intermedis
* Usa una enumeració per a descomprimir en lloc de valors de cadenes
* Corregeix una entrada que desapareix massa aviat de la pàgina actualitzable (error 427801)
* Afegeix l'enumeració DetailsLoadedEvent a un senyal nou
* Refà l'API d'«adoption» (error 417983)
* Corregeix un parell d'endarreriments amb l'URL de l'antic proveïdor
* Elimina l'entrada de la memòria cau abans d'inserir una entrada nova (error 424919)

### KNotification

* No passa consells transitoris (error 422042)
* Corregeix un error de distinció entre majúscules i minúscules de la capçalera AppKit en el macOS
* No invoca cap acció de notificació no vàlida
* Corregeix la gestió de la memòria al «notifybysnore»

### Framework del KPackage

* Elimina X-KDE-PluginInfo-Depends

### KParts

* Fa obsolet el mètode «embed()», per manca d'ús
* Fa que KParts usi KPluginMetaData en lloc de KAboutData

### KQuickCharts

* Refà l'algorisme de suavitzat de línia
* Mou la interpolació d'aplicació al pas de poliment
* Centra adequadament els delegats de punt al diagrama de línies i els compara amb l'amplada de la línia
* Afegeix una casella de selecció «suau» a l'exemple del diagrama de línies
* Assegura que els delegats de punt del diagrama de línies es netegen adequadament
* També mostra al nom en el consell d'eina de l'exemple de pàgina de diagrama de línies
* Documenta LineChartAttached i corregeix un error de tecleig a la documentació de LineChart
* Afegeix les propietats «name» i «shortName» a LineChartAttached
* Documenta més a fons la propietat «pointDelegate»
* Elimina el membre «previousValues» i corregeix els diagrames de línies apilades
* Usa «pointDelegate» a l'exemple de diagrama de línies per a mostrar valors en passar-hi per sobre
* Afegeix la implementació per a «pointDelegate» als diagrames de línies
* LineChart: Mou el càlcul dels punts des d'«updatePaintNode» a «polish»

### KRunner

* Fa obsolets els romanents dels paquets KDE4
* Fa ús de la implementació nova del constructor de connectors KPluginMetaData del KPluginLoader

### KService

* [kapplicationtrader] Corregeix la documentació de l'API
* KSycoca: torna a crear la BBDD quan la versió < versió esperada
* KSycoca: manté el seguiment dels recursos Files de KMimeAssociation

### KTextEditor

* Adapta KComboBox a QComboBox
* Usa «themeForPalette» de KSyntaxHighlighting
* Corregeix una crida «i18n», manca un argument (error 429096)
* Millora la selecció automàtica del tema

### KWidgetsAddons

* No emet dues vegades el senyal «passwordChanged»
* Afegeix KMessageDialog, una variant asíncrona de KMessageBox
* Restaura el mode emergent predeterminat antic de KActionMenu
* Adapta KActionMenu a QToolButton::ToolButtonPopupMode

### KWindowSystem

* Carrega els connectors d'integració del sistema enllaçats estàticament
* Adaptació per a substituir «pid()» per «processId()»

### KXMLGUI

* Presenta HideLibraries i fa obsolet HideKdeVersion
* Reescriu KKeySequenceWidget per a usar KeySequenceRecorder (error 407395)

### Frameworks del Plasma

* [Representation] Només elimina el farciment superior/inferior quan la capçalera/peu siguin visibles
* [PlasmoidHeading] Usa la tècnica de Representation per als «inset»/marges
* Afegeix un component Representation
* [Desktop theme] Reanomena «hint-inset-side-margin» a «hint-side-inset»
* [FrameSvg] Reanomena «insetMargin» a «inset»
* [PC3] Usa la Scrollbar de PC3 a ScrollView
* [Breeze] Informa el consell d'«inset»
* [FrameSvg*] Reanomena «shadowMargins» a «inset»
* [FrameSvg] Posa els marges d'ombra a la memòria cau i força els prefixos
* Finalitza l'animació abans de canviar la durada del ressaltat de la barra de progrés (error 428955)
* [textfield] Corregeix el botó de neteja que se superposa al text (error 429187)
* Mostra el menú desplegable a la posició global correcta
* Usa «gzip -n» per a evitar les data-hores de construcció incrustades
* Usa KPluginMetaData per a llistar «containmentActions»
* Adapta la càrrega de «packageStructure» des de KPluginTrader
* Usa KPluginMetaData per a llistar els DataEngines
* [TabBar] Afegeix ressaltat en el focus del teclat
* [FrameSvg*] Exposa els marges de les ombres
* Fa més clar el valor MarginAreasSeparator
* [TabButton] Alinea i centra la icona i el text quan el text està al costat de la icona
* [SpinBox] Corregeix un error lògic a la direccionalitat del desplaçament
* Corregeix la barra de desplaçament mòbil en el mode RTL
* [PC3 ToolBar] No inhabilita les vores
* [PC3 ToolBar] Usa les propietats correctes del marge SVG per al farciment
* [pc3/scrollview] Elimina «pixelAligned»
* Afegeix àrees de marge

### Purpose

* [bluetooth] Corregeix la compartició de fitxers múltiples (error 429620)
* Llegeix l'etiqueta traduïda d'acció de connector (error 429510)

### QQC2StyleBridge

* «button»: es té en compte en baixar, no premut per a mantenir l'estil
* Redueix la mida dels botons arrodonits en el mòbil
* Corregeix la barra de desplaçament mòbil en el mode RTL
* Corregeix la barra de progrés en el mode RTL
* Corregeix la visualització RTL de RangeSlider

### Solid

* Inclou «errno.h» per a EBUSY/EPERM
* FstabBackend: retorna DeviceBusy quan falla el desmuntatge per EBUSY (error 411772)
* Corregeix la detecció de «libplist» i «libimobiledevice» recents

### Ressaltat de la sintaxi

* Corregeix les dependències dels fitxers generats
* Indexer: corregeix diversos problemes i desactiva 2 verificadors (grup de captura i paraula clau amb delimitador)
* Indexer: carrega tots els fitxers XML en la memòria per a una verificació més fàcil
* Ressaltat del C++: actualització a les Qt 5.15
* Torna a llançar els generadors de sintaxi quan es modifica el fitxer de codi font
* Unitat del Systemd: actualitza al «systemd» v247
* ILERPG: simplifica i prova
* Zsh, Bash, Fish, Tcsh: afegeix «truncate» i «tsort» a les paraules clau d'«unixcommand»
* Latex: diversos entorns matemàtics poden estar imbricats (error 428947)
* Bash: moltes correccions i millores
* Afegeix «--syntax-trace=stackSize»
* php.xml: corregeix un «endforeach» coincident
* Mou aquí «bestThemeForApplicationPalette» des de KTextEditor
* Debchangelog: afegeix Trixie
* alert.xml: afegeix «NOQA» una altra alerta popular al codi font
* cmake.xml: la línia de treball ha decidit posposar «cmake_path» per al proper llançament

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org> Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
