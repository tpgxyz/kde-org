---
aliases:
- ../../kde-frameworks-5.73.0
date: 2020-08-01
layout: framework
libCount: 70
---
### Icones Brisa

+ Centra «help-about» i «help-whatsthis» de 16 píxels
+ Afegeix una icona «kirigami-gallery»
+ Afegeix una icona del Kontrast
+ Afegeix una icona del tipus MIME «applications/pkcs12»
+ Fa que les icones de volum d'àudio del Brisa fosca coincideixin amb el Brisa
+ Fa que l'estil del micròfon sigui més coherent amb les altres icones d'àudio i alineat amb els píxels
+ Empra un 35% d'opacitat per a les ones esvaïdes en les icones de volum d'àudio, i fa que les ones mudes s'esvaeixin
+ Fa que «audio-off» i «audio-volume-muted» siguin la mateixa
+ Afegeix una icona «snap-angle»
+ Esmena la vinculació «application-x-ms-shortcut» de 22 píxels a una icona de 16 píxels
+ Esmena les inconsistències en les icones de tipus MIME entre les versions clares/fosques
+ Esmena les diferències invisibles de clares/fosques a les icones de VSCode i Wayland
+ Afegeix una icona «document-replace» (m'agrada per a l'acció de Sobreescriure)
+ Afegeix una plantilla per a crear scripts en Python que editen els SVG
+ Afegeix una icona d'estat «SMART» (error 423997)
+ Afegeix les icones «task-recurring» i «appointment-recurring» (error 397996)

### Mòduls extres del CMake

+ Afegeix «ecm_generate_dbus_service_file»
+ Afegeix «ecm_install_configured_file»
+ Exporta «Wayland_DATADIR»

### KActivitiesStats

+ Ignora «BoostConfig.cmake» si està present (error 424799)

### KCMUtils

+ Admet l'indicador de ressaltat per als mòduls basats en «QWidget» i «QtQuick»
+ Afegeix un mètode per a netejar el selector de connectors (error 382136)

### KConfig

+ Actualitza la «sGlobalFileName» quan el «TestMode» de «QStandardPaths» estigui canviat
+ API dox: codificació explícitament esperada de l'estat per a claus i noms de grup de KConfig

### KConfigWidgets

+ Kcmodule: indica quan s'ha canviat una opció del valor predeterminat
+ En restablir els valors predeterminats del sistema, no usa la paleta estàndard de l'estil

### KCoreAddons

+ Introdueix «KRandom::shuffle(container)»

### KDeclarative

+ SettingStateBinding: exposa si està habilitat el ressaltat no predeterminat
+ Assegura que el «KF5CoreAddons» estigui instal·lat abans d'usar «KF5Declarative»
+ Afegeix «KF5::CoreAddons» a la interfície pública per a «KF5::QuickAddons»
+ Introdueix els elements «SettingState*» per a facilitar l'escriptura dels KCM
+ Admet les notificacions de configuració a «configpropertymap»

### Complements de la IGU del KDE

+ Mou el «KCursorSaver» des de la libkdepim, millorat

### KImageFormats

+ Adapta la llicència a la LGPL-2.0 o posterior

### KIO

+ KFilterCombo: «application/octet-stream» també té «hasAllFilesFilter»
+ Introdueix «OpenOrExecuteFileInterface» per a gestionar l'obertura dels executables
+ RenameDialog: mostra si els fitxers són idèntics (error 412662)
+ [diàleg de canvi de nom]: port del botó de sobreescriptura a «KStandardGuiItem::Overwrite» (error 424414)
+ Mostra fins a tres accions d'element de fitxer a la línia, no solament una (error 424281)
+ KFileFilterCombo: mostra les extensions si hi ha comentaris MIME repetits
+ [KUrlCompletion]: no annexa / a carpetes completades
+ [Propietats]: afegeix l'algorisme SHA512 al giny per a sumes de verificació (error 423739)
+ [WebDav]: esmena les còpies que inclouen sobreescriptura per a l'esclau de WebDAV (error 422238)

### Kirigami

+ Admet la visibilitat de les accions a «GlobalDrawer»
+ Introdueix el component «FlexColumn»
+ Optimitzacions en les disposicions de mòbils
+ Una capa també ha de cobrir la barra de pestanyes
+ PageRouter: en realitat usa pàgines precarregades en prémer
+ Millora la documentació d'«(Abstract)ApplicationItem»
+ Esmena una falla de segment en tancar/retrocedir de «PageRouter»
+ Fa desplaçable la «ScrollablePage» amb el teclat
+ Millora l'accessibilitat dels camps d'entrada del Kirigami
+ Esmena la construcció estàtica
+ PageRouter: afegeix l'API d'utilitat per a les tasques manuals
+ Introdueix les API del cicle de vida de «PageRouter»
+ Força un ordre Z baix de l'alternativa de programari de «ShadowedRectangle»

### KItemModels

+ KSelectionProxyModel: permet usar el model amb una connexió amb l'estil nou
+ KRearrangeColumnsProxyModel: esmena «hasChildren()» quan encara no hi ha columnes establertes

### KNewStuff

+ [Diàleg en QtQuick]: empra la icona d'actualització que existeix (error 424892)
+ [Diàleg en QtQuick del GHNS]: millora les etiquetes del quadre combinat
+ No mostra el selector «downloaditem» per a un sol element descarregable
+ Canvia la disposició una mica, per a una aparença més equilibrada
+ Esmena la disposició per a la targeta d'estat «EntryDetails»
+ Només enllaça amb «Core», no amb el connector ràpid (perquè això no funciona)
+ Alguna petrificació de la pantalla de benvinguda (i no usa els diàlegs en QML)
+ Empra el diàleg quan es passa un fitxer knsrc, en cas contrari no
+ Elimina el botó de la finestra principal
+ Afegeix un diàleg (per a mostrar el diàleg directament quan es passa un fitxer)
+ Afegeix els nous bits a l'aplicació principal
+ Afegeix un model senzill (almenys per ara) per a mostrar els fitxers knsrc
+ Mou el fitxer en QML principal a un QRC
+ Converteix l'eina de prova del diàleg de KNewStuff en una eina adequada
+ Permet suprimir les entrades actualitzables (error 260836)
+ No dona automàticament el focus al primer element (error 417843)
+ Esmena la visualització dels Detalls i el botó Desinstal·la en el mode de Vista en mosaic (error 418034)
+ Esmena el moviment dels botons quan s'insereix el text de cerca (error 406993)
+ Esmena el paràmetre que manca per a la cadena traduïda
+ Esmena els detalls desplegables d'instal·lació en el diàleg en «QWidgets» (error 369561)
+ Afegeix informació sobre les eines per als diferents modes de vista en el diàleg en QML
+ Estableix l'entrada a desinstal·lar si la instal·lació falla (error 422864)

### KQuickCharts

+ També té en compte les propietats del model quan usa «ModelHistorySource»

### KRunner

+ Implementa el vigilant de KConfig per als connectors habilitats i els executors de KCM (error 421426)
+ No elimina el mètode virtual des de la construcció (error 423003)
+ Fa obsolet «AbstractRunner::dataEngine(...)»
+ Esmena els executors inhabilitats i la configuració de l'executor per al plasmoide
+ Retard en l'emissió de les metadades portant avisos fins al KF 5.75

### KService

+ Afegeix sobrecàrregues per a invocar el terminal amb variables ENV (error 409107)

### KTextEditor

+ Afegeix icones a tots els botons del missatge de fitxer modificat (error 423061)
+ Empra els URL canònics de docs.kde.org

### Framework del KWallet

+ Empra un Nom millor i segueix les HIG
+ Marca l'API com obsoleta també en la descripció de la interfície de D-Bus
+ Afegeix una còpia de org.kde.KWallet.xml sense una API obsoleta

### KWayland

+ plasma-window-management: s'adapta als canvis en el protocol
+ PlasmaWindowManagement: adopta els canvis en el protocol

### KWidgetsAddons

+ KMultiTabBar: icones de pestanya de pintura actives en passar-hi el ratolí per sobre
+ Esmena la «KMultiTabBar» per a pintar la icona de desplaçat cap avall/marcada pel disseny de l'estil
+ Empra la nova icona de sobreescriptura per a l'element Sobreescriure de la IGU (error 406563)

### KXMLGUI

+ Fa públic «KXmlGuiVersionHandler::findVersionNumber» en el «KXMLGUIClient»

### NetworkManagerQt

+ Elimina els secrets del registre

### Frameworks del Plasma

+ Estableix la propietat de cpp sobre la instància de les unitats
+ Afegeix algunes importacions que manques al «PlasmaCore»
+ Elimina l'ús de les propietats de context per tema i unitats
+ PC3: millora l'aspecte del menú
+ Ajusta de nou les icones d'àudio perquè coincideixin amb les icones del Brisa
+ Fa que «showTooltip()» sigui invocable des de QML
+ [PlasmaComponents3] fa honor a la propietat «icon[name|source]»
+ Filtra pels factors de formulari, si està establert
+ Actualitzeu l'estil de la icona de silenci perquè coincideixi amb les icones del Brisa
+ Elimina el registre del tipus MIME trencat per als tipus en què manca el «*» al nom
+ Empra un 35% d'opacitat per als elements esvaïts a les icones de xarxa
+ No mostra els diàlegs de Plasma en els commutadors de tasca (error 419239)
+ Corregeix l'URL d'error de les QT per a hackejar el renderitzat del tipus de lletra
+ No fa ús del cursor de la mà perquè no és coherent
+ [ExpandableListItem]: empra les mides estàndard dels botons
+ [PlasmaComponents3]: mostra l'efecte del focus de «ToolButton» i omet l'ombra quan està pla
+ Unifica l'alçada dels botons PC3, Camps de text i Quadres combinats
+ [PlasmaComponents3]: afegeix les funcions que mancaven a «TextField»
+ [ExpandableListItem]: esmena un error ximple
+ Reescriu el button.svg perquè sigui més fàcil d'entendre
+ Copia els relés de «DataEngine» abans d'iterar (error 423081)
+ Fa que la intensitat del senyal en les icones de xarxa sigui més visible (error 423843)

### QQC2StyleBridge

+ Empra l'estil «elevat» per als botons d'eines no plans
+ Només reserva espai per a la icona en el botó d'eines si realment hi tenim una icona
+ Implementació per a mostrar una fletxa de menú en els botons d'eines
+ Realment esmena l'alçada del separador de menú a PPP alt
+ Actualitza el Mainpage.dox
+ Estableix correctament l'alçada de «MenuSeparator» (error 423653)

### Solid

+ Neteja «m_deviceCache» abans de tornar a inspeccionar (error 416495)

### Ressaltat de la sintaxi

+ Converteix «DetectChar» a un «Detect2Chars» adequat
+ Matemàtiques: algunes millores
+ Doxygen: esmena alguns errors. DoxygenLua: comença només amb --- o --!
+ AutoHotkey: reescriptura completa
+ Nim: esmena els comentaris
+ Afegeix el ressaltat de sintaxi per a Nim
+ Esquema: esmena l'identificador
+ Esquema: esmena els comentaris de referència, comentaris imbricats i altres millores
+ Substitueix algunes ExpReg per a «AnyChar», «DetectChar», «Detect2Chars» o «StringDetect»
+ language.xsd: elimini «HlCFloat» i introdueix el tipus «char»
+ KConfig: esmena $(...) i els operadors + algunes millores
+ C++ de l'ISO: esmena el rendiment en el ressaltat dels nombres
+ Lua: atribut amb Lua54 i algunes altres millores
+ Substitueix «RegExpr=[.]{1,1}» per «DetectChar»
+ Afegeix el ressaltat de PureScript basat en les regles de Haskell. Dona una molt bona aproximació i punt de partida per a PureScript
+ README.md: empra els URL canònics de docs.kde.org

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
