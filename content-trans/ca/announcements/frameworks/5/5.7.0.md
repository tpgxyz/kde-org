---
aliases:
- ../../kde-frameworks-5.7.0
date: '2015-02-14'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### General

- Diverses esmenes per compilar amb les properes Qt 5.5

### KActivities

- S'ha esmenat l'inici i l'aturada de les activitats
- S'ha esmenat la vista prèvia de les activitats que ocasionalment mostraven un fons de pantalla incorrecte

### KArchive

- Crea els fitxers temporals en el directori temporal abans que en el directori actual

### KAuth

- S'ha esmenat la generació dels fitxers del servei d'ajuda de D-Bus del «KAuth»

### KCMUtils

- S'ha esmenat una declaració quan el camí de D-Bus conté un «.»

### KCodecs

- S'ha afegit la implementació del CP949 al KCharsets

### KConfig

- El kconf_update ja no processa més fitxers *.upd del KDE SC 4. S'ha afegit «Version=5» a l'inici del fitxer upd per actualitzacions que s'haurien d'aplicar a les aplicacions Qt5/KF5
- S'ha esmenat el KCoreConfigSkeleton quan s'ha canviat un valor mentre s'està desant

### KConfigWidgets

- KRecentFilesAction: s'ha esmenat l'ordre de l'entrada del menú (de manera que coincideix amb l'ordre de les kdelibs4)

### KCoreAddons

- KAboutData: Crida l'«addHelpOption» i l'«addVersionOption» automàticament, per comoditat i coherència
- KAboutData: Retorna «Si us plau, useu http://bugs.kde.org per a informar d'errors» quan no s'ha definit cap altre correu electrònic o URL
- KAutoSaveFile: allStaleFiles() ara funciona com s'espera per als fitxers locals, també s'ha esmenat staleFiles()
- KRandomSequence ara usa enters internament i exposa l'API «int» per desfer l'ambigüitat en 64 bits
- Definicions de tipus MIME: els fitxers *.qmltypes i *.qmlproject també tenen el tipus MIME text/x-qml
- KShell: fer que el quoteArgs citi els URL amb QChar::isSpace(), els caràcters d'espai no inusuals no es gestionaven adequadament
- KSharedDataCache: s'ha esmenat la creació del directori que conté la memòria cau (error adaptat)

### KDBusAddons

- S'ha afegit el mètode d'ajuda KDEDModule::moduleForMessage per a escriure més dimonis com el kded, per exemple el kiod

### KDeclarative

- S'ha afegit un component traçador
- S'ha afegit un mètode de sobrecàrrega perquè Formats::formatDuration sigui enter
- S'han afegit les propietats noves paintedWidth i paintedHeight al QPixmapItem i al QImageItem
- S'ha esmenat el pintat al QImageItem i al QPixmapItem

### Kded

- S'ha afegit la implementació per carregar els mòduls del kded amb metadades del JSON

### KGlobalAccel

- Ara inclou el component de la versió d'execució, convertint-ho en un framework de nivell 3
- S'ha fet que el dorsal del Windows torni a funcionar
- Reactivat del dorsal del Mac
- S'ha esmenat un error en l'aturada de la versió d'execució de les X11 del KGlobalAccel

### KI18n

- Marca els resultats com a requerits per avisar quan es fa un ús erroni de l'API
- S'ha afegit l'opció BUILD_WITH_QTSCRIPT del sistema de construcció per a permetre un conjunt de funcionalitats reduït en sistemes incrustats

### KInit

- OSX: carregar les biblioteques compartides correctes en temps d'execució
- Esmenes de compilació del Mingw

### KIO

- S'ha esmenat una fallada en els treballs en enllaçar al KIOWidgets però només utilitzant una QCoreApplication
- S'ha esmenat l'edició de les dreceres web
- S'ha afegit l'opció KIOCORE_ONLY, per a compilar només el KIOCore i els seus programes d'ajuda, però no el KIOWidgets ni el KIOFileWidgets, d'aquesta manera es redueixen molt les dependències necessàries
- S'ha afegit la classe KFileCopyToMenu, que afegeix «Copia a/ Mou a» als menús emergents
- Protocols SSL actius: s'ha afegit la implementació per als protocols TLS v1.1 i TLS v1.2, s'ha eliminat SSL v3
- S'ha esmenat negotiatedSslVersion i negotiatedSslVersionName per retornar el protocol negociat real
- Aplica l'URL introduït a la vista en clicar el botó que retorna el navegador d'URL al mode fil d'Ariadna
- S'han esment dues barres de progrés/diàlegs que apareixien en treballs de copiar/moure
- El KIO ara usa el seu dimoni propi, kiod, per als serveis externs al procés que prèviament s'executaven en el kded, per tal de reduir les dependències. Actualment només substitueix el kssld
- S'ha esmenat l'error «No s'ha pogut escriure a &lt;camí&gt;» quan s'activa el kioexec
- S'han esmenat els avisos «QFileInfo::absolutePath: S'ha construït amb un nom de fitxer buit» en usar el KFilePlacesModel

### KItemModels

- S'ha esmenat el KRecursiveFilterProxyModel per les Qt 5.5.0+, ja que el QSortFilterProxyModel ara usa el paràmetre dels rols al senyal dataChanged

### KNewStuff

- Carregar sempre les dades XML des dels URL remots

### KNotifications

- Documentació: es mencionen els requisits de nom de fitxer dels fitxers .notifyrc
- S'ha esmenat un apuntador penjant («dangling») al KNotification
- S'ha esmenat una fuita al knotifyconfig
- S'ha instal·lat una capçalera del knotifyconfig que mancava

### KPackage

- S'ha canviat el nom del «man» del kpackagetool al kpackagetool5
- S'ha esmenat la instal·lació en sistemes de fitxers que no distingeixen majúscules i minúscules

### Kross

- S'ha esmenat el Kross::MetaFunction per tal que funcioni amb el sistema de metaobjectes del Qt5

### KService

- Incloure propietats desconegudes en convertir al KPluginInfo des del KService
- KPluginInfo: s'han esmenat les propietats que no es copien des del KService::Ptr
- OS X: esmena de rendiment per al kbuildsycoca4 (s'ometen els paquets d'app)

### KTextEditor

- S'ha esmenat el desplaçament d'alta precisió del ratolí tàctil
- No emetre el documentUrlChanged durant la recàrrega
- No trencar la posició del cursor en la recàrrega de documents en línies amb tabulacions
- No tornar a (des)plegar la primera línia si estava (des)plegada manualment
- vimode: historial d'ordres mitjançant les tecles de cursor
- No intentar crear un resum quan s'obté un senyal KDirWatch::deleted()
- Rendiment: eliminar les inicialitzacions globals

### KUnitConversion

- S'ha esmenat un procés recursiu infinit a l'Unit::setUnitMultiplier

### KWallet

- Detecta i converteix automàticament les carteres ECB antigues a CBC
- S'ha esmenat l'algorisme d'encriptatge CBC
- Assegurar la llista de carteres s'actualitza quan s'elimina un fitxer de cartera del disc
- Eliminar els &lt;/p&gt; aïllats en el text visible per l'usuari

### KWidgetsAddons

- Usar el kstyleextensions per especificar un element de control personalitzat per representar la barra del kcapacity quan està acceptat, això permet que es pugui posar l'estil adequat al giny
- Proporcionar un nom accessible per al KLed

### KWindowSystem

- S'ha esmenat NETRootInfo::setShowingDesktop(bool) que no funcionava a l'Openbox
- S'ha afegit el mètode d'utilitat KWindowSystem::setShowingDesktop(bool)
- Esmenes en la gestió de format de les icones
- S'ha afegit el mètode NETWinInfo::icccmIconPixmap que proporciona una mapa de píxels de les icones des de la propietat WM_HINTS
- S'ha afegit una sobrecàrrega al KWindowSystem::icon que redueix les crides al servidor X
- S'ha afegit la implementació per _NET_WM_OPAQUE_REGION

### NetworkmanagerQt

- No imprimir cap missatge quant a la propietat no gestionada «AccessPoints»
- S'ha afegit la implementació per al NetworkManager 1.0.0 (no requerit)
- S'ha esmenat la gestió dels secrets VpnSetting
- S'ha afegit la classe «GenericSetting» per a les connexions no gestionades pel NetworkManager
- S'ha afegit la propietat AutoconnectPriority al ConnectionSettings

#### Frameworks del Plasma

- S'ha esmenat l'obertura errònia d'un menú contextual trencat en clicar en la finestra emergent del Plasma amb el botó del mig
- El botó activador canvia a la roda del ratolí
- No redimensionar mai un diàleg més gran que la pantalla
- Desfer la supressió dels plafons quan es desfà la supressió de la miniaplicació
- S'han esmenat les dreceres del teclat
- Restaurar la implementació «hint-apply-color-scheme»
- Torna a carregar la configuració quan el plasmarc canvia
- ...

### Solid

- S'ha afegit energyFull i energyFullDesign a «Battery»

### Canvis en el sistema de construcció (extra-cmake-modules)

- Mòdul ECMUninstallTarget nou per crear un objectiu de desinstal·lació
- Fer que el KDECMakeSettings importi l'ECMUninstallTarget per defecte
- KDEInstallDirs: avís quant a mesclar camins d'instal·lació relatius i absoluts en la línia d'ordres
- S'ha afegit el mòdul «ECMAddAppIcon» per a afegir icones als objectius executables al Windows i el Mac OS X
- S'ha esmenat l'avís CMP0053 amb el CMake 3.1
- No netejar les variables de la memòria cau a KDEInstallDirs

### Integració del marc de treball

- S'ha esmenat l'actualització de l'opció d'un clic simple en temps d'execució
- Esmenes múltiples a la integració amb la safata del sistema
- Instal·lar només l'esquema de colors en els ginys de nivell superior (per esmenar el QQuickWidgets)
- Actualitzar els paràmetres del XCursor a la plataforma X11

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
