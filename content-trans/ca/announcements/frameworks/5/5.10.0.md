---
aliases:
- ../../kde-frameworks-5.10.0
date: '2015-05-08'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### KActivities

- (No s'ha proporcionat cap registre de canvis)

### KConfig

- Generar classes vàlides del QML usant el «kconfigcompiler»

### KCoreAddons

- Nova macro «kcoreaddons_add_plugin» del «cmake» per crear connectors basats en el KPluginLoader més fàcilment.

### KDeclarative

- Esmenar una fallada en la memòria cau de les textures.
- i altres esmenes

### KGlobalAccel

- Afegir el nou mètode «globalShortcut» que recupera la drecera com s'ha definit en l'arranjament global.

### KIdleTime

- Evitar que el «kidletime» falli en la plataforma Wayland

### KIO

- S'ha afegit «KPropertiesDialog::KPropertiesDialog(urls)» i «KPropertiesDialog::showDialog(urls)».
- Recuperació asíncrona de dades basades en el «QIODevice» per «KIO::storedPut» i «KIO::AccessManager::put».
- Esmenar condicions amb el valor de retorn de QFile::rename (error 343329)
- S'ha esmenat KIO::suggestName per suggerir noms millors (error 341773)
- kioexec: S'ha esmenat el camí per ubicacions que es puguin escriure per «kurl» (error 343329)
- Emmagatzemar les adreces d'interès només a user-places.xbel (error 345174)
- Duplicar l'entrada de RecentDocuments si dos fitxers diferents tenen el mateix nom
- Missatge d'error millor si un fitxer únic és massa gran per a la paperera (error 332692)
- Esmenar una fallada del KDirLister sobre la redirecció quan el sòcol invoca «openURL»

### KNewStuff

- Conjunt nou de classes, anomenat KMoreTools i relacionats. Les KMoreTools ajuden a afegir consells quant a eines externes que potencialment encara no estan instal·lades. A més, fa més curts els menús llargs proporcionant una secció principal i altres que també són configurables per l'usuari.

### KNotifications

- Esmenar KNotifications quan s'usa amb el NotifyOSD de l'Ubuntu (error 345973)
- No activar les actualitzacions de notificació quan s'han definit les mateixes propietats (error 345973)
- Presentar l'indicador LoopSound que permet a les notificacions reproduir so en un bucle si ho necessiten (error 346148)
- No fallar si la notificació no té cap giny

### KPackage

- Afegir una funció KPackage::findPackages similar a la KPluginLoader::findPlugins

### KPeople

- Usar «KPluginFactory» per a instanciar els connectors, en lloc de «KService» (es manté per compatibilitat).

### KService

- Esmenar una divisió incorrecta del camí d'entrada (error 344614)

### KWallet

- L'agent de migració ara també valida que la cartera antiga estigui buida abans d'iniciar (error 346498)

### KWidgetsAddons

- KDateTimeEdit: Esmenar perquè l'entrada de l'usuari quedi registrada realment. Esmenar els marges dobles.
- KFontRequester: Esmenar la selecció de només els tipus de lletra monoespai

### KWindowSystem

- No fer dependre KXUtils::createPixmapFromHandle de QX11Info (error 346496)
- Mètode nou NETWinInfo::xcbConnection() -&gt; xcb_connection_t*

### KXmlGui

- Esmenar les dreceres quan s'ha definit una drecera secundària (error 345411)
- Actualitzar la llista de productes/components del Bugzilla per a informes d'error (error 346559)
- Dreceres globals: Permetre configurar també la drecera alternativa

### NetworkManagerQt

- Les capçaleres instal·lades ara estan organitzades com tots els altres Frameworks.

### Frameworks del Plasma

- El PlasmaComponents.Menu ara permet seccions
- Utilitzar el KPluginLoader en lloc del «ksycoca» per carregar els motors de dades C++
- Considerar el gir de «visualParent» en «popupPosition» (error 345787)

### Sonnet

- No intentar ressaltar si no s'ha trobat cap verificador ortogràfic. Això portaria a un bucle infinit amb el temporitzador «rehighlighRequest» disparant-se constantment.

### Integració del marc de treball

- Esmenar els diàlegs de fitxer natius dels ginys QFileDialog: ** Els diàlegs de fitxer oberts amb exec() i sense pare s'obren, però qualsevol interacció d'usuari estava bloquejada d'una manera que no es podia seleccionar cap fitxer ni tancar el diàleg. ** Els diàlegs de fitxer oberts amb open() o show() amb un pare no s'obrien en absolut.

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
