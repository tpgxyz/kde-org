---
aliases:
- ../../kde-frameworks-5.43.0
date: 2018-02-12
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Mòduls nous

KHolidays: biblioteca de càlcul de festius

Aquesta biblioteca proporciona una API del C++ que determina els festius i altres esdeveniments especials d'una regió geogràfica.

Purpose: Ofereix les accions disponibles per a un propòsit específic

Aquest Framework ofereix la possibilitat de crear serveis integrats i accions en qualsevol aplicació sense haver d'implementar-les específicament. El Purpose ofereix mecanismes per a llistar les diferents alternatives a executar donant el tipus d'acció requerida i facilita components de manera que tots els connectors poden rebre tota la informació que necessiten.

### Baloo

- Estat del «balooctl»: Produeix una sortida analitzable
- Soluciona les còpies profundes de carpetes etiquetades de l'esclau KIO. Això trenca el llistat de les carpetes etiquetades a l'arbre d'etiquetes, però és millor que les còpies trencades
- Omet la posada en cua dels fitxers nous que no es poden indexar i els elimina immediatament de l'índex
- Suprimeix de l'índex els fitxers moguts nous que no es poden indexar

### Icones Brisa

- Afegeix les icones del Krusader que manquen per a la sincronització de carpetes (error 379638)
- Actualitza la icona d'eliminar llista amb - en lloc de la icona de cancel·lació (error 382650)
- Afegeix icones per al plasmoide del PulseAudio (error 385294)
- Usa a tot arreu la mateixa opacitat 0,5
- Icona nova del «virtualbox» (error 384357)
- Fa neutre el temps emboirat dia/nit (error 388865)
- Instal·la realment el context nou d'animacions
- El MIME dels fitxers QML ara semblen el mateix en totes les mides (error 376757)
- Actualitza les icones d'animació (error 368833)
- Afegeix una icona d'emblema compartit acolorit
- Soluciona els fitxers «index.theme» trencats, es va perdre el «Context=Status» en «status/64»
- Elimina els permisos d'execució dels fitxers .svg
- La icona d'acció de baixada s'enllaça a «edit-download» (error 382935)
- Actualitza el tema d'icones de la safata del sistema del Dropbox (error 383477)
- Manca «emblem-default-symbolic» (error 382234)
- Escriu el nom del fitxer al tipus MIME (error 386144)
- Usa un logo de l'Octave més específic (error 385048)
- Afegeix les icones de la Caixa forta (error 386587)
- Escala els píxels de les icones d'estat (error 386895)

### Mòduls extres del CMake

- FindQtWaylandScanner.cmake: Usa «qmake-query» per a HINT
- Assegura que cerca «qmlplugindump» basat en Qt5
- ECMToolchainAndroid ja no existeix més (error 389519)
- No defineix la LD_LIBRARY_PATH a «prefix.sh»
- Afegeix FindSeccomp a «find-modules»
- Alternativa al nom de l'idioma per a la cerca de traduccions si falla el nom de «locale»
- Android: Afegeix més inclusions

### KAuth

- Soluciona una regressió d'enllaçat introduïda a la 5.42.

### KCMUtils

- Afegeix consells d'eines als dos botons de cada entrada

### KCompletion

- Soluciona una emissió incorrecta de «textEdited()» des de KLineEdit (error 373004)

### KConfig

- Usa Ctrl+Maj+, com a drecera estàndard per a «Configura &lt;Programa&gt;»

### KCoreAddons

- Quadra les claus «spdx» també amb LGPL-2.1 i LGPL-2.1+
- Usa el mètode molt més ràpid «urls()» des de QMimeData (error 342056)
- Optimitza el dorsal KDirWatch d'«inotify»: mapa l'«inotify wd» a Entry
- Optimització: Usa QMetaObject::invokeMethod amb un functor

### KDeclarative

- [ConfigModule] Reusa el context i el motor del QML, si n'hi ha (error 388766)
- [ConfigPropertyMap] Afegeix una inclusió que manca
- [ConfigPropertyMap] No emet «valueChanged» a la creació inicial

### KDED

- No exporta «kded5» com a un objectiu del CMake

### Compatibilitat amb les KDELibs 4

- Refactoritza Solid::NetworkingPrivate per tenir una implementació compartida i específica de plataforma
- Soluciona l'error del «mingw» de compilació «src/kdeui/kapplication_win.cpp:212:22: error: 'kill' was not declared in this scope»
- Esmena el nom de D-Bus del «kded» a la Guia pràctica del «solid-networking»

### KDesignerPlugin

- Fa opcional la dependència de «kdoctools»

### KDESU

- Fa que el mode KDESU_USE_SUDO_DEFAULT es torni a construir
- Fa que funcioni el «kdesu» quan PWD és /usr/bin

### KGlobalAccel

- Usa la funció «kdbusaddons_generate_dbus_service_file» de CMake des del «kdbusaddons» per a generar un fitxer de servei de D-Bus (error 382460)

### Complements de la IGU del KDE

- Soluciona l'enllaçat del fitxer QCH creat en els documents de QtGui

### KI18n

- Soluciona la cerca de la «libintl» en compilar de forma creuada els paquets natius Yocto

### KInit

- Soluciona la compilació creuada amb MinGW (MXE)

### KIO

- Repara el copiatge del fitxer a la VFAT sense avisos
- kio_file: Omet la gestió d'error dels permisos inicials durant la còpia de fitxers
- kio_ftp: No emet el senyal d'error abans que s'intentin totes les ordres «list» (error 387634)
- Rendiment: Usa el destí de l'objecte KFileItem per resoldre si es pot escriure en lloc de crear una KFileItemListProperties
- Rendiment: Usa el constructor de còpia KFileItemListProperties en lloc de la conversió des de KFileItemList a KFileItemListProperties. Això estalvia la reavaluació de tots els elements
- Millora la gestió d'error al fitxer «ioslave»
- Elimina l'indicador de treball PrivilegeExecution
- KRun: Permet executar «afegeix una carpeta de xarxa» sense pregunta de confirmació
- Permet filtrar llocs basant-se en noms d'aplicació alternatius
- [Uri Filter Search Provider] Evita una eliminació doble (error 388983)
- Soluciona la superposició del primer element a KFilePlacesView
- Desactiva temporalment el funcionament del KAuth al KIO
- previewtest: Permet especificar els connectors habilitats
- [KFileItem] Usa «emblem-shared» per als fitxers compartits
- [DropJob] Activa l'arrossegat i deixat anar en carpetes de només lectura
- [FileUndoManager] Activa desfer canvis en carpetes de només lectura
- Afegeix la implementació per execució amb privilegis als treballs KIO (desactivat temporalment en aquesta publicació)
- Afegeix la implementació per compartir el descriptor de fitxer entre el fitxer de l'esclau KIO i el seu «helper» del KAuth
- Esmena KFilePreviewGenerator::LayoutBlocker (error 352776)
- KonqPopupMenu/Plugin ara pot usar la clau X-KDE-RequiredNumberOfUrls per requerir un determinat nombre de fitxers a seleccionar abans de mostrar-los
- [KPropertiesDialog] Activa l'ajust de paraules per a la descripció de les sumes de verificació
- Usa la funció «kdbusaddons_generate_dbus_service_file» de CMake des del «kdbusaddons» per a generar un fitxer de servei de D-Bus (error 388063)

### Kirigami

- Implementació de ColorGroups
- No hi ha recció de clic si l'element no admet esdeveniments de ratolí
- Solució alternativa per a aplicacions que usen «listitems» incorrectament
- Espai per a la barra de desplaçament (error 389602)
- Proporciona un consell d'eina per a l'acció principal
- CMake: Usa la variable oficial del CMake per construir com a connector estàtic
- Actualitza la designació dels nivells de forma intel·ligible a la «dox» de l'API
- [ScrollView] Desplaça una pàgina amb Maj+roda
- [PageRow] Navega entre nivells amb els botons enrere/endavant del ratolí
- Verifica que el DesktopIcon es pinta amb la relació correcta d'aspecte (error 388737)

### KItemModels

- KRearrangeColumnsProxyModel: esmena una fallada quan no hi ha cap model d'origen
- KRearrangeColumnsProxyModel: torna a implementar «sibling()» de manera que funcioni com s'espera

### KJobWidgets

- Desduplicació de codi a «byteSize(double size)» (error 384561)

### KJS

- Fa opcional la dependència de «kdoctools»

### KJSEmbed

- Desexporta «kjscmd»
- Fa opcional la dependència de «kdoctools»

### KNotification

- S'ha esmenat l'acció de notificació de «Run Command» (error 389284)

### KTextEditor

- Esmena: La vista salta quan el desplaçament després del final del document està actiu (error 306745)
- Usa almenys l'amplada requerida per l'arbre de consells dels arguments
- ExpandingWidgetModel: Cerca la columna més a la dreta segons la ubicació

### KWidgetsAddons

- KDateComboBox: Soluciona una «dateChanged()» no emesa després de teclejar una data (error 364200)
- KMultiTabBar: Soluciona una regressió en la conversió a l'estil «connect()» nou

### Frameworks del Plasma

- Defineix la propietat a Units.qml per als estils del Plasma
- windowthumbnail: Soluciona el codi de selecció GLXFBConfig
- [Default Tooltip] Esmena la mida (error 389371)
- [Plasma Dialog] Només crida els efectes de finestra si és visible
- Esmena una font de brossa al registre referenciada a l'error 388389 (Nom de fitxer buit passat a una funció)
- [Calendar] Ajusta les àncores de la barra d'eines del calendari
- [ConfigModel] Defineix el context QML al ConfigModule (error 388766)
- [Icon Item] Tracta les fonts que comencen amb una barra com a fitxers locals
- Soluciona l'aparença RTL d'una ComboBox (error 387558)

### QQC2StyleBridge

- Afegeix BusyIndicator a la llista de controls amb estil
- Elimina el lliscador en passar per sobre de la barra de desplaçament

### Solid

- [UDisks] Només ignora els fitxers «backing» que no siguin d'usuari si es coneixen (error 389358)
- Els dispositius d'emmagatzematge muntats fora de /media, /run/media, i $HOME ara s'ignoren, així com els dispositius «loop» (error 319998)
- [UDisks Device] Mostra el dispositiu «loop» amb el seu nom de fitxer «backing» i la icona

### Sonnet

- Cerca els diccionaris Aspell al Windows

### Ressaltat de la sintaxi

- Soluciona una «var» «regex» al C#
- Admet caràcters de subratllat als literals numèrics (Python 3.6) (error 385422)
- Ressaltat de fitxers Khronos Collada i glTF
- Esmena el ressaltat inicial de valors que contenen els caràcters ; o #
- AppArmor: Paraules clau noves, millores i esmenes

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
