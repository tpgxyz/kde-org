---
aliases:
- ../../kde-frameworks-5.39.0
date: 2017-10-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Només coincideix amb els tipus MIME reals, no p. ex. «imatge RAW de CD» (error 364884)
- Elimina «pf.path()» del contenidor abans que la referència esdevingui confusa
- Esmena les etiquetes de descripció dels protocols KIO-slave
- Considera els fitxers Markdown com a documents

### Icones Brisa

- Afegeix la icona de desbordament de menú (error 385171)

### Mòduls extres del CMake

- Esmena la compilació de les vinculacions del Python després de 7af93dd23873d0b9cdbac192949e7e5114940aa6

### Integració del marc de treball

- Fa que KStandardGuiItem::discard coincideixi amb QDialogButtonBox::Discard

### KActivitiesStats

- Canvia el límit predeterminat de consultes a zero
- Afegeix l'opció per activar el provador de models

### KCMUtils

- Fa que el «KCMultiDialog» sigui desplaçable (error 354227)

### KConfig

- Fa obsolet KStandardShortcut::SaveOptions

### KConfigWidgets

- Fa obsolets KStandardAction::PasteText i KPasteTextAction

### KCoreAddons

- «desktoptojson»: Millora a l'heurística de la detecció del tipus de servei antic (error 384037)

### KDeclarative

- Torna a llicenciar amb LGPL2.1+
- Afegeix el mètode «openService()» al KRunProxy

### KFileMetaData

- Esmena una fallada quan es destrueixen una o més instàncies de l'ExtractorCollection

### KGlobalAccel

- Reverteix «KGlobalAccel: Adapta «symXModXToKeyQt» al nou mètode de KKeyServer, per solucionar les tecles del teclat numèric» (error 384597)

### KIconThemes

- Afegeix un mètode per reiniciar la paleta personalitzada
- Usa «qApp-&gt;palette()» quan no se n'ha definit cap de personalitzada
- Assigna la mida adequada de la memòria intermèdia
- Permet definir una paleta personalitzada en lloc de «colorSets»
- Exposa el conjunt de colors del full d'estil

### KInit

- Windows: Esmena «El klauncher usa un camí absolut en temps de compilació per cercar kioslave.exe»

### KIO

- kioexec: Vigila el fitxer quan ha finalitzat la còpia (error 384500)
- KFileItemDelegate: Sempre reserva espai per a les icones (error 372207)

### Kirigami

- No instancia el fitxer Theme a BasicTheme
- Afegeix un botó Endavant nou
- Menys contrast del fons del full de la barra de desplaçament
- Una inserció i eliminació més fiables des del menú de desbordament
- Una renderització millor de la icona de context
- Centra amb més cura el botó d'acció
- Usa les mides de les icones per als botons d'accions
- Mida de les icones ajustades perfectament als píxels a l'escriptori
- Efecte seleccionat a la icona falsa de gestió
- Esmena els colors de les manetes
- Color millor per al botó d'acció principal
- Esmena el menú contextual per a l'estil escriptori
- Menú «més» millorat per la barra d'eines
- Un menú adequat per al menú contextual de les pàgines intermèdies
- Afegeix un camp de text que hauria de fer emergir un teclat numèric
- No falla quan es llança amb estils inexistents
- Concepte de ColorSet a Theme
- Simplifica la gestió de la roda (error 384704)
- Exemple nou d'aplicació amb fitxers principals QML per a escriptori/mòbil
- Assegura que «currentIndex» és vàlid
- Genera les metadades «appstream» de la galeria de l'aplicació
- Cerca QtGraphicalEffects, per tal que els empaquetadors no l'oblidin
- No inclou el control sobre la decoració inferior (error 384913)
- Color més clar quan la vista de llista no tingui «activeFocus»
- Afegeix la implementació inicial per les disposicions RTL
- Desactiva les dreceres quan una acció està desactivada
- Crea tota l'estructura del connector en el directori de construcció
- Esmena l'accessibilitat de la pàgina principal de la galeria
- Si el Plasma no és disponible, el KF5Plasma tampoc ho és. Hauria de solucionar l'error de CI

### KNewStuff

- KNewStuffQuick requereix Kirigami 2.1 en lloc de 1.0
- Crea KPixmapSequence adequadament
- No es queixa que el fitxer «knsregistry» no estigui present abans que sigui utilitzat

### Framework del KPackage

- kpackage: Empaqueta una còpia de «servicetypes/kpackage-generic.desktop»
- kpackagetool: Empaqueta una còpia de «servicetypes/kpackage-generic.desktop»

### KParts

- Plantilla KPartsApp: Esmena la ubicació d'instal·lació del fitxer «desktop» de la «kpart»

### KTextEditor

- Ignora la marca predeterminada a la vora de la icona com a marca seleccionable única
- Usa QActionGroup per a la selecció del mode d'entrada
- Esmena la barra de verificació ortogràfica que manca (error 359682)
- Esmena el valor de reserva «blackness» per als caràcters Unicode &gt; 255 (error 385336)
- Esmena la visualització dels espais finals per les línies RTL

### KWayland

- Envia OutputConfig «sendApplied» / «sendFailed» només al recurs correcte
- No falla si un client (legalment) usa un gestor de contrast global suprimit
- Implementa XDG v6

### KWidgetsAddons

- KAcceleratorManager: Estableix el text de les icones en accions per eliminar els marcadors CJK (error 377859)
- KSqueezedTextLabel: Comprimeix el text en canviar el sagnat o els marges
- Usa la icona «edit-delete» per accions de descart destructives (error 385158)
- Soluciona l'error 306944 - Usa la roda del ratolí per augmentar/disminuir les dates (error 306944)
- KMessageBox: Usa la icona d'interrogació per les preguntes dels diàlegs
- KSqueezedTextLabel: Respecta el sagnat, els marges i l'amplada del marc

### KXMLGUI

- Soluciona un bucle de repintat a KToolBar (error 377859)

### Frameworks del Plasma

- Esmena org.kde.plasma.calendar amb les Qt 5.10
- [FrameSvgItem] Itera adequadament els nodes fills
- [Containment Interface] No afegeix accions de contenidor a les accions de la miniaplicació a l'escriptori
- Afegeix un component nou per les etiquetes en gris als «Item Delegates»
- Esmena FrameSVGItem amb el renderitzador per programari
- No anima IconItem en el mode de programari
- [FrameSvg] Usa la connexió d'estil nou
- Possibilita definir un àmbit de color annexat que no s'hereti
- Afegeix un indicador visual extra per al focus de teclat de les caselles de selecció/opcions
- No torna a crear un mapa de píxels nul
- Passa un element a «rootObject()», ja que ara és un «singleton» (error 384776)
- No llista els noms de pestanyes per duplicat
- No accepta el focus actiu en la pestanya
- Registra la revisió 1 per al «QQuickItem»
- [Components del Plasma 3] Soluciona el RTL en diversos ginys
- Esmena una ID no vàlida a «viewitem»
- Actualitza la icona de notificació de correu per obtenir un contrast millor (error 365297)

### qqc2-desktop-style

Mòdul nou: estil de QtQuickControls 2 que usa el QStyle de QWidget per pintar.Això fa possible aconseguir un grau més alt de coherència entre les aplicacions basades en QWidget i les basades en QML.

### Solid

- [solid/fstab] Afegeix la implementació per a les opcions d'estil «x-gvfs» al «fstab»
- [solid/fstab] Intercanvia les propietats de fabricant i producte, permetent l'«i18n» de la descripció

### Ressaltat de la sintaxi

- Esmena les referències «itemData» no vàlides de 57 fitxers de ressaltat
- Afegeix la implementació de camins de cerca personalitzats per a la sintaxi específica d'aplicació i les definicions de tema
- AppArmor: Esmena les regles de D-Bus
- Indexador de ressaltat: Desfactoritza les comprovacions per als bucles «while» més petits
- ContextChecker: Admet el commutador de context «!» i «fallthroughContext»
- Indexador de ressaltat: Comprova l'existència de noms de context referenciats
- Torna a llicenciar el ressaltat del «qmake» a la llicència MIT
- Permet que el ressaltat del «qmake» guanyi sobre el Prolog per als fitxers .pro (error 383349)
- Permet la macro «@» amb claudàtors del Clojure
- Afegeix el ressaltat de sintaxi per als perfils AppArmor
- Indexador de ressaltat: Detecta intervals a-Z/A-z no vàlids a les «regexps»
- Esmena d'intervals en majúscules incorrectes a les «regexps»
- Afegeix fitxers de referència per proves que manquen, sembla correcte
- S'ha afegit la implementació per als fitxers HEX d'Intel a la base de dades del ressaltat de sintaxi
- Desactiva la verificació ortogràfica de cadenes als scripts del Sieve

### ThreadWeaver

- Soluciona una fuita de memòria

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
