---
aliases:
- ../../kde-frameworks-5.42.0
date: 2018-01-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Canvis generals

- Esmenes per als avisos AUTOMOC de CMake 3.10+
- Ús més ampli de l'enregistrament categoritzat, per desconnectar la sortida de depuració predeterminada (useu «kdebugsetting» per reactivar-la)

### Baloo

- Estat del «balooctl»: processa tots els arguments
- Soluciona les consultes d'etiquetes amb múltiples paraules
- Simplifica les condicions per reanomenar
- Soluciona un nom de pantalla UDSEntry incorrecte

### Icones Brisa

- Esmena el nom d'icona «weather-none» -&gt; «weather-none-available» (error 379875)
- Elimina la icona del Vivaldi, ja que la icona original de l'aplicació encaixa perfectament amb el Brisa (error 383517)
- Afegeix diversos MIME que manquen
- Les icones Brisa afegeixen la icona «document-send» (error 388048)
- Actualitza la icona d'artista d'àlbum
- Afegeix la implementació de «labplot-editlayout»
- Elimina els duplicats i actualitza el tema fosc
- Afegeix la implementació d'icones Brisa del Gnumeric

### Mòduls extres del CMake

- Usa «readelf» per cercar dependències de projecte
- Introdueix INSTALL_PREFIX_SCRIPT per definir prefixos fàcilment

### KActivities

- Evita una fallada a «kactivities» si no hi ha cap connexió de D-Bus disponible

### KConfig

- API docs: Explica com usar el KWindowConfig des d'un constructor de diàleg
- Fa obsolet KDesktopFile::sortOrder()
- Esmena el resultat de KDesktopFile::sortOrder()

### KCoreAddons

- Amplia també CMAKE_AUTOMOC_MACRO_NAMES per a la pròpia construcció
- Coincideix les claus de llicència amb «spdx»

### KDBusAddons

- Esmena la detecció del camí absolut per a CMake 3.5 a Linux
- Afegeix la funció «kdbusaddons_generate_dbus_service_file» de CMake

### KDeclarative

- Controls escrits en QML per a la creació del KCM

### KDED

- Usa la funció «kdbusaddons_generate_dbus_service_file» de CMake des del «kdbusaddons» per a generar un fitxer de servei de D-Bus
- Afegeix una propietat usada a la definició del fitxer de servei

### Compatibilitat amb les KDELibs 4

- Informa l'usuari si el mòdul no es pot registrar a org.kde.kded5 i surt amb error
- Esmenes de compilació del Mingw32

### KDocTools

- Afegeix una entitat, per en Michael Pyne
- Afegeix entitats a «contributor.entities», per en Martin Koller
- Esmena l'entrada de Debian
- Afegeix l'entitat Debian a «general.entities»
- Afegeix l'entitat «kbackup», que s'ha importat
- Afegeix l'entitat «latex», ja hi ha 7 entitats diferents a «index.docbooks» en KF5

### KEmoticons

- Afegeix l'esquema (file://). És necessari quan s'usa en el QML i s'ha afegit tot

### KFileMetaData

- Elimina l'extractor basat en QtMultimedia
- Comprova per al Linux en lloc de la TagLib i evita la construcció d'«usermetadatawritertest» al Windows
- Restaura el núm. 6c9111a9 fins que sigui possible una construcció correcta i l'enllaçat sense la TagLib
- Elimina la dependència de la TagLib, causada per una inclusió supèrflua

### KHTML

- Permet finalment desactivar la sortida de depuració usant un enregistrament categoritzat

### KI18n

- No tracta «ts-pmap-compile» com a executable
- Esmena una fuita de memòria a KuitStaticData
- KI18n: Esmena diverses cerques dobles

### KInit

- Elimina codi impossible d'arribar-hi

### KIO

- Analitza adequadament les dates a les galetes en executar una configuració regional no anglesa (error 387254)
- [kcoredirlister] Esmena la creació del subcamí
- Reflecteix l'estat brossa a «iconNameForUrl»
- Reenvia els senyals de QComboBox en lloc dels senyals d'edició de línia de QComboBox
- Soluciona les dreceres web que mostraven el seu camí del fitxer en lloc del seu nom llegible per humans
- TransferJob: esmena quan ja s'ha emès «readChannelFinished» abans que s'iniciï «called()» (error 386246)
- Esmena una fallada, presumiblement des de les Qt 5.10? (error 386364)
- KUriFilter: no retorna un error en fitxers no existents
- Soluciona la creació de camins
- Implementa un diàleg «kfile» a un es pot afegir un giny personalitzat
- Verifica que «qWaitForWindowActive» no falli
- KUriFilter: adaptació per evitar el KServiceTypeTrader
- API dox: usa noms de classes als títols de les captures de pantalla d'exemple
- API dox: tracta també l'exportació de macros de «KIOWIDGETS» a la creació de QCH
- Esmena la gestió de KCookieAdvice::AcceptForSession (error 386325)
- S'ha creat «GroupHiddenRole» per KPlacesModel
- Reenvia la cadena d'error del sòcol a KTcpSocket
- Refactoritza i elimina codi duplicat a «kfileplacesview»
- Emet el senyal «groupHiddenChanged»
- Refactoritza l'ús de l'animació d'ocultació/visualització a KFilePlacesView
- L'usuari ara pot ocultar un grup sencer de llocs des de KFilePlacesView (error 300247)
- Implementació de l'ocultació de grups de llocs al KFilePlacesModel (error 300247)
- [KOpenWithDialog] Elimina una creació redundant de KLineEdit
- Afegeix permetre desfer a BatchRenameJob
- Afegeix BatchRenameJob al KIO
- Esmena un bloc de codi del «doxygen» que no finalitza amb «endcode»

### Kirigami

- Manté interactiu el «flickable»
- Prefix adequat també per a les icones
- Esmena la mida del formulari
- Llegeix «wheelScrollLines» des de «kdeglobals», si existeix
- Afegeix un prefix per als fitxers «kirigami» per evitar conflictes
- Diverses esmenes d'enllaçat estàtic
- Mou els estils «plasma» a «plasma-framework»
- Usa cometes simples per als caràcters coincidents + QLatin1Char
- FormLayout

### KJobWidgets

- Ofereix l'API de QWindow per als decoradors KJobWidgets::

### KNewStuff

- Limita la sol·licitud de la mida de memòria cau
- Requereix la mateixa versió interna que s'està construint
- Evita que s'usin les variables globals després de l'alliberament

### KNotification

- [KStatusNotifierItem] No «restaura» la posició del giny en la seva primera aparició
- Usa les posicions de les icones antigues de la safata del sistema per a les accions Minimitza/Restaura
- Gestiona les posicions dels clics amb el botó esquerre del ratolí en icones antigues de la safata del sistema
- Fa que el menú contextual no sigui una finestra
- Afegeix un comentari explicatiu
- Instancia i carrega de manera relaxada els connectors de KNotification

### Framework del KPackage

- Invalida la memòria cau en temps d'execució en instal·lar
- Implementació experimental per a la càrrega de fitxers «rcc» en el «kpackage»
- Compila amb les Qt 5.7
- Soluciona la indexació de paquets i afegeix l'ús de memòria cau en temps d'execució
- Mètode KPackage::fileUrl() nou

### KRunner

- [RunnerManager] No s'embolica amb el comptador de fils del ThreadWeaver

### KTextEditor

- Esmena la concordança de comodins per als «modelines»
- Esmena una regressió causada en canviar el comportament de la tecla de retrocés
- Adaptat a una API no obsoleta, ja que s'usa en un altre lloc (error 386823)
- Afegeix la inclusió per «std::array» que mancava
- MessageInterface: Afegeix CenterInView com a posició addicional
- Neteja la llista d'inicialització de QStringList

### Framework del KWallet

- Usa un camí correcte del servei executable per a instal·lar el servei de D-Bus del «kwalletd» a Win32

### KWayland

- Esmena una incoherència de nomenclatura
- Crea una interfície per passar paletes de decoració del servidor
- Inclou explícitament les funcions «std::bind»
- [server] Afegeix un mètode IdleInterface::simulateUserActivity
- Esmena una regressió causada per una implementació de compatibilitat inversa en la font de dades
- Afegeix la implementació de la versió 3 de la interfície del gestor de dispositius de dades (error 386993)
- Àmbit dels objectes exportats/importats a la prova
- Esmena un error a WaylandSurface::testDisconnect
- Afegeix el protocol AppMenu explícit
- Esmena l'exclusió del fitxer generat des de la funcionalitat AUTOMOC

### KWidgetsAddons

- Soluciona una fallada a «setMainWindow» en el Wayland

### KXMLGUI

- API dox: fa que el «doxygen» torni a cobrir les macros &amp; funcions relacionades de la sessió
- Desconnecta la ranura «shortcutedit» en destruir el giny (error 387307)

### NetworkManagerQt

- 802-11-x: implementació del mètode PWD EAP

### Frameworks del Plasma

- [Air theme] Afegeix un gràfic de progrés a la barra de tasques (error 368215)
- Plantilles: elimina «stray *» de les capçaleres de les llicències
- Fa «packageurlinterceptor» tan «noop» com sigui possible
- Reverteix «No desmantella el renderitzador i altres treballs ocupats quan s'invoca Svg::setImagePath amb el mateix argument»
- Mou aquí els estils Plasma del Kirigami
- Barres de desplaçament que desapareixen en el mòbil
- Reutilitza la instància del KPackage entre PluginLoader i Applet
- [AppletQuickItem] Defineix l'estil QtQuick Controls 1 només una vegada per motor
- No estableix cap icona de finestra a Plasma::Dialog
- [RTL] - Alinea adequadament el text seleccionat per a RTL (error 387415)
- Inicialitza el factor d'escala al darrer factor d'escala definit a qualsevol instància
- Reverteix «Inicialitza el factor d'escala al darrer factor d'escala definit a qualsevol instància»
- No actualitza quan el FrameSvg subjacent està blocat per repintat
- Inicialitza el factor d'escala al darrer factor d'escala definit a qualsevol instància
- Mou una comprovació «if» dins de #ifdef
- [FrameSvgItem] No crea nodes innecessaris
- No desmantella el renderitzador i altres treballs ocupats quan s'invoca Svg::setImagePath amb el mateix argument

### Prison

- També cerca «qrencode» amb un sufix de depuració

### QQC2StyleBridge

- Simplifica i no intenta blocar els esdeveniments de ratolí
- Si no hi ha «wheel.pixelDelta», usa les línies de desplaçament global de roda
- Les barres de pestanyes de l'escriptori tenen amplades diferents per a cada pestanya
- Assegura un consell de mida no 0

### Sonnet

- No exporta els executables «helper» interns
- Sonnet: esmena els idiomes incorrectes per a suggeriments en textos d'idiomes barrejats
- Elimina una solució temporal antiga i trencada
- No causa un enllaçat circular al Windows

### Ressaltat de la sintaxi

- Indexador de ressaltat: Avís quant al commutador de context «fallthroughContext="#stay"»
- Indexador de ressaltat: Avís quant als atributs buits
- Indexador de ressaltat: Habilita errors
- Indexador de ressaltat: Informa d'«itemDatas» no usats i «itemDatas» que manquin
- Prolog, RelaxNG, RMarkDown: Esmena problemes de ressaltat
- Haml: Esmena un «itemDatas» no vàlid i no usat
- ObjectiveC++: Elimina contexts duplicats de comentaris
- Diff, ObjectiveC++: Neteja i esmena dels fitxers de ressaltat
- XML (Debug): Esmena una regla DetectChar incorrecta
- Indexador de ressaltat: Permet verificació de context «cross-hl»
- Reverteix: Torna a afegir GNUMacros a «gcc.xml», usat per «isocpp.xml»
- email.xml: afegeix «*.email» a les extensions
- Indexador de ressaltat: Verifica bucles infinits
- Indexador de ressaltat: Comprova els noms buits de context i expressions regulars
- Esmena la referència a llistes de paraules clau no existents
- Esmena casos senzills de contexts duplicats
- Esmena els «itemDatas» duplicats
- Esmena les regles DetectChar i Detect2Chars
- Indexador de ressaltat: Comprova les llistes de paraules clau
- Indexador de ressaltat: Avisa dels contexts duplicats
- Indexador de ressaltat: Verifica els «itemDatas» duplicats
- Indexador de ressaltat: Comprova DetectChar i Detect2Chars
- Valida que existeixi un «itemData» per a tots els atributs

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
