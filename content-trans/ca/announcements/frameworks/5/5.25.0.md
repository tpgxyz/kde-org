---
aliases:
- ../../kde-frameworks-5.25.0
date: 2016-08-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### General

- Qt &gt;= 5.5 ara és un requisit

### Attica

- Segueix les redireccions HTTP

### Icones Brisa

- Actualitza les icones 16x del mail- per a reconèixer-les millor
- Actualitza les icones d'estat per al micròfon i àudio perquè tinguin el mateix disseny i grandària
- Icona nova d'aplicació d'Arranjament del sistema
- Afegeix icones del GNOME d'estat simbòlic
- Afegeix la implementació d'icones simbòliques del Gnome 3
- Afegeix icones per a Diaspora i Vector, vegeu phabricator.kde.org/M59
- Icones noves per al Dolphin i Gwenview
- Les icones del temps són icones d'estat i no icones d'aplicació
- Afegeix alguns enllaços a «xliff» gràcies a «gnastyle»
- Afegeix una icona del Kig
- Afegeix icones de tipus MIME, una icona del KRDC, altres icones d'aplicació des de «gnastyle»
- Afegeix la icona del tipus MIME del certificat (error 365094)
- Actualitza les icones del Gimp gràcies al «gnastyle» (error 354370)
- La icona d'acció del globus ara no és cap fitxer enllaçat. Cal usar-la al digiKam
- Actualitza les icones del LabPlot segons el correu 13.07. de l'Alexander Semke
- Afegeix icones d'aplicació des de «gnastyle»
- Afegeix una icona del Kruler, de Yuri Fabirovsky
- Esmena els fitxers SVG trencats gràcies a «fuchs» (error 365035)

### Mòduls extres del CMake

- Esmena les inclusions quan no hi ha les Qt5
- Afegeix un mètode de reserva per a «query_qmake()» quan no hi ha cap instal·lació de les Qt5
- Assegura que ECMGeneratePriFile.cmake es comporta com la resta de l'ECM
- Les dades d'Appstream canvien la seva ubicació preferida

### KActivities

- [KActivities-CLI] Ordres per iniciar i aturar una activitat
- [KActivities-CLI] Establir i obtenir el nom, la icona i la descripció d'una activitat
- S'ha afegit una aplicació de CLI per al control de les activitats
- Afegeix scripts per a canviar a les activitats anteriors i següents
- El mètode per a la inserció en QFlatSet ara retorna l'índex juntament amb l'iterador (error 365610)
- Afegeix les funcions de ZSH per aturar i eliminar activitats menys les actuals
- S'ha afegit la propietat «isCurrent» al KActivities::Info
- Empra iteradors constants en la cerca per activitat

### Eines de Doxygen del KDE

- Moltes millores en el format de la sortida
- Mainpage.dox té ara una major prioritat que README.md

### KArchive

- Gestiona múltiples fluxos GZIP (error 232843)
- Assumeix que un directori és un directori, fins i tot si el bit de permisos s'ha establert malament (error 364071)

### KBookmarks

- KBookmarkGroup::moveBookmark: esmena el valor retornat quan l'element ja es troba en la posició correcta

### KConfig

- Afegeix les dreceres estàndard de DeleteFile i RenameFile

### KConfigWidgets

- Afegeix les accions estàndard de DeleteFile i RenameFile
- La pàgina de configuració ara disposarà de barres de desplaçament quan sigui necessari (error 362234)

### KCoreAddons

- Instal·la les llicències conegudes i les troba en temps d'execució (correcció d'una regressió) (error 353939)

### KDeclarative

- En realitat emet «valueChanged»

### KFileMetaData

- Comprova si troba «xattr» durant el pas de configurar, en cas contrari la compilació pot fallar (si manca el xattr.h)

### KGlobalAccel

- Empra el D-Bus del «klauncher» en lloc del de KRun (error 366415)
- Llança accions de llista de salts mitjançant KGlobalAccel
- KGlobalAccel: esmena l'interbloqueig a la sortida en el Windows

### KHTML

- Implementa la unitat de percentatge en el radi de la vora
- S'han eliminat les propietats de versió amb prefix del fons i del radi de la vora
- Corregeix en 4 valors la funció constructora de transcripció
- Crea objectes de cadena només si s'han d'utilitzar

### KIconThemes

- Millora important del rendiment de «makeCacheKey», ja que és un camí de codi crític a la cerca d'icones
- KIconLoader: reduir el nombre de cerques en donar alternatives
- KIconLoader: millora enorme de la velocitat en carregar icones no disponibles
- No esborra la línia de cerca quan es canvia la categoria
- KIconEngine: esmena el QIcon::hasThemeIcon tornant sempre cert (error 365130)

### KInit

- Adapta el KInit al Mac OS X

### KIO

- Esmena el KIO::linkAs() perquè treballi com anuncia, és a dir, falla si ja existeix la destinació
- Esmena el KIO::put(«file:///camí») per a respectar el «umask» (error 359581)
- Esmena el KIO::pasteActionText per a l'element de destinació nul i URL buit
- Afegeix la implementació per a desfer la creació d'enllaços
- Opció de la IGU per a configurar la MarkPartial global dels esclaus KIO
- Esmena la MaxCacheSize limitada a 99 KiB
- S'han afegit botons al porta-retalls per a la pestanya de sumes de verificació
- KNewFileMenu: soluciona la còpia del fitxer de plantilla des del recurs incrustat (error 359581)
- KNewFileMenu: soluciona la creació d'enllaços a les aplicacions (error 363673)
- KNewFileMenu: soluciona el consell del nom de fitxer nou quan el fitxer ja existeix a l'escriptori
- KNewFileMenu: assegura que el fileCreated() també s'emet per als fitxers d'escriptori de les aplicacions
- KNewFileMenu: soluciona la creació d'enllaços simbòlics amb un objectiu relatiu
- KPropertiesDialog: simplifica l'ús del quadre del botó, esmena el comportament en Esc
- KProtocolInfo: reomple la memòria cau per cercar els protocols instal·lats recentment
- KIO::CopyJob: adapta a «qCDebug» (amb la seva pròpia àrea, ja que pot ser bastant explícit)
- KPropertiesDialog: afegeix la pestanya sumes de verificació
- Neteja el camí de l'URL abans d'inicialitzar el KUrlNavigator

### KItemModels

- KRearrangeColumnsProxyModel: esmenar un «assert» a «index(0, 0)» en un model buit
- Esmena KDescendantsProxyModel::setSourceModel() que no netejava les memòries cau internes
- KRecursiveFilterProxyModel: esmena la corrupció de QSFPM deguda a un filtratge del senyal «rowsRemoved» (error 349789)
- KExtraColumnsProxyModel: implementa «hasChildren()»

### KNotification

- No establir manualment el pare de la subdisposició, silencia els avisos

### Paquets dels Frameworks

- Inferir la ParentApp a partir del connector PackageStructure
- Permetre que el «kpackagetool5» generi informació de l'«appstream» per als components del «kpackage»
- Fa possible carregar el fitxer metadata.json des del «kpackagetool5»

### Kross

- Elimina les dependències no utilitzades del KF5

### KService

- applications.menu: elimina les referències a les categories sense usar
- Actualitza sempre l'analitzador del Trader des de les fonts del «yacc/lex»

### KTextEditor

- No preguntar dues vegades per la sobreescriptura d'un fitxer amb els diàlegs natius
- S'ha afegit la sintaxi FASTQ

### KWayland

- [client] Usa un «QPointer» per al «enteredSurface» en el «Pointer»
- Exposa la geometria en el PlasmaWindowModel
- Afegeix un esdeveniment de geometria a la interfície Plasma Window
- [src/servidor] Verificar que la superfície té un recurs abans d'enviar l'entrada de l'apuntador
- Afegeix la implementació per a la «xdg-shell»
- [servidor] Envia de manera adequada una neteja de selecció abans de l'entrada del focus del teclat
- [servidor] Gestiona les situacions no XDG_RUNTIME_DIR més adequadament

### KWidgetsAddons

- [KCharSelect]: esmena la petada quan se cerca sense que hi hagi present un fitxer de dades (error 300521)
- [KCharSelect] Gestiona els caràcters fora de BMP (error 142625)
- [KCharSelect]: actualitza les kcharselect-data a Unicode 9.0.0 (error 336360)
- KCollapsibleGroupBox: Atura l'animació en el destructor si encara està executant-se
- Actualitza a la paleta del Brisa (sincronitza des del KColorScheme)

### KWindowSystem

- [xcb] Assegura que el senyal «compositingChanged» s'emet si es torna a crear «NETEventFilter» (error 362531)
- Afegida una API de comoditat per consultar el sistema/plataforma de finestres usat per les Qt

### KXMLGUI

- Esmena la mida mínima del consell («cut-off text») (error 312667)
- [KToggleToolBarAction] Respecta la restricció «action/options_show_toolbar»

### NetworkManagerQt

- Per defecte, WPA2-PSK i WPA2-EAP en establir el tipus de seguretat de la configuració de la connexió

### Icones de l'Oxygen

- Afegeix «application-menu» a l'Oxygen (error 365629)

### Frameworks del Plasma

- Mantenir la compatibilitat del sòcol «createApplet» amb els Frameworks 5.24
- No suprimir dues vegades la textura GL a les miniatures (error 365946)
- Afegir el domini de traduccions a l'objecte QML «wallpaper»
- No suprimir manualment les miniaplicacions
- Afegeix un «kapptemplate» al fons de pantalla del Plasma
- Plantilles: registrar les plantilles en la seva pròpia categoria de nivell superior «Plasma/»
- Plantilles: Actualitza els enllaços del wiki techbase als README
- Defineix quines «packagestructures» del Plasma estenen el «plasmashell»
- Implementa una mida per miniaplicacions afegides
- Defineix les PackageStructure del Plasma com a connectors normals PackageStructure del KPackage
- Esmena: actualitzar l'exemple de fons de pantalla config.qml de tardor a QtQuick.Controls
- Usa el KPackage per instal·lar paquets del Plasma
- Si es passa un QIcon com un argument a IconItem::Source, usar-lo
- Afegeix la implementació de superposició a l'IconItem del Plasma
- Afegeix Qt::Dialog als indicadors per defecte per fer QXcbWindow::isTransient() més adequat (error 366278)
- [Tema Brisa del Plasma] Afegeix icones «network-flightmode-on/off»
- Emetre «contextualActionsAboutToShow» abans de mostrar el menú «contextualActions» de la miniaplicació (error 366294)
- [TextField] Vincular a la longitud del TextField en lloc del text
- [Estils de botons] Centrar horitzontalment en el mode només icones (error 365947)
- [Contenidor] Tractar el HiddenStatus com un estat baix
- Afegeix la icona de la safata del sistema del KRuler del Yuri Fabirovsky
- Soluciona l'error infame «els diàlegs es mostren al gestor de tasques» una altra vegada
- Esmena la icona de xarxes sense fil disponibles amb un emblema «?» (error 355490)
- IconItem: Usa una aproximació millor per desactivar l'animació en canviar des d'invisible a visible
- Definir el tipus de finestra Tooltip a ToolTipDialog a través de l'API KWindowSystem

### Solid

- Actualitza sempre l'analitzador del Predicate des de les fonts del «yacc/lex»

### Sonnet

- Hunspell: Neteja el codi de cerca de diccionaris, afegeix els directoris XDG (error 361409)
- Intenta esmenar una mica l'ús del filtre d'idioma de la detecció d'idioma

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
