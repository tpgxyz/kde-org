---
aliases:
- ../../kde-frameworks-5.40.0
date: 2017-11-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Considerar os ficheiros de DjVu documentos (fallo 369195)
- Corrixir a ortografía para que as presentacións de Office de WPS se recoñezan correctamente

### Iconas de Breeze

- engadir folder-stash para a icona de reserva da barra de ferramentas de Dolphin

### KArchive

- Corrixir unha fuga de memoria potencial. Corrixir a lóxica

### KCMUtils

- sen marxes para os módulos de QML do lado de QWidget
- Preparar as variábeis (atopado por coverity)

### KConfigWidgets

- Corrixir a icona de KStandardAction::MoveToTrash

### KCoreAddons

- corrixir a detección de URL con URL duplas como «http://www.foo.bar&lt;http://foo.bar/&gt;»
- Usar HTTPS para os URL de KDE

### Compatibilidade coa versión 4 de KDELibs

- documentación completa para o substituto de disableSessionManagement()
- Facer que kssl compile con OpenSSL 1.1.0 (fallo 370223)

### KFileMetaData

- Corrixir o nome para mostrar da propiedade Generator

### KGlobalAccel

- KGlobalAccel: corrixir (de novo) a compatibilidade coas teclas do teclado numérico

### KInit

- Instalación correcta de start_kdeinit cando DESTDIR e libcap se usan xuntos

### KIO

- Corrixir a visualización de remote:/ en qfiledialog
- Permitir categorías en KfilesPlacesView
- HTTP: corrixir unha cadea de erro para o caso de 207 Estado múltiple
- KNewFileMenu: limpar código morto, descuberto por Coverity
- IKWS: Corrixir potenciais bucles infinitos, descuberto por Coverity
- Función KIO::PreviewJob::defaultPlugins()

### Kirigami

- sintaxe que funciona no anterior Qt 5.7 (fallo 385785)
- organizar a rima de folla de capas de maneira distinta (fallo 386470)
- Mostrar tamén a propiedade salientada do delegado cando non haxa foco
- consellos de tamaño preferidos para o separador
- corrixir o uso de Settings.isMobile
- Permitir que as aplicacións sexan máis ou menos converxentes en sistemas tipo escritorio
- Asegurarse de que o contido de SwipeListItem non cubre a asa (fallo 385974)
- A scrollview de Overlaysheet sempre é ointeractive
- Engadir categorías ao ficheiro de escritorio de galería (fallo 385430)
- Actualizar o ficheiro kirigami.pri
- usar o complemento non instalado para facer as probas
- Marcar Kirigami.Label como obsoleta
- Migrar o uso de Label do exemplo de galería para ser consistente con QQC2
- Migrar os usos en Kirigami.Controls de Kirigami.Label
- facer a zona de desprazamento interactiva ante eventos táctiles
- Mover a chamada de find_package de Git a onde se usa
- usar elementos de vista de lista transparentes de maneira predeterminada

### KNewStuff

- Retirar PreferCache das solicitudes de rede
- Non separar os punteiros compartidos a datos privados ao definir vistas previas
- KMoreTools: Actualizar e corrixir ficheiros de escritorio (fallo 369646)

### KNotification

- Retirar a comprobación das máquinas de SNI ao escoller se usar o modo antigo (fallo 385867)
- Só comprobar se hai iconas antigas da área de notificacións se imos facer unha (fallo 385371)

### Infraestrutura KPackage

- usar os ficheiros de servizo non instalados

### KService

- Preparar os valores
- Preparar algún punteiro

### KTextEditor

- API dox: fix wrong names of methods and args, add missing \\since
- Evitar (certas) quebras ao executar scripts de QML (fallo 385413)
- Evitar unha quebra de QML causada por scripts de sangrado de estilo C
- Aumentar o tamaño da marca final
- corrixir algúns sangradores que sangraban caracteres aleatorios
- Corrixir un aviso de obsolescencia

### KTextWidgets

- Preparar o valor

### KWayland

- [cliente] Retirar as comprobacións de que platformName sexa «wayland»
- Non conectar por duplicado a wl_display_flush
- Protocolo estranxeiro de Wayland

### KWidgetsAddons

- corrixir unha inconsistencia de enfoque de trebello de createKMessageBox
- diálogo de contrasinal máis compacto (fallo 381231)
- Definir correctamente a anchura de KPageListView

### KWindowSystem

- KKeyServer: corrixir a xestión de Meta+Maiús+Imprimir, Alt+Maiús+tecladefrecha, etc.
- Engadir compatibilidade coa plataforma Flatpak
- Usar a API de detección de plataforma de KWindowSystem en vez de código duplicado

### KXMLGUI

- Usar HTTPS para os URL de KDE

### NetworkManagerQt

- 8021xSetting: domain-suffix-match está definido en NM 1.2.0 e versións máis novas
- Permitir «domain-suffix-match» en Security8021xSetting

### Infraestrutura de Plasma

- debuxar manualmente o arco do círculo
- [PlasmaComponents Menu] Engadir ungrabMouseHack
- [FrameSvg] Optimizar updateSizes
- Non colocar un Dialog se o seu tipo é OSD

### QQC2StyleBridge

- Mellorar a compilación como complemento estático
- facer o botón de raio un botón de raio
- usar qstyle para pintar o Dial
- usar unha ColumnLayout para os menús
- corrixir Dialog
- retirar unha propiedade de grupo incorrecta
- Corrixir o formato do ficheiro md para que se corresponda cos dos outros módulos
- comportamento da caixa despregábel máis parecido a qqc1
- solución temporal para QQuickWidgets

### Sonnet

- Engadir o método assignByDictionnary
- Sinal se podemos asignar un dicionario

### Realce da sintaxe

- Makefile: corrixir a coincidencia das expresións regulares en «CXXFLAGS+»

### ThreadWeaver

- Limpeza de CMake: Non definir -std=c++0x a man

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
