---
aliases:
- ../../kde-frameworks-5.39.0
date: 2017-10-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Só considerar coincidencias os tipos MIME reais, p. ex. non considerar «imaxe de CD en cru» (fallo 364884)
- Retirar pf.path() do contedor antes de que .remove() estrague a referencia
- Corrixir a descrición do protocolo de escravo de KIO de etiquetas
- Considerar os ficheiros de Markdown documentos

### Iconas de Breeze

- engadir a icona overflow-menu (fallo 385171)

### Módulos adicionais de CMake

- Corrixir a compilación da API de Python tras 7af93dd23873d0b9cdbac192949e7e5114940aa6

### Integración de infraestruturas

- Facer que KStandardGuiItem::discard coincida con QDialogButtonBox::Discard

### KActivitiesStats

- Cambiouse o límite predeterminado das consultar a cero
- Engadiuse a opción de activar o probador de modelo

### KCMUtils

- Permitir o desprazamento en KCMultiDialog (fallo 354227)

### KConfig

- Marcar KStandardShortcut::SaveOptions como obsoleto

### KConfigWidgets

- Marcar KStandardAction::PasteText e KPasteTextAction como obsoletos

### KCoreAddons

- desktoptojson: mellorar a vella heurística de detección de tipos de servizo (fallo 384037)

### KDeclarative

- Cambiar a licenza a LGPL2.1+
- Engadiuse o método openService() a KRunProxy

### KFileMetaData

- corrixir unha quebra cando se destrúe máis dunha instancia de ExtractorCollection

### KGlobalAccel

- Reverter «KGlobalAccel: migrar ao novo método symXModXToKeyQt de KKeyServer para corrixir as teclas numéricas» (fallo 384597)

### KIconThemes

- engadir un método para restabelecer a paleta personalizada
- usar qApp-&gt;palette() cando non se defina unha personalizada
- asignar o tamaño de búfer correcto
- permitir definir unha paleta personalizada en vez de colorSets
- expoñer o conxunto de cores para a folla de estilos

### KInit

- Windows: Corrixir «klauncher usa a ruta de instalación de tempo de compilación absoluta para atopar kioslave.exe»

### KIO

- kioexec: Vixiar o ficheiro cando rematou de copiar (fallo 384500)
- KFileItemDelegate: Reservar sempre espazo para as iconas (fallo 372207)

### Kirigami

- non crear unha instancia do ficheiro Theme en BasicTheme
- engadir un novo botón de Redirixir
- menos contraste co fondo da barra de desprazamento da folla
- inserción e retirada máis fiábel do menú de desbordamento
- mellor renderización de iconas contextuais
- ter máis coidado ao centrar o botón de acción
- usar iconsizes para os botóns de accións
- tamaños de icona de píxel perfecto en escritorio
- seleccionouse un efecto para falsear a icona de asa
- corrixir a cor das asas
- mellor cor para o botón de acción principal
- corrixir o menú contextual para o estilo de escritorio
- menú «Máis» mellor para a barra de ferramentas
- un menú axeitado para o menú contextual das páxinas intermedias
- engadir un campo de texto que só debería mostrar un teclado numérico
- non quebrar cando se inicie sen estilos existentes
- Concepto de ColorSet no tema
- simplificar a xestión da roda (fallo 384704)
- nova aplicación de exemplo con ficheiros de QML principais para escritorio e móbiles
- asegurarse de que currentIndex é correcto
- Xerar os metadatos de AppStream da aplicación de galería
- Buscar QtGraphicalEffects para que os empaquetadores non o esquezan
- Non incluír o control sobre a decoración de fondo (fallo 384913)
- coloración máis clara cando listview non ten activeFocus
- compatibilidade parcial con disposicións de dereita a esquerda
- Desactivar os atallos cando unha acción estea desactivada
- crear a estrutura completa do complemento no directorio de construción
- corrixir a accesibilidade da páxina principal da galería
- Se Plasma non está dispoñíbel tampouco o está KF5Plasma. Debería corrixir o erro da integración continua

### KNewStuff

- Requirir Kirigami 2.1 en vez de 1.0 para KNewStuffQuick
- Crear KPixmapSequence correctamente
- Non queixarse se o ficheiro de knsregistry non está presente antes de ser útil

### Infraestrutura KPackage

- kpackage: empaquetar unha copia de servicetypes/kpackage-generic.desktop
- kpackagetool: empaquetar unha copia de servicetypes/kpackage-generic.desktop

### KParts

- Modelo de KPartsApp: corrixir o lugar de instalación do ficheiro de escritorio de KPart

### KTextEditor

- Ignorar a marca predeterminada do bordo da icona para unha única marca seleccionábel
- Usar QActionGroup para a selección do método de entrada
- Corrixir a falta da barra de corrección ortográfica (fallo 359682)
- Corrixir o valor de escuridade de reserva dos caracteres con código Unicode &gt; 255 (fallo 385336)
- Corrixir a visualización de espazo sobrante para liñas RTL

### KWayland

- Só enviar sendApplied e sendFailed de OutputConfig ao recurso correcto
- Non quebrar se un cliente usa (correctamente) un xestor de contraste global eliminado
- Compatibilidade coa versión 6 de XDG

### KWidgetsAddons

- KAcceleratorManager: definir o texto da icona nas accións para retirar marcadores de CJK (fallo 377859)
- KSqueezedTextLabel: Apertar o texto ao cambiar o sangrado ou a marxe
- Usar a icona edit-delete para a acción de descarte destrutivo (fallo 385158)
- Corrixir o fallo 306944 - Usar a roda do rato para aumentar ou reducir as datas (fallo 306944)
- KMessageBox: Usar a icona de signo de interrogación final para diálogos de pregunta
- KSqueezedTextLabel: Respectar o sangrado, a marxe e a anchura de marco

### KXMLGUI

- Corrixir o ciclo de pintura de KToolBar (fallo 377859)

### Infraestrutura de Plasma

- Corrixir org.kde.plasma.calendar con Qt 5.10
- [FrameSvgItem] Iterar polos nodos subordinados de maneira axeitada
- [Interface de Containment] Non engadir accións de contedor ás accións do miniaplicativo no escritorio
- Engadir un novo compoñente para as etiquetas agrisadas nos delegados de elemento
- Corrixir FrameSVGItem co renderizador de software
- Non animar IconItem no modo de software
- [FrameSvg] Usar o novo estilo de «connect»
- posibilidade de definir un ámbito de cor anexado para non herdar
- Engadir un indicador visual adicional para o foco do teclado de Checkbox e Radio
- non crear de novo un mapa de píxeles nulo
- Pasar o elemento a rootObject() dado que agora é un singleton (fallo 384776)
- Non listar os nomes de separadores dúas veces
- non aceptar o foco activo no separador
- rexistrar a revisión 1 de QQuickItem
- [Compoñentes de Plasma 3] Corrixir RTL nalgúns trebellos
- Corrixir un identificador incorrecto en viewitem
- actualizar a imaxe de notificación de correo para darlle mellor contraste (fallo 365297)

### qqc2-desktop-style

New module: QtQuickControls 2 style that uses QWidget's QStyle for painting This makes it possible to achieve an higher degree of consistency between QWidget-based and QML-based apps.

### Solid

- [solid/fstab] Engadir compatibilidade con opcións de estilo x-gvfs en fstab
- [solid/fstab] Intercambiar as propiedades de fabricante e produto, permitir a internacionalización da descrición

### Realce da sintaxe

- Corrixir referencias de itemData incorrectas de 57 ficheiros de realce
- Engadir a posibilidade de rutas de busca personalizadas para sintaxe específica de aplicación e definicións de temas
- AppArmor: corrixir regras de D-Bus
- Indexador de realce: retirar comprobacións dos ciclos «mentres» máis pequenos
- ContextChecker: permitir o cambio de contexto de «!» e fallthroughContext
- Indexador de realce: comprobar a existencia de nomes de contexto aos que se fai referencia
- Cambiar a licenza do salientado de QMake a MIT
- Permitir que o realce de qmake cobre preferencia sobre o de Prolog para ficheiros .pro (fallo 383349)
- Permitir macros «@» de clojure con corchetes
- Engadir salientado de sintaxe para perfís de AppArmor
- Indexador de salientado: capturar intervalos incorrectos a-Z e A-z en expresións regulares
- Corrixir intervalos postos en maiúscula incorrectamente en expresións regulares
- engadir ficheiros de referencia que faltan para probas, teñen boa pinta na miña opinión
- Engadir compatibilidade cos ficheiros HEX de Intel para a base de datos de realce de sintaxe
- Desactivar a corrección ortográfica para cadeas en scripts de Sieve

### ThreadWeaver

- Corrixir unha fuga de memoria

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
