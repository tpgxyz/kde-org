---
aliases:
- ../../kde-frameworks-5.19.0
date: 2016-02-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Simplificar a busca e inicialización de complementos de Attica

### Iconas de Breeze

- Moitas iconas novas
- Engadir iconas de tipos MIME que faltaban do conxunto de iconas de Oxygen

### Módulos adicionais de CMake

- ECMAddAppIcon: Usar unha ruta absoluta ao operar sobre iconas
- Asegurarse de que o prefixo se busca en Android
- Engadir un módulo de FindPoppler
- Usar PATH_SUFFIXES en ecm_find_package_handle_library_components()

### KActivities

- Non chamar a exec() desde QML (fallo 357435)
- Agora a biblioteca KActivitiesStats está nun repositorio aparte

### KAuth

- Realizar preAuthAction tamén para infraestruturas con AuthorizeFromHelperCapability
- Corrixir o nome do servizo de D-Bus do axente de Polkit

### KCMUtils

- Corrixir un problema con DPI alto en KCMUtils

### KCompletion

- O método KLineEdit::setUrlDropsEnabled non se pode marcar como obsoleto

### KConfigWidgets

- engadir un esquema de cores «Complementario» a KColorScheme

### KCrash

- Actualizar a documentación de KCrash::initialize. Anímase aos desenvolvedores de aplicacións a chamalo de maneira explícita.

### KDeclarative

- Limpar as dependencias de KDeclarative e QuickAddons
- [KWindowSystemProxy] Engadir un obtedor para showingDesktop
- DropArea: Corrixir que se ignore correctamente o evento dragEnter con preventStealing
- DragArea: Crear un elemento de delegado de recoller
- DragDropEvent: Engadir a función ignore()

### KDED

- Reverter o apaño de BlockingQueuedConnection, Qt 5.6 incluirá unha corrección mellor
- Facer que kded se rexistre cos nomes alternativos indicados polos módulos de kded

### Compatibilidade coa versión 4 de KDELibs

- kdelibs4support require kded (para kdedmodule.desktop)

### KFileMetaData

- Permitir consultar o URL de orixe dun ficheiro

### KGlobalAccel

- Evitar unha quebra cando D-Bus on está dispoñíbel

### Complementos de interface gráfica de usuario de KDE

- Corrixir a lista de paletas dispoñíbeis no diálogo de cores

### KHTML

- Corrixir a detección do tipo de ligazón de icona («favicon»)

### KI18n

- Reducir o uso da API de gettext

### KImageFormats

- Engadir os complementos de imageio kra e ora imageio (só lectura)

### KInit

- Ignorar o porto de vista no escritorio actual na información de inicio de inicialización
- Migrar klauncher a xcb
- Usar un xcb para interactuar con KStartupInfo

### KIO

- Nova clase FavIconRequestJob na nova biblioteca KIOGui, para a obtención de favicons
- Corrixir a quebra de KDirListerCache con dous listadores para un directorio baleiro na caché (fallo 278431)
- Facer que o código para Windows de KIO::stat para o protocolo file:/ saia con error se o ficheiro non existe
- Non asumir que os ficheiros dun directorio de só lectura non se poden eliminar en Windows
- Corrixir o ficheiro .pri para KIOWidgets: depende de KIOCore, non del mesmo
- Reparar a carga automática de kcookiejar, os valores intercambiáronse en 6db255388532a4
- Permitir acceder a kcookiejar desde o nome de servizo de D-Bus org.kde.kcookiejar5
- kssld: instalar un ficheiro de servizo de D-Bus para org.kde.kssld5
- Fornecer un ficheiro de servizo de D-Bus para org.kde.kpasswdserver
- [kio_ftp] corrixir a visualización da data e hora de modificación dos ficheiros e dos directorios (fallo 354597)
- [kio_help] corrixir o lixo que se enviaba ao servir ficheiros estáticos
- [kio_http] Intentar a autenticación NTLMv2 se o servidor rexeita NTLMv1
- [kio_http] corrixir fallos de migración que romperon a caché
- [kio_http] Corrixir a creación de resposta de fase 3 de NTLMv2
- [kio_http] corrixir que se esperase ata que o limpador da caché escoitase o sócket
- kio_http_cache_cleaner: non saír no inicio se o directorio da caché aínda non existe
- Cambiar o nome de D-Bus de kio_http_cache_cleaner para que non saia se o de kde4 está en execución

### KItemModels

- KRecursiveFilterProxyModel::match: Corrixir unha quebra

### KJobWidgets

- Corrixir unha quebra nos diálogos de KJob (fallo 346215)

### Infraestrutura de paquetes

- Evitar atopar o mesmo paquete varias veces desde distintas rutas

### KParts

- PartManager: deixar de facer seguimento ao trebello incluso se xa non é raíz (fallo 355711)

### KTextEditor

- Un comportamento mellor para a funcionalidade de chaves automáticas «inserir chaves arredor»
- Cambiar a clave da opción para forzar un novo valor predeterminado, nova liña ao final do ficheiro = verdadeiro
- Retirar algunhas chamadas sospeitosas a setUpdatesEnabled (fallo 353088)
- Atrasar a emisión de verticalScrollPositionChanged ata que todo sexa consistente para pregar (fallo 342512)
- Parche que actualiza a substitución de etiquetas (fallo 330634)
- Só actualizar a paleta unha vez para o evento de cambio que pertence a qApp (fallo 358526)
- Engadir saltos de liña ao final do ficheiro de maneira predeterminada
- Engadir un ficheiro de salientado de sintaxe de NSIS

### Infraestrutura de KWallet

- Duplicar o descritor de ficheiro ao abrir o ficheiro para ler o ambiente

### KWidgetsAddons

- Corrixir o funcionamento dos trebellos de amigo con KFontRequester
- KNewPasswordDialog: usar KMessageWidget
- Evitar unha quebra ao saír en KSelectAction::~KSelectAction

### KWindowSystem

- Cambiar a cabeceira da licenza de «Biblioteca GPL 2 ou posterior» a «GPL Menor 2.1 ou posterior»
- Corrixir unha quebra se se chama a KWindowSystem::mapViewport sen unha QCoreApplication
- Gardar QX11Info::appRootWindow na caché en eventFilter (fallo 356479)
- Retirar a dependencia de QApplication (fallo 354811)

### KXMLGUI

- Engadir unha opción para desactivar KGlobalAccel en tempo de compilación
- Reparar a ruta do esquema de atallo de aplicación
- Corrixir a lista de ficheiros de atallo (uso incorrecto de QDir)

### NetworkManagerQt

- Comprobar de novo o estado da conexión e outras propiedades para asegurarse de que son os reais (versión 2) (fallo 352326)

### Iconas de Oxygen

- Retirar ficheiros ligados rotos
- Engadir iconas das aplicacións de KDE
- Engadir iconas de lugares de Breeze a Oxygen
- Sincronizas as iconas de tipos MIME de Oxygen coas de Breeze

### Infraestrutura de Plasma

- Engadir a propiedade «separatorVisible»
- Retirada máis explícita de m_appletInterfaces (fallo 358551)
- Usar complementaryColorScheme de KColorScheme
- AppletQuickItem: non intentar definir un tamaño inicial máis grande que o do pai (fallo 358200)
- IconItem: Engadir a propiedade usesPlasmaTheme
- Non cargar a caixa de ferramenta en tipos que non sexan escritorio ou panel
- IconItem: Intentar cargar as iconas de QIcon::fromTheme como SVG (fallo 353358)
- Ignorar a comprobación se só unha parte do tamaño é cero en compactRepresentationCheck (fallo 358039)
- [Unidades] Devolver polo menos 1 ms de duración (fallo 357532)
- Engadir clearActions() para retirar todas as accións de interface de miniaplicativo
- [plasmaquick/dialog] Non usar KWindowEffects para o tipo de xanela Notification
- Marcar Applet::loadPlasmoid() como obsoleto
- [DataModel de PlasmaCore] Non restabelecer o modelo ao retirarse unha fonte
- Corrixir os consellos das marxes en SVG de fondo de panel opaco
- IconItem: Engadir a propiedade «animated»
- [Unity] Axustar o tamaño das iconas do escritorio
- o botón é compose-over-borders
- paintedWidth e paintedheight para IconItem

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
