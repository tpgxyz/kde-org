---
aliases:
- ../../kde-frameworks-5.51.0
date: 2018-10-15
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Engadir chamadas a KIO::UDSEntry::reserve nos escravos de entrada e saída de liña de tempo e etiquetas
- [balooctl] Enviar a liña «Indexando &lt;ficheiro&gt;» cando a indexación comeza
- [FileContentIndexer] Conectar o sinal rematado do proceso de extractor
- [PositionCodec] Evitar a quebra ante datos corruptos (fallo 367480)
- Corrixir unha constante de carácter incorrecta
- [Balooctl] retirar a comprobación de pai de directorio (fallo 396535)
- Permitir retirar cartafoles non existentes de listas de inclusións e exclusións (fallo 375370)
- Usar String para almacenar UDS_USER e UDS_GROUP de tipo String (fallo 398867)
- [tags_kio] Corrixir os parénteses. Non sei como pero o meu revisor de código non o detectara.
- Excluír os ficheiros de xenoma da indexación

### BluezQt

- Crear as API Media e MediaEndpoint

### Iconas de Breeze

- Corrixir «stack-use-after-scope», detectado por ASAN na CI
- Corrixir iconas monocroma sen folla de estilos
- Cambiar drive-harddisk a un estilo máis adaptábel
- Engadir as iconas firewall-config e firewall-applet
- Facer visíbel o cadeado da iconas plasmavault con breeze-dark
- Engadir un símbolo de suma a document-new.svg (fallo 398850)
- Fornecer iconas para aumento duplo de tamaño

### Módulos adicionais de CMake

- Compilar a API de Python coas mesmas marcas de sip que usa PyQt
- Android: permitir pasar unha ruta relativa como directorio de APK
- Android: Ofrecer de maneira axeitada unha reserva para aplicacións que non teñen un manifesto
- Android: asegurarse de que as traducións de Qm se cargan
- Corrixir as construcións para Android cando se usa CMake 3.12.1
- localización: corrixir a coincidencia de díxitos no nome do repositorio
- Engadir QT_NO_NARROWING_CONVERSIONS_IN_CONNECT como marca de compilación predeterminada
- API de linguaxes: Corrixir a xestión de conter que conteñan UTF-8
- Iterar de verdade por CF_GENERATED en vez de comprobar o elemento 0 todo o tempo

### KActivities

- Corrixir unha referencia solta con «auto» converténdose en «QStringBuilder»

### KCMUtils

- xestionar os eventos de volta
- Cambiar o tamaño de KCMUtilDialog manualmente a sizeHint() (fallo 389585)

### KConfig

- Corrixir un erro ao ler listas de rutas
- Agora kcfg_compiler documenta as entradas válidas para o seu tipo «Color»

### KFileMetaData

- retirar o código propio de conversión de QString a TString de taglibwriter
- aumentar a cobertura de probas de taglibwriter
- engadir máis etiquetas básicas para taglibwriter
- retirar o uso da función propia de conversión de TString a QString
- aumentar a versión requirida de taglib a 1.11.1
- ler as etiquetas replaygain

### KHolidays

- engadir as vacacións de Costa do Marfil (en francés) (fallo 398161)
- holiday*hk** - fix date for Tuen Ng Festival in 2019 (bug 398670)

### KI18n

- Restrinxir correctamente o cambio de CMAKE_REQUIRED_LIBRARIES
- Android: Asegurarse de que buscamos os ficheiros MO na ruta correcta

### KIconThemes

- Comezar a debuxar emblemas desde a esquina inferior dereita

### KImageFormats

- kimg_rgb: retirar QRegExp e QString::fromLocal8Bit para optimizar
- [EPS] Corrixir a quebra ao saír da aplicación (intentando persistir a imaxe do portapapeis) (fallo 397040)

### KInit

- Reducir as mensaxes non desexadas do rexistro non comprobando a existencia de ficheiros co nome baleiro (fallo 388611)

### KIO

- permitir a un file:// non local redirixir a un URL de WebDav de Windows
- [KFilePlacesView] Cambiar a icona do menú contextual de «Editar» do panel de «Lugares»
- [Panel de lugares] usar unha icona de rede máis axeitada
- [KPropertiesDialog] Mostrar información de montaxe para cartafoles en / (raíz)
- Corrixir a eliminación de ficheiros de DAV (fallo 355441)
- Evitar QByteArray::remove en AccessManagerReply::readData (fallo 375765)
- Non intentar restaurar lugares de usuario incorrectos
- Permitir cambiar ao directorio superior mesmo cando haxa barras inclinadas ao final do URL
- As quebras de escravos de KIO agora xestiónaas KCrash en vez de un código personalizado
- Corrixiuse que un ficheiro creado a partir de pegar o contido do portapapeis só aparecese tras un atraso
- [PreviewJob] Enviar os complementos de miniaturas activados ao escravo de miniaturas (fallo 388303)
- Mellorar a mensaxe de erro de «espazo insuficiente en disco»
- IKWS: usar o «X-KDE-ServiceTypes», que non está obsoleto, na xeración de ficheiros de escritorio
- Corrixir a cabeceira de destino de WebDAV nas operacións COPY e MOVE
- Avisar ao usuario antes da operación de copiar o mover se o espazo dispoñíbel non é suficiente (fallo 243160)
- Mover o KCM de SMB á categoría de configuración de rede
- lixo: Corrixir a análise da caché de directorysizes
- kioexecd: vixiar a creación e modificación dos ficheiros temporais (fallo 397742)
- Non debuxar marcos e sombras arredor das imaxes con transparencia (fallo 258514)
- Corrixiuse que a icona de tipo de ficheiro do diálogo de propiedades de ficheiro se debuxase borroso en pantallas de DPI alto

### Kirigami

- abrir de maneira axeitada o caixón cando se arrastre pola asa
- marxe adicional cando a pagerow de globaltoolbar é ToolBar
- permitir tamén Layout.preferredWidth para o tamaño da folla
- retirar os últimos restos de controls1
- Permitir a creación de Actions de separador
- permitir un número arbitrario de columnas en CardsGridview
- Non destruír elementos de menú de maneira activa (fallo 397863)
- as iconas en actionButton son monocroma
- non facer as iconas monocroma cando non deben selo
- restaurar o axuste de tamaño arbitrario de ×1,5 de iconas en móbiles
- reciclador de delegados: non solicitar o obxecto de contexto dúas veces
- usar o código interno de onda de material
- controlar a anchura da cabeceira por sourcesize en horizontal
- expoñer todas as propiedades de BannerImage en Cards
- usar DesktopIcon incluso en Plasma
- cargar correctamente rutas file://
- Reverter «Comezar a buscar o contexto desde o propio delegado»
- Engadir un caso de proba que demostra un problema de ámbito en DelegateRecycler
- definir de maneira explícita unha altura para overlayDrawers (fallo 398163)
- Comezar a buscar o contexto desde o propio delegado

### KItemModels

- Usar unha referencia no bucle «for» para tipos cun construtor de copia non trivial

### KNewStuff

- Engadir a posibilidade de etiquetas de Attica (fallo 398412)
- [KMoreTools] Dar unha icona axeitada ao elemento de menú de «Configurar…» (fallo 398390)
- [KMoreTools] Reducir a xerarquía de menús
- Corrixir «Imposíbel usar o ficheiro knsrc para envíos dun lugar non estándar» (fallo 397958)
- Facer que as ferramentas de proba liguen en Windows
- Corrixir a construción en Qt 5.9
- Engadir a posibilidade de etiquetas de Attica

### KNotification

- Corrixiuse unha quebra causada por unha xestión incorrecta do tempo de vida das notificacións de son baseadas en canberra (fallo 398695)

### KNotifyConfig

- Corrixir o consello de ficheiro de interface gráfica: agora KUrlRequester ten QWidget como clase base

### Infraestrutura KPackage

- Usar unha referencia no bucle «for» para tipos cun construtor de copia non trivial
- Mover Qt5::DBus aos destinos de ligazón «PRIVATE»
- Emitir sinais cando se instalar ou desinstala un paquete

### KPeople

- Corrixir sinais que non se emitían ao fusionar dúas persoas
- Non quebrar se se retira a persoa
- Definir PersonActionsPrivate como clase, como se declarou previamente
- Publicar a API de PersonPluginManager

### Kross

- core: xestionar mellores comentarios de accións

### KTextEditor

- Debuxar o marcador de pregado de código só para rexións de pregado de código de varias liñas
- Preparar m_lastPosition
- Scripting: isCode() devolve falso para o texto dsAlert (fallo 398393)
- usar o salientado de script de R para as probas de sangrado de R
- Actualizar o script de sangrado de R
- Corrixir os esquemas de cores claros solarizados e escuros (fallo 382075)
- Non requirir Qt5::XmlPatterns

### KTextWidgets

- ktextedit: cargar o obxecto QTextToSpeech na primeira lectura

### Infraestrutura de KWallet

- Rexistrar os erros de fallo de apertura de carteira

### KWayland

- Non fallar silenciosamente se o dano se envía antes que o búfer (fallo 397834)
- [servidor] Non volver anticipadamente en caso de fallo no código de reserva de touchDown
- [servidor] Corrixir a xestión do búfer de acceso remoto cando a saída non está asociada
- [servidor] Non intentar crear ofertas de datos sen fonte
- [servidor] Interromper o inicio dun arrastre nas condicións axeitadas e sen publicar un erro

### KWidgetsAddons

- [KCollapsibleGroupBox] Respectar a duración das animacións de trebello do estilo (fallo 397103)
- Retirar unha comprobación obsoleta de versión de Qt
- Compilar

### KWindowSystem

- Usar _NET_WM_WINDOW_TYPE_COMBO en vez de _NET_WM_WINDOW_TYPE_COMBOBOX

### KXMLGUI

- Corrixir o URL do fornecedor de OCS no diálogo de información

### NetworkManagerQt

- Usar o valor enumerado coincidente AuthEapMethodUnknown para comparar cun AuthEapMethod

### Infraestrutura de Plasma

- Aumentar as cadeas de versión do tema porque hai novas iconas en 5.51
- Tamén erguer a xanela de configuración cando se use de novo
- Engadir un compoñente que faltaba: RoundButton
- Combinar os ficheiros de iconas de OSD de pantallas e movelos ao tema de iconas de Plasma (fallo 395714)
- [Control desprazábel de Plasma Components 3] Corrixir o tamaño implícito da asa
- [Selector despregábel da versión 3 dos compoñentes de Plasma] Cambiar de entrada coa roda do rato
- Usar as iconas dos botóns cando existan
- Corrixiuse que os nomes das semanas non se mostrasen correctamente no calendario cando a semana comeza cun día que non sexa un luns ou un domingo (fallo 390330)
- [DialogShadows] Usar un desprazamento 0 para os bordos desactivados en Wayland

### Prison

- Corrixir a renderización de códigos Aztec cunhas proporcións non cadradas
- Retirar a asunción sobre as proporcións do código de barras da integración con QML
- Corrixir erros de debuxado causados por erros de redondeo en Code 128
- Engadir compatibilidade con códigos de barras Code 128

### Purpose

- Facer de CMake 3.0 a versión mínima de CMake

### QQC2StyleBridge

- Pequena separación predeterminada cando hai un fondo

### Solid

- Non mostrar un emblema para discos montados, só para discos sen montar
- [Fstab] Retirar a compatibilidade con AIX
- [Fstab] Remove Tru64 (**osf**) support
- [Fstab] Mostrar un nome de compartición non baleiro se un sistema de ficheiros raíz está exportado (fallo 395562)
- Preferir a etiqueta de unidade fornecida tamén para dispositivos de bucle

### Sonnet

- Corrixir a detección de idioma
- Evitar que o realzador borre o texto seleccionado (fallo 398661)

### Realce da sintaxe

- i18n: corrixir a extracción dos nomes dos temas
- Fortran: Salientar as alertas nos comentarios (fallo 349014)
- evitar que o contexto principal poida retirarse
- Garda de transición de estado sen fin
- YAML: engadir estilos de literal e bloque pregábel (fallo 398314)
- Logcat e SELinux: melloras para os novos esquemas de Solarized
- AppArmor: corrixir quebras en regras abertas (en KF 5.50) e melloras para os novos esquemas solarizados
- Fusionar git://anongit.kde.org/syntax-highlighting
- Actualizar cousas de «git ignore»
- Usar unha referencia no bucle «for» para tipos cun construtor de copia non trivial
- Corrección: realce de mensaxes de correo electrónico para parénteses sen pechar na cabeceira do asunto (fallo 398717)
- Perl: corrixir os corchetes, as variábeis, as referencias a cadeas e outras (fallo 391577)
- Bash: corrixir a expansión de parámetros e chaves (fallo 387915)
- Engadir un tema claro solarizado e un tema escuro

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
