---
aliases:
- ../../kde-frameworks-5.9.0
date: '2015-04-10'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
New module: ModemManagerQt (Qt wrapper for ModemManager API) Alternatively, upgrade to Plasma-NM 5.3 Beta when upgrading to ModemManagerQt 5.9.0.

### KActivities

- Permítese esquecer un recurso
- Correccións da construción
- Engadiuse un complemento para rexistrar eventos de notificacións de KRecentDocument

### KArchive

- Respectar a definición de KZip::extraField tamén ao escribir entradas centrais de cabeceiras
- Retirar dúas asercións erróneas, ocorre cando o disco está cheo, fallo 343214

### KBookmarks

- Corrixir a construción con Qt 5.5

### KCMUtils

- Usar un novo sistema de complementos baseado en JSON. Os KCM búscanse en kcms/. De momento aínda se necesita instalar un ficheiro de escritorio instalado en kservices5/ por compatibilidade
- Cargar e cubrir a versión de só QML dos KCM se se pode

### KConfig

- Corrixir unha aserción ao usar KSharedConfig no destrutor dun obxecto global.
- kconfig_compiler: engadir a posibilidade de usar CategoryLoggingName en ficheiros *.kcfgc para xerar chamadas de qCDebug(categoría).

### KI18n

- cargar previamente o catálogo global de Qt ao usar i18n()

### KIconThemes

- Agora KIconDialog pode mostrarse usando os métodos show() e exec() normais de QDialog
- Corrixir KIconEngine::paint para xestionar devicePixelRatios distintos

### KIO

- Activar que KPropertiesDialog mostre tamén información sobre o espazo libre de sistemas de ficheiros remotos (p. ex. smb)
- Corrixir KUrlNavigator con mapas de píxeles de HiDPI
- Facer que KFileItemDelegate xestione os devicePixelRatio non predeterminados nas animacións

### KItemModels

- KRecursiveFilterProxyModel: reorganizado para emitir os sinais correctos no momento axeitado
- KDescendantsProxyModel: xestionar os movementos dos que informa o modelo de orixe.
- KDescendantsProxyModel: corrixir o comportamento cando se fai unha selección ao restabelecer.
- KDescendantsProxyModel: permitir construír e usar KSelectionProxyModel desde QML.

### KJobWidgets

- Propagar o código de erro á interface de D-Bus de JobView

### KNotifications

- Engadiuse unha versión de event() que non require unha icona e usa a predeterminada
- Engadiuse unha versión de event() que acepta StandardEvent eventId e QString iconName

### KPeople

- Permitir estender os metadatos das accións usando tipos predefinidos
- Corrixir que non se actualizase o modelo de maneira axeitada tras retirar un contacto dunha persoa

### KPty

- Expoñer ao mundo se KPty se construíu coa biblioteca utempter

### KTextEditor

- Engadir un ficheiro de salientado para kdesrc-buildrc
- sintaxe: engadiuse compatibilidade cos literais enteiros binarios no ficheiro de realce de PHP

### KWidgetsAddons

- Mellorar a animación de KMessageWidget cando a taxa de píxeles do dispositivo é alta

### KWindowSystem

- Engadir unha realización non funcional de Wayland para KWindowSystemPrivate
- KWindowSystem::icon con NETWinInfo non asociado na plataforma X11.

### KXmlGui

- Preservar o dominio de tradución ao fusionar ficheiros .rc
- Corrixir o aviso en tempo de execución “QWidget::setWindowModified: o título da xanela non contén unha marca de substitución «[*]»”

### KXmlRpcClient

- Instalar traducións

### Infraestrutura de Plasma

- Corrixíronse consellos soltos cando o dono temporal do consello desaparecía ou se baleiraba
- Corrixir que TabBar non se dispuxese correctamente ao principio, como podía observarse, por exemplo, en Kickoff
- Agora as transicións de PageStack usan Animators para unhas mellores animacións
- Agora as transicións de TabGroup usan Animators para unhas mellores animacións
- Facer que Svg e FrameSvg funcionen con QT_DEVICE_PIXELRATIO

### Solid

- Actualizar as propiedades da batería tras continuar

### Cambios do sistema de construción

- Agora as versións dos módulos adicionais de CMake (ECM) sincronízanse coas das infraestruturas de KDE, polo que a nova versión é a 5.9, que sucede á versión 1.8 anterior.
- Many frameworks have been fixed to be useable without searching for their private dependencies. I.e. applications looking up a framework only need its public dependencies, not the private ones.
- Permitir a configuración de SHARE_INSTALL_DIR para xestionar mellor disposicións de varias arquitecturas

### Frameworkintegration

- Corrixir unha quebra potencial ao destruír un QSystemTrayIcon (faino, por exemplo, Trojita), fallo 343976
- Corrixir os diálogos de ficheiro modais nativos en QML, fallo 334963

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
