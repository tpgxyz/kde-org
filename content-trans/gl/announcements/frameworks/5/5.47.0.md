---
aliases:
- ../../kde-frameworks-5.47.0
date: 2018-06-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Rematar a execución da consulta de maneira anticipada se un subtermo devolve un resultado baleiro
- Evitar unha quebra ao ler datos corruptos de bases de datos de termos de documentos (fallo 392877)
- xestionar listas de cadeas como entrada
- Ignorar máis tipos de ficheiros fonte (fallo 382117)

### Iconas de Breeze

- actualizáronse as asas e o menú de sobreposición

### Módulos adicionais de CMake

- Cadea de ferramentas para Android: permitir indicar bibliotecas adicionais manualmente
- Android: Non definir qml-import-paths se está baleiro

### KArchive

- xestionar ficheiros ZIP dentro de ficheiros ZIP (fallo 73821)

### KCMUtils

- [KCModuleQml] Ignorar os controis desactivados ao tabular

### KConfig

- kcfg.xsd - non requirir un elemento kcfgfile

### KConfigWidgets

- Corrixir o esquema de cores «Predeterminado» para que volva coincidir co de Breeze

### KDeclarative

- Definir a propiedade de contexto kcm no contexto correcto
- [Plotter] Non renderizar se m_node é nulo (fallo 394283)

### KDocTools

- Actualizar a lista de entidades ucraínas
- engadir a entidade OSD a general.entites
- Engadir as entidades CIFS, NFS, Samba e SMB a general.entities
- Engadir Falkon, Kirigami, macOS, Solid, USB, Wayland, X11 e SDDM ás entidades xerais

### KFileMetaData

- comprobar que a versión de ffmpeg é polo menos a 3.1, que introduce a API que necesitamos
- buscar as etiquetas «artist» e «albumartist» de álbum en taglibextractor
- popplerextractor: non intentar adiviñar o título se non hai un

### KGlobalAccel

- Asegurarse de que a solicitude de teclado de soltar se procesa antes de emitir o atallo (fallo 394689)

### KHolidays

- holiday_es_es - Corrixir o día da Comunidade de Madrid

### KIconThemes

- Comprobar se group &lt; LastGroup, xa que KIconEffect non xestiona UserGroup

### KImageFormats

- Retirar tipos MIME duplicados de ficheiros JSON

### KIO

- Comprobar se o destino existe tamén ao pegar datos binarios (fallo 394318)
- Funcionalidade de autenticación: devolver a lonxitude real do búfer do sócket
- Funcionalidade de autenticación: unificar a API de compartición de descritor de ficheiro
- Funcionalidade de autenticación: crear o ficheiro de sócket no directorio de tempo de execución do usuario
- Funcionalidade de autenticación: eliminar o ficheiro de sócket despois de usalo
- Funcionalidade de autenticación: mover a tarefa de limpeza do ficheiro de sócket a FdReceiver
- Funcionalidade de autenticación: non usar un sócket abstracto en Linux para compartir un descriptor de ficheiro
- [kcoredirlister] Retirar todos os url.toString() posíbeis
- KFileItemActions: usar o tipo MIME predeterminado como reserva ao seleccionar só ficheiros (fallo 393710)
- Introducir KFileItemListProperties::isFile()
- Agora KPropertiesDialogPlugin pode indicar varios protocolos compatíbeis usando X-KDE-Protocols
- Preservar o fragmento ao redirixir de HTTP a HTTPS
- [KUrlNavigator] Emitir tabRequested cando se fai clic central nunha ruta do menú de selección de ruta
- Rendemento: usar a nova funcionalidade de UDS
- Non redirixir smb:/ a smb:// e logo a smb:///
- Permitir aceptar con clic duplo no diálogo de gardar (fallo 267749)
- Activar a vista previa de maneira predeterminada no diálogo de selector de ficheiros
- Agochar a vista previa de ficheiro cando a icona sexa pequena de máis
- internacionalización: usar de novo a forma plural para a mensaxe de complemento
- Usar un diálogo normal en vez de un diálogo de lista ao botar un ficheiro no lixo ou eliminalo
- Facer que o texto de aviso de operacións de eliminación faga énfase na súa permanencia e irreversibilidade
- Reverter «Mostrar os botóns do modo de vista na barra de ferramentas dos diálogos de abrir e gardar»

### Kirigami

- Mostrar action.main de maneira máis prominente en ToolBarApplicationHeader
- Permitir a Kirigami construírse sen depender do modo de tableta de KWin
- swipefilter correcto en RTL
- cambio de tamaño correcto para contentItem
- corrixir o comportamento de --reverse
- compartir contextobject para poder acceder sempre a i18n
- asegurarse de que o consello está agochado
- asegurarse de non asignar variantes incorrectas ás propiedades das que se fai seguimento
- xestionar non unha MouseArea, sinal dropped()
- retirar os efectos ao cubrir en móbiles
- iconas correctas de overflow-menu-left e overflow-menu-right
- Asa de arrastrar para cambiar a orde dos elementos dunha ListView
- Usar pnemotecnia nos botóns da barra de ferramentas
- Engadíronse ficheiros que faltaban ao .pri de QMake
- [Documentación da API] Corrixir Kirigami.InlineMessageType → Kirigami.MessageType
- corrixir applicationheaders en applicationitem
- Non permitir mostrar ou agochar o caixón cando non hai asa (fallo 393776)

### KItemModels

- KConcatenateRowsProxyModel: desinfectar correctamente a entrada

### KNotification

- Corrixir quebras en NotifyByAudio ao pechar aplicacións

### Infraestrutura KPackage

- kpackage*install**package: fix missing dep between .desktop and .json
- asegurarse de que as rutas de rcc nunca derivan de rutas absolutas

### KRunner

- Procesar as respostas de D-Bus no fío de ::match (fallo 394272)

### KTextEditor

- Non usar maiúsculas iniciais para a opción «Mostrar o número de palabras»
- Facer do contador de palabras e caracteres unha preferencia global

### KWayland

- Aumentar a versión da interface org_kde_plasma_shell
- Engadir «SkipSwitcher» á API
- Engadir un protocolo de saída de XDG

### KWidgetsAddons

- [KCharSelect] Corrixir o tamaño das celas da táboa con Qt 5.11
- [API dox] Remove usage of overload, resulting in broken docs
- [API dox] Tell doxygen "e.g." does not end the sentence, use ". "
- [Documentación da API] Retirar escapado innecesario de HTML
- Non definir automaticamente as iconas predeterminadas de cada estilo
- Facer que KMessageWidget siga o estilo de inlineMessage de Kirigami (fallo 381255)

### NetworkManagerQt

- Converter en mensaxes de depuración a información sobre propiedades non xestionadas
- WirelessSetting: crear a propiedade assignedMacAddress

### Infraestrutura de Plasma

- Modelos: nomes consistentes, corrixir os nomes dos catálogos de traducións e máis
- [Tema Breeze de Plasma] Corrixir a icona de Kleopatra para usar a folla de estilo de cores (fallo 394400)
- [Diálogo] Xestionar correctamente que se minimicen os diálogos (fallo 381242)

### Purpose

- Mellorar a integración con Telegram
- Tratar as matrices internas como restricións OU en vez de E
- Permitir restrinxir os complementos segundo a presenza do seu ficheiro de escritorio
- Permitir filtrar complementos por executábel
- Salientar o dispositivo seleccionado no complemento de KDE Connect
- corrixir problemas de internacionalización en frameworks/purpose/complementos
- Engadir o complemento de Telegram
- kdeconnect: notificalo cando o proceso non poda iniciar (fallo 389765)

### QQC2StyleBridge

- Usar a propiedade pallet só cando se use qtquickcontrols 2.4
- Funcionar con Qt&lt;5.10
- Corrixir a altura da barra de lapelas
- Usar Control.palette
- [RadioButton] Cambiar o nome de «control» a «controlRoot»
- Non definir o espazamento de maneira explícita en RadioButton e CheckBox
- [FocusRect] Usar colocación manual en vez de áncoras
- Resulta que o escintilante nunha vista desprazábel é o contentItem
- Mostrar o rectángulo de foco cando se enfocan CheckBox ou RadioButton
- corrección chapuceira da detección de vista desprazábel
- Non cambiar o pai do escintilante á zona de rato
- [TabBar] Cambiar de separador coa roda do rato
- O control non debe ter subordinados (fallo 394134)
- Restrinxir o desprazamento (fallo 393992)

### Realce da sintaxe

- Perl6: engadir compatibilidade coas extensións .pl6, .p6 e .pm6 (fallo 392468)
- DoxygenLua: corrixir o peche de bloques de comentarios (fallo 394184)
- Engadir pgf aos formatos de ficheiro de estilo LaTeX (o mesmo formato que tikz)
- Engadir palabras clave de PostgreSQL
- Salientado de OpenSCAD
- debchangelog: engadir Cosmic Cuttlefish
- cmake: Corrixir o aviso de DetectChar sobre unha barra invertida sen escapar
- Pony: corrixir o identificador e a palabra clave
- Lua: actualizouse para Lua 5.3

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
