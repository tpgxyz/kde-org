---
date: '2013-12-18'
hidden: true
title: A versión 4.12 das aplicacións de KDE supón un enorme paso adiante na xestión
  de información persoal e melloras xerais
---
A comunidade KDE síntese orgullosa de anunciar as últimas actualizacións principais das aplicacións de KDE que inclúen novas funcionalidades e solucións de erros. Esta versión inclúe melloras masivas no software de xestión de información persoal de KDE que agora ten un rendemento moi superior e inclúe moitas funcionalidades novas. Kate afinou a integración de complementos de Python e engadiu compatibilidade inicial con macros de Vim, e os xogos e as aplicacións educativas traen varias funcionalidades novas.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/kate.png" width="600px">}}

Kate, o editor de texto gráfico máis avanzado de Linux, recibiu de novo melloras no completado de código, desta vez introducindo <a href='http://scummos.blogspot.com/2013/10/advanced-code-completion-filtering-in.html'>código de coincidencias avanzado, xestionando abreviacións e coincidencias parciais en clases</a> (en inglés). Por exemplo, tras escribir «QualIdent» o novo código podería suxerir «QualifiedIdentifier». Kate tamén recibiu <a href='http://dot.kde.org/2013/09/09/kde-commit-digest-18th-august-2013'>compatibilidade inicial con macros de Vim</a> (en inglés). E o mellor de todo: estas melloras afectan tamén a KDevelop e outras aplicacións que usan a tecnoloxía de Kate.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/okular.png" width="600px">}}

O visor de documentos Okular <a href='http://tsdgeos.blogspot.com/2013/10/changes-in-okular-printing-for-412.html'>agora ten en conta as marxes de soporte físico de impresión</a>, permite son e vídeo en epub, recibiu melloras na busca e pode xestionar máis transformacións, incluídas as de metadatos de imaxe Exif. Na ferramenta de diagramas UML Umbrello, agora as asociacións poden <a href='http://dot.kde.org/2013/09/20/kde-commit-digest-1st-september-2013'>debuxarse con distintas disposicións</a> e Umbrello <a href='http://dot.kde.org/2013/09/09/kde-commit-digest-25th-august-2013'>engade información visual cando un trebello está documentado</a>.

O gardián da intimidade KGpg mostra máis información aos usuarios e KWalletManager, a ferramenta para gardar contrasinais, agora pode <a href='http://www.rusu.info/wp/?p=248'>almacenalas en forma de GPG</a> (artigo en inglés). Konsole introduce unha nova funcionalidade: Ctrl+clic para iniciar directamente un URL da saída da consola. Ademais, agora tamén pode <a href='http://martinsandsmark.wordpress.com/2013/11/02/mangonel-1-1-and-more/'>listar procesos ao avisar antes de saír</a> (artigo en inglés).

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/dolphin.png" width="600px">}}

KWebKit adds the ability to <a href='http://dot.kde.org/2013/08/09/kde-commit-digest-7th-july-2013'>automatically scale content to match desktop resolution</a>. File manager Dolphin introduced a number of performance improvements in sorting and showing files, reducing memory usage and speeding things up. KRDC introduced automatic reconnecting in VNC and KDialog now provides access to 'detailedsorry' and 'detailederror' message boxes for more informative console scripts. Kopete updated its OTR plugin and the Jabber protocol has support for XEP-0264: File Transfer Thumbnails. Besides these features the main focus was on cleaning code up and fixing compile warnings.

### Xogos e software educativo

Traballouse en varias áreas dos xogos de KDE. KReversi está <a href='http://tsdgeos.blogspot.ch/2013/10/kreversi-master-is-now-qt-quick-based.html'>agora baseado en QML e Qt Quick</a>, o que o dota dunha experiencia de xogo máis atractiva e fluída. KNetWalk tamén se <a href='http://tsdgeos.blogspot.ch/2013/08/knetwalk-portedx-to-qtquick.html'>migrou</a> cos mesmos beneficios, así como a posibilidade de estabelecer unha grade de anchura e altura personalizadas. Konquest ten agora un novo xogador da intelixencia artificial de dificultade esixente, «Becai».

In the Educational applications there have been some major changes. KTouch <a href='http://blog.sebasgo.net/blog/2013/11/12/what-is-new-for-ktouch-in-kde-sc-4-dot-12/'>introduces custom lesson support and several new courses</a>; KStars has a new, more accurate <a href='http://knro.blogspot.ch/2013/10/demo-of-ekos-alignment-module.html'>alignment module for telescopes</a>, find a <a href='http://www.youtube.com/watch?v=7Dcn5aFI-vA'>youtube video here</a> of the new features. Cantor, which offers an easy and powerful UI for a variety of mathematical backends, now has backends <a href='http://blog.filipesaraiva.info/?p=1171'>for Python2 and Scilab</a>. Read more about the powerful Scilab backend <a href='http://blog.filipesaraiva.info/?p=1159'>here</a>. Marble adds integration with ownCloud (settings are available in Preferences) and adds overlay rendering support. KAlgebra makes it possible to export 3D plots to PDF, giving a great way of sharing your work. Last but not least, many bugs have been fixed in the various KDE Education applications.

### Correo, calendario e información persoal

Traballouse moito en KDE PIM, o grupo de aplicacións de KDE para xestionar correo, calendarios e outra información persoal.

Starting with email client KMail, there is now <a href='http://dot.kde.org/2013/10/11/kde-commit-digest-29th-september-2013'>AdBlock support</a> (when HTML is enabled) and improved scam detection support by extending shortened URLs. A new Akonadi Agent named FolderArchiveAgent allows users to archive read emails in specific folders and the GUI of the Send Later functionality has been cleaned up. KMail also benefits from improved Sieve filter support. Sieve allows for server-side filtering of emails and you can now <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-script-parsing-22/'>create and modify the filters on the servers</a> and <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-12/'>convert existing KMail filters to server filters</a>. KMail's mbox support <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-mboximporter/'>has also been improved</a>.

In other applications, several changes make work easier and more enjoyable. A new tool is introduced, <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>the ContactThemeEditor</a>, which allows for creating KAddressBook Grantlee themes for displaying contacts. The addressbook can now also show previews before printing data. KNotes has seen some <a href='http://www.aegiap.eu/kdeblog/2013/11/what-news-in-kdepim-4-12-knotes/'>serious work on solving bugs</a>. Blogging tool Blogilo can now deal with translations and there are a wide variety of fixes and improvements all over the KDE PIM applications.

Benefiting all applications, the underlying KDE PIM data cache has <a href='http://ltinkl.blogspot.ch/2013/11/this-month-october-in-red-hat-kde.html'>seen much work on performance, stability and scalability</a>, fixing <a href='http://www.progdan.cz/2013/10/akonadi-1-10-3-with-postgresql-fix/'>support for PostgreSQL with the latest Qt 4.8.5</a>. And there is a new command line tool, the calendarjanitor which can scan all calendar data for buggy incidences and adds a debug dialog for search. Some very special hugs go to Laurent Montel for the work he is doing on KDE PIM features!

#### Instalar aplicacións de KDE

Os programas de KDE, incluídas todas as súas bibliotecas e as súas aplicacións, están dispoñíbeis de maneira gratuíta baixo licenzas de código aberto. Os programas de KDE funcionan en varias configuracións de soporte físico e arquitecturas de CPU como ARM e x86, sistemas operativos e funcionan con calquera xestor de xanelas ou contorno de escritorio. Ademais de Linux e outros sistemas operativos baseados en UNIX pode atopar versións para Microsoft Windows da meirande parte das aplicacións de KDE no sitio web de <a href='http://windows.kde.org'>programas de KDE para Windows</a> e versións para o Mac OS X de Apple no <a href='http://mac.kde.org/'>sitio web de programas de KDE para Mac</a>. En Internet poden atoparse construcións experimentais de aplicacións de KDE para varias plataformas móbiles como MeeGo, MS Windows Mobile e Symbian pero non están mantidas. <a href='http://plasma-active.org'>Plasma Active</a> é unha experiencia de usuario para un maior abanico de dispositivos, como computadores de tableta e outros soportes físicos móbiles.

KDE software can be obtained in source and various binary formats from <a href='http://download.kde.org/stable/4.12.0'>download.kde.org</a> and can also be obtained on <a href='/download'>CD-ROM</a> or with any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.

##### Paquetes

Some Linux/UNIX OS vendors have kindly provided binary packages of 4.12.0 for some versions of their distribution, and in other cases community volunteers have done so.

##### Lugares dos paquetes

For a current list of available binary packages of which the KDE's Release Team has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.12.0'>Community Wiki</a>.

The complete source code for 4.12.0 may be <a href='/info/4/4.12.0'>freely downloaded</a>. Instructions on compiling and installing KDE software 4.12.0 are available from the <a href='/info/4/4.12.0#binary'>4.12.0 Info Page</a>.

#### Requisitos do sistema

In order to get the most out of these releases, we recommend to use a recent version of Qt, such as 4.8.4. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.

In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.
