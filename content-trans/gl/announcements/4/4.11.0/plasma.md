---
date: 2013-08-14
hidden: true
title: A versión 4.11 dos espazos de traballo de Plasma continúa refinando a experiencia
  de usuario
---
{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/empty-desktop.png" caption=`Versión 4.11 dos espazos de traballo de Plasma de KDE` width="600px" >}}

In the 4.11 release of Plasma Workspaces, the taskbar – one of the most used Plasma widgets – <a href='http://blogs.kde.org/2013/07/29/kde-plasma-desktop-411s-new-task-manager'>has been ported to QtQuick</a>. The new taskbar, while retaining the look and functionality of its old counterpart, shows more consistent and fluent behavior. The port also resolved a number of long standing bugs. The battery widget (which previously could adjust the brightness of the screen) now also supports keyboard brightness, and can deal with multiple batteries in peripheral devices, such as your wireless mouse and keyboard. It shows the battery charge for each device and warns when one is running low. The Kickoff menu now shows recently installed applications for a few days. Last but not least, notification popups now sport a configure button where one can easily change the settings for that particular type of notification.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/notifications.png" caption=`Mellora da xestión de notificacións` width="600px" >}}

KMix, KDE's sound mixer, received significant performance and stability work as well as <a href='http://kmix5.wordpress.com/2013/07/26/kmix-mission-statement-2013/'>full media player control support</a> based on the MPRIS2 standard.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/battery-applet.png" caption=`O miniaplicativo de batería, no seu novo deseño, en acción` width="600px" >}}

## Xestor de xanelas e compositor de KWin

O noso xestor de xanelas, KWin, recibiu de novo actualizacións significativas afastándose de tecnoloxías vellas e incorporando o protocolo de comunicación XCB. Isto ten como resultado unha xestión de xanelas máis suave e rápida. Tamén se introduciu a compatibilidade con OpenGL 3.1 e OpenGL ES 3.0. Esta versión tamén incorpora a primeira compatibilidade experimental con Wayland, o sucesor de X11. Isto permite usar KWin con X11 sobre o software de Wayland. Para máis información sobre como usar este modo experimental, consulte <a href='http://blog.martin-graesslin.com/blog/2013/06/starting-a-full-kde-plasma-session-in-wayland/'>esta publicación</a>. Tamén se fixeron melloras masivas na interface de scripting de KWin, que agora goza dunha interface de usuario de configuración, novas animacións, efectos gráficos e moitas melloras máis pequenas. Esta versión inclúe mellor compatibilidade con varias pantallas (incluído o brillo de bordos para «esquinas quentes»), mellora de disposición automática rápida de xanelas (con zonas de disposición automática que se poden configurar) e a habitual racha de solucións de fallos e optimizacións. Consulte <a href='http://blog.martin-graesslin.com/blog/2013/06/what-we-did-in-kwin-4-11/'>isto</a> (en inglés) e <a href='http://blog.martin-graesslin.com/blog/2013/06/new-kwin-scripting-feature-in-4-11/'>isto outro</a> (tamén en inglés) para máis información.

## Xestión de monitores e atallos web

A configuración de monitor na configuración do sistema <a href='http://www.afiestas.org/kscreen-1-0-released/'>substituíuse pola nova ferramenta KScreen</a>. KScreen trae aos espazos de traballo de Plasma unha compatibilidade con varios monitores máis intelixente, configurando novas pantallas automaticamente e lembrando a configuración dos monitores configurados manualmente. Inclúe unha interface intuitiva e moi visual e xestiona a reorganización de monitores de maneira sinxela arrastrando e soltando.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kscreen.png" caption=`A nova xestión de monitores de KScreen` width="600px" >}}

Os atallos web, a forma máis sinxela de atopar o que busca en Internet, limpáronse e melloráronse. Moitos actualizáronse para usar conexións cifradas de maneira segura (TLS/SSL), engadíronse novos atallos web e retiráronse algúns atallos obsoletos. Tamén se mellorou o proceso que lle permite engadir os seus propios atallos web. Pode consultar máis detalles <a href='https://plus.google.com/108470973614497915471/posts/9DUX8C9HXwD'>aquí</a>.

Esta versión marca o final da versión 1 dos espazos de traballo de Plasma, parte da serie 4 da compilación de software de KDE. Para facilitar a transición á nova xeración esta versión manterase durante polo menos dous anos. O enfoque en desenvolvemento de funcionalidades pasará agora á versión 2 dos espazos de traballo de Plasma, mentres que as melloras de rendemento e as correccións de erros concentraranse na serie 4.11.

#### Instalar Plasma

Os programas de KDE, incluídas todas as súas bibliotecas e as súas aplicacións, están dispoñíbeis de maneira gratuíta baixo licenzas de código aberto. Os programas de KDE funcionan en varias configuracións de soporte físico e arquitecturas de CPU como ARM e x86, sistemas operativos e funcionan con calquera xestor de xanelas ou contorno de escritorio. Ademais de Linux e outros sistemas operativos baseados en UNIX pode atopar versións para Microsoft Windows da meirande parte das aplicacións de KDE no sitio web de <a href='http://windows.kde.org'>programas de KDE para Windows</a> e versións para o Mac OS X de Apple no <a href='http://mac.kde.org/'>sitio web de programas de KDE para Mac</a>. En Internet poden atoparse construcións experimentais de aplicacións de KDE para varias plataformas móbiles como MeeGo, MS Windows Mobile e Symbian pero non están mantidas. <a href='http://plasma-active.org'>Plasma Active</a> é unha experiencia de usuario para un maior abanico de dispositivos, como computadores de tableta e outros soportes físicos móbiles.

KDE software can be obtained in source and various binary formats from <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> and can also be obtained on <a href='/download'>CD-ROM</a> or with any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.

##### Paquetes

Some Linux/UNIX OS vendors have kindly provided binary packages of 4.11.0 for some versions of their distribution, and in other cases community volunteers have done so. <br />

##### Lugares dos paquetes

For a current list of available binary packages of which the KDE's Release Team has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Community Wiki</a>.

The complete source code for 4.11.0 may be <a href='/info/4/4.11.0'>freely downloaded</a>. Instructions on compiling and installing KDE software 4.11.0 are available from the <a href='/info/4/4.11.0#binary'>4.11.0 Info Page</a>.

#### Requisitos do sistema

In order to get the most out of these releases, we recommend to use a recent version of Qt, such as 4.8.4. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br /> In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.

## Hoxe tamén se anunciaron:

## <a href="../applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> KDE Applications 4.11 Bring Huge Step Forward in Personal Information Management and Improvements All Over</a>

Esta versión marca melloras masivas no software de xestión de información persoal de KDE, que mellora moito o rendemento e inclúe moitas novas funcionalidades. Kate mellora a produtividade de desenvolvedores de Python e JavaScript con novos complementos, Dolphin volveuse máis rápido e as aplicacións educativas traen consigo novas funcionalidades.

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> KDE Platform 4.11 Delivers Better Performance</a>

Esta versión 4.11 da plataforma de KDE continúa centrándose en estabilidade. Estanse realizando novas funcionalidades para a futura versión 5.0 das infraestruturas de KDE, pero para a versión estábel conseguimos introducir optimizacións para a nosa infraestrutura Nepomuk.
