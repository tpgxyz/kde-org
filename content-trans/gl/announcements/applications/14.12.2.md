---
aliases:
- ../announce-applications-14.12.2
changelog: true
date: '2015-02-03'
description: KDE Ships Applications 14.12.2.
layout: application
title: KDE publica a versión 14.12.2 das aplicacións de KDE
version: 14.12.2
---
February 3, 2015. Today KDE released the second stability update for <a href='../14.12.0'>KDE Applications 14.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Entre as máis de 20 correccións de erros rexistradas están melloras no xogo de anagramas Kanagram, o modelador de UML Umbrello, o visor de documentos Okular e o globo virtual Marble.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.16, KDE Development Platform 4.14.5 and the Kontact Suite 4.14.5.
