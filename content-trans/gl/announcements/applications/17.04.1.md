---
aliases:
- ../announce-applications-17.04.1
changelog: true
date: 2017-05-11
description: KDE publica a versión 17.04.1 das aplicacións de KDE
layout: application
title: KDE publica a versión 17.04.1 das aplicacións de KDE
version: 17.04.1
---
May 11, 2017. Today KDE released the first stability update for <a href='../17.04.0'>KDE Applications 17.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

As máis de 20 correccións de erros inclúen melloras en, entre outros, KDE PIM, Dolphin, Gwenview, Kate e Kdenlive.

This release also includes Long Term Support version of KDE Development Platform 4.14.32.
