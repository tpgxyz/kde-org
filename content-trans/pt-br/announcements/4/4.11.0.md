---
aliases:
- ../4.11
custom_about: true
custom_contact: true
date: 2013-08-14
description: O KDE Lança a Versão 4.11 da Área de Trabalho Plasma, as Aplicações e
  a Plataforma.
title: Compilação dos programas do KDE 4.11
---
{{<figure src="/announcements/4/4.11.0/screenshots/plasma-4.11.png" class="text-center" caption=`Espaço de Trabalho Plasma do KDE 4.11` >}} <br />

14 de Agosto de 2013. A Comunidade do KDE orgulha-se em anunciar as últimas alterações importantes da Área de Trabalho Plasma, das Aplicações e da Plataforma de Desenvolvimento, oferecendo novas funcionalidades e correcções, enquanto se prepara a plataforma para futuras evoluções. A Área de Trabalho Plasma 4.11 irá ter um suporte a longo prazo, enquanto a equipa se foca na transição técnica para as Plataformas 5. Apresenta deste modo o último lançamento combinado da Área de Trabalho, das Aplicações e da Plataforma sob o mesmo número de versão.<br />

Esta versão é dedicada à memória de <a href='http://en.wikipedia.org/wiki/Atul_Chitnis'>Atul 'toolz' Chitnis</a>, um grande colaborador do Software Livre e Código Aberto da Índia. Atul conduziu as conferências Linux Bangalore e FOSS.IN desde 2001 e ambas foram eventos marcantes no cenário FOSS (Free and Open-Source Software) na Índia. O KDE Índia nasceu na primeira FOSS.in, realizada em dezembro de 2005. Muitos dos colaboradores indianos do KDE começaram nesses eventos. Devido ao empenho de Atul, o KDE Project Day da FOSS.IN sempre foi um enorme sucesso. Atul nos deixou em 3 de junho, depois de uma longa luta contra o câncer. Que sua alma descanse em paz. Somos gratos pelas suas contribuições para um mundo melhor.

Estas versões foram todas traduzidas para 54 idiomas e esperamos que outros sejam adicionados nas próximas correções mensais do KDE. A Equipe de Documentação atualizou 91 manuais de aplicativos para esta versão.

## <a href="./plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="A Área de Trabalho Plasma do KDE 4.11" width="64" height="64" /> A Área de Trabalho Plasma 4.11 Continua a Afinar a Experiência do Utilizador</a>

Preparando-se para uma manutenção a longo prazo, o Espaço de Trabalho Plasma oferece melhorias para as funcionalidades básicas com uma barra de tarefas mais suave, um widget de bateria mais inteligente e um mixer som melhorado. A inclusão do KScreen traz um tratamento inteligente para vários monitores do ambiente de trabalho, assim como algumas melhorias de desempenho em grande escala, combinadas com pequenos ajustes de usabilidade, para proporcionar uma experiência geral mais agradável.

## <a href="./applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="As Aplicações do KDE 4.11"/> As Aplicações do KDE 4.11 Dão um Enorme Passo em Frente na Gestão de Informações Pessoais e Tiveram Melhorias em Todo o Lado</a>

Esta versão marca grandes melhorias na plataforma do KDE PIM, ganhando melhor desempenho e muitas novas funcionalidades. O Kate melhora a produtividade de programadores em Python e JavaScript com novos plugins, o Dolphin ficou mais rápido e os aplicativos educacionais ganharam diversas novas funcionalidades.

## <a href="./platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="A Plataforma de Desenvolvimento do KDE 4.11"/> A Plataforma do KDE 4.11 Oferece uma Melhor Performance</a>

Esta versão da Plataforma do KDE 4.11 continua com foco na estabilidade. As novas funcionalidades serão implementadas na nossa futura versão do KDE Frameworks 5.0, mas para a versão estável continuaremos com a otimização da plataforma Nepomuk.

<br />
Ao actualizar, observe por favor as <a href='http://community.kde.org/KDE_SC/4.11_Release_Notes'>notas de lançamento</a>.<br />

## Espalhe a palavra e veja o que está acontecendo: Marque como &quot;KDE&quot;

O KDE incentiva as pessoas a espalharem a palavra na Web social. Envie histórias para páginas de notícias, use canais como o delicious, digg, reddit, twitter, identi.ca. Envie imagens para serviços como o Facebook, Flickr, ipernity e Picasa, publicando-as nos grupos apropriados. Crie <i>screencasts</i> e envie-os para o YouTube, Blip.tv e Vimeo. Marque as publicações e materiais enviados como &quot;KDE&quot;. Isto torna-os muito fáceis de localizar, permitindo que a equipe de promoção do KDE analise a cobertura do lançamento do KDE 4.11.

## Festas de lançamento

Como sempre ocorre, os membros da comunidade do KDE organizam festas de lançamento em todo o mundo. Algumas já foram agendadas e muitas outras ainda serão. Encontre <a href='http://community.kde.org/Promo/Events/Release_Parties/4.11'>aqui uma lista das festas</a>. Todos são bem-vindos! Existirá uma grande combinação de companhia interessante e conversas inspiradoras, assim como comida e bebidas. É uma grande oportunidade de aprender mais sobre o que está acontecendo no KDE, envolver-se com esta comunidade ou apenas conhecer outros usuários e colaboradores.

Incentivamos as pessoas a organizarem as suas próprias festas. Elas são divertidas e abertas para qualquer pessoa! Consulte algumas <a href='http://community.kde.org/Promo/Events/Release_Parties'>dicas de como organizar uma festa</a>.

## Sobre estes anúncios de lançamento

Estes anúncios de lançamento foram preparados por Jos Poortvliet, Sebastian Kügler, Markus Slopianka, Burkhard Lück, Valorie Zimmerman, Maarten De Meyer, Frank Reininghaus, Michael Pyne, Martin Gräßlin e outros membros da equipe de promoção e toda a comunidade do KDE. Eles abrangem os destaques das diversas alterações efetuadas nos aplicativos do KDE ao longo dos últimos seis meses.

#### Apoiando o KDE

<a href="http://jointhegame.kde.org/"> <img src="/announcements/4/4.9.0/images/join-the-game.png" class="img-fluid float-left mr-3" alt="Join the Game"/> </a>

O novo <a href='http://jointhegame.kde.org/'>programa de Membros de Suporte</a> está agora aberto. Por 25&euro; por trimestre poderá garantir que a comunidade internacional do KDE continua a crescer e a desenvolver Software Livre de classe mundial.
