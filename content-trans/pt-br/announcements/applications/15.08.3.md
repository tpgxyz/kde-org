---
aliases:
- ../announce-applications-15.08.3
changelog: true
date: 2015-11-10
description: O KDE disponibiliza o KDE Applications 15.08.3
layout: application
title: O KDE disponibiliza o KDE Applications 15.08.3
version: 15.08.3
---
10 de Novembro de 2015. Hoje o KDE lançou a terceira actualização de estabilidade para as <a href='%1'>Aplicações do KDE 15.08</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

Mais de 20 correções de erros registradas incluem melhorias no ark, dolphin, kdenlive, kdepim, kig, lokalize e umbrello.

Esta versão também inclui as versões de Suporte de Longo Prazo da Plataforma de Desenvolvimento do KDE 4.14.14.
