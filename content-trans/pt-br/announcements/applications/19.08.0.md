---
aliases:
- ../announce-applications-19.08.0
changelog: true
date: 2019-08-15
description: O KDE Lança as Aplicações do KDE 19.08.
layout: application
release: applications-19.08.0
title: O KDE disponibiliza o KDE Applications 19.08.0
version: '19.08'
version_number: 19.08.0
---
{{< peertube "/6443ce38-0a96-4b49-8fc5-a50832ed93ce" >}}

{{% i18n_date %}}

A comunidade do KDE tem o prazer de anunciar o lançamento das Aplicações do KDE 19.08.

Esta versão faz parte do compromisso do KDE em oferecer de forma contínua versões melhoradas dos programas que oferecemos aos nossos utilizadores. As novas versões das Aplicações trazem mais funcionalidades e aplicações melhor desenhadas que aumentam a usabilidade e a estabilidade de aplicações como o Dolphin, Konsole, Kate, Okular e todos os seus outros utilitários favoritos do KDE. O nosso objectivo é garantir que você continua produtivo e tornar as aplicações do KDE mais simples e agradáveis para você usar.

Esperemos que goste de todas as novas melhorias que irá encontrar no 19.08!

## O que há de novo nas Aplicações do KDE 19.08

Foram resolvidos mais de 170 erros. Estas correcções reimplementam funcionalidades desactivadas, normalizam atalhos e resolvem estoiros, tornando as suas aplicações mais amigáveis e permitindo-lhe ser mais produtivo.

### Dolphin

O Dolphin é o explorador de ficheiros e pastas do KDE que poderá invocar em qualquer altura, usando o novo atalho global <keycap>Meta + E</keycap>. Existe também uma nova funcionalidade que minimiza a desarrumação no seu ecrã. Quando o Dolphin já estiver em execução, se abrir pastas com outras aplicações, essas pastas irão abrir numa nova página da janela existente, em vez de abrir uma nova janela do Dolphin. É importante referir que este comportamento está activo agora por omissão, mas poderá ser desactivado.

O painel de informações (localizado por omissão à direita do painel principal do Dolphin) foi melhorado de várias formas. Poderá, por exemplo, optar por reproduzir automaticamente os ficheiros multimédia se os realçar no painel principal, e poderá agora seleccionar e copiar o texto visível no painel. Se quiser alterar o que mostra o painel de informações, poderá fazer isso mesmo no painel, dado que o Dolphin não irá abrir uma janela separada quando optar por configurar o painel.

Também resolvemos muitos dos pequenos problemas e erros, garantindo que a sua experiência no uso do Dolphin seja muito mais fluída de um modo geral.

{{<figure src="/announcements/applications/19.08.0/dolphin_bookmark.png" alt=`Imagem da nova funcionalidade de favoritos do Dolphin` caption=`Imagem da nova funcionalidade de favoritos do Dolphin` width="600px" >}}

### Gwenview

O Gwenview é o visualizador de imagens do KDE e, nesta versão, os programadores melhoraram a sua funcionalidade de visualização de miniaturas por todo o lado. O Gwenview consegue usar agora um \"Modo de utilização de baixos recursos\" que carrega miniaturas de baixa resolução (quando estiverem disponíveis. Este novo modo é muito mais rápido e mais eficiente a nível de recursos quando carregar miniaturas das imagens JPEG e dos ficheiros RAW. Nos casos em que o Gwenview não consegue gerar uma miniatura da imagem, agora mostra uma imagem de substituição em vez de reutilizar a miniatura da imagem anterior. Os problemas que o Gwenview tinha ao mostrar miniaturas das câmaras da Sony e da Canon também foram corrigidos.

Para além das alterações no departamento das miniaturas, o Gwenview também implementou agora um novo menu de “Partilha” que permite o envio das imagens para diversos lugares, carregando e apresentando correctamente os ficheiros nos locais remotos acessíveis pelo KIO. A nova versão do Gwenview também mostra bastante mais meta-dados de EXIF nas imagens RAW.

{{<figure src="/announcements/applications/19.08.0/gwenview_share.png" alt=`Novo menu “Partilhar” do Gwenview` caption=`Novo menu “Partilhar” do Gwenview` width="600px" >}}

### Okular

Os programadores introduziram muitas melhorias nas anotações do Okular, o visualizador de documentos do KDE. Para além da interface melhorada nas janelas de configuração das anotações, a anotação de linhas agora poderá ver alguns floreados visuais nas suas extremidades, permitindo transformá-las em setas, por exemplo. A outra coisa que poderá fazer com as anotações será expandi-las ou encolhê-las todas de uma vez.

O suporte do Okular para documentos EPub também recebeu algum esforço adicional nesta versão. O Okular já não estoira mais a tentar carregar ficheiros ePub danificados e a sua performance com ficheiros ePub enormes melhorou de forma significativa. A lista de alterações nesta versão inclui os contornos de páginas melhorados e a ferramenta do Marcador no modo de Apresentação para modos de PPP elevados.

{{<figure src="/announcements/applications/19.08.0/okular_line_end.png" alt=`Configuração da ferramenta de anotações com a nova opção de Fim da Linha` caption=`Configuração da ferramenta de anotações com a nova opção de Fim da Linha` width="600px" >}}

### Kate

Graças aos nossos programadores, foram eliminados três erros incómodos nesta versão do editor de texto avançado do KDE. O Kate mais uma vez traz a sua janela existente para a frente quando for pedido para abrir um novo documento a partir de outra aplicação. A funcionalidade de \"Abertura Rápida\" ordena os itens pelo usado mais recentemente, pré-seleccionando o item de topo. A terceira alteração é na funcionalidade de \"Documentos recentes\", que agora funciona quando a configuração actual está feita para não gravar as definições das janelas individuais.

### Konsole

A mudança mais notória no Konsole, a aplicação de emulação de terminal do KDE, é a melhoria da funcionalidade de colocação lado-a-lado. Poderá dividir a área principal de qualquer forma que desejar, tanto na vertical como na horizontal. As sub-áreas poderão então ser divididas de novo da forma que desejar. Esta versão também adiciona a capacidade de arrastar e largar áreas, para que possa reorganizar facilmente a disposição de acordo com o seu processo de trabalho.

Para além disso, a janela de Configuração recebeu uma grande remodelação para a tornar mais arrumada e simples de usar.

{{< video src-webm="/announcements/applications/19.08.0/konsole-tabs.webm" >}}

### Spectacle

O Spectacle é a aplicação de capturas de fotografias do ecrã do KDE e está a ganhar cada vez mais e melhores funcionalidades interessantes a cada nova versão. Esta versão não é excepção, dado que o Spectacle vem agora com diversas funcionalidades que regulam a funcionalidade de Atraso. Ao tirar uma fotografia com tempo de atraso, o Spectacle irá mostrar o tempo que falta no título da sua janela. Esta informação também fica visível no seu item do Gestor de Tarefas.

Ainda na funcionalidade de Atraso, o botão do Gestor de Tarefas do Spectacle irá também mostrar uma barra de progresso, para que possa ter uma ideia de quando será tirada a fotografia. Finalmente, se repor o Spectacle a partir do seu estado minimizado enquanto espera, irá reparar que o botão para “Tirar uma Nova Fotografia” se transformou num botão para \"Cancelar\". Este botão também contém uma barra de progresso que lhe dá a hipótese de parar a contagem decrescente.

A gravação de fotografias tem também uma nova funcionalidade. Quando tiver gravado uma fotografia, o Spectacle mostra uma mensagem na aplicação que lhe permite abrir a fotografia ou a sua pasta respectiva.

{{< video src-webm="/announcements/applications/19.08.0/spectacle_progress.webm" >}}

### Kontact

O Kontact, o conjunto de aplicações de e-mail/calendário/contactos e 'groupware' geral do KDE, traz o suporte de emojis coloridos de Unicode e do Markdown para o compositor de e-mails. Não só esta versão do KMail permitir que fique com as suas mensagens com bom aspecto, mas também graças à integração com os verificadores gramaticais, como o LanguageTool e o Grammalecte, também o ajudará a verificar e a corrigir o seu texto.

{{<figure src="/announcements/applications/19.08.0/kontact_emoji.png" alt=`Seletor de emoji` caption=`Seletor de emoji` width="600px" >}}

{{<figure src="/announcements/applications/19.08.0/kmail_grammar.png" alt=`Integração com a verificação gramatical do KMail` caption=`Integração com a verificação gramatical do KMail` width="600px" >}}

Ao planejar eventos, e-mails de convite no KMail não são mais deletados após serem respondidos. Agora é possível mover um evento de um calendário para o outro no editor de eventos no KOrganizer.

Por fim, o KAddressBook agora é capaz de enviar mensagens SMS para seus contatos via KDE Connect, resultando numa integração mais conveniente entre seu desktop e seus dispositivos móveis.

### Kdenlive

A nova versão do Kdenlive, a aplicação de edição de vídeos do KDE, tem um novo conjunto de combinações rato-teclado que o ajudarão a ser mais produtivos. Poderá, por exemplo, modificar a velocidade de um 'clip' na linha temporal com o CTRL e depois arrastar o 'clip' ou activar uma antevisão em miniatura dos 'clips' de vídeo, mantendo carregado o Shift e movendo o rato sobre uma miniatura do 'clip' no grupo do projecto. Os programadores também aplicaram algum esforço na usabilidade, tornando as operações de edição de 3 pontos consistentes com os outros editores de vídeo, o que irá ser certamente do seu agrado se estiver a mudar de outro editor qualquer para o Kdenlive.

{{<figure src="https://cdn.kde.org/screenshots/kdenlive/19-08.png" alt=`Kdenlive 19.08.0` caption=`Kdenlive 19.08.0` width="600px" >}}
