---
aliases:
- ../announce-applications-16.04.1
changelog: true
date: 2016-05-10
description: O KDE disponibiliza o KDE Applications 16.04.1
layout: application
title: O KDE disponibiliza o KDE Applications 16.04.1
version: 16.04.1
---
10 de Maio de 2016. Hoje o KDE lançou a primeira actualização de estabilidade para as <a href='../16.04.0'>Aplicações do KDE 16.04</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

Mais de 25 correções de erros registradas incluem melhorias no kdepim, Ark, Kate, Dolphin, Kdenlive, Lokalize, Spectacle, entre outros.

Esta versão também inclui as versões de Suporte de Longo Prazo da Plataforma de Desenvolvimento do KDE 4.14.20.
