---
aliases:
- ../../kde-frameworks-5.36.0
date: 2017-07-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Alle frameworks: optie om het QCH-bestand te bouwen &amp; te installeren met de publieke API-dox

### Baloo

- FindInotify.cmake gebruiken om te bepalen of inotify beschikbaar is

### Breeze pictogrammen

- Niet onnodig afhangen van bash en valideer standaard geen pictogrammen

### Extra CMake-modules

- FindQHelpGenerator: oppakken van Qt4 versie vermijden
- ECMAddQch: hard mislukken als de benodigde hulpmiddelen niet aanwezig zijn, om verrassingen te voorkomen
- perl laten vallen als afhankelijkheid voor ecm_add_qch, niet nodig/gebruikt
- Scan de gehele installatiemap op afhankelijkheden van qml
- Nieuw: ECMAddQch, voor genereren van tagbestanden van qch &amp; doxygen
- KDEInstallDirsTest.relative_or_absolute_usr repareren, gebruik van Qt paden vermijden

### KAuth

- Foutstatus controleren na elk gebruik van PolKitAuthority

### KBookmarks

- Fouten uitsturen wanneer keditbookmarks ontbreekt (bug 303830)

### KConfig

- Reparatie voor CMake 3.9

### KCoreAddons

- FindInotify.cmake gebruiken om te bepalen of inotify beschikbaar is

### KDeclarative

- KKeySequenceItem: maak het mogelijk om Ctrl+Num+1 als een sneltoets op te nemen
- Start slepen met indrukken en houden bij aanraakgebeurtenissen (bug 368698)
- Vertrouw niet op QQuickWindow voor afleveren van QEvent::Ungrab als mouseUngrabEvent (omdat het dat niet langer doet in Qt 5.8+) (bug 380354)

### Ondersteuning van KDELibs 4

- Naar KEmoticons zoeken, wat een afhankelijkheid is volgens CMake config.cmake.in (bug 381839)

### KFileMetaData

- Een extractor toevoegen met qtmultimedia

### KI18n

- Ga na dat de target tsfiles wordt gegenereerd

### KIconThemes

- Meer details over gebruik van pictogramthema's op Mac &amp; MSWin
- Grootte paneelpictogram standaard naar 48

### KIO

- [KNewFileMenu] Menu "Koppeling naar apparaat" verbergen als het leeg zou zijn (bug 381479)
- KIO::rename gebruiken in plaats van KIO::moveAs in setData (bug 380898)
- Positie van dropmenu op Wayland repareren
- KUrlRequester: signaal textEdited sturen naar textChanged() voor teksteigenschap
- [KOpenWithDialog] HTML-escape bestandsnaam
- KCoreDirLister::cachedItemForUrl: maak de cache niet aan als het niet bestond
- "data" gebruiken als bestandsnaam bij kopiëren van data-url's (bug 379093)

### KNewStuff

- Onjuiste foutdetectie voor ontbrekende knsrc-bestanden
- Paginagroottevariable van Engine beschikbaar stellen en gebruiken
- Maak het mogelijk om QXmlStreamReader te gebruiken om een KNS registry-bestand te lezen

### KPackage-framework

- kpackage-genericqml.desktop toegevoegd

### KTextEditor

- Cpu-gebruik piekt na tonen van vi opdrachtbalk (bug 376504)
- Springerig schuifbalk verslepen wanneer mini-map is ingeschakeld
- Spring naar de aangeklikte schuifbalkpositie wanneer minim-map is ingeschakeld (bug 368589)

### KWidgetsAddons

- kcharselect-data bijwerken tot Unicode 10.0

### KXMLGUI

- KKeySequenceWidget: maak het mogelijk om Ctrl+Num+1 op te nemen als een sneltoets (bug 183458)
- "Bij bouwen van hiërarchieën in menu's, maak hun containers ouder hiervan" terugdraaien
- "Transientparent direct gebruiken" terugdraaien

### NetworkManagerQt

- WiredSetting: eigenschappen van wake-on-lan zijn achterwaarts ingebracht in NM 1.0.6
- WiredSetting: eigenschap voor meten zijn achterwaarts ingebracht in NM 1.0.6
- Nieuwe eigenschappen toevoegen aan klassen van instellingen
- Apparaat: apparaatstatistieken toevoegen
- IpTunnel-apparaat toevoegen
- WiredDevice: informatie over vereiste NM versie voor s390SubChannels eigenschap toevoegen
- TeamDevice: nieuwe configuratie eigenschap toevoegen (sinds NM 1.4.0)
- Bedraad apparaat: s390SubChannels eigenschap toevoegen
- Introspections bijwerken (NM 1.8.0)

### Plasma Framework

- Ga na dat grootte heeft eind bereikt na showEvent
- Randen en kleurschema van systeembalkpictogram van vlc repareren
- Containers instellen om focus te hebben binnen de weergave (bug 381124)
- de oude sleutel genereren voor bijwerken van enabledborders (bug 378508)
- wachtwoordknop tonen ook bij lege tekst (bug 378277)
- usedPrefixChanged uitsturen wanneer prefix leeg is

### Solid

- cmake: udisks2 backend op FreeBSD bouwen alleen indien ingeschakeld

### Accentuering van syntaxis

- .julius bestanden als JavaScript accentueren
- Haskell: alle taal-pragma's als sleutelwoorden toevoegen
- CMake: OR/AND niet accentueren na expressie in () (bug 360656)
- Makefile: ongeldige sleutelwoord items in makefile.xml verwijderen
- indexer: foutrapportage verbeteren
- Update van HTM-syntaxis van bestandsversie
- Hoekmodifiërs in HTML attributen toegevoegd
- Test referentiegegevens die de wijzigingen van de vorige commit volgen bijwerken
- Bug 376979 - rechte haakjes in doxygen commentaar brak syntaxis accentuering

### ThreadWeaver

- Om compilerfout MSVC2017 heen werken

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
