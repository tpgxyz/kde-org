---
aliases:
- ../../kde-frameworks-5.66.0
date: 2020-01-11
layout: framework
libCount: 70
---
### Alle frameworks

- Overgezetn van QRegExp naar QRegularExpression
- Overgebracht van qrand naar QRandomGenerator
- Compilatie met Qt 5.15 (bijv. endl is nu Qt::endl, QHash insertMulti vereist nu gebruik van QMultiHash...)

### Attica

- Geen geverifieerde nullptr als een gegevensbron gebruiken
- Meerdere kind-elementen ondersteunen in commentaarelementen
- Een juiste agenttekenreeks instellen voor Attica verzoeken

### Baloo

- Op de juiste manier rapporteren of baloo_file beschikbaar is
- Teruggegeven waarde uit cursor_open controleren
- QML-monitorwaarden initialiseren
- Methoden voor ontleden van URL uit kioslave verplaatsen naar query-object

### BluezQt

- Batterij1-interface toevoegen

### Breeze pictogrammen

- XHTML-pictogram wijzigen om een purper HTML-pictogram te worden
- Hoofdtelefoons en zigzag in het centrum samenvoegen
- Application/x-audacity-project pictogram toevoegen (bug 415722)
- 32px voorkeursysteem toevoegen
- Application/vnd.apple.pkpass pictogram toevoegen (bug 397987)
- pictogram voor ktimetracker die de PNG in de app-repo gebruikt, te vervangen door real breeze SVG
- kipi-pictogram toevoegen, heeft opnieuw doen als een breeze thema svg nodig [of gewoon kipi afbreken]

### Extra CMake-modules

- [android] apk installatiedoel repareren
- PyQt5 gecompileerd met SIP 5 ondersteunen

### Frameworkintegratie

- ColorSchemeFilter uit KStyle verwijderen

### KDE Doxygen hulpmiddelen

- Volledig gekwalificeerde klasse/naamruimte-naam als paginakop tonen (bug 406588)

### KCalendarCore

- README.md verbeteren om een sectie Inleiding te hebben
- Incident geografische coördinaat ook toegankelijk maken als een eigenschap
- RRULE generatie repareren voor tijdzones

### KCMUtils

- KCModuleContainer afkeuren

### KCodecs

- Ongeldige keuze naar enum door het type te wijzigen naar int in plaats van enum

### KCompletion

- KPixmapProvider afkeuren
- [KHistoryComboBox] methode toevoegen om een pictogramleverancier in te stellen

### KConfig

- kconfig EBN transportprotocol opschoning
- getter blootstellen aan configuratie van KConfigWatcher 
- writeFlags met KConfigCompilerSignallingItem repareren
- Een toelichting toevoegen wijzend naar de geschiedenis van knippen en verwijderen die een sneltoets delen

### KConfigWidgets

- "Configure Shortcuts" naar "Configure Keyboard Shortcuts" (bug 39488)

### KContacts

- Opzet van ECM en Qt in lijn brengen met conventies in Frameworks
- Versieafhankelijkheid van ECM specificeren zoals in elk ander framework

### KCoreAddons

- KPluginMetaData::supportsMimeType toevoegen
- [KAutoSaveFile] QUrl::path() gebruiken in plaats van toLocalFile()
- Unbreak gebouwd w/ PROCSTAT: ontbrekende impl. van KProcessList::processInfo toevoegen
- [KProcessList] KProcessList::processInfo optimaliseren (bug 410945)
- [KAutoSaveFile] het commentaar in tempFileName() verbeteren
- KAutoSaveFile gebroken bij lang pad repareren (bug 412519)

### KDeclarative

- [KeySequenceHelper] actueel venster pakken bij ingebed
- Optionele ondertitel toevoegen aan rasterdelegatie
- [QImageItem/QPixmapItem] geen precisie verliezen bij berekeningen

### KFileMetaData

- Gedeeltelijke reparatie voor tekens met accenten in bestandsnaam in Windows
- Niet vereiste private declaraties voor taglibextractor verwijderen
- Gedeeltelijke oplossing voor geaccentueerde tekens op vensters
- xattr: crash repareren op bungelende symbolische koppelingen (bug 414227)

### KIconThemes

- Breeze instellen als standaard thema bij lezen vanuit configuratiebestand
- Top-niveau functie IconSize() afkeuren
- Centrering van geschaalde pictogrammen op hoge dpi pixmaps repareren

### KImageFormats

- pic: ongeldige-enum-waarde ongedefinieerd gedrag repareren

### KIO

- [KFilePlacesModel] ondersteunde controle op schema voor apparaten repareren
- Protocolgegevens inbedden ook voor Windows versie van trash-ioslave
- Ondersteuning toevoegen voor aankoppelen van KIOFuse URL's voor toepassingen die geen KIO gebruiken (bug 398446)
- Ondersteuning voor FileJob toevoegen
- KUrlPixmapProvider afkeuren
- KFileWidget::toolBar afkeuren
- [KUrlNavigator] ondersteuning voor RPM toevoegen aan krarc: (bug 408082)
- KFilePlaceEditDialog: crash repareren bij bewerken van de Trash-plaats (bug 415676)
- Knop toevoegen aan de map in filelight openen om meer details te zien
- Meer details tonen in getoonde waarschuwingsdialoog voor het starten van een beschermde bewerking
- KDirOperator: een vaste regelhoogte voor schuifsnelheid gebruiken
- Additionele velden zoals tijd van verwijderen en originele pad worden nu getoond in de dialoog voor bestandseigenschappen
- KFilePlacesModel: properly parent tagsLister om geheugenlek te vermijden. Geïntroduceerd met D7700
- HTTP ioslave: juiste base-klasse aanroepen in virtual_hook(). De basis van HTTP ioslave is TCPSlaveBase, niet SlaveBase
- Ftp ioslave: tijd met 4 tekens geïnterpreteerd als jaar repareren
- KDirOperator::keyPressEvent opnieuw toevoegen om BC te bewaren
- QStyle gebruiken voor bepalen van pictogramgroottes

### Kirigami

- ActionToolBar: alleen de overflowknop tonen als er zichtbare items in het menu zijn (bug 415412)
- Geen app-sjablonen bouwen en installeren op android
- De marge van de CardsListView niet hard coderen
- Ondersteuning toevoegen voor eigen schermcomponenten aan Actie
- De andere componenten laten groeien als er meer dingen in de header zijn
- Dynamisch aanmaken van item in DefaultListItemBackground verwijderd
- de inklapknop opnieuw introduceren (bug 415074)
- Vensterpictogram van toepassing tonen in AboutPage

### KItemModels

- KColumnHeadersModel toevoegen

### KJS

- Tests voor Math.exp() toegevoegd
- Tests voor verschillende operators voor toekenning toegevoegd
- Speciale gevallen testen van operators voor vermenigvuldigen (*, / en %)

### KNewStuff

- Ga na dat de dialoogtitel juist is met een niet geïnitialiseerde engine
- Het info-pictogram niet tonen op de grote preview-delegate (bug 413436)
- Installeren van archieven ondersteunen met adoptiecommando's (bug 407687)
- De configuratienaam samen met verzoeken verzenden

### KPeople

- enum blootstellen aan de metaobjectcompiler

### KQuickCharts

- Ook de headerbestanden van oprollen corrigeren
- Licentiekoppen voor oprollers corrigeren

### KService

- KServiceTypeProfile afkeuren

### KTextEditor

- Eigenschap "line-count" toevoegen aan de ConfigInterface
- Ongewenst horizontaal schuiven vermijden (bug 415096)

### KWayland

- [plasmashell] documenten voor panelTakesFocus bijwerken om deze generiek te maken
- [plasmashell] signaal toevoegen voor wijzigen van panelTakesFocus

### KXMLGUI

- KActionCollection: bied een signaal changed() als vervanging voor removed()
- Venstertitel van sneltoetsconfiguration aanpassen

### NetworkManagerQt

- Manager: ondersteuning voor AddAndActivateConnection2
- cmake: NM-headers beschouwen als systeem-includes
- Utils::securityIsValid synchroniseren met NetworkManager (bug 415670)

### Plasma Framework

- [ToolTip] Positie afronden
- Wielgebeurtenissen op Slider {} inschakelen
- QWindow-vlag WindowDoesNotAcceptFocus synchroniseren naar wayland plasmashell interface (bug 401172)
- [calendar] Buiten grenzen controleren van toegang tot array in QLocale lookup
- [Plasma Dialog] QXcbWindowFunctions gebruiken voor instelling venstertypes Die Qt WindowFlags niet weet
- [PC3] Plasma voortgangsbalkanimatie aanvullen
- [PC3] Voortgangsbalkindicator alleen tonen wanneer de einden niet zullen overlappen
- [RFC] pictogrammarges van schermconfiguratie repareren (bug 400087)
- [ColorScope] Opnieuw met gewone QObjects werken
- [Breeze Desktop Theme] monochroom pictogram van gebruikers-bureaublad toevoegen
- Standaard breedte van PlasmaComponents3.Button verwijderen
- [PC3 ToolButton] Laat het label rekening houden met complementaire kleurenschema's (bug 414929)
- Achtergrondkleuren toegevoegd aan actieve en inactieve pictogramweergave (bug 370465)

### Omschrijving

- Standaard ECMQMLModules gebruiken

### QQC2StyleBridge

- [ToolTip] Positie afronden
- Tip voor grootte bijwerken wanneer lettertype wijzigt

### Solid

- Eerste / in aangekoppelde beschrijving van toegang tot opslag tonen
- Ga na aangekoppeld nfs-bestandssystemen komen overeen met hun in fstab gedeclareerde tegenhanger (bug 390691)

### Sonnet

- Het signaal gedaan is afgekeurd in het voordeel van spellCheckDone, nu juist uitgestuurd

### Accentuering van syntaxis

- LaTeX: haakjes repareren in sommige commando's (bug 415384)
- TypeScript: "bigint" primitieve type toevoegen
- Python: nummers verbeteren, octalen, binairen en sleutelwoord "breakpoint" toevoegen (bug 414996)
- SELinux: sleutelwoord "glblub" toevoegen en toegangsrechtenlijst bijwerken
- Verschillende verbeteringen aan gitolite syntaxisdefinitie

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
