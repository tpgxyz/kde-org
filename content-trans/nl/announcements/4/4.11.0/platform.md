---
date: 2013-08-14
hidden: true
title: KDE Platform 4.11 levert betere prestaties
---
De functies van KDE Platform 4 zijn bevroren sinds de 4.9 uitgave. Deze versie bevat als gevolg alleen een aantal bugreparaties en verbeteringen van de prestaties.

De Nepomuk semantische opslag- en zoekengine ontving zeer belangrijke verbeteringen van de prestaties, zoals een set van optimalisaties bij lezen die het lezen van gegevens zes keer sneller maakt. Indexering is slimmer geworden en gesplitst in twee stappen. De eerste stap haalt onmiddellijk algemene informatie op (zoals type bestand en naam); extra informatie zoals mediatags, informatie over de auteur, etc. wordt geëxtraheerd in een tweede, iets langzamere, stap. Tonen van metagegevens van nieuw gemaakte of net gedownloade inhoud is nu veel sneller. Bovendien is het systeem van Nepomuk voor het maken van een reservekopie en herstel verbeterd. Nepomuk kan nu ook een variëteit van documentformaten indexeren van documenten, inclusief ODT and docx.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/dolphin.png" caption=`Semantische functies in actie in Dolphin` width="600px">}}

Het geoptimaliseerde format van opslag van Nepomuk en het herschreven programma voor indexering vereist het opnieuw indexeren van enige inhoud van de harde schijf. Het gevolg is dat het opnieuw indexeren een ongewone hoeveelheid van de hulpbronnen van uw computer nodig heeft gedurende een bepaalde periode - afhankelijk van de hoeveelheid die opnieuw geïndexeerd moet worden. Een automatische conversie van de database van Nepomuk zal bij de eerste keer aanmelden uitgevoerd worden.

Er zijn meer kleinere reparaties die <a href='https://projects.kde.org/projects/kde/kdelibs/repository/revisions?rev=KDE%2F4.11'>gevonden kunnen worden in de git-logs</a>.

#### Installeren van het platform voor ontwikkeling van KDE

KDE software, inclusief al zijn bibliotheken en toepassingen, zijn vrij beschikbaar onder Open-Source licenties. KDE software werkt op verschillende hardware configuraties en CPU architecturen zoals ARM- en x86-besturingssystemen en werkt met elk soort windowmanager of bureaubladomgeving. Naast Linux en andere op UNIX gebaseerde besturingssystemen kunt u Microsoft Windows versies van de meeste KDE-toepassingen vinden op de site <a href='http://windows.kde.org'>KDE-software op Windows</a> en Apple Mac OS X versies op de site <a href='http://mac.kde.org/'>KDE software op Mac</a>. Experimentele bouwsels van KDE-toepassingen voor verschillende mobiele platforms zoals MeeGo, MS Windows Mobile en Symbian zijn te vinden op het web maar worden nu niet onderssteund. <a href='http://plasma-active.org'>Plasma Active</a> is een gebruikservaring voor een breder spectrum van apparaten, zoals tabletcomputers en andere mobiele hardware.

KDE software is verkrijgbaar in broncode en verschillende binaire formaten uit<a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> en kan ook verkregen worden op <a href='/download'>CD-ROM</a> of met elk van de <a href='/distributions'>belangrijke GNU/Linux en UNIX systemen</a> van vandaag.

##### Pakketten

Sommige Linux/UNIX OS leveranciers zijn zo vriendelijk binaire pakketten van 4.11.0 voor sommige versies van hun distributie te leveren en in andere gevallen hebben vrijwilligers uit de gemeenschap dat gedaan. <br />

##### Locaties van pakketten

Voor een huidige lijst met beschikbare binaire pakketten waarover het KDE Project is geïnformeerd, bezoekt u de <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Wiki van de gemeenschap</a>.

De complete broncode voor 4.11.0 kan <a href='/info/4/4.11.0'>vrij gedownload worden</a>. Instructies over compileren en installeren van KDE software 4.11.0 zijn beschikbaar vanaf de <a href='/info/4/4.11.0#binary'>4.11.0 informatiepagina</a>.

#### Systeemvereisten

Om het meeste uit deze uitgaven te halen, bevelen we aan om een recente versie van Qt, zoals 4.8.4, te gebruiken. Dit is noodzakelijk om een stabiele ervaring met hoge prestaties te verzekeren omdat sommige verbeteringen, die zijn gemaakt in KDE software, eigenlijk zijn gedaan in het onderliggende Qt-framework.<br /> Om volledige gebruik te maken van de mogelijkheden van de software van KDE, bevelen we ook aan de laatste grafische stuurprogramma's voor uw systeem te gebruiken, omdat dit de gebruikerservaring aanzienlijk verhoogt, beiden in optionele functionaliteit en in algehele prestaties en stabiliteit.

## Eveneens vandaag geannonceerd:

## <a href="./plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Plasma Workspaces 4.11 gaat door met verfijnen van gebruikservaring</a>

Versnellen voor onderhoud op lange termijn, levert Plasma Workspaces verdere verbeteringen aan de basis functionaliteit met een soepelere taakbalk, slimmere widget voor de batterij en verbeterde soundmixer. De introductie van KScreen brengt intelligentere behandeling van meerdere monitoren naar de Werkruimten en verbeteringen van de prestaties op grote schaal, gecombineerd met kleine verbeteringen aan bruikbaarheid, maken dat over het geheel de ervaring plezieriger is.

## <a href="../applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> KDE Applications 4.11 biedt een geweldige stap voorwaarts in Persoonlijk informatiebeheer en overal verbeteringen</a>

Deze uitgave markeert grote verbeteringen in de PIM-stack van KDE, waarmee veel betere prestaties en veel nieuwe functies verkregen worden. Kate verbetert de productiviteit van Python en Javascript ontwikkelaars met nieuwe plug-ins, Dolphin werd sneller en de onderwijstoepassingen brachten verschillende nieuwe functies.
