---
date: 2013-08-14
hidden: true
title: Plasma Workspaces 4.11 gaat door met verfijnen van gebruikservaring
---
{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/empty-desktop.png" caption=`KDE Plasma Workspaces 4.11` width="600px" >}}

In de uitgave 4.11 van Plasma Workspaces is de taakbalk – een van de meest gebruikte Plasma widgets – <a href='http://blogs.kde.org/2013/07/29/kde-plasma-desktop-411s-new-task-manager'>overgebracht naar QtQuick</a>. De nieuwe taakbalk, die het uiterlijk en de functionaliteit van zijn oudere tegenhanger heeft behouden, toont een meer consistente en vloeiend gedrag. Het overbrengen heeft ook een aantal al lang bestaande bugs opgelost. Het widget voor de batterij (die eerder de helderheid van het scherm kon aanpassen) ondersteunt nu ook de helderheid van het toetsenbord en kan overweg met meerdere batterijen in randapparatuur, zoals uw draadloze muis en toetsenbord. Het toont de batterijlading voor elk apparaat en waarschuwt wanneer deze bij een van hen laag is. Het Startmenu toont nu gedurende een paar dagen recent geïnstalleerde toepassingen. Tenslotte, maar niet minder belangrijk, hebben meldingsvensters een instellingenknop waar u gemakkelijk de instellingen voor dat specifieke type melding kan wijzigen.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/notifications.png" caption=`Verbeterde behandeling van meldingen` width="600px" >}}

KMix, de soundmixer van KDE, kreeg een aanzienlijke hoeveelheid werk voor prestaties en stabiliteit evenals <a href='http://kmix5.wordpress.com/2013/07/26/kmix-mission-statement-2013/'>ondersteuning voor volledige besturing van de mediaspeler</a> gebaseerd op de standaard MPRIS2. 

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/battery-applet.png" caption=`De opnieuw ontworpen batterij-applet in actie` width="600px" >}}

## KWin vensterbeheerder en maker van composities

Onze windowmanager, KWin, is opnieuw aanzienlijk bijgewerkt, verder weg van oude technologie en het XCB-communicatieprotocol opgenomen. Dit resulteert in soepeler, sneller windowmanagement. Ondersteuning voor OpenGL 3.1 en OpenGL ES 3.0 is ook geïntroduceerd. Deze uitgave omvat ook de eerste experimentele ondersteuning voor de opvolger van X11, Wayland. Dit geeft u het gebruik van KWin met X11 bovenop een Wayland-stack. Voor meer informatie over hoe deze experimentele modus te gebruiken zie <a href='http://blog.martin-graesslin.com/blog/2013/06/starting-a-full-kde-plasma-session-in-wayland/'>deze blog</a>. Het interface voor scripts in KWin heeft zeer aanzienlijke verbeteringen ondergaan, met UI-ondersteuning voor instellingen, nieuwe animaties en grafisch effecten en vele kleinere verbeteringen. Deze uitgave brengt betere zichtbaarheid van meerdere schermen (inclusief een gloei-optie van randen voor 'hete hoeken'), verbeterd snel achter elkaar zetten van vensters (met instelbare gebieden waarin) en de gebruikelijke reeks van bugraparaties en optimalisaties. Zie <a href='http://blog.martin-graesslin.com/blog/2013/06/what-we-did-in-kwin-4-11/'>hier</a> en <a href='http://blog.martin-graesslin.com/blog/2013/06/new-kwin-scripting-feature-in-4-11/'>hier</a> voor meer details.

## Behandeling van de monitor en websnelkoppelingen

De configuratie van de monitor in Systeeminstellingen is <a href='http://www.afiestas.org/kscreen-1-0-released/'>vervangen door het nieuwe hulpmiddel KScreen</a>. KScreen brengt een intelligentere ondersteuning voor meerdere monitoren naar de Plasma Workspaces, automatische configuratie van nieuwe schermen en herinnering van instellingen voor handmatig ingestelde monitors. Het biedt een intuïtieve, visueelgeoriënteerd interface en behandelt opnieuw arrangeren van monitors door eenvoudig slepen en loslaten.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kscreen.png" caption=`De nieuwe behandeling van de monitor met KScreen` width="600px" >}}

Websneltoetsen, de gemakkelijkste manier om snel te vinden waar u naar zoekt op het web, is opgeschoond en verbeterd. Velen zijn bijgewerkt om veilig versleutelde (TLS/SSL) verbindingen te gebruiken, nieuwe websneltoetsen zijn toegevoegd en een paar verouderde sneltoetsen zijn verwijderd. Het proces van toevoegen van uw eigen websneltoetsen is eveneens verbeterd. Meer details <a href='https://plus.google.com/108470973614497915471/posts/9DUX8C9HXwD'>hier</a>.

Deze uitgave markeert het einde van Plasma Workspaces 1, onderdeel van de KDE SC 4 feature series. Om de overgang naar de volgende generatie gemakkelijker te maken wordt deze uitgave voor minstens twee jaar ondersteund. Focus op feature ontwikkeling zal nu verschuiven naar Plasma Workspaces 2, verbetering van prestaties en reparatie van bugs zal zich concentreren op de 4.11 serie.

#### Plasma installeren

KDE software, inclusief al zijn bibliotheken en toepassingen, zijn vrij beschikbaar onder Open-Source licenties. KDE software werkt op verschillende hardware configuraties en CPU architecturen zoals ARM- en x86-besturingssystemen en werkt met elk soort windowmanager of bureaubladomgeving. Naast Linux en andere op UNIX gebaseerde besturingssystemen kunt u Microsoft Windows versies van de meeste KDE-toepassingen vinden op de site <a href='http://windows.kde.org'>KDE-software op Windows</a> en Apple Mac OS X versies op de site <a href='http://mac.kde.org/'>KDE software op Mac</a>. Experimentele bouwsels van KDE-toepassingen voor verschillende mobiele platforms zoals MeeGo, MS Windows Mobile en Symbian zijn te vinden op het web maar worden nu niet onderssteund. <a href='http://plasma-active.org'>Plasma Active</a> is een gebruikservaring voor een breder spectrum van apparaten, zoals tabletcomputers en andere mobiele hardware.

KDE software is verkrijgbaar in broncode en verschillende binaire formaten uit<a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> en kan ook verkregen worden op <a href='/download'>CD-ROM</a> of met elk van de <a href='/distributions'>belangrijke GNU/Linux en UNIX systemen</a> van vandaag.

##### Pakketten

Sommige Linux/UNIX OS leveranciers zijn zo vriendelijk binaire pakketten van 4.11.0 voor sommige versies van hun distributie te leveren en in andere gevallen hebben vrijwilligers uit de gemeenschap dat gedaan. <br />

##### Locaties van pakketten

Voor een huidige lijst met beschikbare binaire pakketten waarover het KDE Project is geïnformeerd, bezoekt u de <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Wiki van de gemeenschap</a>.

De complete broncode voor 4.11.0 kan <a href='/info/4/4.11.0'>vrij gedownload worden</a>. Instructies over compileren en installeren van KDE software 4.11.0 zijn beschikbaar vanaf de <a href='/info/4/4.11.0#binary'>4.11.0 informatiepagina</a>.

#### Systeemvereisten

Om het meeste uit deze uitgaven te halen, bevelen we aan om een recente versie van Qt, zoals 4.8.4, te gebruiken. Dit is noodzakelijk om een stabiele ervaring met hoge prestaties te verzekeren omdat sommige verbeteringen, die zijn gemaakt in KDE software, eigenlijk zijn gedaan in het onderliggende Qt-framework.<br /> Om volledige gebruik te maken van de mogelijkheden van de software van KDE, bevelen we ook aan de laatste grafische stuurprogramma's voor uw systeem te gebruiken, omdat dit de gebruikerservaring aanzienlijk verhoogt, beiden in optionele functionaliteit en in algehele prestaties en stabiliteit.

## Eveneens vandaag geannonceerd:

## <a href="../applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> KDE Applications 4.11 biedt een geweldige stap voorwaarts in Persoonlijk informatiebeheer en overal verbeteringen</a>

Deze uitgave markeert grote verbeteringen in de PIM-stack van KDE, waarmee veel betere prestaties en veel nieuwe functies verkregen worden. Kate verbetert de productiviteit van Python en Javascript ontwikkelaars met nieuwe plug-ins, Dolphin werd sneller en de onderwijstoepassingen brachten verschillende nieuwe functies.

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> KDE Platform 4.11 levert betere prestaties</a>

Deze uitgave van KDE Platform 4.11 gaat door met focus op stabiliteit. Nieuwe functies zullen geïmplementeerd worden in onze toekomstige uitgave KDE Frameworks 5.0, maar voor de stabiele uitgave is het gelukt om optimalisatie samen te ballen in ons Nepomuk framework.
