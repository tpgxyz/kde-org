---
aliases:
- ../announce-4.11.4
date: 2013-12-03
description: KDE stelt Plasma Workspaces, Applicaties en Platform 4.11.4 beschikbaar.
title: KDE stelt de updates van december voor Plasma Workspaces, Applications en Platform
  beschikbaar
---
3 december 2013. Vandaag heeft KDE updates vrijgegeven voor zijn Workspaces, Applicaties en Development Platform. Deze update is de vierde in een serie van maandelijkse updates voor stabilisatie van de 4.11 serie. Zoals was aangekondigd bij de uitgave, zal workspaces de komende twee jaar doorgaan met het uitbrengen van updates. Deze uitgave bevat alleen bugreparaties en updates van vertalingen en zal een veilige en plezierige update voor iedereen zijn.

Er zijn minstens 65 aangegeven bugreparaties inclusief verbeteringen aan de suite voor beheer van persoonlijke informatie Kontact, het UML-hulpmiddel Umbrello, de windowmanager KWin, de bestandsbeheerder Dolphin en anderen aangebracht. Er zijn vele reparaties voor de stabiliteit.

Een meer complete <a href='https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2013-06-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.11.4&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0='>lijst</a> met wijzigingen is te vinden in de issue-tracker van KDE. Voor een gedetailleerde lijst met wijzigingen die terechtkwamen in 4.11.4, kunt u ook bladeren in de Git-logs.

Om broncode te downloaden of pakketten te installeren ga naar de <a href='/info/4/4.11.4'>Informatiepagina van 4.11.4</a>. Als u meer wilt weten over de 4.11 versies van KDE Workspaces, Applicaties en Development Platform, kijk dan in de <a href='/announcements/4.11/'>Uitgavenotities van 4.11</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`De nieuwe later verzenden werken in Kontact` width="600px">}}

KDE software, inclusief alle bibliotheken en toepassingen, is vrij beschikbaar onder open source licenties. De software van KDE kan verkregen worden als broncode en verschillende binaire formaten uit <a href='http://download.kde.org/stable/4.11.4/'>download.kde.org</a> of vanaf elk van de <a href='/distributions'>belangrijkste GNU/Linux en UNIX systemen</a> van vandaag.
