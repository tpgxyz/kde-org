---
aliases:
- ../announce-applications-17.08.3
changelog: true
date: 2017-11-09
description: KDE stelt KDE Applicaties 17.08.3 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 17.08.3 beschikbaar
version: 17.08.3
---
9 november 2017. Vandaag heeft KDE de derde stabiele update vrijgegeven voor <a href='../17.08.0'>KDE Applicaties 17.08</a> Deze uitgave bevat alleen reparaties van bugs en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan een dozijn aangegeven reparaties van bugs, die verbeteringen bevatten aan Kontact, Ark, Gwenview, KGpg, KWave, Okular, Spectacle, naast andere.

Deze uitgave bevat ook de laatste versie van KDE Development Platform 4.14.38.

Verbeteringen bevatten:

- Workaround een Samba 4.7 regressie met wachtwoord-beschermde SMB shares
- Okular crasht niet langer na bepaalde taken met rotatie
- Ark behoudt bestandsmodificatiedatums bij uitpakken van ZIP-archieven
