---
aliases:
- ../announce-applications-17.12.0
date: 2017-12-14
description: KDE випущено збірку програм KDE 17.12.0
layout: application
title: KDE випущено збірку програм KDE 17.12.0
version: 17.12.0
---
14 грудня 2017 року. Сьогодні KDE випущено збірку програм KDE 17.12.0.

Ми послідовно працюємо над поліпшенням програмного забезпечення, яке є частиною наших збірок програм KDE. Сподіваємося, нові удосконалення та виправлення вад стануть вам у пригоді!

### Нове у програмах KDE 17.12

#### Система

{{<figure src="/announcements/applications/17.12.0/dolphin1712.gif" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, наша програма для керування файлами, у новій версії може зберігати критерії пошуків та обмежувати пошук лише певними теками. Спрощено перейменовування файлів — тепер для цього достатньо подвійного клацання на пункті назви файла. Тепер у вас перед очима буде докладніша інформація щодо файлів: у новій версії дату внесення змін та початкову адресу отриманих з інтернету файлів буде показано на інформаційній панелі програми. Крім того, впроваджено нові стовпчики даних щодо жанру, бітової швидкості та року випуску для мультимедійних файлів.

#### Графіка

Наша потужна програма для перегляду документів, <a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, отримала підтримку роботи із дисплеями високої роздільності та файлами мовою Markdown. Показ документів, які завантажуються повільно удосконалено — тепер він відбувається поступово. Реалізовано можливість надсилання документів електронною поштою безпосередньо з вікна програми.

Нова версія програми для перегляду зображень, <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, може відкривати і підсвічувати зображення на панелі для керування файлами, масштабування тепер відбувається плавніше, поліпшено навігацію за допомогою клавіатури і реалізовано підтримку форматів FITS та Truevision TGA. Також захищено зображення від випадкового вилучення натисканням клавіші Delete, коли жодного зображення не позначено.

#### Звук та відео

Нова версія <a href='https://www.kde.org/applications/multimedia/kdenlive/'>Kdenlive</a> використовує менше пам'яті під час роботи з проєктами, до яких включено багато зображень, типові профілі проміжних кліпів удосконалено, виправлено прикру помилку, пов'язану із переходом на одну секунду вперед під час відтворення у зворотному напрямку.

#### Інструменти

{{<figure src="/announcements/applications/17.12.0/kate1712.png" width="600px" >}}

Поліпшено підтримку формату zip у <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a> за допомогою модуля libzip. У <a href='https://www.kde.org/applications/utilities/kate/'>Kate</a> реалізовано новий <a href='https://frinring.wordpress.com/2017/09/25/ktexteditorpreviewplugin-0-1-0/'>додаток попереднього перегляду</a> за допомогою якого ви зможете бачити у інтерактивному режимі текстовий документ у остаточному форматі з використанням усіх доступних додатків KParts (наприклад, додатків Markdown, SVG, графів Dot, інтерфейсу Qt або латок). Цей додаток також працює у <a href='https://www.kde.org/applications/development/kdevelop/'>KDevelop.</a>

#### Розробка

{{<figure src="/announcements/applications/17.12.0/kuiviewer1712.png" width="600px" >}}

У новій версії <a href='https://www.kde.org/applications/development/kompare/'>Kompare</a> реалізовано контекстне меню у області різниці. Це меню надає змогу пришвидшити доступ до дій з навігації та внесення змін. Якщо ви є розробником, вам може пригодитися новий режим попереднього перегляду об'єктів інтерфейсу користувача у KUIViewer, який використовує опис інтерфейсу з файлів UI Qt (віджетів, вікон тощо) для формування зображення. Також реалізовано підтримку програмного інтерфейсу отримання потокових даних за допомогою KParts.

#### Офіс

Команда розробників <a href='https://www.kde.org/applications/office/kontact'>Kontact</a> виконала значний обсяг робіт із поліпшення і удосконалення комплекту програм. Багато зусиль було вкладено у модернізацію коду, але користувачі також можуть помітити, що поліпшено показ зашифрованих повідомлень, додано підтримку даних text/pgp та <a href='https://phabricator.kde.org/D8395'>Apple® Wallet Pass</a>. Тепер можна скористатися можливістю вибору теки IMAP для повідомлень, які надходитимуть під час відпустки, реалізовано нове попередження у KMail, якщо відкривається повідомлення зі зміненим профілем або способом надходження, реалізовано <a href='https://micreabog.wordpress.com/2017/10/05/akonadi-ews-resource-now-part-of-kde-pim/'>підтримку Microsoft® Exchange™</a>, Nylas Mail та поліпшено імпортування даних з Geary у akonadi-import-wizard, а також виправлено багато вад та впроваджено загальні поліпшення.

#### Ігри

{{<figure src="/announcements/applications/17.12.0/ktuberling1712.jpg" width="600px" >}}

Коло шанувальників нової версії <a href='https://www.kde.org/applications/games/ktuberling/'>KTuberling</a> має розширитися, оскільки програму <a href='https://tsdgeos.blogspot.nl/2017/10/kde-edu-sprint-in-berlin.html'>портовано на Android</a>. Портування <a href='https://www.kde.org/applications/games/kolf/'>Kolf</a>, <a href='https://www.kde.org/applications/games/ksirk/'>KsirK</a> та <a href='https://www.kde.org/applications/games/palapeli/'>Palapeli</a> завершило нашу епопею з портування ігор KDE на Frameworks 5.

### Більше портованих на KDE Frameworks 5 програм

Набір програм, які раніше було засновано на kdelibs4 і які портовано на KDE Frameworks 5, зріс. Тепер його частиною є програма для відтворення музики <a href='https://www.kde.org/applications/multimedia/juk/'>JuK</a>, програма для керування отриманням файлів <a href='https://www.kde.org/applications/internet/kget/'>KGet</a>, мікшер <a href='https://www.kde.org/applications/multimedia/kmix/'>KMix</a>, допоміжні програми <a href='https://www.kde.org/applications/utilities/sweeper/'>Sweeper</a> та <a href='https://www.kde.org/applications/utilities/kmouth/'>KMouth</a>, а також <a href='https://www.kde.org/applications/development/kimagemapeditor/'>KImageMapEditor</a> та допоміжний засіб введення-виведення Zeroconf. Красно дякуємо розробникам, які, тяжко попрацювавши, витратили свій час і зусилля на те, щоб усе це портувати!

### Програми отримали власний розклад випусків

У <a href='https://www.kde.org/applications/education/kstars/'>KStars</a> тепер власний план випусків; докладніше про це у <a href='https://knro.blogspot.de'>блозі розробника</a>. Варто зауважити, що декілька програм, зокрема Kopete і Blogilo <a href='https://community.kde.org/Applications/17.12_Release_Notes#Tarballs_that_we_do_not_ship_anymore'>більше не є частиною</a> стандартного комплекту програм, оскільки їх не було портовано на KDE Frameworks 5 або у них не залишилося активних супровідників.

### Усування вад

Було виправлено понад 110 вад у програмах, зокрема у комплексі програм Kontact Suite, Ark, Dolphin, Gwenview, K3b, Kate, Kdenlive, Konsole, Okular, Umbrello!

### Повний список змін

Якщо хочете прочитати більше про зміни у цьому випуску, <a href='/announcements/changelogs/applications/17.12.0'>зверніться до повного журналу змін</a>. Хоча читання його і може видатися трохи нудним через розмір, це чудовий спосіб дізнатися про внутрішню роботу KDE та відкрити програми і можливості, про які ви могли навіть не здогадуватися.
