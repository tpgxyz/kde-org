---
aliases:
- ../../kde-frameworks-5.51.0
date: 2018-10-15
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Додано виклики KIO::UDSEntry::reserve у допоміжних засобах введення-виведення timeline/tags
- [balooctl] Усунено буферизований рядок «Indexing &lt;файл&gt;» при запуску індексування
- [FileContentIndexer] Сигнал finished з'єднано з процесом видобування даних
- [PositionCodec] Усунено аварійне завершення роботи у випадку пошкодження даних (виправлено ваду 367480)
- Виправлено некоректну символьну сталу
- [Balooctl] Виправлено перевірку батьківського каталогу (виправлено ваду 396535)
- Уможливлено вилучення тек, яких не існує на даний момент, зі списків включення і виключення (виправлено ваду 375370)
- Використано String для зберігання рядкових типів даних UDS_USER і UDS_GROUP (виправлено ваду 398867)
- [tags_kio] Виправлено дужки
- З індексування виключено файли genome

### BluezQt

- Реалізовано програмні інтерфейси Media та MediaEndpoint

### Піктограми Breeze

- Виправлено «stack-use-after-scope», виявлене ASAN у системі інтеграції
- Додано таблиці стилів для монохромних піктограм
- Змінено стиль drive-harddisk на придатніший до адаптації
- Додано піктограми firewall-config і firewall-applet
- Додано замочок на піктограму plasmavault, який є видимим у темі breeze-dark
- Додано символ «плюс» до document-new.svg (виправлено ваду 398850)
- Додано піктограми для збільшення розмірів у два рази

### Додаткові модулі CMake

- Реалізовано збирання прив'язок до python із тими самими прапорцями sip, що використовуються PyQt
- Android: уможливлено передавання відносного шляху як каталогу apk
- Android: реалізовано належне додавання резервного варіанта для програм, які не мають маніфестів
- Android: забезпечено завантаження перекладів Qm
- Виправлено збирання на Android із використанням cmake 3.12.1
- l10n: виправлено відповідні цифри у назві сховища
- Додано QT_NO_NARROWING_CONVERSIONS_IN_CONNECT як типові прапорці збирання
- Прив'язки: належна обробка початкового коду, що містить символи utf-8
- Реалізовано обробку усіх значень CF_GENERATED, а не постійну перевірку значення 0

### KActivities

- Виправлено «зависле» посилання із «auto», яке ставало «QStringBuilder»

### KCMUtils

- Реалізовано керування подіями повернення даних
- Реалізовано зміну розмірів KCMUtilDialog вручну до sizeHint() (виправлено ваду 389585)

### KConfig

- Виправлено ваду при читанні списків шляхів
- kcfg_compiler тепер містить документацію щодо коректних вхідних значень для типу «Color»

### KFileMetaData

- Вилучено власну реалізацію перетворення QString на TString для taglibwriter
- Розширено покриття тестами  taglibwriter
- Реалізовано більш базові мітки для taglibwriter
- Вилучено використання власної функції перетворення TString на QString
- Збільшено номер потрібної для роботи версії taglib до 1.11.1
- Реалізовано читання міток replaygain

### KHolidays

- Додано івуарійські свята (французькою) (виправлено ваду 398161)
- holiday_hk_* — виправлено дату свята Туен-нг у 2019 році (виправлено ваду 398670)

### KI18n

- Реалізовано належне визначення області зміни CMAKE_REQUIRED_LIBRARIES
- Android: забезпечено пошук файлів .mo у належному каталозі

### KIconThemes

- Реалізовано початок малювання емблем із нижнього правого кута

### KImageFormats

- kimg_rgb: оптимізовано QRegExp і QString::fromLocal8Bit
- [EPS] Виправлено аварійне завершення при закритті вікна програми (через намагання зберегти зображення у буфері даних) (виправлено ваду 397040)

### KInit

- Зменшено кількість повідомлень у журналі через перевірки наявності файла із порожньою назвою (виправлено ваду 388611)

### KIO

- Уможливлено нелокальне переспрямовування file:// на адресу WebDav Windows
- [KFilePlacesView] Змінено піктограму для пункту «Змінити» контекстного меню на панелі «Місця»
- [Панель «Місця»] Використано придатнішу піктограму мережі
- [KPropertiesDialog] Реалізовано показ даних щодо монтування для тек у / (корені)
- Виправлено вилучення файлів з DAV (виправлено ваду 355441)
- Усунено QByteArray::remove у AccessManagerReply::readData (виправлено ваду 375765)
- Усунено спроби відновити некоректні записи місць користувача
- Уможливлено зміну каталогу, навіть якщо у адресі є кінцеві символи похилої риски
- Аварійні завершення допоміжних засобів KIO тепер обробляються KCrash, а не власним нетиповим кодом
- Виправлено помилку, пов'язану із тим, що файл, який створено із вставленого вмісту буфера обміну даними, показувався лише із затримкою
- [PreviewJob] Реалізовано надсилання увімкнених додатків мініатюр до допоміжного засобу обробки мініатюр (виправлено ваду 388303)
- Поліпшено повідомлення про помилку «недостатньо місця на диску»
- IKWS: реалізовано використання незастарілого запису «X-KDE-ServiceTypes» при створенні файлів desktop
- Виправлено заголовок призначення WebDAV при діях COPY і MOVE
- Реалізовано попередження користувача до виконання дій з копіювання або пересування, якщо на диску недостатньо місця (виправлено ваду 243160)
- Модуль «Системних параметрів» SMB пересунуто до категорії параметрів мережі
- trash: виправлено обробку кешованих даних щодо розміру каталогів
- kioexecd: реалізовано спостереження за створенням і змінами у тимчасових файлах (виправлено ваду 397742)
- Усунено малювання рамок та тіней навколо зображень із прозорістю (виправлено ваду 258514)
- Виправлено розмиття піктограми типу файлів у діалоговому вікні властивостей файла на екранах із високою роздільністю

### Kirigami

- Реалізовано належне відкриття висувної панелі при перетягуванні за елемент керування
- Додано додаткове поле, якщо globaltoolbar pagerow належить до типу ToolBar
- Реалізовано підтримку Layout.preferredWidth для розмірів аркуша
- Ми позбулися від останніх залишків controls1
- Уможливлено створення дій для роздільника
- Реалізовано довільну кількість стовпчиків у CardsGridview
- Усунено активне руйнування пунктів меню (виправлено ваду 397863)
- Усунено помилку, пов'язану із тим, що піктограми на actionButton є чорно-білими
- Усунено примусову чорно-білу гаму для піктограм там, де її не повинно бути
- Відновлено довільне масштабування *1.5 піктограм на мобільних платформах
- Делегований переробник: усунено подвійний запит щодо контекстного об'єкта
- Використано внутрішню реалізацію брижів на матеріалі
- Реалізовано керування шириною заголовка на основі sourcesize у горизонтальному режимі
- Відкрито усі властивості BannerImage у Cards
- Реалізовано використання DesktopIcon навіть у Плазмі
- Реалізовано належне завантаження шляхів file://
- Скасовано внесок «Реалізовано пошук контексту із самого делегованого об'єкта»
- Додано тест щодо вади із контурами у DelegateRecycler
- Реалізовано явне встановлення висоти overlayDrawers (виправлено ваду 398163)
- Реалізовано пошук контексту із самого делегованого об'єкта

### KItemModels

- Реалізовано використання посилання у циклі for за типами із нетривіальним конструктором копіювання

### KNewStuff

- Додано підтримку міток Attica (виправлено ваду 398412)
- [KMoreTools] Реалізовано належну піктограму для пункту меню «Налаштувати…» (виправлено ваду 398390)
- [KMoreTools] Спрощено ієрархію меню
- Виправлено помилку, пов'язану із неможливістю використання файла knsrc для вивантаження даних із нестандартних місць (виправлено ваду 397958)
- Уможливлено компонування інструментів у Windows
- Виправлено збирання на Qt 5.9
- Додано підтримку міток Attica

### KNotification

- Усунено аварійне завершення роботи через помилки у керуванні часом існування у звукових сповіщеннях на основі canberra (виправлено ваду 398695)

### KNotifyConfig

- Виправлено підказку у файлі інтерфейсу: тепер базовим класом KUrlRequester є QWidget

### Набір бібліотек KPackage

- Реалізовано використання посилання у циклі for за типами із нетривіальним конструктором копіювання
- Qt5::DBus пересунуто до цілей компонування «PRIVATE»
- Реалізовано надсилання сигналів при встановленні і вилученні пакунків

### KPeople

- Виправлено помилку, пов'язану із ненадсиланням сигналів при об'єднанні двох персон
- Усунено аварійне завершення роботи при вилученні персони
- Реалізовано визначення PersonActionsPrivate як класу, як це було оголошено раніше
- Програмний інтерфейс PersonPluginManager зроблено відкритим (public)

### Kross

- Ядро: реалізовано обробку кращих коментарів до дій

### KTextEditor

- Реалізовано малювання позначки згортання коду, лише якщо область згортання коду є багаторядковою
- Ініціалізовано m_lastPosition
- Робота зі скриптами: isCode() повертає false для тексту dsAlert (виправлено ваду 398393)
- Реалізовано використання підсвічування мови R для тестів відступів у R
- Оновлено скрипт встановлення відступів R
- Виправлено схеми кольорів «Вицвіла світла» і «Вицвіла темна» (виправлено ваду 382075)
- Усунено залежність від Qt5::XmlPatterns

### KTextWidgets

- ktextedit: реалізовано «ліниве» завантаження об'єкта QTextToSpeech

### Бібліотека KWallet

- Реалізовано записування до журналу помилок щодо відкриття сховища паролів

### KWayland

- Усунено помилку без повідомлень користувачеві при надсиланні сигналу damage до буферизації (виправлено ваду 397834)
- [сервер] Усунено надто раннє повернення при помилці у резервному коді touchDown
- [сервер] Виправлено обробку віддаленого буфера доступу, якщо виведені дані не обмежено
- [сервер] Усунено можливість створення пропозицій даних без джерела
- [сервер] Реалізовано переривання початку перетягування при правильних умовах без надсилання повідомлення щодо помилки

### KWidgetsAddons

- [KCollapsibleGroupBox] Реалізовано враховування тривалості анімації віджета стилю (виправлено ваду 397103)
- Вилучено застарілу перевірку версії Qt
- Виправлено збирання

### KWindowSystem

- Реалізовано використання _NET_WM_WINDOW_TYPE_COMBO замість _NET_WM_WINDOW_TYPE_COMBOBOX

### KXMLGUI

- Виправлено адресу надавача даних OCS у діалоговому вікні відомостей

### NetworkManagerQt

- Реалізовано використання відповідного значення переліку AuthEapMethodUnknown для порівняння AuthEapMethod

### Бібліотеки Plasma

- Змінено рядки версій теми, оскільки у 5.51 є нові піктограми
- Реалізовано підняття вікна налаштувань, якщо воно використовується повторно
- Додано пропущений компонент: RoundButton
- Поєднано показ файлів піктограм екранної накладки і перехід на тему піктограм Плазми (виправлено ваду 395714)
- [Повзунок із компонентів Плазми 3] Виправлено неявний розмір елемента керування
- [Спадний список із компонентів Плазми 3] Реалізовано перемикання пунктів коліщатком миші
- Реалізовано підтримку піктограм на кнопках, якщо такі є
- Виправлено помилку із неналежним показом назв тижнів у календарі, якщо тиждень починається не з понеділка і не з неділі (виправлено ваду 390330)
- [DialogShadows] Реалізовано використання відступу 0 для вимкнених рамок у Wayland

### Prison

- Виправлено показ кодів Aztec зі співвідношенням ширини і висоти не рівним 1
- Вилучено припущення щодо співвідношення розмірів зображення штрихкоду із інтеграції з QML
- Виправлено неточності у показі, пов'язані із помилками округлення у Code 128
- Додано підтримку штрихкодів Code 128

### Purpose

- Встановлено мінімальну версію cmake 3.0

### QQC2StyleBridge

- Реалізовано невеличкі типові фаски, якщо є тло

### Solid

- Усунено показ емблеми для змонтованих дисків, емблему буде показано лише для незмонтованих дисків
- [Fstab] Вилучено підтримку AIX
- [Fstab] Вилучено підтримку Tru64 (__osf__)
- [Fstab] Реалізовано показ непорожніх назв спільних ресурсів, якщо експортовано кореневу файлову систему (виправлено ваду 395562)
- Реалізовано пріоритет наданої мітки диска і для петльових пристроїв

### Sonnet

- Виправлено помилку у алгоритмі визначення мови
- Реалізовано запобігання вилученню тексту у засобі підсвічування коду (виправлено ваду 398661)

### Підсвічування синтаксису

- i18n: виправлено видобування назв тем
- Fortran: реалізовано підсвічування попереджень у коментарях (виправлено ваду 349014)
- Усунено можливість виштовхування основного контексту
- Реалізовано захист від нескінченного переходу між станами
- YAML: додано стилі блоків literal і folded (виправлено ваду 398314)
- Logcat &amp; SELinux: поліпшення у нових схемах вицвілих кольорів (Solarized)
- AppArmor: усунено аварійні завершення роботи у відкритих правилах (у KF5.50) і поліпшення у нових схемах вицвілих кольорів (Solarized)
- Гілку git://anongit.kde.org/syntax-highlighting об'єднано із основною
- Оновлено дані ігнорування у git
- Реалізовано використання посилання у циклі for за типами із нетривіальним конструктором копіювання
- Виправлення: підсвічування адреси електронної пошти для незакритих дужок у заголовку теми (виправлено ваду 398717)
- Perl: виправлено підсвічування дужок, змінних, посилань на рядки та іншого (виправлено ваду 391577)
- Bash: виправлено розгортання параметрів і дужок (виправлено ваду 387915)
- Додано світлу і темну вицвілі (Solarized) теми

### Відомості щодо безпеки

Випущений код підписано за допомогою GPG з використанням такого ключа: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Відбиток основного ключа: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Обговорити цей випуск та поділитися ідеями можна у розділі коментарів до <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>статті з новиною</a>.
