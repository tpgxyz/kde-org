---
aliases:
- ../../kde-frameworks-5.31.0
date: 2017-02-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Üldine

Many modules now have python bindings.

### Attica

- display_name toetuse lisamine kategooriates

### Breeze'i ikoonid

- liiga palju ikoonimuutusi, et neid siin üles lugeda

### CMake'i lisamoodulid

- Wsuggest-override'i lubamine g++ &gt;= 5.0.0 korral
- fno-operator'i nimede edastamine, kui on toetatud
- ecm_add_app_icon : SVG-failide vaikselt eiramine, kui ei ole toetatud
- Seosed: palju parandusi ja täiustusi

### Raamistike lõimimine

- Mõningate KNSCore'i küsimuste toetamine märguandeid kasutades

### KArchive

- KCompressionDevice (otsimise) parandus töötamaks Qt &gt;= 5.7 peal

### KAuth

- Enamiku näidete uuendamine, vananenute hülgamine

### KConfig

- Linkimise parandus Windowsis: kentrymaptest'i ei lingita KConfigCore'iga

### KConfigWidgets

- ShowMenubarActionFilter::updateAction'is ei tehta midagi, kui menüüribasid ei ole

### KCoreAddons

- Veateate 363427 parandus - ebaturvalisi märke parsiti vääralt URL-i osana (veateade 363427)
- kformat: suhteliste kuupäevade kohase tõlgendamise võimaldamine (veateade 335106)
- KAboutData: dokumenteeriti, et vea e-posti aadress võib olla ka URL

### KDeclarative

- [IconDialog] sobiva ikoonirühma määramine
- [QuickViewSharedEngine] setSize'i kasutamine setWidth/setHeight'i asemel

### KDELibs 4 toetus

- KDE4Defaults.cmake'i sünkroonimine kdelibs'ist
- Cmake'i kontrolli HAVE_TRUNC parandus

### KEmoticons

- KEmoticons: DBus'i kasutamine töötavate protsesside teavitamiseks juhtimismoodulis tehtud muutustest
- KEmoticons: jõudluse oluline täiustamine

### KIconThemes

- KIconEngine: ikooni tsentreerimine nõutavas ristkülikus

### KIO

- KUrlRequester::setMimeTypeFilters'i lisamine
- Konkreetse tp-serveri kataloogide loendi parsimise parandus (veateade 375610)
- grupi/omaniku säilitamine faili kopeerimisel (veateade 103331)
- KRun: runUrl() märkimine iganenuks, eelistades runUrl()-i RunFlags'iga
- kssl: tagamine, et kasutaja sertifikaadikataloog oleks loodud enne kasutamist (veateade 342958)

### KItemViews

- Filtri kohene rakendamine puhverserverile

### KNewStuff

- Ressursside kohandamise võimaldamine peamiselt süsteemiüleste seadistuste jaoks
- Liigutamise korral ajutisse kataloogi paigaldamise ajal välditakse nurjumist
- Klassi security märkimine iganenuks
- Paigaldamisjärgse käsu käivitamist ei blokita (veateade 375287)
- [KNS] Võetakse arvesse distributsiooni tüüpi
- Ei küsita, kas me saame faili /tmp-s

### KNotification

- Märguannete logimise taaslisamine failidele (veateade 363138)
- Mittepüsivate märguannete märkimine lühiajaliseks
- "vaiketoimingute" toetus

### KPackage raamistik

- appdata't ei genereerida, kui on märgitud NoDisplay'ks
- Loendi parandus, kui soovitud asukoht on absoluutne
- kataloogi sisaldavate arhiivide käitlemise parandus (veateade 374782)

### KTextEditor

- minikaardi renderdamise parandus HiDPI keskkonnas

### KWidgetsAddons

- Meetodite lisamine parooli näitamise toimingu peitmiseks
- KToolTipWidget: sisuvidina omanikuõigust ei võeta üle
- KToolTipWidget: peitmine kohe sisu hävitamise järel
- Fookuse tühistamise parandus KCollapsibleGroupBox'is
- Hoiatuse parandus KPixmapSequenceWidget'i hävitamisel
- Ka kaamelkirjas edastamispäiste paigaldamine mitmeklassipäiste klasside korral
- KFontRequester: puuduva fondi lähima vaste leitmine (veateade 286260)

### KWindowSystem

- Tabeldusklahvi muutmise lubamine tõstuklahviga (veateade 368581)

### KXMLGUI

- Veateataja: kohandatud veateates URL-i (mitte ainult e-posti aadressi) lubamine
- Tühjade kiirklahvide vahelejätmine mitmetimõistetavuse kontrollil

### Plasma raamistik

- [Konteineriliides] values() ei ole vajalik, sest contains() otsib võtmeid
- Dialoog: peidetakse, kui fookus läheb ConfigView peale hideOnWindowDeactivate'iga
- [PlasmaComponents'i menüü] omaduse maximumWidth lisamine
- Puuduv ikoon ühendumisel openvpn'iga bluetooth'i võrgu kaudu (veateade 366165)
- Tagamine, et hiirekursori all näidatakse lubatud ListItem'it
- Kõigi kõrguste muutmine kalendripäises võrdseks (veateade 375318)
- värvistiili parandus plasna võrguikoonis (veateade 373172)
- wrapMode'i määramine Text.WrapAnywhere'ile (veateade 375141)
- kalarm'i ikooni uuendamine (veateade 362631)
- oleku korrektne edastamine apletist konteinerisse (veateade 372062)
- KPlugin'i kasutamine kalendripluginate laadimiseks
- esiletõstmise värvi kasutamine valitud tekstis (veateade 374140)
- [Icon Item] suuruse ümardamine, millega me soovime pikselrastrit laadida
- omadus portrait ei oma mingit tähendust, kui tekst puudub (veateade 374815)
- Mitme komponendi omaduse renderType parandus

### Solid

- Omadusest DBus tingitud tõrke hädalahendus (veateade 345871)
- Paroolifraasi puudumist tõlgendatakse Solid::UserCanceled'i tõrkena

### Sonnet

- Kreeka trigrammi andmefaili lisamine
- Segmendivea parandus trigrammi genereerimisel ja konstandi MAXGRAMS avalikustamine päises
- Versioonita libhunspell.so otsimine, mis peaks olema tulevikukindlam

### Süntaksi esiletõstmine

- C++ esiletõstmine: uuendamine Qt 5.8 peale

### Turbeteave

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
