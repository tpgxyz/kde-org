---
aliases:
- ../../kde-frameworks-5.20.0
date: 2016-03-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Breeze'i ikoonid

- Palju uusi ikoone
- Virtualboxi MIME tüübi ikoonide ja veel mõne puuduva MIME tüübi lisamine
- synaptici ja octopi ikooni toetuse lisamine
- Lõikamisikooni parandus (veateade 354061)
- audio-headphones.svg nime parandus (+=d)
- Hinnanguikoonid väiksena piirdega (1px)

### Raamistike lõimimine

- Võimaliku failinime eemaldamine KDEPlatformFileDialog::setDirectory()-s
- Nime järgi ei filtreerita, kui on olemas MIME tüübid

### KActivities

- Qt5::Widgets'i sõltuvuse eemaldamine
- KDBusAddons'i sõltuvuse eemaldamine
- KI18n sõltuvuse eemaldamine
- Kasutamata kaasatute eemaldamine
- Shelliskriptide väljundi täiustamine
- Tegevusi näitava andmemudeli (ActivitiesModel) lisamine teegile
- Vaikimisi ainult teegi ehitamine
- Teenuse ja töötsooni komponentide eemaldamine ehitamisel
- Teegi liigutamine asukohast src/lib/core asukohta src/lib/
- CMake'i hoiatuse parandus
- Krahhi vältimine tegevuste kontekstimenüüs (veateade 351485)

### KAuth

- kded5 ummiku parandus, kui programm kasutab töö lõpetamiseks kauth'i

### KConfig

- KConfigIniBackend: otsingul kuluka lahtihaakimise parandus

### KCoreAddons

- Kdelibs4 seadistuse migreerimise parandus Windowsis
- API lisamine Frameworksi käitusaja versiooniteabe hankimiseks
- KRandom: Ei kasutata 16K /dev/urandom rand() seemneks (veateade 359485)

### KDeclarative

- Nullobjekti viita välja ei kutsuta (veateade 347962)

### KDED

- Võimaldamine kompileerida -DQT_NO_CAST_FROM_ASCII-ga

### KDELibs 4 toetus

- Seansihalduse parandus KApplication'i-põhiste rakenduste korral (veateade 354724)

### KDocTools

- Unicode'i sümbolite kasutamine väljakutsetes

### KFileMetaData

- KRileMetadata võib nüüd pärida ja salvestada teavet algse e-kirja kohta, mille manuses oli salvestatud fail

### KHTML

- Kursori uuendamise parandus vaates
- Stringi mälukasutuse piiramine
- KHTML java apleti näitaja: DBus'i vigase kpasswdserver'i väljakutse parandus

### KI18n

- Porditava importmakro kasutamine nl_msg_cat_cntr korral
- . ja .. uurimine jäetakse rakenduste tõlgete otsimisel vahele
- _nl_msg_cat_cntr kasutuse piiramine GNU gettext'i versioonidega
- KLocalizedString::languages() lisamine
- Gettext'i väljakutsete esitamine ainult siis, kui kataloog on leitud

### KIconThemes

- Tagamine, et muutuja oleks initsialiseeritud

### KInit

- kdeinit: teekide laadimise eelistamine RUNPATH'i alusel
- "Qt5 TODO: use QUrl::fromStringList" teostus

### KIO

- KIO rakenduse-alluva ühenduse katkemise parandus juhtudel, kui appName sisaldab "/" (veateade 357499)
- Nurjumiste korral mitme autentimisviisi proovimine
- help: mimeType() parandus get()-s
- KOpenWithDialog: MIME tüübi nime ja kommentaari näitamine meeldejätmise märkeruudu tekstis (veateade 110146)
- Rida muudatusi kataloogi sisu loendi taaskoostamise vältimiseks failinime muutmise korral (veateade 359596)
- http: m_iError sai nimeks m_kioError
- kio_http: sisu lugemine ja hülgamine 404 järel, kui errorPage=false
- kio_http: MIME tüübi tuvastamise parandus, kui URL-i kõpus seisab '/'
- FavIconRequestJob: juurdepääsumeetodi hostUrl() lisamine, et konqueror suudaks aru saada, milleks oli töö mõeldud
- FavIconRequestJob: töö hangumise parandus, kui seda põhjustas liiga suur lemmikikoon
- FavIconRequestJob: errorString() parandus, sellel oli ainult URL
- KIO::RenameDialog: eelvaatluse toetuse taastamine, kuupäeva- ja suurusepealdiste lisamine (veateade 356278)
- KIO::RenameDialog: topeltkoodi ümberkorraldamine
- QUrl'i teisenduste väära asukoha parandus
- kf5 kio kasutamine kategoorianimes teiste kategooriatega sobitumiseks

### KItemModels

- KLinkItemSelectionModel: uue vaikimisi konstruktori lisamine
- KLinkItemSelectionModel: lingitud valiku mudeli muutmine määratavaks
- KLinkItemSelectionModel: mudeli selectionModel muudatuste käitlemine
- KLinkItemSelectionModel: mudelit ei salvestata kohalikult
- KSelectionProxyModel: kordamisvea parandus
- Vajaduse korral SelectionProxyModel'i oleku lähtestamine
- Omaduse lisamine näitamaks, kas mudelid moodustavad ühendatud ahela
- KModelIndexProxyMapper: ühendatud kontrolli loogika lihtsustamine

### KJS

- Stringi mälukasutuse piiramine

### KNewStuff

- Hoiatuse näitamine, kui mootoris tekib tõrge

### Paketiraamistik

- KDocTools jääb KPackage'is lisavõimaluseks

### KPeople

- Iganenud API kasutuse parandus
- actionType'i lisamine declarative'i pluginale
- Filtreerimisloogika teistpidi pööramine PersonsSortFilterProxyModel'is
- QML-i näite muutmine veidi kasutatavamaks
- actionType'i lisamine PersonActionsModel'ile

### KService

- Koodi lihtsustamine, viitade dereferentsimise vähendamine, konteineriga seotud täiustused
- Testprogrammi kmimeassociations_dumper lisamine innustatult veateatest 359850
- chromiumi/wine rakenduste laadimata jätmise parandus mõne distributsiooni korral (veateade 213972)

### KTextEditor

- Fix highlighting of all occurences in ReadOnlyPart
- Üle QString'i ei itereerita, nagu oleks see QStringList
- Staatilise QMaps'i kohane initsialiseerimine
- toDisplayString(QUrl::PreferLocalFile) eelistamine
- Sisestusmeetodist surrogaatsümboli saatmise toetus
- Krahhi vältimine väljalülitamisel, kui tekstianimatsioon veel käib

### KWalleti raamistik

- KDocTools'i otsimise tagamine
- Negatiivseid arve dbus'ile ei edastata, see tekitab libdbus'is eelduse
- cmake-failide puhastamine
- KWallet::openWallet(Synchronous): 25 sekundi jooksul ei teki ajaületust

### KWindowSystem

- _NET_WM_BYPASS_COMPOSITOR'i toetus (veateade 349910)

### KXMLGUI

- Muu kui enda keele kasutamine varukeelena
- Alates KF5 / Qt5 katkise seansihalduse parandus (veateade 354724)
- Kiirklahviskeemid: globaalselt paigaldatud skeemide toetus
- Qt qHash(QKeySequence) kasutamine ehitamisel Qt 5.6+ peale
- Kiirklahviskeemid: vea parandus, kui kaks sama nimega KXMLGUIClient'i kirjutavad vastastikku teineteise skeemifaili üle
- kxmlguiwindowtest: kiirklahvidialoogi lisamine testimaks kiirklahviskeemide redaktorit
- Kiirklahviskeemid: kasutatavuse täiustamine kasutajaliidese tekste muutes
- Kiirklahviskeemid: skeemiloendi liitkasti täiustamine (automaatne suurus, ei puhastata tundmatu skeemi korral)
- Kiirklahviskeemid: failinime ette ei lisata guiclient'i nime
- Kiirklahviskeemid: kataloogi loomine enne katset salvestada uus kiirklahviskeem
- Kiirklahviskeemid: paigutuse veeriste taastamine, muidu näeb dialoog välja väga ülekoormatuna
- Mälulekke parandus KXmlGui käivitushaagis

### Plasma raamistik

- IconItem: allikat ei kirjuta QIcon::name() kasutamisel üle
- ContainmentInterface: QRect right() ja bottom() kasutuse parandus
- QPixmaps'i käitlemisel topeltkoodi asukoha tõhus eemaldamine
- IconItem'i API dokumentatsiooni lisamine
- Laaditabeli parandus (veateade 359345)
- Aknamaski ei pühita minema geomeetria iga muutumise peale, kui komposiit on aktiivne ja maski pole määratud
- Aplett: krahhi vältimine paneeli eemaldamisel (veateade 345723)
- Teema: pixmap'i puhvri hülgamine teema muutmisel (veateade 359924)
- IconItemTest: vahelejätmine, kui grabToImage nurjub
- IconItem: ikooniteemast laaditud svg-ikoonide värvi muutmise parandus
- svg iconPath'i lahendamise parandus IconItem'is
- If path is passed, pick the tail (bug 359902)
- Omaduste configurationRequired ja reason lisamine
- contextualActionsAboutToShow liigutamine apletti
- ScrollViewStyle: plinkivatel elementidel ei kasutata piirdeid
- DataContainer: pesa kontrollimise parandus enne ühendamist/lahutamist
- ToolTip: mitme geomeetria muutuse vältimine sisu muutumisel
- SvgItem: renderdamislõime Plasma::Theme'i ei kasutata
- AppletQuickItem: omaenda lisatud paigutuse leidmise parandus (veateade 358849)
- Väiksem ekspander tegumiribal
- ToolTip: taimeri näitamise peatamine hideTooltip'i väljakutsumise peale (veateade 358894)
- Ikoonide animeerimise keelamine plasma kohtspikrites
- Kohtspikrite animatsioonidest loobumine
- Vaiketeema järgib värviskeemi
- Parandus: IconItem ei laadinud nimega teemaväliseid ikoone (veateade 359388)
- Muude konteinerite kui töölaud eelistamine containmentAt()-s
- WindowThumbnail: glx pixmap'i hülgamine stopRedirecting()-s (veateade 357895)
- Pärandapletifiltri eemaldamine
- ToolButtonStyle: ei toetuta välisele ID-le
- corona leidmist ei eeldata (veateade 359026)
- Kalender: sobivate edasi-tagasinuppude ja "Täna" nupu lisamine (veateated 336124, 348362, 358536)

### Sonnet

- Isegi kui keel on määratud, ei keelata keele tuvastamist
- Automaatse õigekirja kontrollimise vaikimisi automaatse keelamise keelamine
- TextBreaks'i parandus
- Hunspelli sõnastiku otsingutee puuduva '/' parandus (veateade 359866)
- Sõnastiku otsinguteele &lt;rakenduse kataloog&gt;/../share/hunspell lisamine

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
