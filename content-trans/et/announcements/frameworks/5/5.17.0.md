---
aliases:
- ../../kde-frameworks-5.17.0
date: 2015-12-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- timeline:// kasutatava kuupäevafiltri parandus
- BalooCtl: tagastamine käskude järel
- Baloo::Database::open() puhastamine ja kindlustamine, rohkemate krahhitingimuste käitlemine
- Kontrolli lisamine, et Database::open(OpenDatabase) lõpetaks nurjumisega, kui andmebaasi ei ole olemas

### Breeze'i ikoonid

- Paljude ikoonide lisamine või täiustamine
- laaditabelite kasutamine Breeze'i ikoonides (veateade 126166)
- VEATEADE 355902: süsteemi lukustusekraani parandus ja muutmine
- 24px dialoogiteabe lisamine GTK rakendustele (veateade 355204)

### CMake'i lisamoodulid

- Hoiatust ei anta, kui SVG(Z) ikoone pakutakse mitmes suuruses/detailsustasemes
- Kindlustamine, et tõlked laaditakse põhilõimes (veateade 346188).
- ECM ehitussüsteemi ülevaatamine.
- Clazy lubamise võimaldamine kõigis KDE projektides
- XCB teeki XINPUT vaikimisi ei otsita.
- Ekspordikataloogi puhastamine enne APK taasgenereerimist
- quickgit'i kasutamine Giti hoidla URL-is

### Raamistike lõimimine

- Plasmoidi paigaldamise nurjumise lisamine  faili plasma_workspace.notifyrc

### KActivities

- Lukustuse parandus deemoni esmakäivitamisel
- QAction'i loomise liigutamine põhikõime (veateade 351485).
- clang-format langetab mõnikord halva otsuse (veateade 355495)
- Võimalike sünkroonimisprobleemide kõrvaldamine
- org.qtproject'i kasutamine com.trolltech'i asemel
- libkactivities'i kasutuse eemaldamine pluginatest
- KAStats'i seadistuse eemaldamine API-st
- linkimise ja linkimise katkestamise lisamine ResultModel'isse

### KDE Doxygeni tööriistad

- kgenframeworksapidox'i muutmine töökindlamaks.

### KArchive

- KCompressionDevice::seek() parandus, kui see kutsutatakse välja KTar'i loomisel KCompressionDevice'i peal.

### KCoreAddons

- KAboutData: https:// ja teiste URL-skeemide lubamine koduleheküljel (veateade 355508).
- Omaduse MimeType parandamine kcoreaddons_desktop_to_json() kasutamisel

### KDeclarative

- KDeclarative'i portimine kasutama otse KI18n
- DragArea delegateImage võib nüüd olla string, mille põhjal ikoon automaatselt luuakse
- Uue teegi CalendarEvents lisamine

### KDED

- Keskkonnamuutuja SESSION_MANAGER määramise tühistamine tühjaks määramise asemel

### KDELibs 4 toetus

- Mõne i18n väljakutse parandus.

### KFileMetaData

- m4a märkimine taglib'is loetavaks

### KIO

- Küpsistedialoog: kavandatult tööle panemine
- Failinime pakkumise muutumise millekski juhuslikuks parandus salvestamisel MIME tüübi muutmise korral
- kioexec'i DBus-nime registreerimine (veateade 353037)
- KProtocolManager'i uuendamine seadistuse muutmise järel.

### KItemModels

- KSelectionProxyModel'i kasutuse parandus QTableView's (veateade 352369)
- KRecursiveFilterProxyModel'i lähtemudeli lähtestamise või muutmise parandus.

### KNewStuff

- registerServicesByGroupingNames võib vaikimisi defineerida rohkem elemente
- KMoreToolsMenuFactory::createMenuFromGroupingNames muutmine laisaks

### KTextEditor

- TaskJuggleri and PL/I süntaksi esiletõstmise lisamine
- Võtmesõna lõpetuse keelamise võimaldamine seadistusliideses.
- Puu suuruse muutmine lõpetusmudeli lähtestamisel.

### KWalleti raamistik

- Selliste juhtumite korrektne käitlemine, mil kasutaja meid deaktiveeris

### KWidgetsAddons

- KRatingWidget'i väikese artefakti parandus hi-dpi korral.
- Omaduse ümberkorraldamine ja parandamine, mille tekitas veateate 171343 parandamine

### KXMLGUI

- QCoreApplication::setQuitLockEnabled(true) ei kutsuta intsialiseerimisel välja.

### Plasma raamistik

- Lihtplasmoidi lisamine näidisena developerguide'i
- Mitme plasmoidimalli lisamine kapptemplate'i/kdveloppi
- [calendar] mudeli lähtestamisega viivitamine, kuni vaade on valmis (veateade 355943)
- Peitmise ajal asendit ei muudeta (veateade 354352)
- [IconItem] Krahhi vältimine on null KIconLoader teema korral (veateade 355577)
- Pildifailide lohistamisel paneelile ei pakuta enam nende määramist paneeli taustapildiks
- Plasmoidifaili lohistamisel paneelile või töölauale see paigaldatakse ja lisatakse
- Nüüdseks kasutuseta kded platformstatus'e mooduli eemaldamine (veateade 348840)
- parooliväljale asetamise lubamine
- redigeerimismenüü asukoha parandus, nupu lisamine valimiseks
- [calendar] kasutajaliidese keele kasutamine kuunime hankimisel (veateade 353715)
- [calendar] sündmuste sortimine ka tüübi järgi
- [calendar] plugina teegi liigutamine KDeclarative'i
- [calendar] qmlRegisterUncreatableType vajab veidi rohkem argumente
- Seadistuskategooriate dünaamilise lisamise lubamine
- [calendar] pluginate käitlemise liigutamine eraldi klassi
- Pluginatel lubatakse lisada sündmuste andmeid kalendriapletti (veateade 349676)
- pesa olemasolu kontrollimine enne ühendamist või ühenduse katkestamist (veateade 354751)
- [plasmaquick] OpenGL'i ei lingita otseselt
- [plasmaquick] XCB::COMPOSITE ja DAMAGE sõltuvustest loobumine

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
