---
aliases:
- ../announce-applications-16.04.3
changelog: true
date: 2016-07-12
description: KDE toob välja KDE rakendused 16.04.3
layout: application
title: KDE toob välja KDE rakendused 16.04.3
version: 16.04.3
---
July 12, 2016. Today KDE released the third stability update for <a href='../16.04.0'>KDE Applications 16.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Rohkem kui 20 teadaoleva veaparanduse hulka kuuluvad Arki, Cantori, Kate, Kdepimi, Umbrello ja teiste rakenduste täiustused.

This release also includes Long Term Support version of KDE Development Platform 4.14.22.
