---
aliases:
- ../announce-applications-16.04-beta
custom_spread_install: true
date: 2016-03-24
description: KDE Ships Applications 16.04 Beta.
layout: application
release: applications-16.03.80
title: KDE toob välja rakenduste 16.04 beetaväljalaske
---
24. märts 2015 KDE laskis täna välja rakenduste uute versioonide beetaväljalaske. Kuna sõltuvused ja omadused on külmutatud, on KDE meeskonnad keskendunud vigade parandamisele ja tarkvara viimistlemisele.

Check the <a href='https://community.kde.org/Applications/16.04_Release_Notes'>community release notes</a> for information on new tarballs, tarballs that are now KF5 based and known issues. A more complete announcement will be available for the final release

Et paljud rakendused on juba viidud KDE Frameworks 5 peale, vajavad KDE rakendused 16.04 põhjalikku testimist kvaliteedi ja kasutajakogemuse tagamiseks ja parandamiseks. Kasutajatel on tihtipeale õigus suhtuda kriitiliselt KDE taotlusse hoida kõrget kvaliteeti, sest arendajad pole lihtsalt võimelised järele proovima kõiki võimalikke kombinatsioone. Me loodame oma kasutajate peale, kes oleksid suutelised varakult vigu üles leidma, et me võiksime need enne lõplikku väljalaset ära parandada. Niisiis - palun kaaluge mõtet ühineda meeskonnaga beetat paigaldades <a href='https://bugs.kde.org/'>ja kõigist ette tulevatest vigadest teada andes</a>.

#### KDE rakenduste 16.04 beeta binaarpakettide paigaldamine

<em>Packages</em>. Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications 16.04 Beta (internally 16.03.80) for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.

<em>Package Locations</em>. For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages'>Community Wiki</a>.

#### KDE rakenduste 16.04 beeta kompileerimine

The complete source code for KDE Applications 16.04 Beta may be <a href='http://download.kde.org/unstable/applications/16.03.80/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications/applications-16.03.80'>KDE Applications Beta Info Page</a>.
