---
aliases:
- ../announce-applications-16.12.3
changelog: true
date: 2017-03-09
description: KDE toob välja KDE rakendused 16.12.3
layout: application
title: KDE toob välja KDE rakendused 16.12.3
version: 16.12.3
---
March 9, 2017. Today KDE released the third stability update for <a href='../16.12.0'>KDE Applications 16.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Rohkem kui 20 teadaoleva veaparanduse hulka kuuluvad Kdepimi, Arki, Filelighti, Gwenview, Kate, Kdenlive'i, Okulari ja teiste rakenduste täiustused.

This release also includes Long Term Support version of KDE Development Platform 4.14.30.
