---
aliases:
- ../announce-applications-19.04.3
changelog: true
date: 2019-07-11
description: KDE Ships Applications 19.04.3.
layout: applications
major_version: '19.04'
release: applications-19.04.3
title: KDE toob välja KDE rakendused 19.04.3
version: 19.04.3
---
{{% i18n_date %}}

Today KDE released the third stability update for <a href='../19.04.0'>KDE Applications 19.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Üle 60 teadaoleva veaparanduse hulka kuuluvad muu hulgas Kontacti, Arki, Cantori, JuKi, K3b, Kdenlive'i, KTouchi, Okulari ja Umbrello täiustused.

Täiustused sisaldavad muu hulgas:

- Konquerori ja Kontacti ei taba enam krahh rakendusest väljumisel, kui kasutusel on QtWebEngine 5.13
- Kompositsioonidega rühmade lõikamine ei tekita videoredaktoris Kdenlive enam krahhe
- Umbrello Pythoni importija suudab nüüd käidelda vaikimisi argumentidega parameetreid
