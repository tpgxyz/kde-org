---
date: 2013-08-14
hidden: true
title: Платформа KDE 4.11 обеспечивает лучшую производительность
---
Платформа KDE 4 находится в стадии замороженной функциональности с версии 4.9 и данный выпуск, сохраняя эту установку, включает только исправления ошибок и оптимизацию быстродействия.

The Nepomuk semantic storage and search engine received massive performance improvements, such as a set of read optimizations that make reading data up to six times faster. Indexing has become smarter, being split in two stages. The first stage retrieves general information (such as file type and name) immediately; additional information like media tags, author information, etc. is extracted in a second, somewhat slower stage. Metadata display on newly-created or freshly-downloaded content is now much faster. In addition, the Nepomuk developers improved the backup and restore system. Last but not least, Nepomuk can now also index a variety of document formats including ODF and docx.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/dolphin.png" caption=`Семантические функции в Dolphin` width="600px">}}

Оптимизированный формат хранения данных Nepomuk и переписанный индексатор почтовых сообщений требуют переиндексации некоторого содержимого на диске. Это может привести к увеличению загрузки системы на некоторое время, в зависимости от объёма данных, требующих индексации. Автоматическое преобразование данных Nepomuk будет выполнено при первом входе пользователя в систему.

Имели место и исправления небольших ошибок, о которых можно узнать <a href='https://projects.kde.org/projects/kde/kdelibs/repository/revisions?rev=KDE%2F4.11'>в истории git</a>.

#### Установка платформы разработки KDE

Программное обеспечение KDE, включая все приложения и библиотеки, распространяется бесплатно по лицензиям Open Source. Программные продукты KDE работают на разных аппаратных платформах и процессорах, включая ARM и x86, в разных операционных системах и окружениях рабочего стола с любыми оконными менеджерами. Помимо пакетов для Linux и других операционных систем на базе UNIX, вы можете найти версии KDE-приложений для Microsoft Windows на сайте <a href='http://windows.kde.org'>KDE на Windows</a> и версии для Apple Mac OS X на сайте <a href='http://mac.kde.org/'>KDE на Mac</a>. Кроме этого, в Интернете можно найти экспериментальные версии приложений KDE для различных мобильных платформ, включая MeeGo, MS Windows Mobile и Symbian, но в данный момент они не поддерживаются. <a href='http://plasma-active.org'>Plasma Active</a> — новая платформа, разрабатываемая для широкого спектра мобильных устройств, таких как планшетные компьютеры и телефоны.

KDE software can be obtained in source and various binary formats from <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> and can also be obtained on <a href='/download'>CD-ROM</a> or with any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.

##### Пакеты

Some Linux/UNIX OS vendors have kindly provided binary packages of 4.11.0 for some versions of their distribution, and in other cases community volunteers have done so. <br />

##### Местонахождение пакетов

For a current list of available binary packages of which the KDE's Release Team has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Community Wiki</a>.

The complete source code for 4.11.0 may be <a href='/info/4/4.11.0'>freely downloaded</a>. Instructions on compiling and installing KDE software 4.11.0 are available from the <a href='/info/4/4.11.0#binary'>4.11.0 Info Page</a>.

#### Системные требования

Для того, чтобы наши пакеты работали в полном объёме, мы рекомендуем использовать одну из последних версий Qt, например 4.8.4. Только при таком условии можно гарантировать определённый уровень стабильности и производительности, так как множество улучшений, проявляющихся в KDE, на самом деле происходят в нижележащей платформе Qt.<br />Чтобы вы могли максимально использовать возможности KDE, мы также рекомендуем установить последние графические драйверы для вашей системы, так как это может значительно улучшить работу всей системы, в отношении как функциональности, так и быстродействия и надёжности.

## Другие новости:

## <a href="../plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Plasma Workspaces 4.11 Continues to Refine User Experience</a>

Ориентируясь на длительный срок сопровождения версии, Plasma Workspaces приносит улучшения в базовой функциональности, включая оптимизированную панель задач, более универсальный индикатор батареи и улучшенный регулятор громкости. А с KScreen среда Plasma обретает умное управление несколькими мониторами, значительные улучшения производительности и приятные мелочи, в целом делающие среду ещё удобнее.

## <a href="../applications"><img src="/announcements/4/11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/>KDE Applications 4.11 делают большой шаг вперёд в области управления персональной информацией и получают развитие в целом</a>

Этот выпуск отличается значительными улучшениями в пакете KDE PIM, принёсшими оптимизацию производительности и много новых возможностей. Новые модули Kate облегчают программирование на языках Python и Javascript, Dolphin становится быстрее, а образовательные программы обретают новые функции.
