---
aliases:
- ../../kde-frameworks-5.10.0
date: '2015-05-08'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### KActivities

- (нет списка изменений)

### Модуль KConfig

- Создание классов с поддержкой QML при помощи kconfigcompiler.

### Модуль KCoreAddons

- Новый макрос CMake kcoreaddons_add_plugin для упрощения создания модулей на основе KPluginLoader.

### Модуль KDeclarative

- Исправлена ошибка в кэше текстур, приводившая к аварийному завершению программ
- и другие исправления.

### KGlobalAccel

- Add new method globalShortcut which retrieves the shortcut as defined in global settings.

### KIdleTime

- Prevent kidletime from crashing on platform wayland

### Подсистема ввода-вывода KIO

- Added KPropertiesDialog::KPropertiesDialog(urls) and KPropertiesDialog::showDialog(urls).
- Asynchronous QIODevice-based data fetch for KIO::storedPut and KIO::AccessManager::put.
- Fix conditions with QFile::rename return value (bug 343329)
- Fixed KIO::suggestName to suggest better names (bug 341773)
- kioexec: Fixed path for writeable location for kurl (bug 343329)
- Store bookmarks only in user-places.xbel (bug 345174)
- Duplicate RecentDocuments entry if two different files have the same name
- Better error message if a single file is too large for the trash (bug 332692)
- Fix KDirLister crash upon redirection when the slot calls openURL

### Модуль получения дополнительных материалов KNewStuff

- New set of classes, called KMoreTools and related. KMoreTools helps to add hints about external tools which are potentially not yet installed. Furthermore, it makes long menus shorter by providing a main and more section which is also user-configurable.

### Модуль KNotifications

- Fix KNotifications when used with Ubuntu's NotifyOSD (bug 345973)
- Don't trigger notification updates when setting the same properties (bug 345973)
- Introduce LoopSound flag allowing notifications to play sound in a loop if they need it (bug 346148)
- Don't crash if notification doesn't have a widget

### Модуль KPackage

- Добавлен метод KPackage::findPackages, похожий на KPluginLoader::findPlugins

### Библиотека для работы с контактами KPeople

- Use KPluginFactory for instantiating the plugins, instead of KService (kept for compatibility).

### Модуль KService

- Fix wrong splitting of entry path (bug 344614)

### KWallet

- Migration agent now also check old wallet is empty before starting (bug 346498)

### Модуль KWidgetsAddons

- KDateTimeEdit: Fix so user input actually gets registered. Fix double margins.
- KFontRequester: fix selecting monospaced fonts only

### Модуль KWindowSystem

- Из KXUtils::createPixmapFromHandle убрана зависимость от QX11Info (ошибка № 346496)
- Добавлен новый метод NETWinInfo::xcbConnection() -&gt; xcb_connection_t*

### Модуль KXmlGui

- Исправлена работа комбинаций клавиш, когда установлена дополнительная комбинация клавиш (ошибка № 345411).
- Обновлён список продуктов и компонентов в системе отслеживания ошибок Bugzilla (ошибка № 346559).
- Global shortcuts: allow configuring also the alternate shortcut

### Модуль NetworkManagerQt

- Структура устанавливаемых заголовочных файлов приведена к такому же виду, как у остальных библиотек KDE Frameworks.

### Модуль Plasma framework

- PlasmaComponents.Menu now supports sections
- Use KPluginLoader instead of ksycoca for loading C++ dataengines
- Consider visualParent rotation in popupPosition (bug 345787)

### Sonnet

- Don't try to highlight if there is no spell checker found. This would lead to an infinite loop with rehighlighRequest timer firing constanty.

### FrameworkIntegration

- Fix native file dialogs from widgets QFileDialog: ** File dialogs opened with exec() and without parent were opened, but any user-interaction was blocked in a way that no file could be selected nor the dialog closed. ** File dialogs opened with open() or show() with parent were not opened at all.

Вы можете обсудить эту новость на сайте новостей KDE <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>Dot</a>
