---
aliases:
- ../announce-applications-18.08.2
changelog: true
date: 2018-10-11
description: KDE выпускает KDE Applications 18.08.2
layout: application
title: KDE выпускает KDE Applications 18.08.2
version: 18.08.2
---
October 11, 2018. Today KDE released the second stability update for <a href='../18.08.0'>KDE Applications 18.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Зафиксировано более 10 исправлений ошибок в электронном секретаре Kontact, диспетчере файлов Dolphin, приложении для просмотра изображений Gwenview, калькуляторе KCalc, приложении для UML-моделирования Umbrello и других приложениях.

Некоторые из улучшений:

- Dragging a file in Dolphin can no longer accidentally trigger inline renaming
- KCalc again allows both 'dot' and 'comma' keys when entering decimals
- A visual glitch in the Paris card deck for KDE's card games has been fixed
