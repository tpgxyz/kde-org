---
aliases:
- ../announce-applications-15.08.1
changelog: true
date: 2015-09-15
description: KDE выпускает KDE Applications 15.08.1
layout: application
title: KDE выпускает KDE Applications 15.08.1
version: 15.08.1
---
15 сентября 2015 года. Сегодня KDE выпустило первое стабилизирующее обновление для <a href=''../15.08.0'>KDE Applications 15.08</a>. Этот выпуск содержит только исправления ошибок и обновления переводов, установить его будет приятно и безопасно для всех.

More than 40 recorded bugfixes include improvements to kdelibs, kdepim, kdenlive, dolphin, marble, kompare, konsole, ark and umbrello.

This release also includes Long Term Support version of KDE Development Platform 4.14.12.
