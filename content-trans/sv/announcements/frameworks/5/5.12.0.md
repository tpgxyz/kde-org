---
aliases:
- ../../kde-frameworks-5.12.0
date: 2015-07-10
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Extra CMake-moduler

- Förbättra felrapportering för makrot query_qmake

### BluezQt

- Ta bort alla enheter från anslutningen innan den tas bort (fel 349363)
- Uppdatera länkar i README.md

### KActivities

- Lägg till alternativet att inte spåra användaren i specifika aktiviteter (likt 'privat webbläsning' i en webbläsare)

### KArchive

- Bevara körrättigheter för filer i copyTo()
- Klargör ~KArchive genom att ta bort död kod.

### KAuth

- Gör det möjligt att använda kauth-policy-gen från olika källor

### KBookmarks

- Lägg inte till ett bokmärke när webbadressen är tom och texten är tom
- Koda KBookmark-webbadress för att rätta kompatibilitet med KDE4-program

### KCodecs

- Ta bort sondering av x-euc-tw

### KConfig

- Installera kconfig_compiler i libexec
- Nytt alternativ TranslationDomain= för generering av kod, för användning med TranslationSystem=kde, behövs normalt i bibliotek.
- Gör det möjligt att använda kconfig_compiler från olika källor

### KCoreAddons

- KDirWatch: Upprätta bara en anslutning till FAM om det begärs
- Tillåt filtrering av insticksprogram och program enligt formfaktor
- Gör det möjligt att använda desktoptojson från olika källor

### KDBusAddons

- Klargör slutvärde för unika instanser

### KDeclarative

- Lägg till QQC-klon av KColorButton
- Tilldela ett QmlObject för varje instans av kdeclarative om möjligt
- Gör så att Qt.quit() fungerar från QML-kod
- Sammanfoga grenen 'mart/singleQmlEngineExperiment'
- Implementera sizeHint baserad på implicitWidth/height
- Delklass av QmlObject med statiskt gränssnitt

### Stöd för KDELibs 4

- Rätta implementering av KMimeType::Ptr::isNull
- Aktivera stöd för att strömma KDateTime till kDebug/qDebug igen, för mer SC
- Läs in rätt översättningskatalog för kdebugdialog
- Hoppa inte över dokumentation av metoder vars användning avråds från, så att det går att läsa konverteringstips

### KDESU

- Rätta CMakeLists.txt så att KDESU_USE_SUDO_DEFAULT skickas till kompileringen för användning av suprocess.cpp

### KDocTools

- Uppdatera dokumentmallar för K5

### KGlobalAccel

- Installera privat programmeringsgränssnitt för körning så att Kwin kan tillhandahålla ett insticksprogram för Wayland.
- Reserv för namnupplösning med componentFriendlyForAction

### KIconThemes

- Försök inte rita upp ikonen om storleken är ogiltig

### KItemModels

- Ny proxy-modell: KRearrangeColumnsProxyModel. Den stöder att ordna om och dölja kolumner från källmodellen.

### KNotification

- Rätta punktavbildningstyper i org.kde.StatusNotifierItem.xml
- [ksni] Lägg till metod för att hämta åtgärd enligt namn (fel 349513)

### KPeople

- Implementera möjlighet att filtrera PersonsModel

### KPlotting

- KPlotWidget: lägg till setAutoDeletePlotObjects, rätta minnesläcka i replacePlotObject
- Rätta saknade graderingar när x0 &gt; 0.
- KPlotWidget: ingen anledning att ställa in setMinimumSize eller ändra storlek.

### KTextEditor

- debianchangelog.xml: lägg till Debian/Stretch, Debian/Buster, Ubuntu-Wily
- Rätta beteende för UTF-16 surrogatpar backsteg eller borttagning.
- Låt QScrollBar hantera mushjul-händelser (fel 340936)
- Inför programfix från KWrite utveckling topp uppdatera ren grundläggande HL, "Alexander Clay" &lt;tuireann@EpicBasic.org&gt;

### KTextWidgets

- Rätta aktivering och inaktivering av knappen OK

### Ramverket KWallet

- Importera och förbättra kommandoradsverktyg kwallet-query.
- Stöd för överskrivning av avbildningsposter.

### KXMLGUI

- Visa inte "KDE:s ramverksversion" i dialogrutan Om KDE

### Plasma ramverk

- Gör det mörka temat fullständigt mörkt, också den komplementära gruppen
- Lagra naturlig storlek åtskild av skalfaktor lokalt
- ContainmentView: Krascha inte vid ogiltig metadata för corona
- AppletQuickItem: Försök inte komma åt KPluginInfo om ogiltig
- Rätta enstaka tomma inställningssidor för miniprogram (fel 349250)
- Förbättra stöd för hidpi i rutnätskomponenten Calendar
- Kontrollera att KService har giltig information om insticksprogram innan den används
- [calendar] Säkerställ att rutmönstret ritas om vid temaändringar
- [calendar] Börja alltid räkna veckor från måndag (fel 349044)
- [calendar] Rita om rutmönstret när inställningen att visa veckonummer ändras
- Ett ogenomskinligt tema används nu när bara suddighetseffekt är tillgänglig (fel 348154)
- Vitlista miniprogram och version för separat gränssnitt
- Introducera en ny klass ContainmentView

### Sonnet

- Tillåt användning av färglagd stavningskontroll i QPainTextEdit

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
