---
aliases:
- ../../kde-frameworks-5.69.0
date: 2020-04-05
layout: framework
libCount: 70
---
### Baloo

- [SearchStore] Använd kategoriserad loggning
- [QueryParser] Rätta felaktig detektering av avslutande citationstecken
- [EngineQuery] Tillhandahåll överlagring av toString(Term) för QTest
- [EngineQuery] Ta bort oanvänd positionsmedlem, utöka tester
- [SearchStore] Undvik långa rader och funktionsnästling
- [baloosearch] Hoppa ut tidigt om angiven katalog inte är giltig
- [MTimeDB] Konsolidera kod för hantering av tidsintervall
- [AdvancedQueryParser] Testa om citerade fraser skickas korrekt
- [Term] Tillhandahåll överlagring av toString(Term) för QTest
- [ResultIterator] Ta bort onödig SearchStore framåtdeklaration
- [QueryTest] Gör testfall av fras datadrivet och utöka
- [Inotify] Starta MoveFrom utgångstimer som mest en gång per inotify bakgrundsbehandling
- [UnindexedFileIndexer] Markera bara fil för  innehållsindexering vid behov
- [Inotify] Anropa bara QFile::decode på ett enda ställe
- [QML] Bevaka avregistrering korrekt
- [FileIndexScheduler] Uppdatera förlopp för innehållsindexering oftare
- [FileIndexerConfig] Ersätt QString,bool par i inställning med dedicerad klass
- [QML] Ställ in återstående tid i övervakaren mer tillförlitligt
- [TimeEstimator] Korrigera storlek på bakgrundsbehandling, ta bort inställningsreferens
- [FileIndexScheduler] Skicka ändring av LowPowerIdle tillstånd
- [Debug] Förbättra läsbarhet av positioninfo avlusningsformat
- [Debug] Korrigera utdata för *::toTestMap(), tysta icke fel
- [WriteTransactionTest] Testa bara borttagning av positioner
- [WriteTransaction] Utöka testfall för position
- [WriteTransaction] Undvik att öka storlek på m_pendingOperations två gånger vid ersätt
- [FileIndexScheduler] Städa hantering av firstRun
- [StorageDevices] Rätta ordning för underrättelser om anslutning och initiering
- [Config] Ta bort/avråd från disableInitialUpdate

### Breeze-ikoner

- Rätta felaktiga symboliska länkar
- Flytta hörnveck längst upp till höger i 24 ikoner
- Gör så att find-location visar ett förstoringsglas på en karta, to för att skilja sig från mark-location (fel 407061)
- Lägg till 16 bildpunkters LibreOffice ikoner
- Rätta configure när xmllint inte finns
- Rätta länkning till stilmall för åtta ikoner
- Rätta några färger i stilmallar för två ikonfiler
- Rätta symboliska länkar till fel ikonstorlek
- Lägg till input-dialpad och call-voicemail
- Lägg till buho ikon
- Lägg till calindori ikon med den nya pm-stilen
- Lägg till nota ikon
- [breeze-icons] rätta skuggor i vissa användarikoner (miniprogram/128)
- Lägg till call-incoming/missed/outgoing
- Hantera sed i busybox som GNU sed
- Lägg till transmission-tray-icon
- Förbättra bildpunktsjustering och marginaler för keepassxc ikoner i systembrickan
- Återställ "[breeze-icons] Lägg till telegram-desktop brickikoner"
- Lägg till små ikoner för KeePassXC
- [breeze-icons] lägg till TeamViewer brickikoner
- Lägg till edit-reset
- Ändra document-revert style för att bli mer lik edit-undo
- Ikoner för emoji-kategorier
- Lägg till flameshot brickikoner

### KAuth

- Rätta krav på namnrymd för typ

### KBookmarks

- Koppla loss KBookmarksMenu från KActionCollection

### KCalendarCore

- Rätta återgång till inläsning av vCalendar vid misslyckad inläsning av iCalendar

### KCMUtils

- lyssna på passiveNotificationRequested
- provisorisk lösning för att applicationitem aldrig ska ändra storlek på sig själv

### KConfig

- [KConfigGui] Kontrollera teckensnittsvikt när egenskapen styleName rensas
- KconfigXT: Lägg till en värdeegenskap till fältval i uppräkningstyp

### KCoreAddons

- kdirwatch: rätta en nyligen introducerad krasch (fel 419428)
- KPluginMetaData: hantera ogiltig Mime-type i supportsMimeType

### KCrash

- Flytta definition av setErrorMessage utanför linux ifdef
- Tillåt att tillhandahålla ett felmeddelande från programmet (fel 375913)

### KDBusAddons

- Kontrollera korrekt fil för detektering av sandlåda

### KDeclarative

- Introducera programmeringsgränssnitt för passiva underrättelser
- [KCM Controls GridDelegate] Använd <code>ShadowedRectangle</code>
- [kcmcontrols] Respektera synlighet sidhuvud/sidfot

### KDocTools

- Använd kursiv fetstil 100 % för sect4-rubriker, och fetstil 100 % för sect5-rubriker (fel 419256)
- Uppdatera listan över italienska entiteter
- Använd samma stil för informaltable som för table (fel 418696)

### KIdleTime

- Rätta oändlig rekursion i xscreensaver-insticksprogram

### KImageFormats

- Konvertera HDR-insticksprogram från sscanf() till QRegularExpression. Rättar FreeBSD

### KIO

- Ny klass KIO::CommandLauncherJob i KIOGui för att ersätta KRun::runCommand
- Ny klass KIO::ApplicationLauncherJob i KIOGui för att ersätta KRun::run
- Fil I/O-slav: använd bättre inställning för sendfile systemanrop (fel 402276)
- FileWidgets: Ignorera returhändelser från KDirOperator (fel 412737)
- [DirectorySizeJob] Rätta antal underkataloger vid upplösning av symboliska länkar till kataloger
- Markera KIOFuse monteringar som troligen långsamma
- kio_file: ta hänsyn till KIO::StatResolveSymlink för UDS_DEVICE_ID och UDS_INODE
- [KNewFileMenu] Lägg till filändelse i föreslaget filnamn (fel 61669)
- [KOpenWithDialog] Lägg till generellt namn från .desktop-filer som ett verktygstips (fel 109016)
- KDirModel: implementera att visa en rotnod för den begärda webbadressen
- Registrera startade program som oberoende cgroups
- Lägg till prefixet "Stat" i StatDetails uppräkningsvärden
- Windows: Lägg till stöd för filers skapelsedatum
- KAbstractFileItemActionPlugin: Lägg till saknade citationstecken i kodexempel
- Undvik dubbel hämtning och tillfällig hexadecimal kodning för NTFS-attribut
- KMountPoint: hoppa över utbyte
- Tilldela en ikon till undermenyer för åtgärder
- [DesktopExecParser] Öppna {ssh,telnet,rlogin}:// webbadresser med ktelnetservice (fel 418258)
- Rätta avslutningskod från kioexec när det körbara programmet inte finns (och --tempfiles är angivet)
- [KPasswdServer] ersätt foreach med range/index-baserad for
- KRun's KProcessRunner: avsluta också startunderrättelse vid fel
- [http_cache_cleaner] ersätt användning av foreach med QDir::removeRecursively()
- [StatJob] Använd en QFlag för att ange detaljinformation returnerad av StatJob

### Kirigami

- Snabbfix för D28468 till rätta felaktiga variabelreferenser
- bil av med inkubatorn
- inaktivera mushjul fullständigt i bläddringsbar utanför
- Lägg till stöd för egenskapen initializer i PagePool
- Strukturera om OverlaySheet
- Lägg till objekten ShadowedImage och ShadowedTexture
- [controls/formlayout] Försök inte nollställa implicitWidth
- Lägg till användbara tips om inmatningsmetod till lösenordsfält som förval
- [FormLayout] Ställ in tidsintervall för komprimering till 0
- [UrlButton] Inaktivera när det inte finns någon webbadress
- förenkla storleksändring av sidhuvud (fel 419124)
- Ta bort export av sidhuvud från statisk installation
- Rätta om-sida med Qt 5.15
- Rätta felaktiga sökvägar i kirigami.qrc.in
- Lägg till animeringslängd "veryLongDuration"
- rätta underrättelser för flera rader
- bero inte på att fönster är aktivt för tidtagning
- Lägg till stöd för flera staplade passiva underrättelser
- Rätta aktivering av kant för ShadowedRectangle när objektet skapas
- kontrollera om fönster finns
- Lägg till saknade typer i qrc
- Rätta odefinierad kontroll i menyläget för global låda (fel 417956)
- Återgå till en enkel rektangel när programvaruåtergivning används
- Rätta förmultiplikation och alfablandning för färger
- [FormLayout] Propagera också FormData.enabled till beteckning
- Lägg till ett ShadowedRectangle objekt
- egenskapen alwaysVisibleActions
- skapa inte instanser när programmet håller på att avslutas
- Skicka inte palettändringar om paletten inte ändrades

### KItemModels

- [KSortFilterProxyModel QML] Gör invalidateFilter öppen

### KNewStuff

- Rätta layout i DownloadItemsSheet (fel 419535)
- [QtQuick-dialogruta] Konvertera till UrlBUtton och dölj när det inte finns någon webbadress
- Byt till att använda Kirigami's ShadowedRectangle
- Rätta uppdateringsscenarier när ingen explicit nerladdningslänk är vald (fel 417510)

### KNotification

- Ny klass KNotificationJobUiDelegate

### KNotifyConfig

- Använd libcanberra som huvudsakligt sätt att förhandsgranska ljudet (fel 418975)

### KParts

- Ny klass PartLoader som ersättning till KMimeTypeTrader för delprogram

### KService

- KAutostart: Lägg till statisk metod för att kontrollera startvillkor
- KServiceAction: lagra överliggande tjänst
- Läs X-Flatpak-RenamedFrom stränglistan riktigt från skrivbordsfiler

### KTextEditor

- Gör så att det kompilerar med Qt 5.15
- rätta vikningskrasch för vikning av enstaka radvikningar (fel 417890)
- [VIM-läge] Lägg till kommandona g&lt;up&gt; g&lt;down&gt; (fel 418486)
- Lägg till MarkInterfaceV2, till s/QPixmap/QIcon/g för markörsymboler
- Rita inlineNotes efter radbrytningsmarkörer har ritats

### Kwayland

- [xdgoutput] Skicka bara initialt namn och beskrivning om angiven
- Lägg till XdgOutputV1 version 2
- Sänd ut programmeny till resurser när de registreras
- Tillhandahåll en implementering för ritplattegränssnitt
- [server] Gör inte antaganden om ordning av damage_buffer och attach begäran
- Skicka en dedicerad fd till varje tangentbord för xkb tangentavbildning (fel 381674)
- [server] Introducera SurfaceInterface::boundingRect()

### KWidgetsAddons

- Ny klass KFontChooserDialog (baserad på KFontDialog från KDELibs4Support)
- [KCharSelect] Förenkla inte enstaka tecken i sökning (fel 418461)
- Om vi använder readd för ett objekt måste vi först rensa det. Annars ser vi duplicerade listor
- Uppdatera kcharselect-data till Unicode 13.0

### KWindowSystem

- Rätta EWMH non-compliance för NET::{OnScreenDisplay,CriticalNotification}
- KWindowSystem: avråd från KStartupInfoData::launchedBy, oanvänd
- Exponera programmeny via KWindowInfo

### Plasma ramverk

- Lägg till sidelement
- [pc3/busyindicator] Dölj när den inte kör
- Uppdatera fästning av fönster, lägg till flera storlekar, ta bort redundant edit-delete
- Skapa ett nytt element TopArea genom att använda widgets/toparea svg
- Lägg till Plasmoid heading svg
- Gör så att egenskapen highlighted också fungerar för roundbutton

### Prison

- Exponera också den verkliga minimala storleken i QML
- Lägg till en ny uppsättning storleksfunktioner för streckkod
- Förenkla hantering av minimal storlek
- Flytta bildskalningslogik för streckkod till AbstractBarcode
- Lägg till programmeringsgränssnitt för att kontrollera om en streckkod är en- eller tvådimensionell

### QQC2StyleBridge

- [Dialog] Använd <code>ShadowedRectangle</code>
- Rätta storleksändring av CheckBox och RadioButton (fel 418447)
- Använd <code>ShadowedRectangle</code>

### Solid

- [Fstab] Säkerställ att alla filsystemtyper är unika
- Samba: Säkerställ att skilja monteringar som delar samma källa (fel 418906)
- hårdvaruverktyg: definiera syntax via syntaxargument

### Sonnet

- Rätta att Sonnet autodetect misslyckas för indiska språk
- Skapa ConfigView en ohanterad ConfigWidget

### Syntaxfärgläggning

- LaTeX: rätta matematiska parenteser i valfria beteckningar (fel 418979)
- Lägg till Inno inställningssyntax, inklusive inbäddade Pascal-skript
- Lua: lägg till # som ytterligare avgränsare för att aktivera automatisk komplettering med <code>#någonting</code>
- C: ta bort ' som avskiljare för siffror
- lägg till några kommentar om grejerna med hoppa över position
- optimera matchning av dynamiska reguljära uttryck (fel 418778)
- rätta reguljära uttrycksregler som felaktigt är markerade som dynamiska
- utöka indexering för att detektera dynamic=true reguljära uttryck som inte har några platsmarkörer att anpassa
- Add Överfart av QL-färgläggning
- Agda: nyckelord uppdaterade till 2.6.0 och rätta flyttalspunkter

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
