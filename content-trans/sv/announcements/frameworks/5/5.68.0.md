---
aliases:
- ../../kde-frameworks-5.68.0
date: 2020-03-07
layout: framework
libCount: 70
---
### Baloo

- [ModifiedFileIndexer] Korrigera tidskontroller för nya filer
- [ModifiedFileIndexer] Utelämna BasicIndexingJob körning när den inte krävs
- Synkronisera IndexerConfig vid avslutning (fel 417127)
- [FileIndexScheduler] Tving utvärdering av indexerState efter suspend/resume

### BluezQt

- Rätta fel i konverteringsincheckning av QRegularExpression

### Breeze-ikoner

- Lägg till network-wireless-hotspot ikon
- Flytta telegram panelikoner till statuskategori
- [breeze-icons] Lägg till telegram-desktop brickikoner (fel 417583)
- [breeze-icons] Ny 48 bildpunkters telegram ikon
- Lägg till rss ikoner i åtgärd
- Ta bort 48 bildpunkters telegram ikoner
- Snabbfix för att säkerställa att validering inte görs parallellt med generering
- Ny yakuake logotyp/ikon
- Rätta inkonsekvens och dubbletter för network-wired/wireless ikoner
- Rätta gamla värden på textfärger för osd-* ikoner
- installera bara genererade ikoner om de har genererats
- undantag alla sökvägar för att säkerställa att CI-systemet fungerar
- ange -e för generatorskriptet så att det ger fel på ett riktigt sätt vid fel
- bygge: rätta bygge när installationsprefixet inte är skrivbart av användaren
- snabbfixa nya 24 bildpunkters generatorn att använda bash istället för sh
- Generera också 24@2x symboliska kompatibilitetslänkar
- Generera 24 bildpunkters svartvita ikoner automatiskt
- Lägg till ikoner som bara fanns i actions/24 i actions/22
- Ställ in dokumentskala till 1.0 för alla actions/22 ikoner
- Lägg till nya <code>smiley-add</code> ikoner
- Gör shapes och shape-choose ikoner konsekventa med andra -shape ikoner
- Gör smiley-shape konsekvent med andra -shape ikoner
- Gör flower-shape och hexagon-shape ikoner konsekventa med andra -shape ikoner
- Ersätt &lt;use/&gt; med &lt;path/&gt; i muondiscover.svg
- Lägg till statusikoner: data-error, data-warning, data-information
- Lägg till för org.kde.Ikona
- lägg till vvave ikon
- lägg till puremaps ikoner
- Förena utseendet på alla i koner som innehåller 🚫 (inget tecken)
- Ny ikon för KTimeTracker (fel 410708)
- Optimera KTrip och KDE Itinerary ikoner
- uppdatera travel-family ikoner

### Extra CMake-moduler

- Stöd NDK r20 och Qt 5.14
- Ladda QM-filer från assets: webbadresser på Android
- Lägg till ecm_qt_install_logging_categories och ecm_qt_export_logging_category
- ECMGeneratePriFile: laga användning där LIB_NAME inte är ett målnamn
- ECMGeneratePriFile: Rätta statiska konfigurationer

### Integrering med ramverk

- [KStyle] Ställ in färgen på KMessageWidgets till den korrekta från aktuellt färgschema

### KActivities

- Rätta problem med att hitta Boost inkluderingskataloger
- Använd exponerade D-Bus metoder för att byta aktiviteter med kommandoradsgränssnitt

### KAuth

- [KAuth] Lägg till stöd för åtgärdsinformation i Polkit1-gränssnitt
- [policy-gen] Rätta koden för att verkligen använda korrekt fångstgrupp
- Använd inte Policykit gränssnitt
- [polkit-1] Förenkla uppslagning av om åtgärden Polkit1Backend finns
- [polkit-1] Returnera en felstatus i actionStatus om ett fel uppstår
- Beräkna KAuthAction::isValid vid behov

### KBookmarks

- Byt namn på åtgärder för att vara konsekvent

### KCalendarCore

- Uppdatera synlighetscache när synlighet för anteckningsbok ändras

### KCMUtils

- Kontrollera activeModule innan den används (fel 417396)

### KConfig

- [KConfigGui] Rensa styleName teckensnittsegenskap för reguljära teckenstilar (fel 378523)
- Rätta kodgenerering för poster med min/max (fel 418146)
- KConfigSkeletonItem : tillåt att ställa in en KconfigGroup för att läsa och skriva i nästlade grupper
- Rätta is&lt;PropertyName&gt;Immutable generad egenskap
- Lägg till setNotifyFunction i KPropertySkeletonItem
- Lägg till is&lt;PropertyName&gt;Immutable för att veta om en egenskap inte är ändringsbar

### KConfigWidgets

- Ändra "Redisplay" till "Refresh"

### KCoreAddons

- lägg till tips om att QIcon kan användas som en programlogotyp

### KDBusAddons

- Avråd från KDBusConnectionPool

### KDeclarative

- Exponera signalen capture i KeySequenceItem
- Rätta storlek på huvud i GridViewKCM (fel 417347)
- Tillåt klass härledd från ManagedConfigModule att explicit registrera KCoreConfigSkeleton
- Tillåt användning av KPropertySkeletonItem i ManagedConfigModule

### KDED

- Lägg till väljaren --replace i kded5

### KDE GUI Addons

- [UrlHandler] Hantera att öppna nätdokumentation för inställningsmoduler
- [KColorUtils] Ändra färgtonsintervall för getHcy() till [0.0, 1.0)

### KHolidays

- Uppdatera japanska helger
- holiday_jp_ja - rätta stavning av National Foundation Day (fel 417498)

### KI18n

- Stöd Qt 5.14 på Android

### KInit

- Gör så att kwrapper/kshell startar klauncher5 vid behov

### KIO

- [KFileFilterCombo] Lägg inte till ogiltig QMimeType i mimes filter (fel 417355)
- [src/kcms/*] Ersätt foreach (avråds från) med range/index-baserad for
- KIO::iconNameForUrl(): hantera fallet med fil/katalog under trash:/
- [krun] Dela implementering av runService och runApplication
- [krun] Ta bort stöd för KToolInvocation från KRun::runService
- Förbättra KDirModel för att undvika visa '+' om det inte finns några underkataloger
- Rätta att köra Terminal på Wayland (fel 408497)
- KIO::iconNameForUrl: rätta sökning efter kde protokollikoner (fel 417069)
- Korrigera skiftläge för "basic link" objekt
- Ändra "AutoSkip" till "Skip All" (fel 416964)
- Rätta minnesläcka i KUrlNavigatorPlacesSelector::updateMenu
- fil I(O-slave: stoppa kopiering så snart I/O-slaven dödas
- [KOpenWithDialog] Välj automatiskt resultatet om modellfiltret bara har en matchning (fel 400725)

### Kirigami

- Visa verktygstips med fullständig webbadress för webbadressknapp med överskriden text
- Ha också tillbakadragna verktygsrader på rullningsbara sidor med sidfot
- Rätta PrivateActionToolButton beteende med showText eller IconOnly
- Rätta ActionToolBar/PrivateActionToolButton i kombination med QQC2 Action
- Flytta alltid markerat menyalternativ inom räckhåll
- Övervaka språkändringshändelser, och skicka vidare dem till QML-gränssnittet
- Stöd Qt 5.14 på Android
- ha inte overlaysheets under sidhuvud
- använd reserv när inläsning av ikon misslyckas
- Saknar länkar till pagepool källkodsfiler
- Icon: rätta återgivning av image: webbadresser vid hög upplösning (fel 417647)
- Krascha inte när ikoners bredd eller höjd är 0 (fel 417844)
- rätta marginaler i OverlaySheet
- [examples/simplechatapp] Sätt alltid isMenu till true
- [RFC] Reducera storlek på nivå 1 rubriker och öka vaddering på vänster sida för sidtitlar
- synkronisera storlekstips riktigt med tillståndsmaskin (fel 417351)
- Lägg till stöd för statiska platformtheme insticksprogram
- gör så att headerParent justeras riktigt när det finns en rullningslist
- Rätta flikrad med beräkning
- Lägg till PagePoolAction i QRC-fil
- tillåt verktygsradsstil på mobil
- Gör så att dokumentation av programmeringsgränssnitt reflekterar att Kirigami inte bara är en mobil verktygslåda

### KItemModels

- KRearrangeColumnsProxyModel: inaktivera tillfälligt assert på grund av fel i QTreeView
- KRearrangeColumnsProxyModel: nollställ i setSourceColumns()
- Flytta Plasmas SortFilterProxyModel till KItemModel QML-insticksprogram

### KJS

- Exponera funktioner för utvärdering av tidsgränshantering i öppet programmeringsgränssnitt

### KNewStuff

- Rätta klick på delegat bara på miniatyrbild (fel 418368)
- Rätta rullning på sidan EntryDetails (fel 418191)
- Ta inte bort CommentsModel två gånger (fel 417802)
- Täck också insticksprogrammet qtquick i filen för installerade kategorier
- Använd rätt översättningskatalog för att visa översättningar
- Rätta stängningsrubrik och grundlayout för KNSQuick dialoger (fel 414682)

### KNotification

- Gör kstatusnotifieritem tillgänglig utan dbus
- Anpassa åtgärdsnumrering i Android så att den fungerar som i KNotifications
- Skriv upp Kai-Uwe som underhållsansvarig för knotifications
- Ta alltid bort html om servern inte stöder det
- [android] Skicka defaultActivated vid beröring av underrättelsen

### KPeople

- rätta generering av pri-fil

### KQuickCharts

- Skriv inte ut fel om ogiltiga roller när roleName inte är tilldelat
- Använd plattform utanför skärm för tester på Windows
- Ta bort nerladdning av glsl-validering från valideringsskript
- Rätta valideringsfel i linjediagrammens skuggning
- Uppdatera kärnprofilskuggning för linjediagram så att den motsvarar compat
- Lägg till kommentar om kontroll av gränser
- Linjediagram: Lägg till stöd för kontroll av gränser för min/max y-gränser
- Lägg till funktionen sdf_rectangle i sdf-bibliotek
- [linjediagram] Skydda mot division med 0
- Linjediagram: Reducera antal punkter per segment
- Förlora inte punkter i slutet av ett linjediagram

### Kross

- Qt5::UiTools är inte valfri i den här modulen

### KService

- Ny frågemekanism för program: KApplicationTrader

### KTextEditor

- Lägg till ett alternativ för dynamisk radbrytning inne i ord
- KateModeMenuList: överlappa inte rullningslisten

### Kwayland

- Lägg till dbus-sövägar för programmeny i gränssnittet org_kde_plasma_window
- Registry: förstör inte återanropet vid global synkronisering
- [surface] Rätta buffertposition när buffertar kopplas till ytor

### KWidgetsAddons

- [KMessageWidget] Tillåt stilen att ändra vår palett
- [KMessageWidget] Rita den med QPainter istället för att använda stilmall
- Reducera nivå 1 rubrikstorlek något

### ModemManagerQt

- Använd inte generering och installation av qmake pri fil, för närvarande felaktig

### NetworkManagerQt

- Stöd SAE i securityTypeFromConnectionSetting
- Använd inte generering och installation av qmake pri fil, för närvarande felaktig

### Oxygen-ikoner

- Stöd också data-error/warning/information med storlekarna 32,46,64,128
- Lägg till åtgärdsobjektet "plugins", för att motsvara Breeze ikoner
- Lägg till statusikoner: data-error, data-warning, data-information

### Plasma ramverk

- Buttons: tillåt att skala upp ikoner
- Försök använda färgschemat i aktuellt tema för QIcon (fel 417780)
- Dialog: koppla bort från QWindow signaler i destruktor
- Rätta minnesläcka i ConfigView och Dialog
- rätta tips om layoutstorlek för knappbeteckningar
- säkerställ att storlekstips är heltal och jämna
- stöd bredd/höjd för ikon (fel 417514)
- Ta bort hårdkodade färger (fel 417511)
- Konstruera NullEngine med KPluginMetaData() (fel 417548)
- Reducera nivå 1 rubrikstorlek något
- Centrera verktygstips ikon/bild vertikalt
- stöd visningsegenskaper för knappar
- Varna inte för ogiltig metadata i insticksprogram (fel 412464)
- verktygstips har alltid normal färggrupp
- [Tests] Gör så att radiobutton3.qml använder PC3
- Optimera kod när filer släpps på skrivbordet (fel 415917)

### Prison

- Rätta pri-fil så att den inte misslyckas för inkluderingar med blandat skiftläge
- Rätta pri-fil så att den har qmake-namn på QtGui som beroende

### Syfte

- Skriv om insticksprogram för nextcloud
- Döda stöd för twitter

### QQC2StyleBridge

- ScrollView: Använd rullningslistens höjd för undre vaddering, inte bredd

### Solid

- Rätta inverterad logik i IOKitStorage::isRemovable

### Sonnet

- Rätta segmenteringsfel vid avslutning

### Syntaxfärgläggning

- Rätta slut på minne på grund av stora sammanhangsstackar
- Allmän uppdatering av färgläggning för CartoCSS
- Lägg till syntaxfärgläggning för Java egenskaper
- TypeScript: lägg till privata fält och import/export av enbart typ, och några rättningar
- Lägg till FreeCAD FCMacro utökning i python färgläggningsdefinition
- Uppdateringar för CMake 3.17
- C++: constinit nyckelord och std::format syntax för strängar. Förbättring av printf format
- RPM spec: diverse förbättringar
- Makefile färgläggning: rätta variabelnamn i "else" villkor (fel 417379)
- Lägg till syntaxfärgläggning för Solidity
- Små förbättringar i vissa XML-filer
- Makefile färgläggning: lägg till substitutioner (fel 416685)

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
