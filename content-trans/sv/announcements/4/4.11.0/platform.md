---
date: 2013-08-14
hidden: true
title: KDE plattform 4.11 levererar bättre prestanda
---
KDE:s plattform 4 har haft funktioner frysta sedan utgåva 4.9. Den här versionen innehåller därigenom bara ett antal felrättningar och prestandaförbättringar.

Det semantiska gränssnittet i Nepomuk har erhållit omfattande prestandaoptimeringar, såsom en mängd läsoptimeringar för att göra inläsning av data mer än sex gånger snabbare. Indexering har blivit smartare, genom en uppdelning i två steg. Det första steget hämtar allmän information (såsom filtyp och namn) omedelbart. Ytterligare information, som mediataggar, författarinformation och liknande extraheras i ett andra, något långsammare, steg. Visning av metadata för nyskapad eller just nerladdad data är nu mycket snabbare. Dessutom har systemet för säkerhetskopiering och återställning i Nepomuk förbättrats av utvecklarna. Sist men inte minst kan Nepomuk nu också indexera en mängd olika dokumentformat inklusive ODF och docx.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/dolphin.png" caption=`Semantiska funktioner använda i Dolphin` width="600px">}}

Nepomuks optimerade lagringsformat och omskrivna indexering av e-post kräver att visa delar av hårddiskens innehåll indexeras om. Följaktligen kommer körning av indexeringen konsumera en ovanlig mängd beräkningsprestanda under en viss period, beroende på storleken hos innehållet som behöver indexeras om. En automatisk konvertering av Nepomuks databas körs vid första inloggning.

Det har gjorts flera mindre rättningar som <a href='https://projects.kde.org/projects/kde/kdelibs/repository/revisions?rev=KDE%2F4.11'>går att hitta i Git-loggarna</a>.

#### Installera KDE:s utvecklingsplattform

KDE:s programvara, inklusive alla dess bibliotek och program, är fritt tillgänglig med licenser för öppen källkod. KDE:s programvara kör på diverse hårdvarukonfigurationer och processorarkitekturer, såsom ARM och x86, olika operativsystem och fungerar med alla sorters fönsterhanterare och skrivbordsmiljöer. Förutom Linux och andra UNIX-baserade operativsystem finns det versioner av de flesta KDE-program för Microsoft Windows på <a href='http://windows.kde.org'>KDE:s programvara för Windows</a>, och versioner för Apple Mac OS X på <a href='http://mac.kde.org/'>KDE:s programvara för Mac</a>. Experimentella byggen av KDE-program för diverse mobila plattformar som MeeGo, MS Windows Mobile och Symbian finns på webben, men stöds för närvarande inte. <a href='http://plasma-active.org'>Plasma Aktiv</a> erbjuder användningsmöjlighet på en större omfattning apparater, som läsplattor och annan mobil hårdvara. <br />

KDE:s programvara kan erhållas som källkod och som diverse binärformat från <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> och kan också erhållas på <a href='/download'>Cd-rom</a> eller med något av de <a href='/distributions'>större GNU/Linux- och UNIX-systemen</a> som levereras idag.

##### Paket

Vissa Linux- och UNIX-operativsystemleverantörer har vänligen tillhandahållit binärpaket av 4.11.0 för vissa versioner av sina distributioner, och i andra fall har volontärer i gemenskapen gjort det. <br />

##### Paketplatser

Besök <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Gemenskapens Wiki</a> för en aktuell lista med tillgängliga binärpaket som har kommit till KDE-projektets kännedom.

Fullständig källkod för 4.11.0 kan <a href='/info/4/4.11.0'>laddas ner fritt</a>. Instruktioner om hur man kompilerar och installerar KDE:s programvara 4.11.0 är tillgängliga på <a href='/info/4/4.11.0#binary'>informationssidan om 4.11.0</a>.

#### Systemkrav

För att få ut så mycket som möjligt av dessa utgåvor, rekommenderar vi att använda en aktuell version av Qt, såsom 4.8.4. Det är nödvändigt för att försäkra sig om en stabil och högpresterande upplevelse, eftersom vissa förbättringar som har gjorts av KDE:s programvara har i själva verket skett i det underliggande Qt-ramverket.<br /> För att helt dra nytta av kapaciteten i KDE:s programvara, rekommenderar vi också att använda de senaste grafikdrivrutinerna för systemet, eftersom det avsevärt kan förbättra användarupplevelsen både när det gäller tillvalsfunktioner, och övergripande prestanda och stabilitet.

## Också tillkännagivet idag:

## <a href="../plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Plasma arbetsrymder 4.11 fortsätter förfina användarupplevelsen</a>

Plasma arbetsrymder växlar upp för långtidsunderhåll, och levererar ytterligare förbättringar av grundfunktionerna med en smidigare aktivitetsrad, smartare grafisk batterikomponent och förbättrad ljudmixer. Introduktionen av Kscreen ger arbetsrymderna intelligent hantering av flera bildskärmar, och storskaliga förbättringar av prestanda kombinerat med små justeringar av användbarhet leder till en trevligare övergripande upplevelse.

## <a href="../applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> KDE-program 4.11 med enorma framsteg när det gäller personlig informationshantering och förbättringar överallt</a>

Utgåvan markerar omfattande förbättringar av KDE:s svit för personlig informationshantering, vilket ger mycket bättre prestanda och många nya funktioner. Kate förbättrar produktiviteten för Python- och Javascript-utvecklare med nya insticksprogram. Dolphin har blivit snabbare, och utbildningsprogrammen medför diverse nya funktioner.
