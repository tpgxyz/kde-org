---
date: 2013-08-14
hidden: true
title: Plasma arbetsrymder 4.11 fortsätter förfina användarupplevelsen
---
{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/empty-desktop.png" caption=`KDE Plasma arbetsrymder 4.11` width="600px" >}}

I utgåva 4.11 av Plasma arbetsrymder har aktivitetsraden, en av de mest använda grafiska komponenterna i Plasma, <a href='http://blogs.kde.org/2013/07/29/kde-plasma-desktop-411s-new-task-manager'>överförts till QtQuick</a>. Den nya aktivitetsraden visar ett likformigare och smidigare beteende, medan den behåller utseendet och funktionen från dess gamla motsvarighet. Överföringen löste också ett antal långvariga fel. Den grafiska batterikomponenten (som tidigare kunde justera bildskärmens ljusstyrka) stöder nu också tangentbordets ljusstyrka, och kan hantera flera batterier i periferienheter, såsom trådlös mus och tangentbord. Den visar batteriladdningen för varje enhet och varnar när någon håller på att ta slut. Menyn Kickoff visar nu senast installerade program under några dagar. Sist men inte minst, ståtar nu underrättelserutor med en inställningsknapp där man enkelt kan ändra inställningarna för just den här typen av underrättelse.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/notifications.png" caption=`Förbättrad hantering av underrättelser` width="600px" >}}

Kmix, KDE:s ljudmixer, har fått signifikanta prestanda- och stabilitetsförbättringar, samt <a href='http://kmix5.wordpress.com/2013/07/26/kmix-mission-statement-2013/'>fullständigt stöd för kontroll av mediaspelare</a> baserat på MPRIS2-standarden.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/battery-applet.png" caption=`Det omkonstruerade batteriminiprogrammet i arbete` width="600px" >}}

## Kwin fönster- och sammansättningshanterare

Vår fönsterhanterare, Kwin, har återigen fått väsentliga uppdateringar, som går ifrån föråldrade teknologier och införlivar kommunikationsprotokollet 'XCB'. Det resulterar i smidigare, snabbare fönsterhantering. Stöd för OpenGL 3.1 och OpenGL ES 3.0 har också introducerats. Utgåvan omfattar också det första experimentella stödet för efterföljaren till X11, Wayland. Det låter oss använda Kwin och X11 ovanpå en Wayland-arkitektur. För mer information om hur detta experimentella läge används, se <a href='http://blog.martin-graesslin.com/blog/2013/06/starting-a-full-kde-plasma-session-in-wayland/'>detta inlägg</a>. Skriptgränssnittet i Kwin har fått omfattande förbättringar, och ståtar nu med användargränssnitt för inställning, nya animeringar och grafiska effekter, samt många mindre förbättringar. Utgåvan medför bättre medvetenhet om flera bildskärmar (inklusive alternativ för glöd vid kanter i 'varma hörn'), förbättrad snabb ruthantering (med inställningsbara rutområden) och den vanliga mängden felrättningar och optimeringar. Se <a href='http://blog.martin-graesslin.com/blog/2013/06/what-we-did-in-kwin-4-11/'>här</a> och <a href='http://blog.martin-graesslin.com/blog/2013/06/new-kwin-scripting-feature-in-4-11/'>här</a> för mer detaljerad information.

## Bildskärmshantering och webbgenvägar

Bildskärmsinställningen i systeminställningarna också har <a href='http://www.afiestas.org/kscreen-1-0-released/'>ersatts med det nya verktyget Kscreen</a>. Kscreen bidrar med intelligentare stöd för flera skärmar och kommer ihåg inställningar för bildskärmar som har ställts in manuellt. Det ståtar med ett intuitivt, visuellt orienterat gränssnitt och hanterar att arrangera om bildskärmar via enkelt drag och släpp.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kscreen.png" caption=`Den nya bildskärmshanteraren Kscreen` width="600px" >}}

Webbgenvägar, det enklaste sättet att snabbt hitta vad du söker efter på webben, har städats upp och förbättrats. Många har uppdaterats att använda säkra krypterade anslutningar (TLS/SSL), nya webbgenvägar har lagts till och några få föråldrade genvägar har tagits bort. Processen för att lägga till egna webbgenvägar har också förbättrats. Mer information finns <a href='https://plus.google.com/108470973614497915471/posts/9DUX8C9HXwD'>här</a>.

Utgåvan utgör slutet av Plasma arbetsrymder 1, som ingår i serien KDE:s programvarusamling 4. För att förenkla övergången till nästa generation kommer utgåvan att stödjas under minst två år. Funktionsutvecklingens fokus kommer nu att skifta till Plasma arbetsrymder 2, medan prestandaförbättringar och felrättningar koncentreras på 4.11-serien.

#### Installera Plasma

KDE:s programvara, inklusive alla dess bibliotek och program, är fritt tillgänglig med licenser för öppen källkod. KDE:s programvara kör på diverse hårdvarukonfigurationer och processorarkitekturer, såsom ARM och x86, olika operativsystem och fungerar med alla sorters fönsterhanterare och skrivbordsmiljöer. Förutom Linux och andra UNIX-baserade operativsystem finns det versioner av de flesta KDE-program för Microsoft Windows på <a href='http://windows.kde.org'>KDE:s programvara för Windows</a>, och versioner för Apple Mac OS X på <a href='http://mac.kde.org/'>KDE:s programvara för Mac</a>. Experimentella byggen av KDE-program för diverse mobila plattformar som MeeGo, MS Windows Mobile och Symbian finns på webben, men stöds för närvarande inte. <a href='http://plasma-active.org'>Plasma Aktiv</a> erbjuder användningsmöjlighet på en större omfattning apparater, som läsplattor och annan mobil hårdvara. <br />

KDE:s programvara kan erhållas som källkod och som diverse binärformat från <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> och kan också erhållas på <a href='/download'>Cd-rom</a> eller med något av de <a href='/distributions'>större GNU/Linux- och UNIX-systemen</a> som levereras idag.

##### Paket

Vissa Linux- och UNIX-operativsystemleverantörer har vänligen tillhandahållit binärpaket av 4.11.0 för vissa versioner av sina distributioner, och i andra fall har volontärer i gemenskapen gjort det. <br />

##### Paketplatser

Besök <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Gemenskapens Wiki</a> för en aktuell lista med tillgängliga binärpaket som har kommit till KDE-projektets kännedom.

Fullständig källkod för 4.11.0 kan <a href='/info/4/4.11.0'>laddas ner fritt</a>. Instruktioner om hur man kompilerar och installerar KDE:s programvara 4.11.0 är tillgängliga på <a href='/info/4/4.11.0#binary'>informationssidan om 4.11.0</a>.

#### Systemkrav

För att få ut så mycket som möjligt av dessa utgåvor, rekommenderar vi att använda en aktuell version av Qt, såsom 4.8.4. Det är nödvändigt för att försäkra sig om en stabil och högpresterande upplevelse, eftersom vissa förbättringar som har gjorts av KDE:s programvara har i själva verket skett i det underliggande Qt-ramverket.<br /> För att helt dra nytta av kapaciteten i KDE:s programvara, rekommenderar vi också att använda de senaste grafikdrivrutinerna för systemet, eftersom det avsevärt kan förbättra användarupplevelsen både när det gäller tillvalsfunktioner, och övergripande prestanda och stabilitet.

## Också tillkännagivet idag:

## <a href="../applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> KDE-program 4.11 med enorma framsteg när det gäller personlig informationshantering och förbättringar överallt</a>

Utgåvan markerar omfattande förbättringar av KDE:s svit för personlig informationshantering, vilket ger mycket bättre prestanda och många nya funktioner. Kate förbättrar produktiviteten för Python- och Javascript-utvecklare med nya insticksprogram. Dolphin har blivit snabbare, och utbildningsprogrammen medför diverse nya funktioner.

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> KDE plattform 4.11 levererar bättre prestanda</a>

Den här utgåvan av KDE plattform 4.11 fortsätter att fokusera på stabilitet. Nya funktioner implementeras för den framtida utgåvan av KDE:s Ramverk 5.0, men för den stabila utgåvan lyckades vi klämma in optimeringar för vårt Nepomuk-ramverk.
