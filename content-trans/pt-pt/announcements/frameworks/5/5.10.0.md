---
aliases:
- ../../kde-frameworks-5.10.0
date: '2015-05-08'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### KActivities

- (sem registo de alterações fornecido)

### KConfig

- Gerar classes de prova do QML, usando o 'kconfigcompiler'

### KCoreAddons

- Nova macro do CMake kcoreaddons_add_plugin para criar 'plugins' baseados no KPluginLoader de forma mais simples.

### KDeclarative

- Correcção de estoiro na 'cache' de texturas.
- e outras correcções

### KGlobalAccel

- Adição do novo método 'globalShortcut', que devolve o atalho como estiver definido na configuração global.

### KIdleTime

- Evitar que o 'kidletime' estoire na plataforma Wayland

### KIO

- Adição dos métodos KPropertiesDialog::KPropertiesDialog(urls) e KPropertiesDialog::showDialog(urls).
- Obtenção assíncrona de dados, baseada no QIODevice, para o KIO::storedPut e o KIO::AccessManager::put.
- Correcção de condições com o valor devolvido pelo QFile::rename (erro 343329)
- Correcção do KIO::suggestName para sugerir nomes melhores (erro 341773)
- kioexec: Correcção da localização de escrita do kurl (erro 343329)
- Gravação dos favoritos apenas no user-places.xbel (erro 345174)
- Item RecentDocuments duplicado, caso dois ficheiros diferentes tenham o mesmo nome
- Mensagem de erro melhor, caso um único ficheiro seja demasiado grande para o caixote do lixo (erro 332692)
- Correcção do estoiro do KDirLister com o redireccionamento provado pelo 'slot', ao chamar o 'openURL'

### KNewStuff

- Novo conjunto de classes, chamadas de KMoreTools ou com outros nomes relacionados. As KMoreTools ajudam a adicionar sugestões sobre as ferramentas externas que potencialmente não estejam instaladas. Para além disso, torna os menus longos mais curtos, oferecendo uma secção principal e outra adicional, que possa também ser configurada pelo utilizador.

### KNotifications

- Correcção do KNotifications quando é usado com o NotifyOSD da Ubuntu (erro 345973)
- Não despoletar actualizações das notificações quando definir as mesmas propriedades (erro 345973)
- Introdução da opção LoopSound, que permite que as notificações toquem um som em ciclo, caso precisem disso (erro 346148)
- Não estoirar se uma notificação não tiver associado um item gráfico

### KPackage

- Adição de uma função KPackage::findPackages, semelhante ao KPluginLoader::findPlugins

### KPeople

- Uso do KPluginFactory para instanciar os 'plugins', em vez do KService (mantido por questões de compatibilidade).

### KService

- Correcção da divisão errada da localização do item (erro 344614)

### KWallet

- O agente de migração agora também verifica, antes de começar, se a carteira antiga está vazia (erro 346498)

### KWidgetsAddons

- KDateTimeEdit: Correcção para que os dados introduzidos pelo utilizador sejam mesmo registados. Correcção das margens duplas.
- KFontRequester: correcção da selecção apenas de tipos de letra monoespaçados

### KWindowSystem

- Não depender do QX11Info no KXUtils::createPixmapFromHandle (erro 346496)
- novo método NETWinInfo::xcbConnection() -&gt; xcb_connection_t*

### KXmlGui

- Correcção dos atalhos quando definir um atalho secundário (erro 345411)
- Actualização da lista de componentes/produtos no Bugzilla para os relatórios de erros (erro 346559)
- Atalhos globais: permitir a configuração também do atalho alternativo

### NetworkManagerQt

- Os ficheiros de inclusão são agora organizados como em todas as outras plataformas.

### Plataforma do Plasma

- O PlasmaComponents.Menu agora suporta secções
- Uso do KPluginLoader em vez do ksycoca para carregar os motores de dados em C++
- Considerar a rotação do 'visualParent' no popupPosition (erro 345787)

### Sonnet

- Não tentar realçar nada, caso não existia nenhum verificador ortográfico. Isto iria provocar um ciclo infinito com o temporizador do 'rehighlighRequest' a disparar de forma constante.

### Frameworkintegration

- Correcção das janelas de ficheiros nativas no QFileDialog: ** As janelas de ficheiros abertas com o exec() e sem um item-pai eram abertas, mas qualquer interacção com o utilizador era bloqueada de tal forma que não era possível seleccionar nenhum ficheiro nem fechar a janela. ** As janelas de ficheiros abertas com o open() ou o show() com um item-pai não eram abertas de todo.

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
