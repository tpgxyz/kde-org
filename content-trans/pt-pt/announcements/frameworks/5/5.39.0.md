---
aliases:
- ../../kde-frameworks-5.39.0
date: 2017-10-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Só corresponder a tipos MIME reais e não p.ex. com "imagem de CD em bruto" (erro 364884)
- Remoção do pf.path() do contentor, antes de a referência ficar invalidada com o it.remove()
- Correcção da descrição do protocolo do KIO-slave 'tags'
- Classificação dos ficheiros Markdown como Documentos

### Ícones do Brisa

- adição do ícone 'overflow-menu' (erro 385171)

### Módulos Extra do CMake

- Correcção da compilação das interfaces em Python depois da modificação '7af93dd23873d0b9cdbac192949e7e5114940aa6'

### Integração da Plataforma

- Correspondência do KStandardGuiItem::discard ao QDialogButtonBox::Discard

### KActivitiesStats

- Mudança do limite predefinido da pesquisa para zero
- Adição de opção para activar o teste do modelo

### KCMUtils

- Mudança do KCMultiDialog para deslizante (erro 354227)

### KConfig

- Descontinuação do KStandardAction::SaveOptions

### KConfigWidgets

- Descontinuação do KStandardAction::PasteText e do KPasteTextAction

### KCoreAddons

- desktoptojson: Melhoria na heurística da detecção do tipo de serviço (erro 384037)

### KDeclarative

- Reposição da licença como LGPL2.1+
- Adição do método openService() ao KRunProxy

### KFileMetaData

- correcção de um estoiro quando mais que uma instância do ExtractorCollection é destruída

### KGlobalAccel

- Reversão do "KGlobalAccel: migração para o novo método port 'symXModXToKeyQt' do KKeyServer, para corrigir as teclas do teclado numérico" (erro 384597)

### KIconThemes

- adição de um método para repor a paleta predefinida
- uso do qApp-&gt;palette() quando não está definida nenhuma paleta personalizada
- alocação do tamanho de 'buffer' adequado
- permitir a utilização de uma paleta personalizada em vez de conjuntos de cores
- exposição do conjunto de cores da folha de estilo

### KInit

- Windows: Correcção da mensagem 'klauncher uses absolute compile time install path for finding kioslave.exe' (o 'klauncher' usa o local de instalação absoluto, na altura da compilação, para encontrar o 'kioslave.exe')

### KIO

- kioexec: Monitorização do ficheiro quando terminou a sua cópia (erro 384500)
- KFileItemDelegate: Reservar sempre espaço para os ícones (erro 372207)

### Kirigami

- não instanciar o ficheiro Theme no BasicTheme
- adição de um novo botão 'Avançar'
- menos contraste com o fundo da barra de posicionamento da folha
- inserção e remoção mais fiável do menu de sobreposição
- melhor desenho do ícone de contexto
- mais cuidado ao centrar o botão da acção
- uso dos tamanhos dos ícones para os botões de acções
- tamanhos de ícones perfeitos ao pixel no ecrã
- efeito seleccionado para falsificar o ícone da pega
- correcção da cor das pegas
- melhor cor para o botão da acção principal
- correcção do menu de contexto para o estilo do ecrã
- melhor menu "mais" para a barra de ferramentas
- um menu adequado para o menu de contexto das páginas intermédias
- adição de um campo de texto que deverá invocar um teclado
- não estoirar quando lançar com estilos inexistentes
- Conceito de ColorSet (conjunto de cores) no Theme
- simplificação da gestão da roda (erro 384704)
- nova aplicação de exemplo com ficheiros QML principais para computadores/dispositivos móveis
- garantia de que o 'currentIndex' é válido
- Geração dos meta-dados do Appstream da aplicação de galeria
- Pesquisa pelo QtGraphicalEffects, para que os criadores de pacotes não o esqueçam
- Não incluir o controlo sobre a decoração do fundo (erro 384913)
- coloração mais clara quando a lista não tem 'activeFocus' (foco activo)
- algum suporte para os formatos RTL (direita-para-esquerda)
- Desactivar os atalhos quando uma acção está desactivada
- criação da estrutura total do 'plugin' na pasta de compilação
- correcção da acessibilidade da página principal da galeria
- Se o Plasma não estiver disponível, o KF5Plasma também não. Deverá corrigir o erro de IC (integração contínua)

### KNewStuff

- Forçar o Kirigami 2.1 em vez do 1.0 para o KNewStuffQuick
- Criação adequada de uma KPixmapSequence
- Não dar nenhum aviso de que o ficheiro 'knsregistry' não está presente antes de ser útil

### Plataforma KPackage

- kpackage: fornecimento de uma cópia do 'servicetypes/kpackage-generic.desktop'
- kpackagetool: fornecimento de uma cópia do 'servicetypes/kpackage-generic.desktop'

### KParts

- Modelo de KPartsApp: correcção do local de instalação do ficheiro 'desktop' do KPart

### KTextEditor

- Ignorar a marca predefinida no contorno de ícones para a marca de selecção única
- Uso do QActionGroup para a selecção do modo de entrada
- Correcção da barra de ortografia em falta (erro 359682)
- Correcção do valor de contingência de "escuridão" para os caracteres Unicode &gt; 255 (erro 385336)
- Correcção da visualização de espaços finais para as linhas RTL

### KWayland

- Só enviar o 'sendApplied' / 'sendFailed' do OutputConfig para o recurso correcto
- Não estoirar se um cliente (legalmente) usar o gestor de contraste global removido
- Suporte para o XDG v6

### KWidgetsAddons

- KAcceleratorManager: definição do texto do ícone nas acções para remover as marcas CJK (erro 377859)
- KSqueezedTextLabel: Redução do texto ao mudar a indentação ou a margem
- Uso do ícone 'edit-delete' para a acção de remoção destrutiva (erro 385158)
- Correcção do Erro 306944 - Uso da roda do rato para incrementar / decrementar as datas (erro 306944)
- KMessageBox: Uso do ícone de ponto de interrogação para as janelas com perguntas
- KSqueezedTextLabel: Respeitar a indentação, margem e largura da moldura

### KXMLGUI

- Correcção do ciclo de actualização do KToolBar (erro 377859)

### Plataforma do Plasma

- Correcção do org.kde.plasma.calendar com o Qt 5.10
- [FrameSvgItem] Iteração adequada pelos nós-filhos
- [ContainmentInterface] Não adicionar acções do contentor às acções da 'applet' no ecrã
- Adição de um novo componente para os textos a cinzento nas Delegações de Itens
- Correcção do FrameSVGItem com o sistema de desenho por 'software'
- Não animar o IconItem no modo por 'software'
- [FrameSvg] Uso do 'connect' no novo estilo
- possibilidade de definir um novo espaço de cores ligado para não herdar
- Adição de uma nova indicação visual para o foco por teclado do Checkbox/Radio
- não recriar uma imagem nula
- Passagem do item para o rootObject(), dado que agora é um 'singleton' (instância única) (erro 384776)
- Não listar os nomes das páginas duas vezes
- não aceitar o foco activo na página
- registo da versão 1 para o QQuickItem
- [PlasmaComponents 3] Correcção do RTL em alguns elementos
- Correcção de ID inválido no 'viewItem'
- actualização do ícone de notificação de correio para obter um melhor contraste (erro 365297)

### qqc2-desktop-style

Novo módulo: o estilo do QtQuickControls 2 que usa o QStyle do QWidget para pintar. Isto possibilita obter um maior nível de consistência entre as aplicações baseadas no QWidget e as baseadas em QML.

### Solid

- [solid/fstab] Adição do suporte para as opções do estilo 'x-gvfs' no 'fstab'
- [solid/fstab] Troca das propriedades do fabricante e do produto; possibilidade de tradução da descrição

### Realce de Sintaxe

- Correcção das referências inválidas ao 'itemData' em 57 ficheiros de realce
- Adição do suporte para localizações de pesquisa personalizadas para as definições de sintaxe e de temas específicos da aplicação
- AppArmor: correcção das regras do DBus
- Indexação do realce: factorização das verificações para um ciclo 'while' mais reduzido
- ContextChecker: suporte da mudança de contexto '!' e  do 'fallthroughContext'
- Indexação do realce: verificação da existências de nomes de contextos referenciados
- Reposição da licença do realce do QMake com a licença MIT
- Permitir ao realce do QMake se sobrepor ao Prolog para os ficheiros '.pro' (erro 383349)
- Suporte da macro "@" do Clojure com os parêntesis
- Adição do realce de sintaxe para os Perfis do AppArmor
- Indexação do realce: Captura de intervalos 'a-Z/A-z' inválidos nas expressões regulares
- Correcção de intervalos com capitalizações incorrectas nas expressões regulares
- adição dos ficheiros de referência em falta para os testes - parecem estar OK
- Adição do suporte aos ficheiros HEX da Intel para o motor de realce de sintaxe
- Desactivação da verificação ortográfica para os textos nos programas em Sieve

### ThreadWeaver

- Correcção de fuga de memória

### Informação de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
