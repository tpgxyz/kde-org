---
aliases:
- ../announce-applications-15.04.0
changelog: true
date: '2015-04-15'
description: O KDE Lança as Aplicações do KDE 15.04.
layout: application
title: O KDE Lança as Aplicações do KDE 15.04.0
version: 15.04.0
---
15 de Abril de 2015. Hoje o KDE lançou as Aplicações do KDE 15.04. Com esta versão, um conjunto de 72 aplicações foi migrado para as <a href='https://dot.kde.org/2013/09/25/frameworks-5'>Plataformas do KDE 5</a>. A equipa tenta ao máximo trazer a melhor qualidade para o seu ambiente de trabalho e para essas aplicações. Como tal, contamos consigo para enviar as suas reacções.

Com esta versão, existem novas adições à lista de aplicações baseadas nas Plataformas do KDE 5, incluindo o <a href='https://www.kde.org/applications/education/khangman/'>KHangMan</a>, <a href='https://www.kde.org/applications/education/rocs/'>Rocs</a>, <a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, <a href='https://www.kde.org/applications/development/kompare'>Kompare</a>, <a href='https://kdenlive.org/'>Kdenlive</a>, <a href='https://userbase.kde.org/Telepathy'>Telepathy para KDE</a> e <a href='https://games.kde.org/'>alguns jogos do KDE</a>.

O Kdenlive é uma das melhores aplicações de edição de vídeo não-linear disponíveis. Terminou recentemente o seu <a href='https://community.kde.org/Incubator'>processo de incubação</a> para se tornar um projecto oficial do KDE e foi migrado para as Plataformas do KDE 5. A equipa por trás desta obra de arte decidiu que o Kdenlive deveria ser lançada em conjunto com as Aplicações do KDE. Algumas das funcionalidades novas são a funcionalidade de gravação automática dos novos projectos e uma estabilização de filmes corrigida.

O Telepathy do KDE é a ferramenta para mensagens instantâneas. Foi migrado para as Plataformas do KDE 5 e Qt5 e é um novo membro das Aplicações do KDE. Está praticamente completo, excepto na interface de chamadas de áudio e vídeo, que ainda se encontra em falta.

Sempre que possível, o KDE usa a tecnologia existente, tal como foi feito com o novo KAccounts, que também é usado no SailfishOS e no Unity da Canonical. Dentro das aplicações do KDE, só é usado de momento no Telepathy do KDE. Contudo, no futuro, irá ver uma utilização mais abrangente pelas aplicações, como o Kontact e o Akonadi.

No módulo de <a href='https://edu.kde.org/'>Educação do KDE</a>, o Cantor recebeu algumas novas funcionalidades relacionadas com o seu suporte para Python: foi adicionada uma nova infra-estrutura para Python 3 e novas categorias para Obter Coisas Novas. O Rocs foi completamente remodelado: o núcleo de base da teoria dos grafos foi remodelado, a separação das estruturas de dados foi removida e foi introduzido um documento de grafo mais genérico como entidade central dos grafos, assim como foi revista a API de programação para os algoritmos de grafos, que consiste agora apenas numa API unificada. O KHangMan foi migrado para o QtQuick e foi-lhe dado algum embelezamento no processo. Da mesma forma, o Kanagram recebeu um novo modo para 2 jogadores e as letras são agora botões para carregar, podendo à mesma ser escritos como antes.

Para além das típicas correcções de erros, o <a href='https://www.kde.org/applications/development/umbrello/'>Umbrello</a> recebeu algumas melhorias de usabilidade e estabilidade desta vez. Para além disso, a funcionalidade para Procurar poderá agora ser limitada por categoria: classe, interface, pacote, operações ou atributos.
