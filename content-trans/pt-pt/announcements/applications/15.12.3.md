---
aliases:
- ../announce-applications-15.12.3
changelog: true
date: 2016-03-16
description: O KDE Lança as Aplicações do KDE 15.12.3
layout: application
title: O KDE Lança as Aplicações do KDE 15.12.3
version: 15.12.3
---
15 de Março de 2016. Hoje o KDE lançou a terceira actualização de estabilidade para as <a href='../15.12.0'>Aplicações do KDE 15.12</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

As mais de 15 correcções de erros registadas incluem as melhorias nos módulos 'kdepim' e nas aplicações 'akonadi', 'ark', 'kblocks', 'kcalc', 'ktouch' e 'umbrello', entre outros.

Esta versão também inclui as versões de Suporte de Longo Prazo da Plataforma de Desenvolvimento do KDE 4.14.18.
