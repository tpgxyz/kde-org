---
aliases:
- ../announce-applications-15.04-rc
date: '2015-03-26'
description: KDE Ships Applications 15.04 Release Candidate.
layout: application
title: KDE toimittaa KDE Applications 15.04:n julkaisuehdokkaan
---
26. maaliskuuta 2015. Tänään KDE julkaisi julkaisuehdokkaan uusista KDE-sovellusten julkaisuista. Nyt kun riippuvuus- ja ominaisuusjäädytykset ovat paikoillaan, KDE-kehitysryhmä keskittyy virheiden korjaamiseen sekä hiomiseen.

With the various applications being based on KDE Frameworks 5, the KDE Applications 15.04 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the team by installing the release <a href='https://bugs.kde.org/'>and reporting any bugs</a>.
