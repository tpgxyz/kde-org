---
aliases:
- ../announce-applications-17.12-rc
custom_spread_install: true
date: 2017-12-01
description: KDE veröffentlicht den Freigabekandidaten der Anwendungen 17.12
layout: application
release: applications-17.11.90
title: KDE veröffentlicht den Freigabekandidaten der KDE-Anwendungen 17.12
---
01.Dezember 2017. Heute veröffentlicht KDE den Freigabekandidaten der neuen Version der KDE-Anwendungen. Mit dem Einfrieren von Abhängigkeiten und Funktionen konzentriert sich das KDE-Team auf die Behebung von Fehlern und Verbesserungen.

In den <a href='https://community.kde.org/Applications/17.12_Release_Notes'>Veröffentlichungshinweisen der KDE-Gemeinschaft</a> finden Sie Informationen über neue Quelltextarchive, Portierungen zu KF5 und bekannte Probleme. Eine vollständigere Ankündigung erscheint mit der endgültigen Version.

Es sind sind gründliche Tests für die Veröffentlichung der KDE-Anwendungen 17.12 nötig, um die Qualität und Benutzererfahrung beizubehalten und zu verbessern. Benutzer, die KDE täglich benutzen, sind sehr wichtig, um die hohe Qualität der KDE-Software zu erhalten, weil Entwickler nicht jede mögliche Kombination von Anwendungsfällen testen können. Diese Benutzer können Fehler finden, so dass sie vor der endgültigen Veröffentlichung korrigiert werden können. Beteiligen Sie sich beim KDE-Team und installieren Sie den Freigabekandidaten und berichten Sie alle <a href='https://bugs.kde.org/'>Fehler</a>.

#### Binärpakete für KDE-Anwendungen 17.12 (Freigabekandidat) installieren

<em>Pakete</em>. Einige Anbieter von Linux-/UNIX-Betriebssystemen haben dankenswerterweise Binärpakete des Veröffentlichungskandidaten der KDE-Anwendungen für 17.12 (intern 17.11.90) für einige Versionen Ihrer Distributionen bereitgestellt, ebenso wie freiwillige Mitglieder der Gemeinschaft. Zusätzliche binäre Pakete und Aktualisierungen der jetzt verfügbaren Pakete werden in den nächsten Wochen bereitgestellt.

<em>Paketquellen</em>. Eine aktuelle Liste aller Binärpakete, von denen das KDE-Projekt in Kenntnis gesetzt wurde, finden Sie im <a href='http://community.kde.org/Binary_Packages'>Community Wiki</a>.

#### Kompilieren des Freigabekandidaten der KDE-Anwendungen 17.12

Der vollständige Quelltext des Freigabekandidaten der KDE-Anwendungen 17.12 kann <a href='http://download.kde.org/unstable/applications/17.11.90/src/'>hier</a> heruntergeladen werden. Anweisungen zum Kompilieren und Installieren finden Sie auf der <a href='/info/applications/applications-17.11.90'>Infoseite des Freigabekandidaten der KDE-Anwendungen 17.12</a>.
