---
aliases:
- ../announce-applications-19.08.3
changelog: true
date: 2019-11-07
description: KDE veröffentlicht die Anwendungen 19.08.
layout: application
major_version: '19.08'
release: applications-19.08.3
title: KDE veröffentlicht die Anwendungen 19.08.3
version: 19.08.3
---
{{% i18n_date %}}

Heute veröffentlicht KDE die erste Aktualisierung der <a href='../19.08.0'>KDE-Anwendungen 19.08</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

More than a dozen recorded bugfixes include improvements to Kontact, Ark, Cantor, K3b, Kdenlive, Konsole, Okular, Spectacle, Umbrello, among others.

Verbesserungen umfassen:

- In the video-editor Kdenlive, compositions no longer disappear when reopening a project with locked tracks
- Okular's annotation view now shows creation times in local time zone instead of UTC
- Keyboard control has been improved in the Spectacle screenshot utility
