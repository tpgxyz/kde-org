---
aliases:
- ../announce-applications-18.04.3
changelog: true
date: 2018-07-12
description: KDE veröffentlicht die KDE-Anwendungen 18.04.3
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 18.04.3
version: 18.04.3
---
12. Juli 2018. Heute veröffentlicht KDE die dritte Aktualisierung der <a href='../18.04.0'>KDE-Anwendungen 18.04</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als 20 aufgezeichnete Fehlerkorrekturen enthalten unter anderem Verbesserungen für Kontact, Ark, Cantor, Dolphin, Gwenview und KMag.

Verbesserungen umfassen:

- Compatibility with IMAP servers that do not announce their capabilities has been restored
- Ark can now extract ZIP archives which lack proper entries for folders
- KNotes on-screen notes again follow the mouse pointer while being moved
