---
aliases:
- ../announce-4.11.5
date: 2014-01-07
description: KDE veröffentlicht die Plasma-Arbeitsbereiche, Anwendungen und Plattform
  4.11.5
title: KDE veröffentlicht 4.11.5
---
7. Januar 2014. Heute veröffentlicht KDE Aktualisierungen der Arbeitsbereiche, der Anwendungen und Entwicklerplattform. Dies ist die fünfte monatliche Aktualisierung zur Stabilisierung in KDE 4.11. Wie in der Ankündigung zur Veröffentlichung angemerkt, werden die Arbeitsbereiche bis August 2015 aktualisiert. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Viele aufgezeichnete Fehlerkorrekturen enthalten Verbesserungen für das integrierte Programm Kontact für persönliche Informationsverwaltung, das UML-Programm Umbrello, den Dokumentbetrachter Okular, den Webbrowser Konqueror, das Dateiverwaltungsprogramm Dolphin und andere Programme. Der Plasma-Rechner kann jetzt mit griechischen Zeichen umgehen und Okular kann Seiten mit langen Titeln drucken. Außerdem erhielt Konqueror bessere Unterstützung für Web-Schriften durch eine Fehlerkorrektur.

Eine umfassendere <a href='https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2013-06-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.11.5&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0='>Liste der Änderungen</a> finden Sie im KDE-Fehlermeldungssystem. Eine genaue Liste der Änderungen für 4.11.5 finden Sie auch in den Git-Protokollen.

Um den Quelltext oder Pakete zur Installation herunterzuladen, gehen Sie zur Seite <a href='/info/4/4.11.5'>Informationen über 4.11.5</a>. Möchten Sie weitere Informationen über der Version von 4.11 der KDE-Arbeitsbereiche, der Anwendungen oder der Entwicklerplattform erhalten, lesen Sie bitte die <a href='/announcements/4.11/'>Hinweise zur Veröffentlichung von 4.11</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`Der Arbeitsablauf für das spätere Senden in Kontact` width="600px">}}

KDE-Software einschließlich aller Bibliotheken und Anwendungen ist frei unter „Open Source“-Lizenzen verfügbar. KDE-Software kann als Quelltext und in verschiedenen binären Formaten von <a href='http://download.kde.org/stable/4.11.5/'>download.kde.org</a> oder von vielen der <a href='/distributions'>wichtigsten GNU/Linux- und Unix-Distributionen</a> heruntergeladen werden.
