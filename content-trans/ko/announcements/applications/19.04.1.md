---
aliases:
- ../announce-applications-19.04.1
changelog: true
date: 2019-05-09
description: KDE Ships Applications 19.04.1.
layout: application
major_version: '19.04'
release: applications-19.04.1
title: KDE에서 KDE 프로그램 19.04.1 출시
version: 19.04.1
---
{{% i18n_date %}}

Today KDE released the first stability update for <a href='../19.04.0'>KDE Applications 19.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Kontact, Ark, Cantor, Dolphin, Kdenlive, Spectacle, Umbrello 등에 20개 이상의 버그 수정 및 기능 개선이 있었습니다.

개선 사항:

- 데스크톱에 있는 파일에 태그를 지정할 때 더 이상 태그 이름이 잘리지 않음
- KMail의 텍스트 공유 플러그인에서 충돌이 수정됨
- 비디오 편집기 Kdenlive의 여러 문제점이 수정됨
