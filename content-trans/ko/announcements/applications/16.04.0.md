---
aliases:
- ../announce-applications-16.04.0
changelog: true
date: 2016-04-20
description: KDE에서 KDE 프로그램 16.04.0 출시
layout: application
title: KDE에서 KDE 프로그램 16.04.0 출시
version: 16.04.0
---
2016년 4월 20일. KDE에서는 KDE 프로그램 16.04를 출시했습니다. 이 릴리스에는 사용 편의성 개선, 새로운 기능 추가, 사소한 버그 수정 등이 있었으며 KDE 프로그램을 통해서 장치를 더 완벽하게 사용하는 것에 한 걸음 더 다가갔습니다.

<a href='https://www.kde.org/applications/graphics/kcolorchooser/'>KColorChooser</a>, <a href='https://www.kde.org/applications/utilities/kfloppy/'>KFloppy</a>, <a href='https://www.kde.org/applications/games/kmahjongg/'>KMahjongg</a> and <a href='https://www.kde.org/applications/internet/krdc/'>KRDC</a> have now been ported to KDE Frameworks 5 and we look forward to your feedback and insight into the newest features introduced with this release. We would also highly encourage your support for <a href='https://www.kde.org/applications/education/minuet/'>Minuet</a> as we welcome it to our KDE Applications and your input on what more you'd like to see.

### KDE에 새로 추가됨

{{<figure src="/announcements/applications/16.04.0/minuet1604.png" class="text-center" width="600px">}}

A new application has been added to the KDE Education suite. <a href='https://minuet.kde.org'>Minuet</a> is a Music Education Software featuring full MIDI support with tempo, pitch and volume control, which makes it suitable for both novice and experienced musicians.

Minuet includes 44 ear-training exercises about scales, chords, intervals and rhythm, enables the visualization of musical content on the piano keyboard and allows for the seamless integration of your own exercises.

### 더 믿을 수 있는 도우미

{{<figure src="/announcements/applications/16.04.0/khelpcenter1604.png" class="text-center" width="600px">}}

Plasma와 함께 배포되었던 KHelpCenter는 이제 KDE 프로그램의 일부로 배포됩니다.

KHelpCenter 팀의 대규모 버그 처리 활동으로 버그 49개를 해결했으며, 이 버그는 대부분 검색 기능 개선과 오작동에 관한 것입니다.

내부 문서 검색이 더 이상 사용되지 않는 소프트웨어 ht::/dig에서 새로운 Xapian 기반 색인 및 검색 시스템으로 변경되었습니다. man 페이지, info 페이지, KDE 소프트웨어 도움말에서 검색할 수 있으며 문서 페이지에 책갈피 기능이 추가되었습니다.

코드 유지 관리를 통한 KHelpCenter 개선을 위해서 KDELibs4 지원을 제거하고 코드 및 기타 사소한 버그를 수정했습니다.

### 공격적인 버그 잡기

The Kontact Suite had a whopping 55 bugs resolved; some of which were related to issues with setting alarms, and in the import of Thunderbird mails, downsizing Skype &amp; Google talk icons in the Contacts Panel View, KMail related workarounds such as folder imports, vCard imports, opening ODF mail attachments, URL inserts from Chromium, the tool menu differences with the app started as a part of Kontact as opposed to a standalone use, missing 'Send' menu item and a few others. The support for Brazilian Portuguese RSS feeds has been added along with the fixing of the addresses for the Hungarian and Spanish feeds.

The new features include a redesign of the KAddressbook contact editor, a new default KMail Header theme, improvements to the Settings Exporter and a fix of Favicon support in Akregator. The KMail composer interface has been cleaned up along with the introduction of a new Default KMail Header Theme with the Grantlee theme used for the 'About' page in KMail as well as Kontact and Akregator. Akregator now uses QtWebKit - one of the major engines to render webpages and execute javascript code as the renderer web engine and the process of implementing support for QtWebEngine in Akregator and other Kontact Suite applications has already begun. While several features were shifted as plugins in kdepim-addons, the pim libraries were split into numerous packages.

### 깊게 압축하기

{{<figure src="/announcements/applications/16.04.0/ark1604.png" class="text-center" width="600px">}}

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, the archive manager, has had two major bugs fixed so that now Ark warns the user if an extraction fails due to lack of sufficient space in the destination folder and it does not fill up the RAM during the extraction of huge files via drag and drop.

Ark now also includes a properties dialog which displays information like the type of archive, compressed and uncompressed size, MD5/SHA-1/SHA-256 cryptographic hashes about the currently opened archive.

Ark can also now open and extract RAR archives without utilizing the non-free rar utilities and can open, extract and create TAR archives compressed with the lzop/lzip/lrzip formats.

The User Interface of Ark has been polished by reorganizing the menubar and the toolbar so as to improve the usability and to remove ambiguities, as well as to save vertical space thanks to the status-bar now hidden by default.

### 더 정밀한 동영상 편집

{{<figure src="/announcements/applications/16.04.0/kdenlive1604.png" class="text-center" width="600px">}}

<a href='https://www.kde.org/applications/multimedia/kdenlive/'>Kdenlive</a>, the non-linear video editor has numerous new features implemented. The titler now has a grid feature, gradients, the addition of text shadow and adjusting letter/line spacing.

Integrated display of audio levels allows easy monitoring of the audio in your project alongwith new video monitor overlays displaying playback framerate, safe zones and audio waveforms and a toolbar to seek to markers and a zoom monitor.

There has also been a new library feature which allows to copy/paste sequences between projects and a split view in timeline to compare and visualise the effects applied to the clip with the one without them.

The render dialog has been rewritten with an added option to get faster encoding hence, producing large files and the speed effect has been made to work with sound as well.

Curves in keyframes have been introduced for a few effects and now your top chosen effects can be accessed quickly via the favourite effects widget. While using the razor tool, now the vertical line in the timeline will show the accurate frame where the cut will be performed and the newly added clip generators will enable you to create color bar clips and counters. Besides this, the clip usage count has been re-introduced in the Project Bin and the audio thumbnails have also been rewritten to make them much faster.

The major bug fixes include the crash while using titles (which requires the latest MLT version), fixing corruptions occurring when the project frames per second rate is anything other than 25, the crashes on track deletion, the problematic overwrite mode in the timeline and the corruptions/lost effects while using a locale with a comma as the separator. Apart from these, the team has been consistently working to make major improvements in stability, workflow and small usability features.

### 그리고 더 있습니다!

<a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, the document viewer brings in new features in the form of customizable inline annotation width border, allowance of directly opening embedded files instead of only saving them and also the display of a table of content markers when the child links are collapsed.

<a href='https://www.kde.org/applications/utilities/kleopatra'>Kleopatra</a> introduced improved support for GnuPG 2.1 with a fixing of selftest errors and the cumbersome key refreshes caused by the new GnuPG (GNU Privacy Guard) directory layout. ECC Key generation has been added with the display of Curve details for ECC certificates now. Kleopatra is now released as a separate package and support for .pfx and .crt files has been included. To resolve the difference in the behaviour of imported secret keys and generated keys, Kleopatra now allows you to set the owner trust to ultimate for imported secret keys. 
