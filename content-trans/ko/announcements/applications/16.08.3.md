---
aliases:
- ../announce-applications-16.08.3
changelog: true
date: 2016-11-10
description: KDE에서 KDE 프로그램 16.08.3 출시
layout: application
title: KDE에서 KDE 프로그램 16.08.3 출시
version: 16.08.3
---
November 10, 2016. Today KDE released the third stability update for <a href='../16.08.0'>KDE Applications 16.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

kdepim, Ark, Okteta, Umbrello, KMines 등에 20개 이상의 버그 수정 및 기능 개선이 있었습니다.

This release also includes Long Term Support version of KDE Development Platform 4.14.26.
