---
aliases:
- ../announce-applications-17.08.1
changelog: true
date: 2017-09-07
description: KDE에서 KDE 프로그램 17.08.1 출시
layout: application
title: KDE에서 KDE 프로그램 17.08.1 출시
version: 17.08.1
---
September 7, 2017. Today KDE released the first stability update for <a href='../17.08.0'>KDE Applications 17.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Kontact, Gwenview, Kdenlive, Konsole, KWalletManager, Okular, Umbrello, KDE 게임 등에 20개 이상의 버그 수정 및 기능 개선이 있었습니다.

This release also includes Long Term Support version of KDE Development Platform 4.14.36.
