---
aliases:
- ../announce-applications-18.12.2
changelog: true
date: 2019-02-07
description: KDE Uygulamalar 18.12.2.'yi Gönderdi
layout: application
major_version: '18.12'
release: applications-18.12.2
title: KDE, KDE Uygulamaları 18.12.2'yi Gönderdi
version: 18.12.2
---
{{% i18n_date %}}

Bugün KDE, <a href='../18.12.0'>KDE Uygulamaları 18.12</a> için ikinci kararlılık güncellemesini yayınladı. Bu sürüm yalnızca hata düzeltmeleri ve çeviri güncellemelerini içerir, herkes için güvenli ve hoş bir güncelleme sağlar.

More than a dozen recorded bugfixes include improvements to Kontact, Ark, Konsole, Lokalize, Umbrello, among others.

İyileştirmeler şunları içerir:

- Ark artık yerleşik görüntüleyicinin içinden kaydedilen dosyaları silmiyor</li>
- Adres defteri artık kişileri birleştirirken doğum günlerini hatırlıyor</li>
- Umbrello'da birkaç eksik diyagram görüntüleme güncellemesi düzeltildi</li>
