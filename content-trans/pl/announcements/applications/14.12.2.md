---
aliases:
- ../announce-applications-14.12.2
changelog: true
date: '2015-02-03'
description: KDE wydało Aplikacje KDE 14.12.2.
layout: application
title: KDE wydało Aplikacje KDE 14.12.2
version: 14.12.2
---
3 lutego 2015. Dzisiaj KDE wydało drugie uaktualnienie stabilizujące <a href='../14.12.0'>Aplikacji KDE 14.12</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

Więcej niż 20 zarejestrowanych poprawek błędów uwzględnia ulepszenia do gry Kanagra, modelera UML Umbrello, przeglądarki dokumentów Okular oraz wirtualnego globu Marble.

To wydanie zawiera także wersje o długoterminowym wsparciu: Przestrzeni Roboczych 4.11.16, Platformę Programistyczną KDE 4.14.5 oraz Pakiet Kontact 4.14.5 .
