---
aliases:
- ../announce-applications-17.08.3
changelog: true
date: 2017-11-09
description: KDE wydało Aplikacje KDE 17.08.3
layout: application
title: KDE wydało Aplikacje KDE 17.08.3
version: 17.08.3
---
9 listopada 2017. Dzisiaj KDE wydało pierwsze uaktualnienie stabilizujące <a href='../17.08.0'>Aplikacji KDE 17.08</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

About a dozen recorded bugfixes include improvements to Kontact, Ark, Gwenview, KGpg, KWave, Okular, Spectacle, among others.

To wydanie zawiera także ostatnią wersję Platformy Programistycznej KDE  4.14.38.

Wśród ulepszeń znajdują się:

- Work around a Samba 4.7 regression with password-protected SMB shares
- Okular no longer crashes after certain rotation jobs
- Ark preserves file modification dates when extracting ZIP archives
