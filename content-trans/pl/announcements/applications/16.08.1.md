---
aliases:
- ../announce-applications-16.08.1
changelog: true
date: 2016-09-08
description: KDE wydało Aplikacje KDE 16.08.1
layout: application
title: KDE wydało Aplikacje KDE 16.08.1
version: 16.08.1
---
8 wrzesień 2016. Dzisiaj KDE wydało pierwsze uaktualnienie stabilizujące <a href='../16.08.0'>Aplikacji KDE 16.08</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

Więcej niż 45 zarejestrowanych poprawek błędów uwzględnia ulepszenia do kdepim, kate, kdenlive, konsole, marble, kajongg, kopete, umbrello, oraz innych.

To wydanie zawiera także długoterminowo wspieraną wersję Platformy Programistycznej KDE 4.14.24.
