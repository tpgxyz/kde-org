---
aliases:
- ../announce-applications-17.08-beta
date: 2017-07-21
description: KDE wydało Aplikacje 17.08 Beta.
layout: application
release: applications-17.07.80
title: KDE wydało betę Aplikacji KDE 17.08
---
21 lipca 2017. Dzisiaj KDE wydało wersję beta nowej wersji Aplikacji KDE. Wersja ta zamraża wszelkie zmiany w zależnościach i funkcjonalności, a zespół KDE będzie się skupiał jedynie na naprawianiu w niej błędów i dalszym udoskonalaniu.

Check the <a href='https://community.kde.org/Applications/17.08_Release_Notes'>community release notes</a> for information on tarballs that are now KF5 based and known issues. A more complete announcement will be available for the final release

Aplikacje KDE, wydanie 17.08 potrzebuje wypróbowania go w szerokim zakresie, aby utrzymać i ulepszyć jakość i odczucia użytkownika. Obecnie użytkownicy są znaczącym czynnikiem przy utrzymywaniu wysokiej jakości KDE, bo programiści po prostu nie mogą wypróbować każdej możliwej konfiguracji. Liczymy, że wcześnie znajdziesz błędy, tak aby mogły zostać poprawione przed wydaniem końcowym. Proszę rozważyć dołączenie do zespołu poprzez zainstalowanie bety i <a href='https://bugs.kde.org/'>zgłaszanie wszystkich błędów</a>.
