---
aliases:
- ../announce-applications-16.08-beta
date: 2016-07-22
description: KDE wydało Aplikacje KDE 16.08 Beta.
layout: application
release: applications-16.07.80
title: KDE wydało betę Aplikacji KDE 16.08
---
22 lipiec 2016. Dzisiaj KDE wydało betę nowej wersji Aplikacji KDE. Wersja ta zamraża wszelkie zmiany w zależnościach i funkcjonalności, a zespół KDE będzie się skupiał jedynie na naprawianiu w niej błędów i dalszym polerowaniu.

Sprawdź <a href='https://community.kde.org/Applications/16.08_Release_Notes'>listę zmian społeczności</a>, aby uzyskać informację dotyczącą archiwów, obecnie opartych na KF5 oraz ich znanych błędów. Bardziej kompletny komunikat będzie dostępny po wydaniu wersji ostatecznej

Aplikacje KDE, wydanie 16.08 potrzebuje wypróbowania go w szerokim zakresie, aby utrzymać i ulepszyć jakość i odczucia użytkownika. Obecnie użytkownicy są znaczącym czynnikiem przy utrzymywaniu wysokiej jakości KDE, bo programiści po prostu nie mogą wypróbować każdej możliwej konfiguracji. Liczymy, że wcześnie znajdziesz błędy, tak aby mogły zostać poprawione przed wydaniem końcowym. Proszę rozważyć dołączenie do zespołu poprzez zainstalowanie bety i <a href='https://bugs.kde.org/'>zgłaszanie wszystkich błędów</a>.
