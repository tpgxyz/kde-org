---
aliases:
- ../announce-applications-18.04.2
changelog: true
date: 2018-06-07
description: KDE wydało Aplikacje KDE 18.04.2
layout: application
title: KDE wydało Aplikacje KDE 18.04.2
version: 18.04.2
---
7 czerwca 2018. Dzisiaj KDE wydało drugie uaktualnienie stabilizujące <a href='../18.04.0'>Aplikacji KDE 18.04</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

About 25 recorded bugfixes include improvements to Kontact, Cantor, Dolphin, Gwenview, KGpg, Kig, Konsole, Lokalize, Okular, among others.

Wśród ulepszeń znajdują się:

- Image operations in Gwenview can now be redone after undoing them
- KGpg no longer fails to decrypt messages without a version header
- Exporting of Cantor worksheets to LaTeX has been fixed for Maxima matrices
