---
aliases:
- ../../kde-frameworks-5.12.0
date: 2015-07-10
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Dodatkowe moduły CMake

- Ulepszono zgłaszanie makra query_qmake

### BluezQt

- Usuwaj wszystkie urządzenia z adaptera przed usunięciem adaptera (błąd 349363)
- Uaktualniono dowiązania w README.md

### KAktywności

- Dodano opcję, aby nie śledzić użytkownika przy pewnych działaniach (tryb podobny do trybu 'prywatnego przeglądania' w przeglądarkach internetowych)

### KArchiwum

- Zachowano uprawnienia do wykonywania plików w copyTo()
- Oczyszczono ~KArchive usuwając martwy kod.

### KAuth

- Od teraz można używać kauth-policy-gen z różnych źródeł

### KBookmarks

- Nie dodawaj zakładki, gdy adres url i tekst są puste
- Zakodowano adres URL KBookmark, aby zapewnić zgodnośc z aplikacjami KDE4

### KKodeki

- Usuwaj próbnik x-euc-tw

### KConfig

- Wgraj kconfig_compiler w libexec
- Nowa opcja tworzenia kodu TranslationDomain=, do użytku z TranslationSystem=kde; normalnie jest to potrzebne w bibliotekach.
- Od teraz można używać kconfig_compiler z różnych źródeł

### KCoreAddons

- KDirWatch: Nawiązuj połączenia tylko do FAM, jeśli zażądano
- Umożliwiono filtrowanie wtyczek i aplikacji po współczynniku kształtu
- Od teraz można używać desktoptojson z różnych źródeł

### KDBusAddons

- Ustalono jednoznacznie wartość wyjściową dla niepowtarzalnych wystąpień

### KDeclarative

- Dodano QQC, który jest klonem KColorButton
- Przypisano QmlObject dla każdego wystąpienia kdeclarative, gdy to możliwe
- Sprawiono, że Qt.quit() z kodu QML zadziała
- Scalono gałęzie 'mart/singleQmlEngineExperiment'
- Zaimplementowano sizeHint w oparciu o implicitWidth/height
- Podklasa QmlObject z silnikiem statycznym

### Obsługa KDELibs 4

- Naprawiono implementację KMimeType::Ptr::isNull.
- Przywrócono wsparcie dla strumieniowania KDateTime do kDebug/qDebug, dla większej liczby SC
- Wczytuj poprawny katalog tłumaczeń dla kdebugdialog
- Nie pomijaj dokumentowania przestarzałych metod, tak aby ludzie mogli zapoznać się ze wskazówkami nt. przenoszenia

### KDESU

- Naprawiono CMakeLists.txt, tak aby przekazywało KDESU_USE_SUDO_DEFAULT do kompilacji, tak aby suprocess.cpp mogło go używać

### KDocTools

- Uaktualniono szablony docbook K5

### KGlobalAccel

- instalowane jest API prywatnych bibliotek uruchomieniowych, aby umożliwić KWin dostarczanie wtyczek dla Waylanda.
- Funkcja zapasowa do rozwiązywania nazw przez componentFriendlyForAction

### KIconThemes

- Nie próbuj malować ikon, jeśli ich rozmiar jest nieprawidłowy

### KItemModels

- Nowy model pośredni: KRearrangeColumnsProxyModel. Obsługuje on zmianę kolejności i ukrywanie kolumn z modelu źródłowego.

### KNotification

- Naprawiono rodzaje piksmap w org.kde.StatusNotifierItem.xml
- [ksni] Dodano metodę do uzyskiwania akcji po jej nazwie (błąd 349513)

### KLudzie

- Zaimplementowano filtrowanie PersonsModel

### KPlotting

- KPlotWidget: dodano setAutoDeletePlotObjects, naprawiono wyciek pamięci wreplacePlotObject
- Dodano brakujące znaczniki kres gdy x0 &gt; 0.
- KPlotWidget: ne ma potrzeby aby ustawiać setMinimumSize lub zmieniać rozmiar.

### KTextEditor

- debianchangelog.xml: dodano Debian/Stretch, Debian/Buster, Ubuntu-Wily
- Naprawiono zachowanie backspace/delete dla pary surogatów UTF-16.
- QScrollBar powinno obsługiwać ZdarzeniaRolki (błąd 340936)
- Zastosowano łątkę z wersji rozwojowej KWrite, aby uaktualnić podświetlanie czystego basica, "Alexander Clay" &lt;tuireann@EpicBasic.org&gt;

### KTextWidgets

- Naprawiono włączanie/wyłączanie przycisku ok

### Szkielet Portfela

- Zaimportowano i ulepszono narzędzie wiersza kwallet-query poleceń.
- Dodano obsługę zastępowania wpisów na mapie

### KXMLGUI

- "Wersja Szkieletów KDE" nie będzie pokazywana w oknie dialogowym o KDE

### Szkielety Plazmy

- Uczyniono ciemny wystrój w pełni ciemnym, co wpłynęło także na grupy uzupełniające
- Przechowuj naturalny rozmiar osobno poprzez współczynnik skalowania
- WidokPojemnika: nie ulegaj usterce przy nieprawidłowych metadanych corona
- AppletQuickItem: Nie próbuj uzyskać dostępu do obiektu KPluginInfo, jeśli nie jest on prawidłowy
- Naprawiono okazjonalnie puste strony ustawień apletów (błąd 349250)
- Ulepszono obsługę hidpi w składniku siatki kalendarza
- Sprawdź czy KUsługi mają prawidłowe informacje o wtyczce zanim zaczną jej używać
- [kalendarz] Zapewniono, że siatka jest przemalowywana przy zmianie wystroju
- [kalendarz] Liczenie tygodni zaczynaj zawsze od poniedziałku (błąd 349044)
- [kalendarz] Przy zmianie ustawienia pokazywania numerów tygodnia siatka zostanie ponownie wyświetlona
- Gdy dostępny jest tylko efekt rozmycia, to używany jest wystrój nieprzezroczysty (błąd 348154)
- Biała lista apletów/wersji dla osobnego silnika
- Wprowadzono nową klasę ContainmentView

### Sonnet

- Zezwolono na używanie podświetlania sprawdzanego tekstu w QPainTextEdit

Możesz przedyskutować i podzielić się pomysłami dotyczącymi tego wydania w dziale komentarzy <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artykułu dot</a>.
